﻿Imports SasClient
Public Class SasSession
    Public Function SasConnect(ByRef ErrorMessage As String, ByVal app As String, _
                               ByVal host As String, ByVal port As Short, _
                               Optional ByVal user As String = "", _
                               Optional ByVal pwd As String = "") _
                               As SacSessionX
        Try
            Dim SacSession = New SacSessionX
            SacSession.Host = host
            SacSession.Port = port
            SacSession.Timeout = 60000
            SacSession.Application = app
            If user <> String.Empty Then SacSession.User = user
            If pwd <> String.Empty Then SacSession.Pass = pwd
            SacSession.Open()
            ErrorMessage = ""
            Return SacSession
        Catch ex As Exception
            Select Case Err.Number
                Case 13
                    ErrorMessage = "Λάθος αριθμός πόρτας"
                Case Else
                    ErrorMessage = ex.Message
            End Select
            Return Nothing
        End Try
    End Function
End Class

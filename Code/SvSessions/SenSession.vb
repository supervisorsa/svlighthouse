﻿Imports SasClient

Public Class SenSession

    Public Function ConnectToSen1(ByRef ErrorMessage As String, _
                                  ByVal SasAliasName As String, _
                                  ByVal Host As String, _
                                  ByVal UserName As String, _
                                  ByVal PassWord As String, _
                                  ByVal CmpCode As String, _
                                  ByVal WrhID As Integer, _
                                  Optional ByVal Port As Integer = 400, _
                                  Optional ByVal FiscalYearID As String = "") As SacSessionX
        'Dim fileIni As String

        Dim SacSession As SacSessionX
        ErrorMessage = ""
        'Dim textLine As String
        'Dim SplitTextLine As Object

        Try
            SacSession = New SacSessionX

            SacSession.Host = Host
            SacSession.Port = Port
            SacSession.Application = "SEN"
            SacSession.Open()

            Dim oSenApp As SacObjectX
            Dim v As Object
            Dim arr(5) As Object
            Dim arr0(1) As Object
            Dim VersionStr As String
            oSenApp = SacSession.CreateObject("TSenApplication", False)

            arr0(0) = ""
            VersionStr = oSenApp.Call("ISenApplication_GetVersionString", arr0)(0)
            arr(0) = UserName
            arr(1) = PassWord
            arr(2) = SasAliasName
            arr(3) = ""
            arr(4) = ""
            v = oSenApp.Call("ISenApplication_Login", arr)

            Dim arr1(2) As Object
            arr1(0) = "cmpcode"
            arr1(1) = GetFullCode(CmpCode)
            v = oSenApp.Call("ISenApplication_SetVariable", arr1)
            Dim fis As SacObjectX
            fis = SacSession.CreateObject("TFiscalYearSrv")
            Dim fiyid As String
            If FiscalYearID = "" Then
                fiyid = fis.Call("IFiscalYear_GetCurrentFiscalYear")(0)
            Else
                Dim arr2(0) As Object
                arr2(0) = CDate(FiscalYearID)
                fiyid = fis.Call("IFiscalYear_GetFiscalForDate", arr2)(0)
            End If
            arr1(0) = "fiyid"
            arr1(1) = fiyid
            v = oSenApp.Call("ISenApplication_SetVariable", arr1)
            arr1(0) = "wrhid"
            arr1(1) = GetFullCode(WrhID)
            v = oSenApp.Call("ISenApplication_SetVariable", arr1)
            arr1(0) = "workingdate"
            arr1(1) = Now
            v = oSenApp.Call("ISenApplication_SetVariable", arr1)

            SacSession.Timeout = 100000
            Return SacSession
        Catch ex As Exception
            ErrorMessage = ex.Message
            Return Nothing
        End Try

    End Function

    Private Function GetFullCode(ByVal code As String) As String
        Dim TempCode As String = code
        Do Until TempCode.Length >= 3
            TempCode = "0" & TempCode
        Loop
        Return TempCode
    End Function

End Class

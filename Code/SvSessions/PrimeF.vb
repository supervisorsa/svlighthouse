﻿Imports Commercial

Public Class PrimeF

    'Public ConnectedToPrime As Boolean
    Private IclaSession As ICLASession

    Private _DB As String
    Public Property DB() As String
        Get
            Return _DB
        End Get
        Set(ByVal value As String)
            _DB = value
        End Set
    End Property

    Private _Server As String
    Public Property Server() As String
        Get
            Return _Server
        End Get
        Set(ByVal value As String)
            _Server = value
        End Set
    End Property

    Private _DbUser As String
    Public Property DbUser() As String
        Get
            Return _DbUser
        End Get
        Set(ByVal value As String)
            _DbUser = value
        End Set
    End Property

    Private _DbPwd As String
    Public Property DbPwd() As String
        Get
            Return _DbPwd
        End Get
        Set(ByVal value As String)
            _DbPwd = value
        End Set
    End Property

    Private _AppDB As String
    Public Property AppDB() As String
        Get
            Return _AppDB
        End Get
        Set(ByVal value As String)
            _AppDB = value
        End Set
    End Property

    Private _AppDate As Date
    Public Property AppDate() As Date
        Get
            Return _AppDate
        End Get
        Set(ByVal value As Date)
            _AppDate = value
        End Set
    End Property

    Private _AppUsername As String
    Public Property AppUsername() As String
        Get
            Return _AppUsername
        End Get
        Set(ByVal value As String)
            _AppUsername = value
        End Set
    End Property

    Private _AppPassword As String
    Public Property AppPassword() As String
        Get
            Return _AppPassword
        End Get
        Set(ByVal value As String)
            _AppPassword = value
        End Set
    End Property

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        _DB = txtDB.Text
        _Server = txtServer.Text
        _DbUser = txtDbUser.Text
        _DbPwd = txtDbPwd.Text
        _AppDB = cmbAppDBs.Text
        _AppDate = dteAppDate.Value.Date
        _AppUsername = txtAppUsername.Text
        _AppPassword = txtAppPassword.Text
        Close()
    End Sub

    Private Sub PrimeForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtDB.Text = _DB
        txtServer.Text = _Server
        txtDbUser.Text = _DbUser
        txtDbPwd.Text = _DbPwd
        cmbAppDBs.Text = _AppDB
        dteAppDate.Value = If(_AppDate = Date.MinValue, Now.Date, _AppDate)
        txtAppUsername.Text = _AppUsername
        txtAppPassword.Text = _AppPassword
        chkManual.Checked = True
    End Sub

    Private Sub chkManual_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkManual.CheckedChanged
        If Not chkManual.Checked Then
            cmbAppDBs.Items.Clear()
            Dim CurrentSession As New PrimeSession
            Dim ErrorMessage As String = ""
            Dim AllDBs As List(Of String) = CurrentSession.AvailableDBs(ErrorMessage)

            If ErrorMessage <> "" Then
                MessageBox.Show(ErrorMessage)
            Else
                For Each item In AllDBs
                    cmbAppDBs.Items.Add(item)
                Next
            End If
        End If
    End Sub

    Private Sub cmbAppDBs_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAppDBs.SelectedValueChanged
        txtDB.Text = cmbAppDBs.Text
    End Sub
End Class
﻿Imports SasClient

Public Class SrsForm

    'Public ConnectedToSrs As Boolean
    Private SacSession As SacSessionX
    'Public CurrentWarehouses As Prime

    'Private CurrentCompany As String
    'Private CurrentWarehouse As String
    'Private CurrentFiscal As String

    Private _Host As String
    Public Property Host() As String
        Get
            Return _Host
        End Get
        Set(ByVal value As String)
            _Host = value
        End Set
    End Property

    Private _Port As String
    Public Property Port() As String
        Get
            Return _Port
        End Get
        Set(ByVal value As String)
            _Port = value
        End Set
    End Property

    Private _SasAlias As String
    Public Property SasAlias() As String
        Get
            Return _SasAlias
        End Get
        Set(ByVal value As String)
            _SasAlias = value
        End Set
    End Property

    Private _SrsUser As String
    Public Property SrsUser() As String
        Get
            Return _SrsUser
        End Get
        Set(ByVal value As String)
            _SrsUser = value
        End Set
    End Property

    Private _SrsPwd As String
    Public Property SrsPwd() As String
        Get
            Return _SrsPwd
        End Get
        Set(ByVal value As String)
            _SrsPwd = value
        End Set
    End Property

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        _Host = txtHost.Text
        _Port = txtPort.Text
        _SasAlias = cmbAlias.Text
        _SrsUser = txtUser.Text
        _SrsPwd = txtPWD.Text
        Close()
    End Sub

    Private Sub SrsForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtHost.Text = _Host
        txtPort.Text = _Port
        cmbAlias.Text = _SasAlias
        txtUser.Text = _SrsUser
        txtPWD.Text = _SrsPwd
        chkManual.Checked = True
    End Sub

    Private Sub chkManual_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkManual.CheckedChanged
        EnableDB(chkManual.Checked)
        btnSrsLogin.Enabled = Not chkManual.Checked
        If chkManual.Checked Then
            cmbAlias.Items.Clear()
        End If
    End Sub

    Private Sub cmbAlias_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAlias.Enter
        If Not chkManual.Checked Then
            cmbAlias.Items.Clear()
            Dim CurrentSession As SasSession = New SasSession
            Dim ErrorMessage As String = ""
            CurrentSession.SasConnect(ErrorMessage, "SRSBack", txtHost.Text, txtPort.Text)
            If ErrorMessage = String.Empty Then
                GetDBAliases(cmbAlias)
            Else
                MessageBox.Show(ErrorMessage)
            End If
        End If
    End Sub

    Private Sub EnableDB(ByVal bool As Boolean)
        'If Not bool Then
        '    cmbDB.Items.Clear()
        '    cmbDB.Text = ""
        'End If
        'txtDB.Enabled = bool
        'cmbDB.Enabled = bool
        'lblDB.Enabled = bool
        'btnCreate.Enabled = bool
    End Sub

    Public Sub GetDBAliases(ByVal cmb As ComboBox)
        Dim kbmDBs As kbm = New kbm
        If IsConnectedToSas() Then
            kbmDBs.DataPacket = SacSession.Call("GetRegistry", 1)
            Do Until kbmDBs.Eof
                If kbmDBs.Field("Alias").Value.Equals("SRSDATA") Then
                    cmb.Items.Insert(0, kbmDBs.Field("Alias").Value)
                Else
                    cmb.Items.Add(kbmDBs.Field("Alias").Value)
                End If
                kbmDBs.Next()
            Loop
        End If
    End Sub

    Private Function IsConnectedToSas() As Boolean
        Try
            Return SacSession.active
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnSrsLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSrsLogin.Click
        Try
            If IsConnectedToSas() Then SacSession.Close()
            Dim CurrentSession As SasSession = New SasSession
            Dim ErrorMessage As String = ""
            SacSession = CurrentSession.SasConnect(ErrorMessage, "SRSBack", _
                                      txtHost.Text, txtPort.Text, _
                                      txtUser.Text, txtPWD.Text)
            If ErrorMessage <> String.Empty Then
                MessageBox.Show(ErrorMessage)
            Else
                MessageBox.Show("Επιτυχής σύνδεση")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function GetFullCode(ByVal code As String) As String
        Dim TempCode As String = code
        Do Until TempCode.Length >= 3
            TempCode = "0" & TempCode
        Loop
        Return TempCode
    End Function

End Class
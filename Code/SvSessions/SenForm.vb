﻿
Public Class SenForm

    Public ConnectedToSen As Boolean
    Private SacSession As SasClient.SacSessionX
    'Public CurrentWarehouses As SvClass

    Private CurrentCompany As String
    Private CurrentWarehouse As String
    Private CurrentFiscal As String

    Private _Host As String
    Public Property Host() As String
        Get
            Return _Host
        End Get
        Set(ByVal value As String)
            _Host = value
        End Set
    End Property

    Private _Port As String
    Public Property Port() As String
        Get
            Return _Port
        End Get
        Set(ByVal value As String)
            _Port = value
        End Set
    End Property

    Private _SasAlias As String
    Public Property SasAlias() As String
        Get
            Return _SasAlias
        End Get
        Set(ByVal value As String)
            _SasAlias = value
        End Set
    End Property

    Private _SenUser As String
    Public Property SenUser() As String
        Get
            Return _SenUser
        End Get
        Set(ByVal value As String)
            _SenUser = value
        End Set
    End Property

    Private _SenPwd As String
    Public Property SenPwd() As String
        Get
            Return _SenPwd
        End Get
        Set(ByVal value As String)
            _SenPwd = value
        End Set
    End Property

    Private _CmpCode As String
    Public Property CmpCode() As String
        Get
            Return _CmpCode
        End Get
        Set(ByVal value As String)
            _CmpCode = value
        End Set
    End Property

    Private _WrhID As String
    Public Property WrhID() As String
        Get
            Return _WrhID
        End Get
        Set(ByVal value As String)
            _WrhID = value
        End Set
    End Property

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        _Host = txtHost.Text
        _Port = txtPort.Text
        _SasAlias = cmbAlias.Text
        _SenUser = txtUser.Text
        _SenPwd = txtPWD.Text
        _CmpCode = If(SvClass.ValidComboItem(cmbCompanies), _
                                  cmbCompanies.SelectedItem.ID, cmbCompanies.Text)
        _WrhID = If(SvClass.ValidComboItem(cmbWarehouses), _
                                  cmbWarehouses.SelectedItem.ID, GetFullCode(cmbWarehouses.Text))
        Close()
    End Sub

    Private Sub SenForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtHost.Text = _Host
        txtPort.Text = _Port
        cmbAlias.Text = _SasAlias
        txtUser.Text = _SenUser
        txtPWD.Text = _SenPwd
        chkManual.Checked = True
        'LoadCompany()
    End Sub

    Private Sub LoadCompany()
        CurrentCompany = _CmpCode
        CurrentWarehouse = GetFullCode(_WrhID)
        cmbCompanies.Text = CurrentCompany
        cmbWarehouses.Text = CurrentWarehouse
    End Sub

    Private Sub btnSenConnection_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSenConnection.Click
        If chkManual.Checked Then
            Dim CurrentSession As New SenSession
            Dim ErrorMessage As String = ""
            Dim TrialSession As SasClient.SacSessionX
            TrialSession = CurrentSession.ConnectToSen1(ErrorMessage, _
                                                     cmbAlias.Text, _
                                                     txtHost.Text, _
                                                     txtUser.Text, _
                                                     txtPWD.Text, _
                                                     cmbCompanies.Text, _
                                                     cmbWarehouses.Text, _
                                                     txtPort.Text)
            If ErrorMessage.Equals(String.Empty) Then
                MessageBox.Show("Σύνδεση με SEN επιτυχής")
            Else
                MessageBox.Show("Αποτυχία σύνδεσης με SEN" & vbCrLf & ErrorMessage)
            End If
            If TrialSession IsNot Nothing AndAlso TrialSession.active = True Then
                TrialSession.Disconnect()
                TrialSession.Close()
            End If
        Else

        End If
    End Sub

    Private Sub chkManual_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkManual.CheckedChanged
        EnableDB(chkManual.Checked)
        btnSenLogin.Enabled = Not chkManual.Checked
        btnSenConnection.Enabled = chkManual.Checked
        lblDate.Enabled = btnSenLogin.Enabled
        dteDate.Enabled = False 'btnSenLogin.Enabled
        cmbCompanies.Enabled = Not btnSenLogin.Enabled
        cmbFiscal.Enabled = cmbCompanies.Enabled
        cmbWarehouses.Enabled = cmbCompanies.Enabled
        If chkManual.Checked Then
            cmbAlias.Items.Clear()
            cmbCompanies.Items.Clear()
            cmbFiscal.Items.Clear()
            cmbWarehouses.Items.Clear()
            btnSenConnection.Enabled = True
            LoadCompany()
        Else
            CurrentCompany = Nothing
            CurrentWarehouse = Nothing
        End If
    End Sub

    Private Sub cmbAlias_Enter(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbAlias.Enter
        If Not chkManual.Checked Then
            cmbAlias.Items.Clear()
            SasConnect(txtHost.Text, txtPort.Text)
            GetDBAliases(cmbAlias)
        End If
    End Sub

    Private Sub EnableDB(ByVal bool As Boolean)
        'If Not bool Then
        '    cmbDB.Items.Clear()
        '    cmbDB.Text = ""
        'End If
        'txtDB.Enabled = bool
        'cmbDB.Enabled = bool
        'lblDB.Enabled = bool
        'btnCreate.Enabled = bool
    End Sub

    Public Sub SasConnect(ByVal host As String, ByVal port As Short)
        Try
            SacSession = New SasClient.SacSessionX
            SacSession.Host = host
            SacSession.Port = port
            SacSession.Timeout = 60000
            SacSession.Application = "SEN"
            SacSession.Open()
        Catch ex As Exception
            Select Case Err.Number
                Case 13
                    MessageBox.Show("Λάθος αριθμός πόρτας", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Case Else
                    MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Select
        End Try
    End Sub

    Public Sub GetDBAliases(ByVal cmb As ComboBox)
        Dim kbmDBs = New SasClient.kbm
        If IsConnectedToSas() Then
            cmb.Items.Insert(0, "SEN_DB")
            'Nikos
            'kbmDBs.DataPacket = SacSession.Call("GetRegistry", 1)
            'Do Until kbmDBs.Eof
            '    If kbmDBs.Field("Alias").Value.Equals("SENDATA") Then
            '        cmb.Items.Insert(0, kbmDBs.Field("Alias").Value)
            '    Else
            '        cmb.Items.Add(kbmDBs.Field("Alias").Value)
            '    End If
            '    kbmDBs.Next()
            'Loop
            'Nikos
        End If
    End Sub

    Private Function IsConnectedToSas() As Boolean
        Try
            Return SacSession.active
        Catch ex As Exception
            Return False
        End Try
    End Function

    Private Sub btnSenLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSenLogin.Click
        Try
            If IsConnectedToSas() Then SacSession.Close()
            SasConnect(txtHost.Text, txtPort.Text)
            SetLoginInfo(cmbAlias.Text, txtUser.Text, txtPWD.Text)
            btnSenConnection.Enabled = False
            cmbFiscal.Items.Clear()
            cmbWarehouses.Items.Clear()
            cmbCompanies.Text = String.Empty
            cmbFiscal.Text = ""
            cmbWarehouses.Text = ""
            cmbCompanies.Enabled = False
            cmbFiscal.Enabled = False
            cmbWarehouses.Enabled = False
            cmbCompanies.Items.Clear()
            If ConnectedToSen Then
                cmbCompanies.Enabled = True
                GetCompanies(cmbCompanies, If(CurrentCompany Is Nothing, 0, CType(CurrentCompany, Integer)))
                UpdateExportValues(If(CurrentFiscal Is Nothing, 0, CType(CurrentFiscal, Integer)), _
                                   If(CurrentCompany Is Nothing, 0, CType(CurrentCompany, Integer)))
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Sub SetLoginInfo(ByVal DBAlias As String, ByVal User As String, ByVal Pwd As String)
        Dim oSenAppl As SasClient.SacObjectX
        If IsReference(SacSession) Then
            oSenAppl = SacSession.CreateObject("TSenApplication")
        Else
            ConnectedToSen = False
        End If
        If IsReference(oSenAppl) Then
            oSenAppl.Call("ISenApplication_Login", New Object() {User, Pwd, DBAlias, GetClientVersion(), ""})
            ConnectedToSen = True
        Else
            ConnectedToSen = False
        End If
    End Sub

    Function GetClientVersion() As String
        Try
            Dim oSenAppl As SasClient.SacObjectX
            oSenAppl = SacSession.CreateObject("TSenApplication")
            Return If(IsReference(oSenAppl), _
                      oSenAppl.Call("ISenApplication_GetVersionString", New Object() {})(0), _
                      "")
        Catch ex As Exception
            Return ""
        End Try
    End Function


    Sub GetCompanies(ByVal cmb As ComboBox, Optional ByVal SelectedCompany As Integer = 0)
        Dim Companies As SasClient.SacObjectX
        Dim kbmCompanies As SasClient.kbm
        If IsConnectedToSas() Then
            Companies = SacSession.CreateObject("TCompanySrv", True)
            kbmCompanies = New SasClient.kbm
            kbmCompanies.DataPacket = Companies.Call("ISenServerPDC_LoadRecords", New Object() {"", "", "CMPNAME;CMPCODE;CMPID"})(0)

            Do Until kbmCompanies.Eof
                SvClass.InsertToCombo(kbmCompanies.Field("CMPCODE").Value, _
                                      kbmCompanies.Field("CMPNAME").Value, _
                                      cmb, SelectedCompany, True)
                kbmCompanies.Next()
            Loop
        End If
    End Sub

    Private Sub UpdateExportValues(Optional ByVal Fiscal As Integer = 0, Optional ByVal Company As Integer = 0)
        If Not Company.Equals(0) Then
            InitializeDB(GetClientVersion, Company)
            cmbCompanies.Enabled = False
        End If
        If Not Fiscal.Equals(0) And SvClass.ValidComboItem(cmbCompanies) Then
            SetFiscal(Fiscal)
            cmbFiscal.Enabled = False
        End If
        If Not Fiscal.Equals(0) And SvClass.ValidComboItem(cmbCompanies) _
        And SvClass.ValidComboItem(cmbFiscal) Then
            cmbWarehouses.Enabled = False
            btnSenConnection.Enabled = True
        End If
    End Sub


    Sub GetFiscals(ByVal cmb As ComboBox, Optional ByVal SelectedFiscal As Integer = 0)
        Dim Fiscals As SasClient.SacObjectX
        Dim kbmFiscals = New SasClient.kbm
        Fiscals = SacSession.CreateObject("TFiscalYearSrv")
        kbmFiscals.DataPacket = Fiscals.Call("ISEnServerPDC_LoadRecords", New Object() {"", "", "FIYID;FIYTITLE;FIYBCISNATIONAL"})(0)
        Do Until kbmFiscals.Eof
            SvClass.InsertToCombo(kbmFiscals.Field("FIYID").Value, _
                                  kbmFiscals.Field("FIYTITLE").Value, _
                                  cmb, SelectedFiscal, True)
            kbmFiscals.Next()
        Loop
    End Sub

    Sub SetFiscal(ByVal FiscalYear As String)
        Dim oSenAppl As SasClient.SacObjectX
        oSenAppl = SacSession.CreateObject("TSenApplication")
        oSenAppl.Call("ISenApplication_SetVariable", New Object() {"FIYID", FiscalYear})
    End Sub

    Sub InitializeDB(ByVal SenVersion As String, ByVal CompanyCode As String)
        Try
            Dim oSenAppl As SasClient.SacObjectX = SacSession.CreateObject("TSenApplication")
            oSenAppl.Call("ISenApplication_InitializeDB", New Object() {"COMPANY:" & CompanyCode, SenVersion, ""})
        Catch ex As Exception

        End Try
    End Sub


    Sub GetWarehouses(ByVal cmb As ComboBox, Optional ByVal SelectedWarehouse As Integer = 0)
        Dim Warehouses As SasClient.SacObjectX
        Dim kbmWarehouses = New SasClient.kbm
        Warehouses = SacSession.CreateObject("TWarehouseSrv")
        kbmWarehouses.DataPacket = Warehouses.Call("ISEnServerPDC_LoadRecords", New Object() {"", "", "WRHID;WRHCODE;WRHNAME"})(0)

        'CurrentWarehouses = New SvClass
        Do Until kbmWarehouses.Eof
            If kbmWarehouses.Field("WRHID").Value <> -1 Then
                'CurrentWarehouses.Add(kbmWarehouses.Field("WRHID").Value, kbmWarehouses.Field("WRHNAME").Value)
                SvClass.InsertToCombo(kbmWarehouses.Field("WRHID").Value, _
                                      kbmWarehouses.Field("WRHNAME").Value, _
                                      cmb, SelectedWarehouse, True)
            End If
            kbmWarehouses.Next()
        Loop
    End Sub

    Sub SetVariables(ByVal Warehouse As String, ByVal WorkingDate As Date)
        Dim oSenAppl As SasClient.SacObjectX = SacSession.CreateObject("TSenApplication")
        oSenAppl.Call("ISenApplication_SetVariable", New Object() {"WRHID", Warehouse})
        oSenAppl.Call("ISenApplication_SetVariable", New Object() {"WORKINGDATE", WorkingDate})
    End Sub

    Private Sub cmbCompanies_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbCompanies.TextChanged
        If Not chkManual.Checked Then
            btnSenConnection.Enabled = False
            cmbFiscal.Items.Clear()
            cmbWarehouses.Items.Clear()
            cmbFiscal.Text = ""
            cmbWarehouses.Text = ""
            cmbFiscal.Enabled = False
            cmbWarehouses.Enabled = False

            If SvClass.ValidComboItem(cmbCompanies) Then
                InitializeDB(GetClientVersion, GetFullCode(cmbCompanies.SelectedItem.ID.ToString))
                cmbFiscal.Enabled = True
                GetFiscals(cmbFiscal, If(CurrentFiscal Is Nothing, 0, CType(CurrentFiscal, Integer)))
                UpdateExportValues(If(CurrentFiscal Is Nothing, 0, CType(CurrentFiscal, Integer)))
            End If
        End If
    End Sub

    Private Function CheckExport() As Boolean
        Return (SvClass.ValidComboItem(cmbCompanies) And _
        SvClass.ValidComboItem(cmbFiscal) And _
        SvClass.ValidComboItem(cmbWarehouses))
    End Function

    Private Sub cmbWarehouses_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbWarehouses.TextChanged
        'If Not chkManual.Checked Then
        '    btnSenConnection.Enabled = CheckExport()
        'End If
    End Sub

    Private Sub cmbFiscal_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmbFiscal.TextChanged
        If Not chkManual.Checked Then
            btnSenConnection.Enabled = False
            cmbWarehouses.Items.Clear()
            cmbWarehouses.Text = ""
            cmbWarehouses.Enabled = False
            If SvClass.ValidComboItem(cmbFiscal) Then
                cmbWarehouses.Enabled = True
                SetFiscal(cmbFiscal.SelectedItem.ID.ToString)
                GetWarehouses(cmbWarehouses, CurrentWarehouse)
                UpdateExportValues()
            End If
        End If
    End Sub


    Private Function GetFullCode(ByVal code As String) As String
        Dim TempCode As String = code
        If code IsNot Nothing Then
            Do Until TempCode.Length >= 3
                TempCode = "0" & TempCode
            Loop
        End If
        Return TempCode
    End Function
End Class
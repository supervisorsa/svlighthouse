﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PrimeF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PrimeF))
        Me.dteAppDate = New System.Windows.Forms.DateTimePicker
        Me.cmbAppDBs = New System.Windows.Forms.ComboBox
        Me.chkManual = New System.Windows.Forms.CheckBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.txtServer = New System.Windows.Forms.TextBox
        Me.txtDbUser = New System.Windows.Forms.TextBox
        Me.txtAppPassword = New System.Windows.Forms.TextBox
        Me.txtAppUsername = New System.Windows.Forms.TextBox
        Me.lblDate = New System.Windows.Forms.Label
        Me.lblAlias = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblPort = New System.Windows.Forms.Label
        Me.lblHost = New System.Windows.Forms.Label
        Me.lblPwd = New System.Windows.Forms.Label
        Me.lblUser = New System.Windows.Forms.Label
        Me.txtDbPwd = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtDB = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'dteAppDate
        '
        Me.dteAppDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteAppDate.Location = New System.Drawing.Point(127, 95)
        Me.dteAppDate.Name = "dteAppDate"
        Me.dteAppDate.Size = New System.Drawing.Size(243, 22)
        Me.dteAppDate.TabIndex = 2
        '
        'cmbAppDBs
        '
        Me.cmbAppDBs.FormattingEnabled = True
        Me.cmbAppDBs.Location = New System.Drawing.Point(127, 55)
        Me.cmbAppDBs.Name = "cmbAppDBs"
        Me.cmbAppDBs.Size = New System.Drawing.Size(243, 22)
        Me.cmbAppDBs.TabIndex = 1
        '
        'chkManual
        '
        Me.chkManual.AutoSize = True
        Me.chkManual.Location = New System.Drawing.Point(28, 22)
        Me.chkManual.Name = "chkManual"
        Me.chkManual.Size = New System.Drawing.Size(178, 18)
        Me.chkManual.TabIndex = 0
        Me.chkManual.Text = "Χωρίς επιβεβαίωση σύνδεσης"
        Me.chkManual.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(28, 224)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(342, 45)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(415, 224)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(342, 45)
        Me.btnSave.TabIndex = 9
        Me.btnSave.Text = "Αποθήκευση αλλαγών"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtServer
        '
        Me.txtServer.Location = New System.Drawing.Point(514, 55)
        Me.txtServer.Name = "txtServer"
        Me.txtServer.Size = New System.Drawing.Size(243, 22)
        Me.txtServer.TabIndex = 5
        '
        'txtDbUser
        '
        Me.txtDbUser.Location = New System.Drawing.Point(514, 137)
        Me.txtDbUser.Name = "txtDbUser"
        Me.txtDbUser.Size = New System.Drawing.Size(243, 22)
        Me.txtDbUser.TabIndex = 6
        '
        'txtAppPassword
        '
        Me.txtAppPassword.Location = New System.Drawing.Point(127, 178)
        Me.txtAppPassword.Name = "txtAppPassword"
        Me.txtAppPassword.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtAppPassword.Size = New System.Drawing.Size(243, 22)
        Me.txtAppPassword.TabIndex = 4
        '
        'txtAppUsername
        '
        Me.txtAppUsername.Location = New System.Drawing.Point(127, 137)
        Me.txtAppUsername.Name = "txtAppUsername"
        Me.txtAppUsername.Size = New System.Drawing.Size(243, 22)
        Me.txtAppUsername.TabIndex = 3
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblDate.Location = New System.Drawing.Point(25, 101)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(88, 14)
        Me.lblDate.TabIndex = 118
        Me.lblDate.Text = "Ημερομηνία :"
        '
        'lblAlias
        '
        Me.lblAlias.AutoSize = True
        Me.lblAlias.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblAlias.Location = New System.Drawing.Point(412, 181)
        Me.lblAlias.Name = "lblAlias"
        Me.lblAlias.Size = New System.Drawing.Size(86, 14)
        Me.lblAlias.TabIndex = 117
        Me.lblAlias.Text = "DB Κωδικός :"
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = True
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(25, 58)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(59, 14)
        Me.lblCompany.TabIndex = 116
        Me.lblCompany.Text = "Εταιρία :"
        '
        'lblPort
        '
        Me.lblPort.AutoSize = True
        Me.lblPort.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblPort.Location = New System.Drawing.Point(412, 140)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(89, 14)
        Me.lblPort.TabIndex = 115
        Me.lblPort.Text = "DB Χρήστης :"
        '
        'lblHost
        '
        Me.lblHost.AutoSize = True
        Me.lblHost.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblHost.Location = New System.Drawing.Point(412, 58)
        Me.lblHost.Name = "lblHost"
        Me.lblHost.Size = New System.Drawing.Size(75, 14)
        Me.lblHost.TabIndex = 114
        Me.lblHost.Text = "DB Server :"
        '
        'lblPwd
        '
        Me.lblPwd.AutoSize = True
        Me.lblPwd.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblPwd.Location = New System.Drawing.Point(25, 181)
        Me.lblPwd.Name = "lblPwd"
        Me.lblPwd.Size = New System.Drawing.Size(65, 14)
        Me.lblPwd.TabIndex = 113
        Me.lblPwd.Text = "Κωδικός :"
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblUser.Location = New System.Drawing.Point(25, 140)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(68, 14)
        Me.lblUser.TabIndex = 112
        Me.lblUser.Text = "Χρήστης :"
        '
        'txtDbPwd
        '
        Me.txtDbPwd.Location = New System.Drawing.Point(514, 178)
        Me.txtDbPwd.Name = "txtDbPwd"
        Me.txtDbPwd.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtDbPwd.Size = New System.Drawing.Size(243, 22)
        Me.txtDbPwd.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label1.Location = New System.Drawing.Point(412, 101)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 14)
        Me.Label1.TabIndex = 120
        Me.Label1.Text = "DB :"
        '
        'txtDB
        '
        Me.txtDB.Location = New System.Drawing.Point(514, 98)
        Me.txtDB.Name = "txtDB"
        Me.txtDB.Size = New System.Drawing.Size(243, 22)
        Me.txtDB.TabIndex = 119
        '
        'PrimeF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(786, 293)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtDB)
        Me.Controls.Add(Me.txtDbPwd)
        Me.Controls.Add(Me.lblDate)
        Me.Controls.Add(Me.lblAlias)
        Me.Controls.Add(Me.lblCompany)
        Me.Controls.Add(Me.lblPort)
        Me.Controls.Add(Me.lblHost)
        Me.Controls.Add(Me.lblPwd)
        Me.Controls.Add(Me.lblUser)
        Me.Controls.Add(Me.dteAppDate)
        Me.Controls.Add(Me.cmbAppDBs)
        Me.Controls.Add(Me.chkManual)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.txtServer)
        Me.Controls.Add(Me.txtDbUser)
        Me.Controls.Add(Me.txtAppPassword)
        Me.Controls.Add(Me.txtAppUsername)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "PrimeF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Σύνδεση Prime"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dteAppDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents cmbAppDBs As System.Windows.Forms.ComboBox
    Friend WithEvents chkManual As System.Windows.Forms.CheckBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtServer As System.Windows.Forms.TextBox
    Friend WithEvents txtDbUser As System.Windows.Forms.TextBox
    Friend WithEvents txtAppPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtAppUsername As System.Windows.Forms.TextBox
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents lblAlias As System.Windows.Forms.Label
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblPort As System.Windows.Forms.Label
    Friend WithEvents lblHost As System.Windows.Forms.Label
    Friend WithEvents lblPwd As System.Windows.Forms.Label
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents txtDbPwd As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtDB As System.Windows.Forms.TextBox
End Class

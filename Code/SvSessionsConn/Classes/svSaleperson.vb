﻿Public Class svSaleperson
    Public Sub ImportSaleperson(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svSalepersons.InsertOnSubmit(Me)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateSalepersonForOrder(ByVal SameSource As Boolean)
        If SameSource Then
            OrderSalepersonSourceID = _SourceID
            OrderSalepersonDestinationID = _DestinationID
        Else
            OrderSalepersonSourceID = _DestinationID
            OrderSalepersonDestinationID = _SourceID
        End If
        ConnContext.SubmitChanges()
    End Sub

End Class

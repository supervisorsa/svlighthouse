﻿Public Class SvTraderAddress

    Public Sub ImportTraderAddress(ByVal IsNew As Boolean, ByVal TypicalUpdate As Boolean)
        If TypicalUpdate Then
            _LastModificationSource = Now
        Else
            _LastModificationDestination = Now
        End If
        _IsDirty = True

        If IsNew Then
            ConnContext.svTraderAddresses.InsertOnSubmit(Me)
        End If
        'ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(ByVal TypicalUpdate As Boolean, Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            If TypicalUpdate Then
                DestinationID = svDestinationID
            Else
                SourceID = svDestinationID
            End If

        End If
        IsDirty = False
        If TypicalUpdate Then
            LastModificationDestination = Now
        Else
            LastModificationSource = Now
        End If
        'ConnContext.SubmitChanges()
    End Sub

    Public Function FullAddress1(ByVal IncludeHiLevel As Boolean, ByVal IncludePhones As Boolean) As String
        Dim AddressText As String = ""
        If _Address1 IsNot Nothing AndAlso _Address1 <> "" Then AddressText &= _Address1 & ", "
        If _Address2 IsNot Nothing AndAlso _Address2 <> "" Then AddressText &= _Address2 & ", "
        If _City IsNot Nothing AndAlso _City <> "" Then AddressText &= _City & ", "

        If IncludeHiLevel Then
            If _PostalCode IsNot Nothing AndAlso _PostalCode <> "" Then AddressText &= _PostalCode & ", "
            If _State IsNot Nothing AndAlso _State <> "" Then AddressText &= _State & ", "
            If _Country IsNot Nothing AndAlso _Country <> "" Then AddressText &= _Country & ", "
        End If

        If IncludePhones Then
            If _Phone1 IsNot Nothing AndAlso _Phone1 <> "" Then AddressText &= _Phone1 & ", "
            If _Phone2 IsNot Nothing AndAlso _Phone2 <> "" Then AddressText &= _Phone2 & ", "
            If _MobilePhone IsNot Nothing AndAlso _MobilePhone <> "" Then AddressText &= _MobilePhone & ", "
        End If

        If AddressText <> "" Then AddressText.Substring(0, AddressText.Length - 2)

        Return AddressText
    End Function

    Public Sub UpdateDestination(ByVal svDestinationID As String)
        DestinationID = svDestinationID
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateVmAddress(ByVal TraderID As String, ByVal CustomerID As String)
        SourceID = TraderID
        TraderSourceID = TraderID
        CustomerSourceID = CustomerID
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateFsMainAddress(ByVal DestinationID)
        TraderDestinationID = DestinationID
        CustomerDestinationID = DestinationID
        LastUpdate(True, ((-1) * Convert.ToInt32(DestinationID)).ToString)
        ConnContext.SubmitChanges()
    End Sub
End Class

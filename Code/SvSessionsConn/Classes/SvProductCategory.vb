﻿Public Class SvProductCategory
    Public Sub ImportCategory(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svProductCategories.InsertOnSubmit(Me)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateDestination(ByVal svDestinationID As String)
        DestinationID = svDestinationID
        ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property ParentCategory() As SvProductCategory
        Get
            Dim CurrentParent = From t In ConnContext.svProductCategories _
                           Where t.SourceID.Equals(_SourceBelongsTo) _
                           Select t
            If CurrentParent.Count = 1 Then
                If CurrentParent.FirstOrDefault.DestinationID IsNot Nothing AndAlso _
                _DestinationBelongsTo <> CurrentParent.FirstOrDefault.DestinationID Then
                    DestinationBelongsTo = CurrentParent.FirstOrDefault.DestinationID
                    ConnContext.SubmitChanges()
                End If
                Return CurrentParent.FirstOrDefault
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Sub UpdateParentDestination(ByVal ParentDestination As String)
        DestinationBelongsTo = ParentDestination
    End Sub

End Class

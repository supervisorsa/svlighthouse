﻿Public Class svTraderCategory
    Public Sub ImportCategory(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svTraderCategories.InsertOnSubmit(Me)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateDestination(ByVal svDestinationID As String)
        DestinationID = svDestinationID
        ConnContext.SubmitChanges()
    End Sub

End Class
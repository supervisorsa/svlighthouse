﻿Public Class svCountry
    Public Sub UpdateCountryForTrader(ByVal SameSource As Boolean) ', Optional ByVal UseCustomerValues As Boolean = False
        If SameSource Then
            TraderCountrySourceID = _SourceID
            TraderCountryDestinationID = _DestinationID
        Else
            TraderCountrySourceID = _DestinationID
            TraderCountryDestinationID = _SourceID
        End If
        ConnContext.SubmitChanges()
    End Sub
End Class

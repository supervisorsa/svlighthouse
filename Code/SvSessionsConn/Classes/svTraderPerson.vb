﻿Public Class svTraderPerson
    Public Sub ImportTraderPerson(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svTraderPersons.InsertOnSubmit(Me)
        End If
        'ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        'ConnContext.SubmitChanges()
    End Sub
End Class

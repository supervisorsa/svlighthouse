﻿Public Class svPaymentMethod
    Public Sub ImportPayment(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svPaymentMethods.InsertOnSubmit(Me)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdatePaymentForOrder(ByVal SameSource As Boolean)
        If SameSource Then
            OrderPaymentSourceID = _SourceID
            OrderPaymentDestinationID = _DestinationID
        Else
            OrderPaymentSourceID = _DestinationID
            OrderPaymentDestinationID = _SourceID
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdatePaymentForTrader(ByVal SameSource As Boolean)
        If SameSource Then
            TraderPaymentSourceID = _SourceID
            TraderPaymentDestinationID = _DestinationID
        Else
            TraderPaymentSourceID = _DestinationID
            TraderPaymentDestinationID = _SourceID
        End If
        ConnContext.SubmitChanges()
    End Sub
End Class

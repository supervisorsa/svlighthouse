﻿Public Class ProductResource


    Private _ProductResource As ProductResource
    Public Property ProductResources() As ProductResource
        Get
            Return _ProductResource
        End Get
        Set(ByVal value As ProductResource)
            _ProductResource = value
        End Set
    End Property

    Public Sub ImportProductResource(ByVal aa As String, ByVal RL1 As String, ByVal productId As String, ByVal colorid As String, ByVal RURI As String)
        Try


            Me.ProductId = productId
            Me.ColorId = colorid
            ResourceURI = RURI
            ResourceNameL1 = RL1
            DisplayOrder = aa
            UpdatedOn = Now
            CreatedOn() = UpdatedOn
            LhContext.ProductResources.InsertOnSubmit(Me)
            LhContext.SubmitChanges()

        Catch ex As Exception
            SvSys.ErrorLog(("LH ImportProductResource ").PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try

    End Sub
End Class

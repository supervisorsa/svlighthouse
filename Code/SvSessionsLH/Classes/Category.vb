﻿Public Class Category

    Private _SvProductCategory As SvProductCategory
    Public Property SvProductCategory() As SvProductCategory
        Get
            Return _SvProductCategory
        End Get
        Set(ByVal value As SvProductCategory)
            _SvProductCategory = value
        End Set
    End Property

    Public Sub ImportCategory(ByVal IsNew As Boolean)
        NameL1 = _SvProductCategory.Name
        UpdatedOn = Now


        If IsNew Then
            CategoryId = _SvProductCategory.SourceID
            CreatedOn = _UpdatedOn
            LhContext.Categories.InsertOnSubmit(Me)
        End If

        LhContext.SubmitChanges()

        _SvProductCategory.LastUpdate(_CategoryId.ToString)
    End Sub
End Class

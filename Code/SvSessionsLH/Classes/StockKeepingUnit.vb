﻿Public Class StockKeepingUnit

    Private _SvProductCode As svProductCode
    Private _SvProduct As SvProduct
    Public Property SvProductCode() As svProductCode
        Get
            Return _SvProductCode
        End Get
        Set(ByVal value As svProductCode)
            _SvProductCode = value
        End Set
    End Property
    Public Property SvProduct() As SvProduct
        Get
            Return _SvProduct
        End Get
        Set(ByVal value As SvProduct)
            _SvProduct = value
        End Set
    End Property
    Private _WarehouseID As String
    Public WriteOnly Property WarehouseID() As String
        Set(ByVal value As String)
            _WarehouseID = value
        End Set
    End Property

    Public Sub ImportStock(ByVal IsNew As Boolean, ByVal ConnString As String)

        Try


            ProductId = _SvProductCode.ProductSourceID
            If (String.IsNullOrEmpty(Trim(_SvProductCode.Color))) Or String.IsNullOrEmpty(Trim(_SvProductCode.Size)) Then Exit Sub


            ColorId = GetColorID(_SvProductCode.Color, ConnString, _SvProductCode.ColorValue)
            SizeId = GetSizeID(_SvProductCode.Size, ConnString, _SvProductCode.SizeValue)

            UpdatedOn = Now
            UpdatedStockOn = UpdatedOn
            UpdatedPriceOn = UpdatedOn

            AvailabilityType = "1"

            If _SvProductCode.CodeBalance(_WarehouseID) Is Nothing Then
                Stock = 0
            Else
                Stock = _SvProductCode.CodeBalance(_WarehouseID).Quantity
            End If
            Currency = 978
            'NEW REQUEST FROM LIGHTHOUSE DEFAULT VALUE FOR VAT 15/12/2015
            TaxClass = "1"
            'TaxClass = _SvProductCode.CodeProduct.ProductVat.DestinationID
            'NEW REQUEST FROM LIGHTHOUSE DEFAULT VALUE FOR VAT 15/12/2015

            RetailCurrentPrice = _SvProductCode.CodeProduct.RetailPrice
            IsOnline = True



            If IsNew Then
                SKUId = _SvProductCode.ID.ToString
                CreatedOn = UpdatedOn
                LhContext.StockKeepingUnits.InsertOnSubmit(Me)
            End If



            LhContext.SubmitChanges()

            _SvProductCode.LastUpdate(SKUId.ToString)
        Catch ex As Exception
            SvSys.ErrorLog(("LH ImportStock " & _SvProductCode.ProductSourceID).PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
    End Sub

    Public Function GetColorID(ByVal ColorName As String, ByVal ConnString As String, Optional ByVal ColorValue As String = "") As String

        Try

            Dim ColorExists = (From t In LhContext.Colors _
                               Where t.NameL1.Equals(ColorName)).FirstOrDefault

            If ColorExists Is Nothing Then
                Dim NewColor As New Color

                NewColor.NameL1 = ColorName
                NewColor.ColorId = Left(ColorName, 50)
                NewColor.UpdatedOn = Now
                NewColor.CreatedOn = NewColor.UpdatedOn
                NewColor.NameL2 = ColorValue
                LhContext.Colors.InsertOnSubmit(NewColor)
                LhContext.SubmitChanges()

                Return NewColor.ColorId
            Else
                If Not (String.IsNullOrEmpty(Trim(ColorValue))) Then
                    Dim Lh As New dataSvLhDataContext(ConnString)
                    Lh.ExecuteCommand("UPDATE Colors SET NAMEL2 = {0} WHERE COLORID= {1}", ColorValue, ColorExists.ColorId)
                End If
                Return ColorExists.ColorId
            End If
        Catch ex As Exception
            SvSys.ErrorLog(("GetColorID" & ColorName).PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
    End Function

    Public Function GetSizeID(ByVal SizeName As String, ByVal ConnString As String, Optional ByVal SizeValue As String = "") As String

        Try

            Dim SizeExists = (From t In LhContext.Sizes _
                               Where t.NameL1.Equals(SizeName)).FirstOrDefault

            If SizeExists Is Nothing Then
                Dim NewSize As New Size

                NewSize.NameL1 = SizeName
                NewSize.SizeId = Left(SizeName, 50)
                NewSize.UpdatedOn = Now
                NewSize.CreatedOn = NewSize.UpdatedOn
                NewSize.NameL2 = SizeValue
                LhContext.Sizes.InsertOnSubmit(NewSize)
                LhContext.SubmitChanges()

                Return NewSize.SizeId
            Else
                If Not (String.IsNullOrEmpty(Trim(SizeValue))) Then
                    Dim Ll As New dataSvLhDataContext(ConnString)
                    Ll.ExecuteCommand("UPDATE SIZES SET NAMEL2 = {0} WHERE SIZEID= {1}", SizeValue, SizeExists.SizeId)
                End If
                Return SizeExists.SizeId
            End If
        Catch ex As Exception
            SvSys.ErrorLog(("GetSizeID" & SizeName).PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try

    End Function


End Class


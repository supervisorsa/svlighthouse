﻿Imports System.Threading
Imports System.Globalization
Imports System.Windows.Forms

Public Class SvLH
    Private ConnSession As SvConnData


    'Public Enum GlobalIndeces
    '    TraderCodeMask = 0
    'End Enum

    'Public Enum OrderIndeces
    '    Status = 0
    '    EmplID = 1
    '    Payway = 2
    '    OfferType = 3
    '    ContactName = 4
    '    EmplName = 5
    'End Enum

    'Public Enum OrderDetailIndeces
    '    StockName = 0
    '    EmplID = 1
    'End Enum

    'Public Enum TaskIndeces
    '    Status = 0
    '    EmplID = 1
    '    EmplAssignedID = 2
    '    TaskType = 3
    '    ContactName = 4
    '    EmplName = 5
    '    DepartmentID = 6
    'End Enum

    Public Sub Connect(ByVal svServer As String, ByVal svUser As String, _
                       ByVal svPwd As String, ByVal svDB As String, _
                       ByVal svSession As SvConnData)
        LhContext = New dataSvLhDataContext(DbConn.ConstructConnString( _
                                                  svServer, _
                                                  svUser, _
                                                  svPwd, _
                                                  svDB, ""))
        ConnSession = svSession
    End Sub

    Public Sub Connect(ByVal svServer As String, ByVal svUser As String, _
                       ByVal svPwd As String, ByVal svDB As String, _
                       ByVal svSession As SvConnData, ByVal ParamsList As List(Of String))
        LhContext = New dataSvLhDataContext(DbConn.ConstructConnString( _
                                                  svServer, _
                                                  svUser, _
                                                  svPwd, _
                                                  svDB, ""))
        ConnSession = svSession
        InitParams(ParamsList)
    End Sub

    Public Sub InitParams(ByVal ParamsList As List(Of String))
        'TraderCodeMask = ParamsList(GlobalIndeces.TraderCodeMask)
    End Sub

    Public Sub Connect(ByVal ConnString As String, ByVal svSession As SvConnData)
        LhContext = New dataSvLhDataContext(ConnString)
        ConnSession = svSession
    End Sub

    Public Sub Connect(ByVal ConnString As String, ByVal svSession As SvConnData, ByVal ParamsList As List(Of String))
        LhContext = New dataSvLhDataContext(ConnString)
        ConnSession = svSession
        InitParams(ParamsList)
    End Sub

    'Public Sub ImportTraders(ByVal AllowInsert As Boolean, _
    '                           ByVal AllowUpdate As Boolean, _
    '                           Optional ByVal OtherTraderID As String = "", _
    '                           Optional ByRef FsContactID As String = "", _
    '                           Optional ByVal TypicalUpdate As Boolean = True)
    '    Dim TraderList As List(Of SvTrader)
    '    If OtherTraderID <> "" Then
    '        TraderList = ConnSession.LoadTraders(False, TypicalUpdate, False, False)
    '        If TypicalUpdate Then
    '            TraderList = (TraderList.AsQueryable.Where(Function(f) f.SourceTraderID.Equals(OtherTraderID))).ToList
    '        Else
    '            TraderList = (TraderList.AsQueryable.Where(Function(f) f.DestinationTraderID.Equals(OtherTraderID))).ToList
    '        End If
    '    Else
    '        TraderList = ConnSession.LoadTraders(True, TypicalUpdate, False, False)
    '    End If

    '    If OtherTraderID <> "" Then
    '        If TraderList.Count = 1 Then
    '            If AllowUpdate Or AllowInsert Then
    '                FsContactID = ImportSingleTrader(TraderList.FirstOrDefault, AllowInsert, AllowUpdate, TypicalUpdate)
    '            Else
    '                If TypicalUpdate Then
    '                    FsContactID = TraderList.FirstOrDefault.DestinationTraderID
    '                Else
    '                    FsContactID = TraderList.FirstOrDefault.SourceTraderID
    '                End If
    '            End If
    '        Else
    '            Throw New Exception("Θα έπρεπε να υπάρχει μοναδική εγγραφή Trader")
    '        End If
    '    Else
    '        For Each trd In TraderList
    '            ImportSingleTrader(trd, AllowInsert, AllowUpdate, TypicalUpdate)
    '        Next
    '    End If
    'End Sub

    'Public Sub ImportTraderFinancial()
    '    Dim FinancialList As List(Of SvTraderFinancial)

    '    FinancialList = ConnSession.LoadTradersFinancial(True)

    '    For Each fin In FinancialList
    '        If fin.FinancialTrader IsNot Nothing Then
    '            Dim ContactExists = (From t In LhContext.Contacts _
    '                              Where t.ID.Equals(fin.FinancialTrader.FinancialTraderDestinationID) _
    '                              Select t).FirstOrDefault
    '            If ContactExists IsNot Nothing Then
    '                ContactExists.SvTraderFinancial = fin
    '                ContactExists.ImportFinancial()
    '            End If
    '        End If
    '    Next

    'End Sub

    'Private Function ImportSingleTrader(ByVal trd As SvTrader, _
    '                                  ByVal AllowInsert As Boolean, _
    '                                  ByVal AllowUpdate As Boolean, _
    '                                  ByVal TypicalUpdate As Boolean) As String
    '    Dim CustomerExists As Contact
    '    Dim CustomerCodeExists As Contact

    '    If trd.DestinationTraderID IsNot Nothing Then
    '        CustomerExists = (From t In LhContext.Contacts _
    '                          Where t.ID.Equals(trd.DestinationTraderID) _
    '                          Select t).FirstOrDefault
    '    End If

    '    If CustomerExists Is Nothing Then
    '        CustomerCodeExists = (From t In LhContext.Contacts _
    '                              Where t.Euro.Equals(trd.DestinationCustomerCode) _
    '                              Select t).FirstOrDefault
    '    End If

    '    If CustomerCodeExists IsNot Nothing Then
    '        Throw New Exception("Ο κωδικός πελάτη υπάρχει ήδη")
    '    End If

    '    If CustomerExists Is Nothing Then
    '        If AllowInsert Then
    '            Dim NewCustomer As New Contact
    '            NewCustomer.SvTrader = trd
    '            NewCustomer.ImportCustomer(True, TypicalUpdate)
    '        End If
    '    Else
    '        If AllowUpdate Then
    '            CustomerExists.SvTrader = trd
    '            CustomerExists.ImportCustomer(False, TypicalUpdate)
    '        End If
    '    End If
    '    If TypicalUpdate Then
    '        Return trd.DestinationTraderID
    '    Else
    '        Return trd.SourceTraderID
    '    End If
    'End Function

    'Public Sub ExportTradersCategories()
    '    Dim AllCategories = From t In LhContext.svExportContactsCategories _
    '             Select t

    '    Dim CategoriesList = ConnSession.LoadTradersCategories(False, False)

    '    For Each cat In AllCategories
    '        Dim CatID As String = cat.ID.ToString

    '        Dim CategoryExists = (From t In CategoriesList _
    '                            Where t.SourceID.Equals(CatID) _
    '                            Select t).FirstOrDefault
    '        If CategoryExists Is Nothing Then
    '            Dim NewCategory As New svTraderCategory With { _
    '            .ID = Guid.NewGuid, _
    '            .LastModificationSource = Now, _
    '            .SourceID = cat.ID, _
    '            .IsDirty = True, _
    '            .IsActive = True, _
    '            .Name = cat.Name, _
    '            .SourceBelongsTo = cat.BelongsTo}

    '            NewCategory.ImportCategory(True)
    '            'SvConn.svTraders.InsertOnSubmit(NewCategory)
    '        Else
    '            CategoryExists.LastModificationSource = Now
    '            CategoryExists.IsDirty = True
    '            CategoryExists.IsActive = True
    '            CategoryExists.Name = cat.Name
    '            CategoryExists.SourceBelongsTo = cat.BelongsTo

    '            CategoryExists.ImportCategory(False)
    '        End If
    '    Next
    'End Sub

    Public Sub ExportSingleTrader(ByVal ord As Order, _
                                  ByVal AllowInsert As Boolean, _
                                  ByVal AllowUpdate As Boolean, _
                                  ByVal TypicalUpdate As Boolean, _
                                  ByVal ExportCategories As Boolean, _
                                  ByRef MainTraderAddressID As String)
        Dim TraderList = ConnSession.LoadTraders(False, TypicalUpdate, False, False)

        Dim TraderExists = (From t In TraderList _
                            Where t.SourceTraderID.Equals(ord.CustomerLoginId.ToString) _
                            Select t).FirstOrDefault

        If TraderExists Is Nothing Then
            Dim NewTrader As New SvTrader
            NewTrader.ID = Guid.NewGuid
            NewTrader.LastModificationSource = Now
            NewTrader.IsDirty = True
            NewTrader.FullName = If(ord.BillingIsInvoice, ord.BillingCompanyName, ord.BillingLastName & " " & ord.BillingFirstName)
            NewTrader.CompanyName = ord.BillingCompanyName
            NewTrader.SourceTraderCode = ord.CustomerLoginId
            NewTrader.SourceCustomerCode = ord.CustomerLoginId
            NewTrader.Username = ord.CustomerLoginEmail
            NewTrader.Email = ord.BillingEmail
            NewTrader.Phone1 = ord.BillingPhones
            NewTrader.Phone2 = ""
            NewTrader.MobilePhone = ord.BillingPhones
            NewTrader.Country = ord.BillingCountryCode
            NewTrader.City = ord.BillingCity
            NewTrader.Region = ord.BillingArea
            NewTrader.Address1 = ord.BillingAddressLine1
            NewTrader.Address2 = ord.BillingAddressLine2
            NewTrader.PostalCode = ord.BillingPostalCode
            NewTrader.TIN = ord.BillingTaxIdentifier
            NewTrader.TaxOffice = ord.BillingTaxAgency
            NewTrader.VatSourceID = ord.BillingBuyerCategory
            NewTrader.PaymentMethodSourceID = ord.PaymentMethodId
            'NewTrader.SalesmanSourceID = ord.MainSellerID
            NewTrader.IsWholesale = ord.BillingIsInvoice

            If TypicalUpdate Then
                NewTrader.SourceTraderID = ord.CustomerLoginId
                NewTrader.SourceCustomerID = ord.CustomerLoginId
            Else
                NewTrader.DestinationTraderID = ord.CustomerLoginId
                NewTrader.DestinationCustomerID = ord.CustomerLoginId
            End If

            ExportTraderAddress(ord, NewTrader, MainTraderAddressID, TypicalUpdate)
            'ExportTraderPerson(NewTrader)

            NewTrader.ImportTrader(True, TypicalUpdate)

            'If ExportCategories Then ExportTraderCategories(NewTrader)
        ElseIf AllowUpdate Then
            'If TraderExists.LastModificationSource < ord.MachineModDate Then
            TraderExists.LastModificationSource = Now
            TraderExists.IsDirty = True
            TraderExists.FullName = If(ord.BillingIsInvoice, ord.BillingCompanyName, ord.BillingLastName & " " & ord.BillingFirstName)
            TraderExists.CompanyName = ord.BillingCompanyName
            TraderExists.SourceTraderCode = ord.CustomerLoginId
            TraderExists.SourceCustomerCode = ord.CustomerLoginId
            TraderExists.Username = ord.CustomerLoginEmail
            TraderExists.Email = ord.BillingEmail
            TraderExists.Phone1 = ord.BillingPhones
            TraderExists.Phone2 = ""
            TraderExists.MobilePhone = ord.BillingPhones
            TraderExists.Country = ord.BillingCountryCode
            TraderExists.City = ord.BillingCity
            TraderExists.Region = ord.BillingArea
            TraderExists.Address1 = ord.BillingAddressLine1
            TraderExists.Address2 = ord.BillingAddressLine2
            TraderExists.PostalCode = ord.BillingPostalCode
            TraderExists.TIN = ord.BillingTaxIdentifier
            TraderExists.TaxOffice = ord.BillingTaxAgency
            TraderExists.VatSourceID = ord.BillingBuyerCategory
            TraderExists.PaymentMethodSourceID = ord.PaymentMethodId
            'TraderExists.SalesmanSourceID = ord.MainSellerID
            TraderExists.IsWholesale = ord.BillingIsInvoice

            ExportTraderAddress(ord, TraderExists, MainTraderAddressID, TypicalUpdate)
            'ExportTraderPerson(TraderExists)

            TraderExists.ImportTrader(False, TypicalUpdate)

            'If ExportCategories Then ExportTraderCategories(TraderExists)
            'End If
        End If
    End Sub

    'Public Sub ExportTraders1(Optional ByVal AllowInsert As Boolean = True, _
    '                         Optional ByVal AllowUpdate As Boolean = True, _
    '                         Optional ByVal TypicalUpdate As Boolean = True, _
    '                         Optional ByVal ExportCategories As Boolean = True, _
    '                         Optional ByRef MainTraderAddressID As String = "")
    '    Dim AllTraders = From t In LhContext.svExportCustomers _
    '                     Select t

    '    For Each cus In AllTraders
    '        ExportSingleTrader(cus, AllowInsert, AllowUpdate, TypicalUpdate, ExportCategories, MainTraderAddressID)
    '    Next
    'End Sub

    Private Sub ExportTraderAddress(ByVal ord As Order, _
                                    ByVal CurrentTrader As SvTrader, _
                                    ByRef MainTraderAddressID As String, _
                                    ByVal TypicalUpdate As Boolean)

        Dim AddressList = CurrentTrader.TraderAddresses(TypicalUpdate)

        Dim MainAddressExists = (From t In AddressList _
                                 Where t.SourceID.Equals((Convert.ToInt16(-1 * CurrentTrader.SourceTraderID).ToString)) _
                                 Select t).FirstOrDefault
        If MainAddressExists Is Nothing Then
            Dim NewAddress As New SvTraderAddress
            NewAddress.ID = Guid.NewGuid
            NewAddress.LastModificationSource = Now
            NewAddress.IsDirty = True
            NewAddress.AddressType = "Main"
            NewAddress.AddressName = "Main"
            NewAddress.IsMain = True
            NewAddress.FirstName = CurrentTrader.FirstName
            NewAddress.LastName = CurrentTrader.LastName
            NewAddress.CompanyName = CurrentTrader.FullName
            NewAddress.Email = CurrentTrader.Email
            NewAddress.Phone1 = CurrentTrader.Phone1
            NewAddress.Phone2 = CurrentTrader.Phone2
            NewAddress.FaxNumber = CurrentTrader.FaxNumber
            NewAddress.MobilePhone = CurrentTrader.MobilePhone
            NewAddress.Country = CurrentTrader.Country
            NewAddress.State = CurrentTrader.State
            NewAddress.City = CurrentTrader.City
            NewAddress.Region = CurrentTrader.Region
            NewAddress.Address1 = CurrentTrader.Address1
            NewAddress.Address2 = CurrentTrader.Address2
            NewAddress.PostalCode = CurrentTrader.PostalCode
            NewAddress.TIN = CurrentTrader.TIN

            If TypicalUpdate Then
                NewAddress.TraderSourceID = CurrentTrader.SourceTraderID
                NewAddress.CustomerSourceID = CurrentTrader.SourceCustomerID
                NewAddress.SourceID = -1 * CurrentTrader.SourceTraderID
            Else
                NewAddress.TraderDestinationID = CurrentTrader.DestinationTraderID
                NewAddress.CustomerDestinationID = CurrentTrader.DestinationCustomerID
                NewAddress.DestinationID = -1 * CurrentTrader.SourceTraderID
            End If

            NewAddress.ImportTraderAddress(True, TypicalUpdate)
        Else
            MainAddressExists.LastModificationSource = Now
            MainAddressExists.IsDirty = True
            MainAddressExists.AddressType = "Main"
            MainAddressExists.AddressName = "Main"
            MainAddressExists.IsMain = True
            MainAddressExists.FirstName = CurrentTrader.FirstName
            MainAddressExists.LastName = CurrentTrader.LastName
            MainAddressExists.CompanyName = CurrentTrader.FullName
            MainAddressExists.Email = CurrentTrader.Email
            MainAddressExists.Phone1 = CurrentTrader.Phone1
            MainAddressExists.Phone2 = CurrentTrader.Phone2
            MainAddressExists.FaxNumber = CurrentTrader.FaxNumber
            MainAddressExists.MobilePhone = CurrentTrader.MobilePhone
            MainAddressExists.Country = CurrentTrader.Country
            MainAddressExists.State = CurrentTrader.State
            MainAddressExists.City = CurrentTrader.City
            MainAddressExists.Region = CurrentTrader.Region
            MainAddressExists.Address1 = CurrentTrader.Address1
            MainAddressExists.Address2 = CurrentTrader.Address2
            MainAddressExists.PostalCode = CurrentTrader.PostalCode
            MainAddressExists.TIN = CurrentTrader.TIN

            MainAddressExists.ImportTraderAddress(False, TypicalUpdate)
        End If

        MainTraderAddressID = -1 * CurrentTrader.SourceTraderID

        Dim AddressExists = (From t In AddressList _
              Where t.SourceID.Equals(ord.CustomerLoginId.ToString) _
              And t.AddressType = "Delivery" _
              Select t).FirstOrDefault
        If AddressExists Is Nothing Then
            Dim NewAddress As New SvTraderAddress
            NewAddress.ID = Guid.NewGuid
            NewAddress.LastModificationSource = Now
            NewAddress.IsDirty = True
            NewAddress.AddressType = "Delivery"
            NewAddress.AddressName = "Delivery"
            NewAddress.IsMain = False
            NewAddress.FirstName = ord.ShippingFirstName
            NewAddress.LastName = ord.ShippingLastName
            NewAddress.CompanyName = CurrentTrader.FullName
            NewAddress.Email = ord.ShippingEmail
            NewAddress.Phone1 = ord.ShippingPhones
            NewAddress.Phone2 = ""
            NewAddress.Country = ord.ShippingCoutryCode
            NewAddress.City = ord.ShippingCity
            NewAddress.Region = ord.ShippingArea
            NewAddress.Address1 = ord.ShippingAddressLine1
            NewAddress.Address2 = ord.ShippingAddressLine2
            NewAddress.PostalCode = ord.ShippingPostalCode
            NewAddress.TIN = CurrentTrader.TIN

            If TypicalUpdate Then
                NewAddress.TraderSourceID = CurrentTrader.SourceTraderID
                NewAddress.CustomerSourceID = CurrentTrader.SourceCustomerID
                NewAddress.SourceID = ord.CustomerLoginId.ToString
            Else
                NewAddress.TraderDestinationID = CurrentTrader.DestinationTraderID
                NewAddress.CustomerDestinationID = CurrentTrader.DestinationCustomerID
                NewAddress.DestinationID = ord.CustomerLoginId.ToString
            End If

            NewAddress.ImportTraderAddress(True, TypicalUpdate)
        Else
            AddressExists.LastModificationSource = Now
            AddressExists.IsDirty = True
            AddressExists.AddressType = "Delivery"
            AddressExists.AddressName = "Delivery"
            AddressExists.IsMain = False
            AddressExists.FirstName = ord.ShippingFirstName
            AddressExists.LastName = ord.ShippingLastName
            AddressExists.CompanyName = CurrentTrader.FullName
            AddressExists.Email = ord.ShippingEmail
            AddressExists.Phone1 = ord.ShippingPhones
            AddressExists.Phone2 = ""
            AddressExists.Country = ord.ShippingCoutryCode
            AddressExists.City = ord.ShippingCity
            AddressExists.Region = ord.ShippingArea
            AddressExists.Address1 = ord.ShippingAddressLine1
            AddressExists.Address2 = ord.ShippingAddressLine2
            AddressExists.PostalCode = ord.ShippingPostalCode
            AddressExists.TIN = CurrentTrader.TIN

            AddressExists.ImportTraderAddress(False, TypicalUpdate)
        End If

    End Sub

    ''Private Sub ExportTraderPerson(ByVal CurrentTrader As SvTrader)

    ''    Dim PersonList = CurrentTrader.svTraderPersons.ToList 'ConnSession.LoadTraderPersons(CurrentTrader)


    ''    Dim AllPersons = From t In LhContext.ContactPersons _
    ''                     Where t.ContactID.Equals(CurrentTrader.SourceTraderID) _
    ''                     Select t

    ''    For Each prs In AllPersons
    ''        Dim PersonExists = (From t In PersonList _
    ''              Where t.SourceID.Equals(prs.ID.ToString) _
    ''              Select t).FirstOrDefault
    ''        If PersonExists Is Nothing Then
    ''            Dim NewPerson As New svTraderPerson With { _
    ''            .svTrader = CurrentTrader, _
    ''            .ID = Guid.NewGuid, _
    ''            .SourceID = prs.ID, _
    ''            .LastModificationSource = Now, _
    ''            .IsDirty = True, _
    ''            .Name = prs.Name, _
    ''            .JobTitle = prs.Position}

    ''            NewPerson.ImportTraderPerson(True)
    ''        Else
    ''            PersonExists.LastModificationSource = Now
    ''            PersonExists.IsDirty = True
    ''            PersonExists.Name = prs.Name
    ''            PersonExists.JobTitle = prs.Position

    ''            PersonExists.ImportTraderPerson(False)
    ''        End If
    ''    Next

    ''End Sub

    'Private Sub ExportTraderCategories(ByVal CurrentTrader As SvTrader)
    '    Dim AllCategories = From t In LhContext.ContactAndCategories _
    '                        Join cc In LhContext.svExportContactsCategories _
    '                        On t.CategoryID Equals cc.ID _
    '                        Where t.ContactID.Equals(CurrentTrader.SourceTraderID) _
    '                        Select t

    '    'Dim CategoriesList = CurrentTrader.Categories

    '    Dim CategoriesIDs As New List(Of String)

    '    For Each cat In AllCategories
    '        CategoriesIDs.Add(cat.CategoryID.ToString)
    '        '    If CategoriesList.Count = 0 Then
    '        '        CategoriesIDs.Add(cat.CategoryID)
    '        '    Else
    '        '        Dim CategoryExists = (From t In CategoriesList _
    '        '                            Where t.SourceID.Equals(cat.CategoryID.ToString) _
    '        '                            And t.DestinationID IsNot Nothing _
    '        '                            Select t).FirstOrDefault
    '        '        If CategoryExists Is Nothing Then
    '        '            CategoriesIDs.Add(cat.CategoryID)
    '        '        End If
    '        '    End If
    '    Next
    '    CurrentTrader.ImportTraderCategory1(CategoriesIDs)

    'End Sub

    Public Sub ExportOrders(ByVal ImportTrader As Boolean, _
                             Optional ByVal AllowInsert As Boolean = True, _
                             Optional ByVal AllowUpdate As Boolean = True, _
                             Optional ByVal SameDirection As Boolean = True, _
                             Optional ByVal ExportCategories As Boolean = True, _
                             Optional ByVal DocTypeID As String = "")

        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)


        Dim OrdersList = ConnSession.LoadOrders(False, SvConnData.OrderTypes.Sales)

        Dim AllOrders = From t In LhContext.Orders _
                        Where t.StatusId = 1 _
                        Select t

        For Each ord In AllOrders

            Dim OrderExists = (From t In OrdersList _
                             Where t.SourceID.Equals(ord.WebOrderId.ToString) _
                             Select t).FirstOrDefault
            If OrderExists Is Nothing Then
                If ImportTrader Then
                    ExportSingleTrader(ord, AllowInsert, AllowUpdate, SameDirection, ExportCategories, "")
                    ConnSession.UpdateTraderDestinationCodes("LH.", "LH.", "{0:000000}")
                End If

                Dim DeliveryAddresID As String = ""
                Dim MainAddresID As String = ""

                Dim NewOrder As New SvOrder
                NewOrder.ID = Guid.NewGuid
                NewOrder.SourceID = ord.WebOrderId
                NewOrder.OrderCode = ord.OrderCode

                NewOrder.LastModificationSource = ord.CheckoutCompletedOn
                NewOrder.OrderTraderAddressID = DeliveryAddresID
                NewOrder.IsDirty = True
                NewOrder.OrderTraderID = ord.CustomerLoginId
                'NewOrder.OrderStatus = ord.DotID
                'NewOrder.DocTypeID = ord.DosID
                'NewOrder.DocTypeName = ord.DosCode
                NewOrder.OrderPaymentID = ord.PaymentMethodId
                'NewOrder.Fiscal = ord.Fiscal
                NewOrder.OrderDiscountNoVat = 0
                NewOrder.OrderDiscountWithVat = 0

                For Each item In ord.OrderAllowancesOrCharges.Where(Function(f) f.IsCharge)
                    If item.AllowanceOrChargeCode = "SHI" Then
                        NewOrder.ShippingCostNoVat = item.Amount - item.VATAmount
                        NewOrder.ShippingCostWithVat = item.Amount
                    ElseIf item.AllowanceOrChargeCode = "PC" Then
                        NewOrder.SurchargeNoVat = item.Amount - item.VATAmount
                        NewOrder.SurchargeWithVat = item.Amount
                    End If
                Next

                'NewOrder.OrderConsignmentID = ord.SendWayID
                'NewOrder.SalesmanSourceID = ord.EmplID
                'NewOrder.BraDestinationID = ord.BraID
                'NewOrder.OrderDiscountPerc = ord.OfferDiscountPerc
                'NewOrder.WarehouseDestinationID = ord.WrhID

                NewOrder.OrderDate = Now.Date
                NewOrder.Notes = ord.CustomerComments

                NewOrder.OrderTraderAddressID = ord.CustomerLoginId


                Dim ProductsList = ord.OrderItems

                For Each prd In ProductsList

                    

                    Dim NewOrderDetail As New SvOrderDetail
                    'NewOrderDetail.svOrder = NewOrder

                    NewOrderDetail.ID = Guid.NewGuid
                    NewOrderDetail.SourceID = prd.OrderItemId
                    NewOrderDetail.OrderSourceID = NewOrder.SourceID
                    NewOrderDetail.LastModificationSource = Now
                    NewOrderDetail.IsDirty = True
                    NewOrderDetail.DetailProductSourceID = prd.ProductId
                    NewOrderDetail.Quantity = Convert.ToDouble(prd.Quantity)
                    'NewOrderDetail.FinalDiscountPerc = prd.DiscPerc
                    NewOrderDetail.ItemDiscountPerc = Math.Round(Convert.ToDouble(prd.DiscountAmount / prd.NetAmount), 2)
                    NewOrderDetail.UnitCataloguePriceNoVat = Math.Round(Convert.ToDouble((prd.NetAmount - prd.DiscountAmount) / prd.Quantity), 6)
                    NewOrderDetail.UnitCataloguePriceWithVat = Math.Round(Convert.ToDouble((prd.TotalAmount - (prd.DiscountAmount * prd.VATPercent)) / prd.Quantity), 6)

                    Dim ProdCode = ConnSession.GetProductsCodeById(prd.SKUId)

                    If ProdCode IsNot Nothing Then
                        NewOrderDetail.Color = ProdCode.Color
                        NewOrderDetail.Size = ProdCode.Size
                    End If




                    NewOrderDetail.ImportOrderDetail(True)


                    'NewOrderDetail.Color = 

                    'NewOrder.Details.Add(NewOrderDetail)
                Next

                NewOrder.ImportOrder(True)
            End If
        Next

    End Sub

    'Public Sub ImportOrders(ByVal ParamsList As List(Of String), _
    '                          Optional ByVal DefaultTraderID As String = "", _
    '     Optional ByVal DefaultSalesman As String = "", _
    '                          Optional ByVal AllowInsert As Boolean = True, _
    '                          Optional ByVal AllowUpdate As Boolean = True, _
    '                          Optional ByVal SameDirection As Boolean = True)
    '    Dim FsContactID As String = ""

    '    Dim OrderList = ConnSession.LoadOrders(True, SvConnData.OrderTypes.Sales)

    '    For Each ord In OrderList

    '        If DefaultTraderID = "" Then
    '            ImportTraders(AllowInsert, AllowUpdate, ord.OrderTraderID, FsContactID, SameDirection)
    '        Else
    '            FsContactID = DefaultTraderID
    '        End If

    '        If FsContactID = "" Then
    '            Throw New Exception("Δεν έχει οριστεί πελάτης")
    '        End If
    '        Dim OrderExists As IQueryable(Of Offer)

    '        If ord.DestinationID IsNot Nothing Then
    '            OrderExists = From t In LhContext.Offers _
    '                              Where t.ID.ToString.Equals(ord.DestinationID) _
    '                              Select t
    '        End If

    '        If OrderExists Is Nothing OrElse OrderExists.Count = 0 Then
    '            Dim NewOrder As New Offer

    '            Dim CurrentEmpl = From t In LhContext.Empls _
    '                             Where t.ID.Equals(ParamsList(OrderIndeces.EmplID)) _
    '                             Select t

    '            Dim CurrentContact = From t In LhContext.Contacts _
    '                                 Where t.ID.Equals(FsContactID) _
    '                                 Select t

    '            If CurrentEmpl.Count = 1 AndAlso CurrentContact.Count = 1 Then
    '                ParamsList.Insert(OrderIndeces.ContactName, CurrentContact.FirstOrDefault.Name)
    '                ParamsList.Insert(OrderIndeces.EmplName, CurrentEmpl.FirstOrDefault.Name)

    '                NewOrder.SvOrder = ord

    '                'Dim AllItems As New List(Of SvOrderDetail)

    '                'For Each detail In ord.svOrderDetails
    '                '    AllItems.Add(detail)
    '                'Next

    '                'NewOrder.AllLines = AllItems

    '                'NewOrder.SvTrader = Trader

    '                'Dim AddressList = ConnSession.LoadTraderAddresses(Trader)

    '                'Dim DeliveryTraderAddress = (From t In AddressList _
    '                '                             Where t.SourceID.Equals(ord.OrderTraderAddressID) _
    '                '                             Select t).FirstOrDefault

    '                'NewOrder.SvTraderDeliveryAddress = DeliveryTraderAddress

    '                'Dim MainAddressID = New Guid((From t In AddressList _
    '                '         Where t.TraderSourceID.Equals(ord.OrderTraderID) _
    '                '         And t.AddressType = ParamsList(OrderIndeces.DeliveryAddressType) _
    '                '         Select t.DestinationID).FirstOrDefault)

    '                'NewOrder.MainAddressID = MainAddressID

    '                NewOrder.ImportOrder(ParamsList, True, FsContactID, DefaultSalesman)
    '            Else
    '                Throw New Exception("Λάθος στον υπάλληλο/πελάτη")
    '            End If
    '        End If
    '    Next
    'End Sub

    Public Sub ImportStocks(ByVal WarehouseID As String, ByVal ConnString As String)
        Dim CodeList = ConnSession.LoadProductsCodes(True, False)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Dim Weight As Decimal = 0
        Dim Volume As Decimal = 0
        For Each cde In CodeList
            Try
                If cde.CodeProduct Is Nothing Then
                    Throw New Exception("Δεν υπάρχει είδος")
                End If

                Dim StockExists As IQueryable(Of StockKeepingUnit)

                If cde.DestinationID IsNot Nothing Then
                    StockExists = From t In LhContext.StockKeepingUnits _
                                      Where t.SKUId.ToString.Equals(cde.DestinationID) _
                                      Select t
                End If

                If cde.ProductSourceID IsNot Nothing Then
                    Dim Prod = ConnSession.GetProductsById(cde.ProductSourceID)
                    If Prod.Item(0) IsNot Nothing Then
                        Weight = IIf(Prod.Item(0).ProductVolumeWeight IsNot Nothing, Prod.Item(0).ProductVolumeWeight, 0)
                        Volume = IIf(Prod.Item(0).ProductWeight IsNot Nothing, Prod.Item(0).ProductWeight, 0)
                    End If

                End If



                If StockExists Is Nothing OrElse StockExists.Count = 0 Then
                    Dim NewStock As New StockKeepingUnit
                    NewStock.VolumetricWeight = Volume
                    NewStock.Weight = Weight
                    NewStock.WarehouseID = WarehouseID
                    NewStock.SvProductCode = cde
                    NewStock.ImportStock(True, ConnString)
                Else
                    Dim ExistingStock As StockKeepingUnit = StockExists.FirstOrDefault
                    ExistingStock.WarehouseID = WarehouseID
                    ExistingStock.VolumetricWeight = Volume
                    ExistingStock.Weight = Weight
                    ExistingStock.SvProductCode = cde
                    ExistingStock.ImportStock(False, ConnString)
                End If
            Catch ex As Exception
                SvSys.ErrorLog(("LH ImportStock " & cde.SourceID).PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
    End Sub


    'Public Sub ImportConsignmentMethods()
    '    Dim SendWayList = ConnSession.LoadConsignments(True)

    '    For Each cns In SendWayList
    '        Dim SendWayExists As IQueryable(Of SendWay)
    '        If cns.DestinationID IsNot Nothing Then
    '            SendWayExists = From t In LhContext.SendWays _
    '                            Where t.ID.ToString.Equals(cns.DestinationID) _
    '                            Select t
    '        End If

    '        If SendWayExists Is Nothing OrElse SendWayExists.Count = 0 Then
    '            Dim NewSendWay As New SendWay
    '            NewSendWay.SvConsignmentMethod = cns
    '            NewSendWay.ImportConsignmentMethod(True)
    '        Else
    '            Dim ExistingSendWay As SendWay = SendWayExists.FirstOrDefault
    '            ExistingSendWay.SvConsignmentMethod = cns
    '            ExistingSendWay.ImportConsignmentMethod(False)
    '        End If
    '    Next
    'End Sub

    'Public Sub ImportPayways()
    '    Dim PaywayList = ConnSession.LoadPayments(True)

    '    For Each paw In PaywayList
    '        Dim PaywayExists As IQueryable(Of Payway)
    '        If paw.DestinationID IsNot Nothing Then
    '            PaywayExists = From t In LhContext.PayWays _
    '                            Where t.ID.ToString.Equals(paw.DestinationID) _
    '                            Select t
    '        End If

    '        If PaywayExists Is Nothing OrElse PaywayExists.Count = 0 Then
    '            Dim NewPayway As New Payway
    '            NewPayway.SvPaymentMethod = paw
    '            NewPayway.ImportPayway(True)
    '        Else
    '            Dim ExistingPayway As Payway = PaywayExists.FirstOrDefault
    '            ExistingPayway.SvPaymentMethod = paw
    '            ExistingPayway.ImportPayway(False)
    '        End If
    '    Next
    'End Sub

    'Public Sub ImportVat()
    '    Dim VatList = ConnSession.LoadVat(True)

    '    For Each vt In VatList
    '        Dim VatExists As IQueryable(Of VatCat)
    '        If vt.DestinationID IsNot Nothing Then
    '            VatExists = From t In LhContext.VATCATs _
    '                            Where t.ID.ToString.Equals(vt.DestinationID) _
    '                            Select t
    '        End If

    '        If VatExists Is Nothing OrElse VatExists.Count = 0 Then
    '            Dim NewVat As New VatCat
    '            NewVat.SvVat = vt
    '            NewVat.ImportVat(True)
    '        Else
    '            Dim ExistingVat As VatCat = VatExists.FirstOrDefault
    '            ExistingVat.SvVat = vt
    '            ExistingVat.ImportVat(False)
    '        End If
    '    Next
    'End Sub


    'Public Sub ImportServices()
    '    Dim ServiceList = ConnSession.LoadProducts(True, False, False, True, False)

    '    For Each srv In ServiceList
    '        Dim ServiceExists As IQueryable(Of Item)
    '        If srv.DestinationID IsNot Nothing Then
    '            ServiceExists = From t In LhContext.Items _
    '                            Where t.ID.ToString.Equals(srv.DestinationID) _
    '                            Select t
    '        End If

    '        If ServiceExists Is Nothing OrElse ServiceExists.Count = 0 Then
    '            Dim NewItem As New Item
    '            NewItem.SvProduct = srv
    '            NewItem.ImportService(True)
    '        Else
    '            Dim ExistingService As Item = ServiceExists.FirstOrDefault
    '            ExistingService.SvProduct = srv
    '            ExistingService.ImportService(False)
    '        End If
    '    Next
    'End Sub

    Public Sub ImportProducts(ByVal PreferredCategories As List(Of Integer))
        Dim ProductList = ConnSession.LoadProducts(True, False, False, True, False)

        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Dim ci As CultureInfo = Thread.CurrentThread.CurrentCulture


        For Each item In ProductList
            Try
                LhContext = New dataSvLhDataContext(LhContext.Connection.ConnectionString)

                Dim ProductExists As IQueryable(Of Product)
                If item.DestinationID IsNot Nothing Then
                    ProductExists = From t In LhContext.Products _
                                    Where t.ProductId.Equals(item.DestinationID) _
                                    Select t
                End If

                If ProductExists Is Nothing OrElse ProductExists.Count = 0 Then
                    Dim NewProduct As New Product
                    NewProduct.SvProduct = item
                    NewProduct.ImportProduct(True, PreferredCategories)
                Else
                    Dim ExistingProduct As Product = ProductExists.FirstOrDefault
                    ExistingProduct.SvProduct = item
                    ExistingProduct.ImportProduct(False, PreferredCategories)
                End If

            Catch ex As Exception
                SvSys.ErrorLog(("LH ImportProducts " & item.SourceID).PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
    End Sub


    Public Sub ImportProductsCategories(ByVal ParentOnly As Boolean) 'to kalei me false
        Dim CategoryList = ConnSession.LoadProductsCategories(True, ParentOnly) 'dialexe ta isDirty=1



        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Dim ci As CultureInfo = Thread.CurrentThread.CurrentCulture

        For Each item In CategoryList
            Try
                Dim CategoryExists As IQueryable(Of Category)
                If item.DestinationID IsNot Nothing Then
                    CategoryExists = From t In LhContext.Categories _
                                    Where t.CategoryId.ToString.Equals(item.DestinationID) _
                                    Select t
                End If

                If CategoryExists Is Nothing OrElse CategoryExists.Count = 0 Then
                    Dim NewCategory As New Category
                    NewCategory.SvProductCategory = item
                    NewCategory.ImportCategory(True)
                Else
                    Dim NewCategory As Category = CategoryExists.FirstOrDefault
                    NewCategory.SvProductCategory = item
                    NewCategory.ImportCategory(False)
                End If
    
            Catch ex As Exception
                SvSys.ErrorLog(("LH ImportProductsCategories " & item.SourceID).PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try
        Next
    End Sub

    Public Sub ImportProductsResources(ByVal ConnString As String)
        Dim ProductList = ConnSession.GetProductProductCodes()

        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)


        Dim c = New String() {"", "b", "s", "d"}
        'Dim d = New String() {"Κύρια φώτο", "πίσω μέρος", "πλαϊνο", "λεπτομέρεια"}
        Dim RL1 As String = "", RURI As String = ""
        Dim SKU As New StockKeepingUnit

        For i = 0 To ProductList.GetUpperBound(0)
            Try

                Dim ProductId As String = ProductList(i, 1).ToString
                Dim colorid As String = ""
                colorid = SKU.GetColorID(ProductList(i, 2).ToString, ConnString)
                If (Not String.IsNullOrEmpty(Trim(ProductId))) And (Not String.IsNullOrEmpty(Trim(colorid))) Then


                    Dim ProductExists = From t In LhContext.ProductResources _
                                                Where t.ColorId.Equals(colorid) And t.ProductId.Equals(ProductId) _
                                                Select t


                    If ProductExists.Count = 0 Then
                        Try
                            For aa = 0 To 3

                                Dim NewProductResources As New ProductResource

                                RURI = "images\" & ProductList(i, 0).ToString & "_" & IIf(colorid.Length < 3, colorid.Substring(0, colorid.Length), colorid.Substring(0, 3)) & c(aa) & ".jpg"
                                RL1 = ProductList(i, 0).ToString
                                RL1 = IIf(RL1.StartsWith("05.1529."), RL1.Replace("05.1529.", ""), RL1)
                                RL1 = RL1 & "_" & IIf(colorid.Length < 3, colorid.Substring(0, colorid.Length), colorid.Substring(0, 3))

                                NewProductResources.ImportProductResource(aa + 1, RL1, ProductId, colorid, RURI)

                            Next
                        Catch ex As Exception
                            SvSys.ErrorLog(("LH ImportProductsResources ").PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
                        End Try
                    End If
                End If
            Catch ex As Exception
                SvSys.ErrorLog(("LH ImportProductsResources ").PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
            End Try


        Next
    End Sub


    'Public Sub ExportProducts()
    '    Dim AllProducts = From t In LhContext.svExportProducts _
    '                   Select t

    '    'If Not SvGUID.IsEmptyGuid(CategoryID) AndAlso _
    '    'Not SvGUID.IsEmptyGuid(CategoryRootID) Then
    '    '    Dim CategoryItems = (From t In Glx.TEITEMCATEGORies _
    '    '                      Where t.TEMSCTIDROOT.Equals(CategoryRootID) _
    '    '                      And t.TEMSCTIDLEAF.Equals(CategoryID) _
    '    '                      Select t.TEITEMID).ToList

    '    '    AllItems = AllItems.Where(Function(f) CategoryItems.Contains(f.TEID))

    '    'End If

    '    Dim ProductList = ConnSession.LoadProducts(False, False, False, True, False)

    '    For Each item In AllProducts
    '        Dim ProductExists = (From t In ProductList _
    '                          Where t.SourceID.Equals(item.ID.ToString) _
    '                          Select t).FirstOrDefault

    '        Dim NoVatPrice As Double

    '        'If item.TEVAT

    '        If ProductExists Is Nothing Then
    '            'If item.ItemActive Then
    '            Dim NewProduct As New SvProduct With { _
    '            .ID = Guid.NewGuid, _
    '            .SourceID = item.ID.ToString, _
    '            .LastModificationSource = Now, _
    '            .IsDirty = True, _
    '            .Name = item.Name, _
    '            .ProductCode = item.Code, _
    '            .RetailPrice = item.RetailPrice, _
    '            .WholesalePrice = item.WholesalePrice, _
    '            .CurrencyName = "EUR", _
    '            .Description1 = "", _
    '            .VatSourceID = item.VatID.ToString, _
    '            .IsActive = item.ItemActive, _
    '            .ProductWeight = 0, _
    '            .ProductLength = 0, _
    '            .ProductWidth = 0, _
    '            .ProductHeight = 0, _
    '            .DefaultBalance = item.DefaultBalance, _
    '            .CountUnitName = "TEM", _
    '            .WeightUnitName = "γρμ.", _
    '            .LengthUnitName = "mm"}

    '            NewProduct.ImportProduct(True)

    '            ProductExists = NewProduct
    '            'End If
    '        Else
    '            ProductExists.LastModificationSource = Now
    '            ProductExists.IsDirty = True
    '            ProductExists.Name = item.Name
    '            ProductExists.ProductCode = item.Code
    '            ProductExists.RetailPrice = item.RetailPrice
    '            ProductExists.WholesalePrice = item.WholesalePrice
    '            ProductExists.Description1 = ""
    '            ProductExists.VatSourceID = item.VatID.ToString
    '            ProductExists.IsActive = item.ItemActive
    '            ProductExists.ProductWeight = 0
    '            ProductExists.ProductLength = 0
    '            ProductExists.ProductWidth = 0
    '            ProductExists.ProductHeight = 0
    '            ProductExists.DefaultBalance = item.DefaultBalance

    '            ProductExists.ImportProduct(False)
    '        End If

    '    Next

    '    Dim InactiveProducts = From t In ProductList _
    '                         Where Not AllProducts.Select(Function(f) f.ID.ToString).Contains(t.SourceID) _
    '                         Select t

    '    For Each item In InactiveProducts
    '        item.SetActive(False)
    '    Next
    'End Sub

    'Public Sub ExportVat()
    '    Dim AllVat = From t In LhContext.svExportVats _
    '           Select t
    '    Dim VatList = ConnSession.LoadVat(False)

    '    For Each Vat In AllVat
    '        Dim VatExists = (From t In VatList _
    '                          Where t.SourceID.Equals(Vat.ID.ToString) _
    '                          Select t).FirstOrDefault
    '        If VatExists Is Nothing Then
    '            Dim NewVat As New svVat With { _
    '            .ID = Guid.NewGuid, _
    '            .SourceID = Vat.ID.ToString, _
    '            .LastModificationSource = Now, _
    '            .IsDirty = True, _
    '            .Name = Vat.Name, _
    '            .VatPercentage = Vat.Percentage}

    '            NewVat.ImportVat(True)
    '        Else
    '            VatExists.LastModificationSource = Now
    '            VatExists.IsDirty = True
    '            VatExists.Name = Vat.Name
    '            VatExists.VatPercentage = Vat.Percentage

    '            VatExists.ImportVat(False)
    '        End If
    '    Next
    'End Sub

    'Public Sub UpdateContactsFromSen(Optional ByVal ContactID As String = "")
    '    Dim TraderList = ConnSession.LoadTraders(False, True, False, False, ContactID)

    '    For Each trd In TraderList
    '        If trd.DestinationTraderID IsNot Nothing Then
    '            Dim CurrentContact = (From t In LhContext.Contacts _
    '                                 Where t.ID.ToString.Equals(trd.SourceTraderID) _
    '                                 Select t).FirstOrDefault
    '            If CurrentContact IsNot Nothing Then
    '                CurrentContact.SvTrader = trd
    '                CurrentContact.UpdateFromSen()
    '                For Each adr In trd.TraderAddresses(True)
    '                    If adr.SourceID < 0 AndAlso CurrentContact.SenAdrId <> adr.DestinationID Then
    '                        CurrentContact.UpdateAddressFromSen(adr.DestinationID)
    '                    Else
    '                        Dim CurrentAddress = (From t In LhContext.ContactAddresses _
    '                                              Where t.ID.ToString.Equals(adr.SourceID) _
    '                                              Select t).FirstOrDefault
    '                        If CurrentAddress IsNot Nothing AndAlso CurrentAddress.SENID <> adr.DestinationID Then
    '                            CurrentAddress.SvTraderAddress = adr
    '                            CurrentAddress.UpdateFromSen()
    '                        End If
    '                    End If
    '                Next
    '            End If
    '        End If
    '    Next
    'End Sub

    'Public Sub UpdateOffersFromSen()
    '    Dim OrderList = ConnSession.LoadOrders(False, SvConnData.OrderTypes.Sales)
    '    For Each ord In OrderList
    '        If ord.DestinationID IsNot Nothing Then
    '            Dim CurrentOffer = (From t In LhContext.Offers _
    '                                Where t.ID.ToString.Equals(ord.SourceID) _
    '                                Select t).FirstOrDefault
    '            If CurrentOffer IsNot Nothing Then
    '                CurrentOffer.SvOrder = ord
    '                CurrentOffer.UpdateFromSen()
    '            End If
    '        End If
    '    Next
    'End Sub

End Class

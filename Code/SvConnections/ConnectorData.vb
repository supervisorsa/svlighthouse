﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports Commercial

Public Class ConnectorData
    Implements IDisposable

    Public EnDecrypt As New SvCrypto(SvCrypto.SymmProvEnum.Rijndael)
    Private GlobalKey As String = "Connr"

    Public AppPath As String
    Public TaskDescription As String
    Public ConnectionId1 As Integer
    Public ConnectionId2 As Integer

    Public SvConnId As Integer
    Public SvConnSession As New SqlConnection

    Public SenSession1 As SacSessionX
    Public SrsSession1 As SacSessionX
    Public SqlSession1 As New SqlConnection
    Public ControlSession1 As New SqlConnection
    Public PrimeSession1 As ICLASession
    Public File1 As KeyValuePair(Of String, String)

    Public SenSession2 As SacSessionX
    Public SrsSession2 As SacSessionX
    Public SqlSession2 As New SqlConnection
    Public ControlSession2 As New SqlConnection
    Public PrimeSession2 As ICLASession
    Public File2 As KeyValuePair(Of String, String)

    Public _SalesPersons As New List(Of SvSessionsConn.SvConnData.SvStructure)
    Public WriteOnly Property SalesPersons() As List(Of SvSessionsConn.SvConnData.SvStructure)
        Set(ByVal value As List(Of SvSessionsConn.SvConnData.SvStructure))
            _SalesPersons = value
        End Set
    End Property

    Public _Payways As Dictionary(Of String, String)
    Public WriteOnly Property Payways() As Dictionary(Of String, String)
        Set(ByVal value As Dictionary(Of String, String))
            _Payways = value
        End Set
    End Property

    Public _ItemsCategories As Dictionary(Of KeyValuePair(Of String, String), String)
    Public WriteOnly Property ItemsCategories() As Dictionary(Of KeyValuePair(Of String, String), String)
        Set(ByVal value As Dictionary(Of KeyValuePair(Of String, String), String))
            _ItemsCategories = value
        End Set
    End Property

    'Public _ItemsDuration As Dictionary(Of String, String)
    'Public WriteOnly Property ItemsDuration() As Dictionary(Of String, String)
    '    Set(ByVal value As Dictionary(Of String, String))
    '        _ItemsDuration = value
    '    End Set
    'End Property

    Public _ContactsCategories As Dictionary(Of KeyValuePair(Of String, String), KeyValuePair(Of String, String))
    Public WriteOnly Property ContactsCategories() As Dictionary(Of KeyValuePair(Of String, String), KeyValuePair(Of String, String))
        Set(ByVal value As Dictionary(Of KeyValuePair(Of String, String), KeyValuePair(Of String, String)))
            _ContactsCategories = value
        End Set
    End Property

    Public Function GetSenEmplID(ByVal SqlEmplID As String) As String
        Dim SenEmplID As String = "0"
        For Each item In _SalesPersons
            If item.KeyID.Equals(SqlEmplID) Then SenEmplID = item.ValueID
        Next
        Return SenEmplID
    End Function

    Public Function GetSenPaywayID(ByVal SqlPaywayID As String) As String
        Dim SenPaywayID As String = "0"
        For Each item In _Payways
            If item.Key.Equals(SqlPaywayID) Then SenPaywayID = item.Value
        Next
        Return SenPaywayID
    End Function

    Public Function GetSqlCategoryID(ByVal SenCategoryID As String) As String
        Dim SqlCategoryID As String = "0"
        For Each item In _ItemsCategories
            Dim SqlPair As KeyValuePair(Of String, String)
            SqlPair = item.Key
            If item.Value.Equals(SenCategoryID) Then SqlCategoryID = SqlPair.Key
        Next
        Return SqlCategoryID
    End Function

    Public Function GetSqlEmplID(ByVal SenEmplID As String) As String
        Dim SqlEmplID As Integer = 0
        For Each item In _SalesPersons
            If item.ValueID.Equals(SenEmplID) Then SqlEmplID = item.KeyID
        Next
        Return SqlEmplID
    End Function

    Public Function GetSqlPaywayID(ByVal SenPaywayID As String) As String
        Dim SqlPaywayID As Integer = 0
        For Each item In _Payways
            If item.Value.Equals(SenPaywayID) Then SqlPaywayID = item.Key
        Next
        Return SqlPaywayID
    End Function

    Public Function GetSqlDuration(ByVal SenCategoryID As String) As String
        Dim SqlDuration As String = 0
        For Each item In _ItemsCategories
            Dim SqlPair As KeyValuePair(Of String, String)
            SqlPair = item.Key
            If item.Value.Equals(SenCategoryID) Then SqlDuration = SqlPair.Value
        Next
        Return SqlDuration
    End Function

    Public Function GetSenContactCategoryID(ByVal SqlCategoryID As String) As String
        Dim SenCategoryID As String = 0
        For Each item In _ContactsCategories
            Dim SqlPair As KeyValuePair(Of String, String)
            Dim SenPair As KeyValuePair(Of String, String)
            SqlPair = item.Key
            SenPair = item.Value

            If SqlPair.Key.Equals(SqlCategoryID) Then SenCategoryID = SenPair.Key
        Next
        Return SenCategoryID
    End Function

    Public Function GetSenContactCategoryParentID(ByVal SqlCategoryID As String) As String
        Dim SenCategoryID As Integer = 0
        For Each item In _ContactsCategories
            Dim SqlPair As KeyValuePair(Of String, String)
            Dim SenPair As KeyValuePair(Of String, String)
            SqlPair = item.Key
            SenPair = item.Value

            If SqlPair.Key.Equals(SqlCategoryID) Then SenCategoryID = SenPair.Value
        Next
        Return SenCategoryID
    End Function

    Public _StockAutoCat As Boolean
    Public WriteOnly Property StockAutoCat() As Boolean
        Set(ByVal value As Boolean)
            _StockAutoCat = value
        End Set
    End Property

    Public _ContactAutoCat As Boolean
    Public WriteOnly Property ContactAutoCat() As Boolean
        Set(ByVal value As Boolean)
            _ContactAutoCat = value
        End Set
    End Property

    Public _AcaCode As String
    Public WriteOnly Property AcaCode() As String
        Set(ByVal value As String)
            _AcaCode = value
        End Set
    End Property

    Public _Currency As String
    Public WriteOnly Property Currency() As String
        Set(ByVal value As String)
            _Currency = value
        End Set
    End Property

    Public Function ConnectToSql(ByVal Server As String, _
                            ByVal DB As String, _
                            ByVal User As String, _
                            ByVal Pwd As String) _
                            As String
        Try
            If SqlSession1.State = ConnectionState.Open AndAlso SvConnSession.State = ConnectionState.Open Then
                SqlSession2 = New SqlConnection
                SqlSession2.ConnectionString = DbConn.ConstructConnString( _
                    Server, _
                    User, _
                    EnDecrypt.Decrypting(Pwd, GlobalKey, "-"), _
                    DB, "")
                SqlSession2.Open()
                Return ""
            ElseIf SvConnSession.State = ConnectionState.Open Then
                SqlSession1 = New SqlConnection
                SqlSession1.ConnectionString = DbConn.ConstructConnString( _
                    Server, _
                    User, _
                    EnDecrypt.Decrypting(Pwd, GlobalKey, "-"), _
                    DB, "")
                SqlSession1.Open()
                Return ""
            Else
                SvConnSession = New SqlConnection
                SvConnSession.ConnectionString = DbConn.ConstructConnString( _
                    Server, _
                    User, _
                    EnDecrypt.Decrypting(Pwd, GlobalKey, "-"), _
                    DB, "")
                SvConnSession.Open()
                Return ""
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function DisconnectSql() As String
        Try
            If SvConnSession.State = ConnectionState.Open Then
                SvConnSession.Close()
                SvConnSession.Dispose()
            End If
            If SqlSession1.State = ConnectionState.Open Then
                SqlSession1.Close()
                SqlSession1.Dispose()
            End If
            If SqlSession2.State = ConnectionState.Open Then
                SqlSession2.Close()
                SqlSession2.Dispose()
            End If
            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ConnectToSen1(ByVal Host As String, _
                                  ByVal SasAlias As String, _
                                  ByVal SenUser As String, _
                                  ByVal SenPwd As String, _
                                  ByVal CmpCode As String, _
                                  ByVal WrhID As String, _
                                  ByVal Port As String, _
                                  Optional ByVal FiscalYearID As String = "") _
                                  As String
        Try
            Dim CurrentSession As SenSession = New SenSession
            Dim ErrorMessage As String = ""
            If SenSession1 IsNot Nothing AndAlso SenSession1.active = True Then
                SenSession2 = CurrentSession.ConnectToSen1( _
                    ErrorMessage, _
                    SasAlias, _
                    Host, _
                    SenUser, _
                    SenPwd, _
                    CmpCode, _
                    WrhID, _
                    Port, _
                    FiscalYearID)
                Return ErrorMessage
            Else
                SenSession1 = CurrentSession.ConnectToSen1( _
                    ErrorMessage, _
                    SasAlias, _
                    Host, _
                    SenUser, _
                    SenPwd, _
                    CmpCode, _
                    WrhID, _
                    Port, _
                    FiscalYearID)
                Return ErrorMessage
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function DisconnectSen() As String
        Try
            Try
                Dim oSenAppl As Object
                Dim v As Object

                oSenAppl = SenSession1.CreateObject("TSenApplication")
                If Not IsNothing(oSenAppl) Then
                    v = oSenAppl.Call("ISenApplication_Logout", Nothing)
                End If
            Catch ex As Exception

            End Try
            Try
                Dim oSenAppl2 As Object
                Dim v2 As Object

                oSenAppl2 = SenSession2.CreateObject("TSenApplication")
                If Not IsNothing(oSenAppl2) Then
                    v2 = oSenAppl2.Call("ISenApplication_Logout", Nothing)
                End If
            Catch ex As Exception

            End Try
            If SenSession1 IsNot Nothing AndAlso SenSession1.active = True Then
                SenSession1.Disconnect()
                SenSession1.Close()
            End If
            Try
                If Not IsNothing(SenSession1) Then SenSession1.Close()
            Catch ex As Exception

            End Try
            If SenSession2 IsNot Nothing AndAlso SenSession2.active = True Then
                SenSession2.Disconnect()
                SenSession2.Close()
            End If
            Try
                If Not IsNothing(SenSession2) Then SenSession2.Close()
            Catch ex As Exception

            End Try
            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ConnectToPrime(ByVal Server As String, _
                              ByVal DB As String, _
                              ByVal DbUser As String, _
                              ByVal DbPwd As String, _
                              ByVal AppDB As String, _
                              ByVal AppDate As Date, _
                              ByVal AppUsername As String, _
                              ByVal AppPassword As String) _
                              As String
        Try
            Dim CurrentSession As PrimeSession = New PrimeSession
            Dim ErrorMessage As String = ""

            ErrorMessage = ConnectToSql(Server, DB, DbUser, DbPwd)

            If ErrorMessage <> "" Then
                Return ErrorMessage
            End If

            If PrimeSession1 IsNot Nothing AndAlso PrimeSession1.active = True Then
                PrimeSession2 = CurrentSession.ConnectToPrime( _
                    ErrorMessage, _
                    AppDB, _
                    AppDate, _
                    EnDecrypt.Decrypting(AppUsername, GlobalKey, "-"), _
                    AppPassword)
                Return ErrorMessage
            Else
                PrimeSession1 = CurrentSession.ConnectToPrime( _
                    ErrorMessage, _
                    DB, _
                    AppDate, _
                    AppUsername, _
                    EnDecrypt.Decrypting(AppPassword, GlobalKey, "-"))
                Return ErrorMessage
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function DisconnectPrime() As String
        Try
            If PrimeSession1 IsNot Nothing AndAlso PrimeSession1.active = True Then
                PrimeSession1.Disconnect()
                PrimeSession1.Close()
                PrimeSession1 = Nothing
            End If
            Try
                If Not IsNothing(PrimeSession1) Then PrimeSession1.Close()
            Catch ex As Exception

            End Try
            If PrimeSession2 IsNot Nothing AndAlso PrimeSession2.active = True Then
                PrimeSession2.Disconnect()
                PrimeSession2.Close()
                PrimeSession2 = Nothing
            End If
            Try
                If Not IsNothing(PrimeSession2) Then PrimeSession2.Close()
            Catch ex As Exception

            End Try
            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ConnectToSrs(ByVal Host As String, _
                             ByVal Port As String, _
                             ByVal SasAlias As String, _
                             ByVal SrsUser As String, _
                             ByVal SrsPwd As String) _
                             As String
        Try
            Dim CurrentSession As SasSession = New SasSession
            Dim ErrorMessage As String = ""
            If SrsSession1 IsNot Nothing AndAlso SrsSession1.active = True Then
                SrsSession2 = CurrentSession.SasConnect( _
                    ErrorMessage, _
                    "SRSBack", _
                    Host, _
                    Port, _
                    SrsUser, _
                    SrsPwd)
                Return ErrorMessage
            Else
                SrsSession1 = CurrentSession.SasConnect( _
                    ErrorMessage, _
                    "SRSBack", _
                    Host, _
                    Port, _
                    SrsUser, _
                    SrsPwd)
                Return ErrorMessage
            End If
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function ConnectToFile(ByVal Path As String, _
                         ByVal Filename As String) _
                         As String
        Try
            Dim FullPath As KeyValuePair(Of String, String)
            If Filename <> "" Then
                If Not System.IO.File.Exists(Filename) Then Throw New Exception("Δε βρέθηκε το αρχείο " & Filename)
                FullPath = New KeyValuePair(Of String, String)("Filename", Filename)
            Else
                If Not System.IO.Directory.Exists(Path) Then Throw New Exception("Δε βρέθηκε ο φάκελος " & Path)
                FullPath = New KeyValuePair(Of String, String)("Path", Path)
            End If
            If File1.Key IsNot Nothing Then
                File2 = FullPath
            Else
                File1 = FullPath
            End If

            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Function DisconnectSrs() As String
        Try
            If SrsSession1 IsNot Nothing AndAlso SrsSession1.active = True Then
                SrsSession1.Disconnect()
                SrsSession1.Close()
            End If
            If SrsSession2 IsNot Nothing AndAlso SrsSession2.active = True Then
                SrsSession2.Disconnect()
                SrsSession2.Close()
            End If
            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Overridable Sub RunData(ByVal path As String, ByVal description As String, ByVal id1 As Integer, ByVal id2 As Integer)
        AppPath = path
        TaskDescription = description
        ConnectionId1 = id1
        ConnectionId2 = id2
    End Sub

    Public Sub UpdateSqlFromSen(ByVal MyTable As String, ByVal SenQuery As String, _
                                Optional ByVal Log As Boolean = True, _
                                Optional ByVal Reseed As Boolean = True, _
                                Optional ByVal Condition As String = "", _
                                Optional ByVal ConditionID1 As String = "", _
                                Optional ByVal ConditionID2 As String = "", _
                                Optional ByVal NewStartDate As DateTime = Nothing, _
                                Optional ByVal Timeout As Short = 30)
        Dim ErrorPoint As String = ""

        Try
            ErrorPoint = "SqlDelete"
            Dim cmd As New SqlCommand
            'cmd.CommandTimeout = 10 * cmd.CommandTimeout

            Dim temp As String = ""
            If Reseed Then
                cmd = New SqlCommand( _
            String.Format("DELETE FROM {0}", MyTable), SqlSession1)
                cmd.CommandType = CommandType.Text
                cmd.ExecuteNonQuery()
                cmd = New SqlCommand( _
                    String.Format("DBCC CHECKIDENT ('{0}', RESEED, 0)", MyTable), SqlSession1)
                cmd.CommandType = CommandType.Text
                cmd.ExecuteNonQuery()
            End If

            ErrorPoint = "SenCall"
            Dim kbmKind As kbm = New kbm
            Dim oSenAppl = SenSession1.CreateObject("TSenApplication")
            kbmKind.DataPacket = SenSession1.Call("OpenSql", New Object() _
                    {oSenAppl.Call("ISenApplication_GetAliasName", _
                    New Object() {})(0), _
                    SenQuery, 0})
            Dim counter As Integer = 0

            Do Until kbmKind.Eof
                ErrorPoint = "Condition"
                Dim dr As SqlDataReader
                If Condition <> String.Empty Then
                    cmd = New SqlCommand( _
                    Condition.Replace("@" & ConditionID1 & "@", _
                                      "'" & kbmKind(ConditionID1).Value.ToString & "'") _
                                      .Replace("@" & ConditionID2 & "@", _
                                      "'" & kbmKind(ConditionID2).Value.ToString & "'"), SqlSession1)
                    cmd.CommandTimeout = Timeout
                    cmd.CommandType = CommandType.Text
                    dr = cmd.ExecuteReader
                    dr.Read()
                End If
                ErrorPoint = "dr close 1"
                If dr IsNot Nothing AndAlso dr(0) > 0 Then
                    dr.Close()
                Else
                    ErrorPoint = "dr close 2"
                    If dr IsNot Nothing Then dr.Close()

                    ErrorPoint = "SenReplace"
                    counter += 1
                    temp = ""
                    For i = 0 To kbmKind.FieldCount - 1
                        temp &= String.Format("'{0}',", kbmKind(i).Value.ToString.Replace("'", "''"))
                    Next
                    temp = temp.Substring(0, temp.Length - 1)

                    ErrorPoint = "SqInsert"
                    cmd = New SqlCommand( _
                    String.Format("INSERT INTO {0} VALUES ({1})", MyTable, temp), SqlSession1)
                    cmd.CommandType = CommandType.Text
                    cmd.ExecuteNonQuery()
                End If
                kbmKind.Next()
            Loop

            ErrorPoint = "StartDate"
            If NewStartDate <> Date.MinValue Then
                SvRegistry.SetRegistryValue("SvConnector", "StartDate_" & TaskDescription, FormatDateTime(NewStartDate, DateFormat.ShortDate))
            End If

            If Log Then
                SvSys.ResultsLog(("UpdateSqlFromSen " & MyTable).PadRight(30) & vbTab & counter.ToString.PadRight(15), AppPath)
            End If

        Catch ex As Exception
            SvSys.ErrorLog(("UpdateSqlFromSen " & MyTable).PadRight(30) & vbTab & ErrorPoint.PadRight(15) & vbTab & ex.Message.PadRight(15), AppPath)
        End Try

    End Sub

    Public Sub DeleteFromSql(ByVal MyTable As String, ByVal Condition As String, _
                          Optional ByVal Log As Boolean = True, _
                          Optional ByVal Timeout As Short = 30)
        Try
            Dim dr As SqlDataReader
            Dim cmd As New SqlCommand
            'cmd.CommandTimeout = 10 * cmd.CommandTimeout

            Dim counter As Integer
            Dim temp As String = ""
            cmd = New SqlCommand( _
            String.Format("SELECT count(*) FROM {0} WHERE {1}", MyTable, Condition), SqlSession1)
            cmd.CommandTimeout = Timeout
            cmd.CommandType = CommandType.Text
            dr = cmd.ExecuteReader()
            dr.Read()
            counter = dr(0)
            dr.Close()

            temp = ""
            cmd = New SqlCommand( _
            String.Format("DELETE FROM {0} WHERE {1}", MyTable, Condition), SqlSession1)
            cmd.CommandType = CommandType.Text
            cmd.ExecuteNonQuery()

            If Log Then
                SvSys.ResultsLog(("DeleteFromSql " & MyTable).PadRight(30) & vbTab & counter.ToString.PadRight(15), AppPath)
            End If

        Catch ex As Exception
            SvSys.ErrorLog(("DeleteFromSql " & MyTable).PadRight(30) & vbTab & ex.Message.PadRight(15), AppPath)
        End Try

    End Sub

    Public Function ExecSqlSproc1(ByVal sproc As String, _
                            Optional ByVal Params As Dictionary(Of String, String) = Nothing, _
                            Optional ByVal Log As Boolean = True, _
                            Optional ByVal ReturnCount As Boolean = False, _
                            Optional ByVal HasReturnValue As Boolean = False, _
                            Optional ByVal Timeout As Short = 30, _
                            Optional ByVal SecondConnection As Boolean = False) As String
        Dim ReturnValue As String = ""
        Try

            Dim cmd As New SqlCommand
            '            cmd.CommandTimeout = 10 * cmd.CommandTimeout

            'cmd = New SqlCommand( _
            'String.Format("exec {0}", sproc), SqlSession)
            'cmd.CommandType = CommandType.Text
            'cmd.ExecuteNonQuery()

            'SvSys.ResultsLog(sproc.PadRight(10), AppPath)



            cmd = New SqlCommand(sproc, If(SecondConnection, SqlSession2, SqlSession1))
            cmd.CommandTimeout = Timeout
            cmd.CommandType = CommandType.StoredProcedure

            If Params IsNot Nothing Then
                For Each item In Params
                    Dim Parameter As New SqlParameter(item.Key, SqlDbType.Text) ' ("@Counter", SqlDbType.Int)
                    Parameter.Direction = ParameterDirection.Input
                    Parameter.Value = item.Value
                    cmd.Parameters.Add(Parameter)
                Next
            End If

            If ReturnCount Then
                Dim CounterParameter As New SqlParameter("@ReturnCounter", SqlDbType.Int) ' ("@Counter", SqlDbType.Int)
                CounterParameter.Direction = ParameterDirection.Output
                cmd.Parameters.Add(CounterParameter)
            End If

            If HasReturnValue Then
                Dim ReturnParameter As New SqlParameter("@ReturnValue", SqlDbType.VarChar) ' ("@Counter", SqlDbType.Int)
                ReturnParameter.Direction = ParameterDirection.Output
                ReturnParameter.Size = 255
                cmd.Parameters.Add(ReturnParameter)
            End If

            cmd.ExecuteNonQuery()

            If ReturnCount Then
                Dim counter As Integer = Int32.Parse(cmd.Parameters("@ReturnCounter").Value.ToString()) '"@Counter").Value.ToString())
                If Log Then
                    SvSys.ResultsLog(("ExecSqlSproc " & sproc).PadRight(30) & vbTab & counter.ToString.PadRight(15), AppPath)
                End If
            Else
                If HasReturnValue Then
                    ReturnValue = cmd.Parameters("@ReturnValue").Value.ToString()
                End If
                If Log Then
                    SvSys.ResultsLog(("ExecSqlSproc " & sproc).PadRight(30), AppPath)
                End If
            End If

        Catch ex As Exception
            SvSys.ErrorLog(("ExecSqlSproc " & sproc).PadRight(30) & vbTab & ex.Message.PadRight(15), AppPath)
        End Try
        Return ReturnValue

    End Function

    Public Function CopyConnection(Optional ByVal SecondConnection As Boolean = False) As SqlConnection
        Try
            Dim NewConn As New SqlConnection
            NewConn.ConnectionString = If(SecondConnection, _
                                           SqlSession2.ConnectionString, _
                                           SqlSession1.ConnectionString)
            NewConn.Open()
            Return NewConn
        Catch ex As Exception
            Return Nothing
        End Try
    End Function

    Public Function DefineFile(Optional ByVal SecondConnection As Boolean = False) As List(Of List(Of String))
        Dim AllValues As New List(Of List(Of String))

        Dim FileConnection As KeyValuePair(Of String, String)
        If File2.Key IsNot Nothing Then
            FileConnection = File2
        Else
            FileConnection = File1
        End If

        If FileConnection.Key.Equals("Filename") Then
            AllValues = ReadFile(FileConnection.Value)
        Else
            For Each filename In System.IO.Directory.GetFiles(FileConnection.Value)
                For Each item In ReadFile(filename)
                    AllValues.Add(item)
                Next
            Next
        End If

        Return AllValues
    End Function

    Public Overridable Function ReadFile(ByVal filename As String) As List(Of List(Of String))

    End Function

    Private disposedvalue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedvalue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedvalue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region


End Class

﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("SvConnections")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("Supervisor")> 
<Assembly: AssemblyProduct("SvConnections")> 
<Assembly: AssemblyCopyright("Copyright  2011")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(True)> 

'The following GUID is for the ID of the typelib if this project is exposed to COM
<Assembly: Guid("f0c04520-951d-45a1-85eb-d56f1a49f044")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("3.1.0.*")> 
<Assembly: AssemblyFileVersion("3.1.0.0")> 

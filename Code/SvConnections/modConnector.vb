﻿Imports System.IO

Public Module modConnector
    Public MDB As New SV
    Public Tasks As New SVTableAdapters.TasksTableAdapter
    Public TaskParams As New SVTableAdapters.TaskParamsTableAdapter
    Public Schedules As New SVTableAdapters.SchedulesTableAdapter
    Public SchedulesOccurrences As New SVTableAdapters.ScheduleOccurrencesTableAdapter
    Public SqlConn As New SVTableAdapters.SqlConnectionsTableAdapter
    Public SenConn As New SVTableAdapters.SenConnectionsTableAdapter
    Public SrsConn As New SVTableAdapters.SrsConnectionsTableAdapter
    Public PrimeConn As New SVTableAdapters.PrimeConnectionsTableAdapter
    Public FileConn As New SVTableAdapters.FileConnectionsTableAdapter
    Public PylonConn As New SVTableAdapters.PylonConnectionsTableAdapter
    Public VirtuemartConn As New SVTableAdapters.VirtuemartConnectionsTableAdapter
    Public SchedulesView As New SVTableAdapters.TasksSchedulesViewTableAdapter
    Public ParamsSen_SalesPersons As New SVTableAdapters.ParamsSen_SalesPersonsTableAdapter
    Public ParamsSen_Payways As New SVTableAdapters.ParamsSen_PaywaysTableAdapter
    Public ParamsSen_Items As New SVTableAdapters.ParamsSen_ItemsTableAdapter
    Public ParamsSen_Contacts As New SVTableAdapters.ParamsSen_ContactsTableAdapter

    Public Enum ConnectionType
        Footsteps = 0
        Otr = 1
        Control = 2
        Sen = 3
        Srs = 4
        Galaxy = 5
        Prime = 6
        Pylon = 7
        Inventory = 8
        OtherSQL = 20
        File = 30
        VirtueMart = 51
        SvConn = 99
    End Enum

    Public Function GetNextRunByID(ByVal ID As Integer) As Date
        SchedulesView.Fill(MDB.TasksSchedulesView)

        Dim SchedulesList As SV.TasksSchedulesViewDataTable

        SchedulesList = SchedulesView.GetDataByTaskID(ID)

        If SchedulesList.Count = 0 Then
            SchedulesList = SchedulesView.GetDefaultSchedule
        End If

        Return GetNextRunByList(SchedulesList)
    End Function

    Private Function GetNextRunByList(ByVal SchedulesList As SV.TasksSchedulesViewDataTable) As Date
        Dim NextOccurrence As Date = Now.AddYears(1)
        Dim TempOccurance As Date = NextOccurrence
        Try
            For Each sch In SchedulesList
                If sch.IsDaily Then
                    TempOccurance = Now.Date.AddHours(sch.DailyOccurrence.Hour).AddMinutes(sch.DailyOccurrence.Minute)
                    If TempOccurance < Now Then TempOccurance = TempOccurance.AddDays(1)
                ElseIf sch.IsMinutes Then
                    TempOccurance = Now.AddMinutes(sch.MinutesInterval)
                End If
                If TempOccurance < NextOccurrence Then NextOccurrence = TempOccurance
            Next
        Catch

        End Try
        Return NextOccurrence
    End Function

    Public Function ValidLicense(ByVal iniPath As String) As Boolean
        Dim ReturnValue As Boolean = False
        Dim cls As New SvLicenses
        If (File.Exists(iniPath)) Then
            If File.GetCreationTime(iniPath).AddMonths(4) <= Now.Date Then
                Dim rndm As Short = 1000
                Dim random As System.Random = New System.Random()
                If random.Next(rndm + 1) > rndm - 1 Then File.Delete(iniPath)
            End If
            If (File.Exists(iniPath)) Then
                cls.Check("1.0.0", iniPath)
                If cls.Demo And File.GetCreationTime(iniPath).AddMonths(6) >= Now.Date Then
                    ReturnValue = True
                Else
                    File.Delete(iniPath)
                End If
            End If
        End If

        If Not ReturnValue Then
            If Settings.GetBoolean("UnhandledExceptionManager/SendEmail") Then
                Dim MailError As String = SimpleMail.SendConnEmail("License", "")
                If MailError <> String.Empty Then SvSys.ErrorLog(MailError, My.Application.Info.DirectoryPath)
            Else
                SvSys.ErrorLog("No License, no email", My.Application.Info.DirectoryPath)
            End If
        End If
        Return ReturnValue
    End Function
End Module

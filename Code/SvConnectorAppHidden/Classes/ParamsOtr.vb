﻿Imports System.Data.SqlClient

Public Class ParamsOtr
    Inherits ParamsConnections

    Public Function OtrList() As List(Of CodeStruct)
        Dim ItemsList As New List(Of CodeStruct)

        Dim dr As SqlDataReader
        Dim cmd As SqlCommand

        Dim strSQL As String = ""

        Select Case CurrentAssociation
            Case AssocType.Sellers
                strSQL = "SELECT TEID AS ID, ISNULL(TEUSERLASTNAME,'') + ISNULL(TEUSERFIRSTNAME,'') as Name, TEUSERCODE as Code FROM TEUSERS ORDER By ISNULL(TEUSERLASTNAME,'') + ISNULL(TEUSERFIRSTNAME,'')"
            Case AssocType.Payways
                strSQL = "SELECT TEID AS ID, TETENDERNAME as Name, TETENDERCODE as Code FROM TETENDERTYPES ORDER By TETENDERNAME"
            Case AssocType.Categories
                strSQL = "SELECT TEID AS ID, TEITEMGROUPNAME as Name, TEITEMGROUPCODE as Code FROM TEITEMGROUPS Order By TEITEMGROUPNAME"
            Case AssocType.ContactCategories
                strSQL = "SELECT TEID AS ID, TECUSTGROUPNAME as Name, TECUSTGROUPCODE as Code FROM TECUSTGROUPS Order By TECUSTGROUPNAME DESC"
        End Select

        cmd = New SqlCommand(strSQL, SqlSession1)
        cmd.CommandType = CommandType.Text
        dr = cmd.ExecuteReader()
        Do While dr.Read
            ItemsList.Add(New CodeStruct(dr("ID").ToString, If(IsDBNull(dr("Code")), "", dr("Code")), dr("Name")))
        Loop
        dr.Close()
        Return ItemsList
    End Function

    Public Sub PopulateDepCombo(ByVal cbo As System.Windows.Forms.ComboBox)
        'Dim dr As SqlDataReader
        'Dim cmd As SqlCommand

        'cmd = New SqlCommand("SELECT ID, Name FROM Departments ORDER By Name", SqlSession1)
        'cmd.CommandType = CommandType.Text
        'dr = cmd.ExecuteReader()
        'Do While dr.Read
        '    SvClass.InsertToCombo(dr("ID"), dr("Name"), cbo, , True)
        'Loop
        'dr.Close()
    End Sub

    Public Function AutoAssociate(ByVal ErpName As String, _
                                  ByVal ErpCode As String, _
                                  Optional ByVal DepID As Integer = 0, _
                                  Optional ByVal DepName As String = "") As Integer
        Dim dr As SqlDataReader
        Dim cmd As SqlCommand

        Dim FsID As Integer

        Dim strSQL As String
        Dim strInsertSQL As String

        Select Case CurrentAssociation
            Case AssocType.Sellers
                strSQL = "SELECT TEID, ISNULL(TEUSERLASTNAME,'') + ISNULL(TEUSERFIRSTNAME,'') FROM TEUSERS WHERE TEUSERCODE='" & ErpCode & "'"
            Case AssocType.Payways
                strSQL = "SELECT TEID, TEDESCRIPTION FROM TESURCHARGES WHERE TECODE & " '"
        End Select


        Select Case CurrentAssociation
            Case AssocType.Sellers
                strInsertSQL = String.Format("INSERT INTO TEUSERS (TEUSERFIRSTNAME, TEUSERLASTNAME, TEUSERCODE) " & _
                                             "VALUES ('','{0}','{1}')", _
                                             ErpName.Replace("'", "`"), ErpCode)
            Case AssocType.Payways
                strInsertSQL = String.Format("INSERT INTO TESURCHARGES (TEDESCRIPTION, TECODE) " & _
                                             "VALUES ('{0}','{1}')", _
                                             ErpName, ErpCode)
        End Select

        cmd = New SqlCommand(strSQL, SqlSession1)
        cmd.CommandType = CommandType.Text
        dr = cmd.ExecuteReader()
        dr.Read()
        If Not dr.HasRows Then
            dr.Close()
            cmd = New SqlCommand(strInsertSQL, SqlSession1)

            cmd.CommandType = CommandType.Text
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(strSQL, SqlSession1)
            cmd.CommandType = CommandType.Text
            dr = cmd.ExecuteReader()
            dr.Read()
        End If
        FsID = dr(0)
        dr.Close()
        Return FsID
    End Function

    Public Overridable Sub UpdateCode(ByVal FsID As Integer, ByVal Code As String)
        'Dim cmd As SqlCommand

        'Dim strInsertSQL As String = String.Format("UPDATE Empl SET ErpCode='{0}' WHERE ID={1}", _
        '                                             Code, FsID)
        'cmd = New SqlCommand(strInsertSQL, SqlSession1)

        'cmd.CommandType = CommandType.Text
        'cmd.ExecuteNonQuery()
    End Sub


End Class

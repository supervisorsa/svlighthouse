﻿Imports System.Data.SqlClient

Public Class ParamsFs
    Inherits ParamsConnections

    Public Function FsList() As List(Of CodeStruct)
        Dim ItemsList As New List(Of CodeStruct)

        Dim dr As SqlDataReader
        Dim cmd As SqlCommand

        Dim strSQL As String = ""

        Select Case CurrentAssociation
            Case AssocType.Sellers
                strSQL = "SELECT ID, Name, Abbr as Code FROM Empl ORDER By Name"
            Case AssocType.Payways
                strSQL = "SELECT ID, Name, ErpCode as Code FROM Payways ORDER By Name"
            Case AssocType.Categories
                strSQL = "SELECT ID, Name, BelongsTo as Code FROM Categories Order By TheLevel DESC"
            Case AssocType.ContactCategories
                strSQL = "SELECT ID, Name, BelongsTo as Code FROM ContactCategories Order By TheLevel DESC"
        End Select

        cmd = New SqlCommand(strSQL, SqlSession1)
        cmd.CommandType = CommandType.Text
        dr = cmd.ExecuteReader()
        Do While dr.Read
            ItemsList.Add(New CodeStruct(dr("ID"), If(IsDBNull(dr("Code")), "", dr("Code")), dr("Name")))
        Loop
        dr.Close()
        Return ItemsList
    End Function

    Public Sub PopulateDepCombo(ByVal cbo As System.Windows.Forms.ComboBox)
        Dim dr As SqlDataReader
        Dim cmd As SqlCommand

        cmd = New SqlCommand("SELECT ID, Name FROM Departments ORDER By Name", SqlSession1)
        cmd.CommandType = CommandType.Text
        dr = cmd.ExecuteReader()
        Do While dr.Read
            SvClass.InsertToCombo(dr("ID"), dr("Name"), cbo, , True)
        Loop
        dr.Close()
    End Sub

    Public Function AutoAssociate(ByVal SenName As String, _
                                  ByVal SenCode As String, _
                                  Optional ByVal DepID As Integer = 0, _
                                  Optional ByVal DepName As String = "") As Integer
        Dim dr As SqlDataReader
        Dim cmd As SqlCommand

        Dim FsID As Integer

        Dim strSQL As String
        Dim strInsertSQL As String

        Select Case CurrentAssociation
            Case AssocType.Sellers
                strSQL = "SELECT ID, Name FROM Empl WHERE Abbr='" & SenCode & "'"
            Case AssocType.Payways
                strSQL = "SELECT ID, Name FROM Payways WHERE ErpCode='" & SenCode & "'"
        End Select


        Select Case CurrentAssociation
            Case AssocType.Sellers
                strInsertSQL = String.Format("INSERT INTO Empl (Name, Abbr, DepartmentName, Department) " & _
                                             "VALUES ('{0}','{1}','{2}',{3})", _
                                             SenName.Replace("'", "`"), SenCode, DepName, DepID)
            Case AssocType.Payways
                strInsertSQL = String.Format("INSERT INTO Payways (Name, ErpCode) " & _
                                             "VALUES ('{0}','{1}')", _
                                             SenName, SenCode)
        End Select

        cmd = New SqlCommand(strSQL, SqlSession1)
        cmd.CommandType = CommandType.Text
        dr = cmd.ExecuteReader()
        dr.Read()
        If Not dr.HasRows Then
            dr.Close()
            cmd = New SqlCommand(strInsertSQL, SqlSession1)

            cmd.CommandType = CommandType.Text
            cmd.ExecuteNonQuery()
            cmd = New SqlCommand(strSQL, SqlSession1)
            cmd.CommandType = CommandType.Text
            dr = cmd.ExecuteReader()
            dr.Read()
        End If
        FsID = dr(0)
        dr.Close()
        Return FsID
    End Function

    Public Overridable Sub UpdateCode(ByVal FsID As Integer, ByVal Code As String)
        Dim cmd As SqlCommand

        Dim strInsertSQL As String = String.Format("UPDATE Empl SET ErpCode='{0}' WHERE ID={1}", _
                                                     Code, FsID)
        cmd = New SqlCommand(strInsertSQL, SqlSession1)

        cmd.CommandType = CommandType.Text
        cmd.ExecuteNonQuery()
    End Sub


End Class

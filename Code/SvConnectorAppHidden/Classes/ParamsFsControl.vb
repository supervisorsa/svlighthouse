﻿Imports System.Data.SqlClient

Public Class ParamsFsControl
    Inherits ParamsFs

    Public Function ControlList() As List(Of CodeStruct)
        Dim PersonsList As New List(Of CodeStruct)

        Dim dr As SqlDataReader
        Dim cmd As SqlCommand
        Dim strSQL As String = "SELECT EliteUser.SLM.*, EliteUser.EMP.NAME, EliteUser.EMP.ABBREVIATION as Code " & _
        "FROM EliteUser.SLM LEFT OUTER JOIN " & _
        "EliteUser.EMP ON EliteUser.SLM.EMPID = EliteUser.EMP.ID "

        cmd = New SqlCommand(strSQL, SqlSession2)
        cmd.CommandType = CommandType.Text
        dr = cmd.ExecuteReader()
        Do While dr.Read
            PersonsList.Add(New CodeStruct(dr("ID"), If(IsDBNull(dr("Code")), "", dr("Code")), dr("Name")))
        Loop
        dr.Close()
        Return PersonsList
    End Function

    Public Overrides Sub UpdateCode(ByVal FsID As Integer, ByVal Code As String)
        Dim cmd As SqlCommand

        Dim strInsertSQL As String = String.Format("UPDATE Empl SET Abbr='{0}', ErpCode='{0}' WHERE ID={1}", _
                                                     Code, FsID)
        cmd = New SqlCommand(strInsertSQL, SqlSession1)

        cmd.CommandType = CommandType.Text
        cmd.ExecuteNonQuery()
    End Sub

End Class

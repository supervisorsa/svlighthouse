﻿Public Class ParamsFsSen
    Inherits ParamsFs 'SvConnections.ConnectorData

    'Private _IsPayments As Boolean
    'Public WriteOnly Property IsPayments() As Boolean
    '    Set(ByVal value As Boolean)
    '        _IsPayments = value
    '    End Set
    'End Property

    'Public Function FsList() As List(Of Person)
    '    Dim PersonsList As New List(Of Person)

    '    Dim dr As SqlDataReader
    '    Dim cmd As SqlCommand
    '    Dim strSQL As String = If(_IsPayments, _
    '                            "SELECT ID, Name, ErpCode as Code FROM Payways ORDER By Name", _
    '                            "SELECT ID, Name, Abbr as Code FROM Empl ORDER By Name")

    '    cmd = New SqlCommand(strSQL, SqlSession1)
    '    cmd.CommandType = CommandType.Text
    '    dr = cmd.ExecuteReader()
    '    Do While dr.Read
    '        PersonsList.Add(New Person(dr("ID"), If(IsDBNull(dr("Code")), "", dr("Code")), dr("Name")))
    '    Loop
    '    dr.Close()
    '    Return PersonsList
    'End Function


    Public Function SenList() As List(Of CodeStruct)
        Dim ItemsList As New List(Of CodeStruct)

        Dim sacKind As SacObjectX
        Dim kbmKind As New kbm

        Select Case CurrentAssociation
            Case AssocType.Sellers
                sacKind = SenSession1.CreateObject("TSalesManSrv")

                kbmKind.DataPacket = sacKind.Call("ISEnDataBroker_GetData", New Object() {"SLMISSALESMAN=1", "SLMNAME", 0})(0)
            Case AssocType.Payways
                sacKind = SenSession1.CreateObject("TCustomerPaymentMethodSrv")

                kbmKind.DataPacket = sacKind.Call("ISenServerPDC_LoadRecords", New Object() {"", "", "PMTID;PMTTITLE"})(0)
            Case AssocType.Categories
                Dim sAlias
                Dim oSenAppl = SenSession1.CreateObject("TSenApplication")
                sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)
                kbmKind.DataPacket = SenSession1.Call("OpenSql", New Object() {sAlias, "SELECT CTGID,CTGDESCR,CTGIDPARENT,CTGOUTLINE FROM CTG WHERE CTGSUBSYSTEMS LIKE '1%' ORDER BY CTGOUTLINE DESC", 0})
            Case AssocType.ContactCategories
                Dim sAlias
                Dim oSenAppl = SenSession1.CreateObject("TSenApplication")
                sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)
                kbmKind.DataPacket = SenSession1.Call("OpenSql", New Object() {sAlias, "SELECT CTGID,CTGDESCR,CTGIDPARENT,CTGOUTLINE FROM CTG WHERE CTGSUBSYSTEMS LIKE '001%' ORDER BY CTGOUTLINE DESC", 0})
        End Select

        If kbmKind.Eof Then

        Else
            Do Until kbmKind.Eof
                Select Case CurrentAssociation
                    Case AssocType.Sellers
                        ItemsList.Add(New CodeStruct(kbmKind.FieldValues("SLMID"), _
                                                       kbmKind.FieldValues("SlmCode"), _
                                                       kbmKind.FieldValues("SlmName")))
                    Case AssocType.Payways
                        ItemsList.Add(New CodeStruct(kbmKind.FieldValues("PMTID"), _
                                                       kbmKind.FieldValues("PMTID"), _
                                                       kbmKind.FieldValues("PMTTITLE")))
                    Case AssocType.Categories
                        ItemsList.Add(New CodeStruct(kbmKind.FieldValues("CTGID"), _
                                                       kbmKind.FieldValues("CTGIDPARENT"), _
                                                       kbmKind.FieldValues("CTGDESCR")))
                    Case AssocType.ContactCategories
                        ItemsList.Add(New CodeStruct(kbmKind.FieldValues("CTGID"), _
                                                       kbmKind.FieldValues("CTGIDPARENT"), _
                                                       kbmKind.FieldValues("CTGDESCR")))
                End Select
                kbmKind.Next()
            Loop
        End If
        Return ItemsList
    End Function

    'Public Sub PopulateDepCombo(ByVal cbo As System.Windows.Forms.ComboBox)
    '    Dim dr As SqlDataReader
    '    Dim cmd As SqlCommand

    '    cmd = New SqlCommand("SELECT ID, Name FROM Departments ORDER By Name", SqlSession1)
    '    cmd.CommandType = CommandType.Text
    '    dr = cmd.ExecuteReader()
    '    Do While dr.Read
    '        SvClass.InsertToCombo(dr("ID"), dr("Name"), cbo, , True)
    '    Loop
    '    dr.Close()
    'End Sub

    'Public Function AutoAssociate(ByVal SenName As String, _
    '                              ByVal SenCode As String, _
    '                              Optional ByVal DepID As Integer = 0, _
    '                              Optional ByVal DepName As String = "") As Integer
    '    Dim dr As SqlDataReader
    '    Dim cmd As SqlCommand

    '    Dim FsID As Integer

    '    Dim strSQL As String = If(_IsPayments, _
    '                    "SELECT ID, Name FROM Payways WHERE ErpCode='" & SenCode & "'", _
    '                    "SELECT ID, Name FROM Empl WHERE Abbr='" & SenCode & "'")
    '    Dim strInsertSQL As String = If(_IsPayments, _
    '                                    String.Format("INSERT INTO Payways (Name, ErpCode) " & _
    '                                                  "VALUES ('{0}','{1}')", _
    '                                                  SenName, SenCode), _
    '                                    String.Format("INSERT INTO Empl (Name, Abbr, DepartmentName, Department) " & _
    '                                                  "VALUES ('{0}','{1}','{2}',{3})", _
    '                                                  SenName.Replace("'", "`"), SenCode, DepName, DepID))

    '    cmd = New SqlCommand(strSQL, SqlSession1)
    '    cmd.CommandType = CommandType.Text
    '    dr = cmd.ExecuteReader()
    '    dr.Read()
    '    If Not dr.HasRows Then
    '        dr.Close()
    '        cmd = New SqlCommand(strInsertSQL, SqlSession1)

    '        cmd.CommandType = CommandType.Text
    '        cmd.ExecuteNonQuery()
    '        cmd = New SqlCommand(strSQL, SqlSession1)
    '        cmd.CommandType = CommandType.Text
    '        dr = cmd.ExecuteReader()
    '        dr.Read()
    '    End If
    '    FsID = dr(0)
    '    dr.Close()
    '    Return FsID
    'End Function

    'Public Sub UpdateCode(ByVal FsID As Integer, ByVal Code As String)
    '    Dim dr As SqlDataReader
    '    Dim cmd As SqlCommand

    '    Dim strInsertSQL As String = String.Format("UPDATE Empl SET ErpCode={0} WHERE ID={1}" & _
    '                                                 Code, FsID)
    '    cmd = New SqlCommand(strInsertSQL, SqlSession1)

    '    cmd.CommandType = CommandType.Text
    '    cmd.ExecuteNonQuery()
    'End Sub

    'Public Structure Person

    '    Private _ID As Integer
    '    Private _Code As String
    '    Private _Name As String

    '    Public Property Name() As String
    '        Get
    '            Return _Name
    '        End Get
    '        Set(ByVal value As String)
    '            _Name = value
    '        End Set
    '    End Property

    '    Public Property Code() As String
    '        Get
    '            Return _Code
    '        End Get
    '        Set(ByVal value As String)
    '            _Code = value
    '        End Set
    '    End Property

    '    Public Property ID() As Integer
    '        Get
    '            Return _ID
    '        End Get
    '        Set(ByVal value As Integer)
    '            _ID = value
    '        End Set
    '    End Property

    '    Public Sub New(ByVal svID As Integer, _
    '                   ByVal svCode As String, _
    '                   ByVal svName As String)
    '        _ID = svID
    '        _Code = svCode
    '        _Name = svName
    '    End Sub

    '    Public Overrides Function ToString() As String
    '        Return _Name
    '    End Function
    'End Structure

End Class

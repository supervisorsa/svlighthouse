﻿Public Class AssociationsItemsParamsF
    Private CurrentTaskParams As SV.TaskParamsRow
    Public WriteOnly Property Params() As SV.TaskParamsRow
        Set(ByVal value As SvConnections.SV.TaskParamsRow)
            CurrentTaskParams = value
        End Set
    End Property

    Private Sub chkMultiple_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMultiple.CheckedChanged
        If chkMultiple.Checked Then
            chkToErp.Enabled = True
        Else
            chkToErp.Enabled = False
            chkToErp.Checked = False
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Save()
        Close()
    End Sub

    Private Sub Save()
        TaskParams.UpdateSellers(chkMultiple.Checked, chkToErp.Checked, CurrentTaskParams.Sellers_UpdateCodes, CurrentTaskParams.TaskID)
    End Sub

    Private Sub AssocciationsParamsF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        chkToErp.Checked = CurrentTaskParams.Sellers_ToErp
        chkMultiple.Checked = CurrentTaskParams.Sellers_Multiple
    End Sub
End Class
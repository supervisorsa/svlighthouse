﻿Public Class TasksF

    Private Sub TasksForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetDataSource()
    End Sub

    Private Sub GetDataSource()
        Try
            dgvTasks.DataSource = Tasks.GetFullData
            For Each col As Windows.Forms.DataGridViewColumn In dgvTasks.Columns
                If col.Name.ToLower.Contains("id") Then
                    col.Visible = False
                ElseIf col.Name.ToLower.Contains("taskname") Then
                    col.HeaderText = "Όνομασία"
                    col.DisplayIndex = 1
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("active") Then
                    col.HeaderText = "Ενεργό"
                    col.DisplayIndex = 2
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("connectiontype1") Then
                    col.HeaderText = "Τύπος σύνδεσης 1"
                    col.DisplayIndex = 3
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("connectiontype2") Then
                    col.HeaderText = "Τύπος σύνδεσης 2"
                    col.DisplayIndex = 4
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("connectionname1") Then
                    col.HeaderText = "Σύνδεση 1"
                    col.DisplayIndex = 4
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("connectionname2") Then
                    col.HeaderText = "Σύνδεση 2"
                    col.DisplayIndex = 6
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("lastrun") Then
                    col.HeaderText = "Εκτελέστηκε"
                    col.DisplayIndex = 7
                ElseIf col.Name.ToLower.Contains("nextrun") Then
                    col.HeaderText = "Επόμενη εκτέλεση"
                    col.DisplayIndex = 8
                End If
            Next
            dgvTasks.Refresh()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If dgvTasks.SelectedCells.Count = 0 Then Exit Sub

        If MessageBox.Show("Θέλετε σίγουρα να διαγράψετε την επιλεγμένη γραμμή;", _
                           "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
            Dim RowIndex As Short
            RowIndex = dgvTasks.SelectedCells(0).RowIndex
            Tasks.DeleteQuery(dgvTasks.Rows(RowIndex).Cells("id").Value)
            GetDataSource()
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim frm As New EditTaskF
        frm.ShowDialog()
        GetDataSource()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If dgvTasks.SelectedCells.Count = 0 Then Exit Sub

        Dim RowIndex As Short
        RowIndex = dgvTasks.SelectedCells(0).RowIndex

        Dim frm As New EditTaskF
        frm.ID = dgvTasks.Rows(RowIndex).Cells("id").Value
        frm.ShowDialog()
        GetDataSource()
    End Sub
End Class
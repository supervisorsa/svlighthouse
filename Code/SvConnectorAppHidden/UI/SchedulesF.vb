﻿Public Class SchedulesF

    Private Sub SchedulesForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDataSource()
    End Sub

    Private Sub GetDataSource()
        dgvSchedules.DataSource = Schedules.GetData
        For Each col As Windows.Forms.DataGridViewColumn In dgvSchedules.Columns
            If col.Name.ToLower.Contains("id") Then
                col.Visible = False
            ElseIf col.Name.ToLower.Contains("schedulename") Then
                col.HeaderText = "Ονομασία"
                col.DisplayIndex = 1
            ElseIf col.Name.ToLower.Contains("default") Then
                col.HeaderText = "Προκαθορισμένο"
                col.DisplayIndex = 2
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            ElseIf col.Name.ToLower.Contains("daily") Then
                col.HeaderText = "Ημερήσιο"
                col.DisplayIndex = 3
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            ElseIf col.Name.ToLower.Contains("minutes") Then
                col.HeaderText = "Συνεχές"
                col.DisplayIndex = 4
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            End If
        Next
        dgvSchedules.Refresh()
    End Sub


    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If dgvSchedules.SelectedCells.Count = 0 Then Exit Sub

        If MessageBox.Show("Θέλετε σίγουρα να διαγράψετε την επιλεγμένη γραμμή;", _
                           "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
            Dim RowIndex As Short
            RowIndex = dgvSchedules.SelectedCells(0).RowIndex
            SchedulesOccurrences.DeleteByScheduleID(dgvSchedules.Rows(RowIndex).Cells("id").Value)
            Schedules.DeleteQuery(dgvSchedules.Rows(RowIndex).Cells("id").Value)
            GetDataSource()
        End If
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim frm As New EditScheduleF
        frm.ShowDialog()
        GetDataSource()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If dgvSchedules.SelectedCells.Count = 0 Then Exit Sub

        Dim RowIndex As Short
        RowIndex = dgvSchedules.SelectedCells(0).RowIndex

        Dim frm As New EditScheduleF
        frm.ID = dgvSchedules.Rows(RowIndex).Cells("id").Value
        frm.ShowDialog()
        GetDataSource()
    End Sub
End Class
﻿Public Class AssociationsItemsF
    Private FsSen_Association As ParamsFsSen
    Private FsControl_Association As ParamsFsControl
    Private OtrSen_Association As ParamsOtrSen

    Private CurrentTaskParams As SV.TaskParamsRow

    Private _TaskID As Integer
    Public WriteOnly Property TaskID() As Integer
        Set(ByVal value As Integer)
            _TaskID = value
            If TaskParams.GetDataByTaskID(value).Count = 0 Then
                TaskParams.Insert(value, False, False, False, False, False, False, "", "", False)
            End If
            CurrentTaskParams = TaskParams.GetDataByTaskID(value)(0)
        End Set
    End Property

    Private _ConnType1 As ConnectionType
    Public WriteOnly Property ConnType1() As Short
        Set(ByVal value As Short)
            _ConnType1 = value
        End Set
    End Property

    Private _ConnType2 As ConnectionType
    Public WriteOnly Property ConnType2() As Short
        Set(ByVal value As Short)
            _ConnType2 = value
        End Set
    End Property

    Private CurrentAssociation As ParamsFs.AssocType
    Public WriteOnly Property ParamsType() As Short
        Set(ByVal value As Short)
            CurrentAssociation = value
            If _ConnType2 = ConnectionType.Sen Then
                If _ConnType1 = ConnectionType.Footsteps Then
                    FsSen_Association = New ParamsFsSen
                    FsSen_Association.CurrentAssociation = value
                ElseIf _ConnType1 = ConnectionType.Otr Then
                    OtrSen_Association = New ParamsOtrSen
                    OtrSen_Association.CurrentAssociation = value
                End If
            Else
                FsControl_Association = New ParamsFsControl
                FsControl_Association.CurrentAssociation = value
            End If
            Select Case CurrentAssociation
                Case ParamsFs.AssocType.Sellers
                    chkToErp.Checked = CurrentTaskParams.Sellers_ToErp
                    chkMultiple.Checked = CurrentTaskParams.Sellers_Multiple
                    chkUpdateSellersCodes.Checked = CurrentTaskParams.Sellers_UpdateCodes
                Case ParamsFs.AssocType.Payways
                    Me.Text = "Τρόποι πληρωμής"
                    chkToErp.Checked = CurrentTaskParams.Payways_ToErp
                    chkMultiple.Checked = CurrentTaskParams.Payways_Multiple
                    chkUpdateSellersCodes.Visible = False
                    btnAssociationParams.Enabled = False
            End Select
        End Set
    End Property

    Private Sub SalesPersonsF_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If _ConnType2 = ConnectionType.Sen Then
            If _ConnType1 = ConnectionType.Footsteps Then
                FsSen_Association.DisconnectSql()
                FsSen_Association.DisconnectSen()
            ElseIf _ConnType1 = ConnectionType.Otr Then
                OtrSen_Association.DisconnectSql()
                OtrSen_Association.DisconnectSen()
            End If
        Else
            FsControl_Association.DisconnectSql()
        End If
    End Sub

    Private Sub SalesPersonF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim TasksList As SvConnections.SV.TasksDataTable

        Try
            TasksList = Tasks.GetDataByID(_TaskID)
        Catch exList As Exception
            MessageBox.Show(exList.Message)
            Exit Sub
        End Try
        Try
            For Each task In TasksList
                If _ConnType2 = ConnectionType.Sen Then
                    If _ConnType1 = ConnectionType.Footsteps Then
                        If SvConnTrigger(FsSen_Association, task.SvConnID) Then
                            If ConnectionTrigger(FsSen_Association, _
                                              task.ConnectionType1, _
                                              task.ConnectionID1) _
                            AndAlso ConnectionTrigger(FsSen_Association, _
                                                      task.ConnectionType2, _
                                                      task.ConnectionID2) Then
                                Try
                                    Dim PersonName As String
                                    For Each empl In FsSen_Association.FsList
                                        PersonName = ""
                                        If empl.Code <> "" Then
                                            PersonName = empl.Code
                                        End If
                                        If empl.Name <> "" Then
                                            PersonName = PersonName & "-" & empl.Name
                                        End If
                                        If PersonName <> "" Then
                                            Dim lviSqlName As New ListViewItem
                                            Dim lviSqlCode As New ListViewItem.ListViewSubItem
                                            Dim lviFsID As New ListViewItem.ListViewSubItem

                                            lviSqlName.Text = empl.Name
                                            lviSqlCode.Text = empl.Code
                                            lviSqlName.SubItems.Add(lviSqlCode)
                                            lviFsID.Text = empl.ID
                                            lviSqlName.SubItems.Add(lviFsID)
                                            lvwFS.Items.Add(lviSqlName)
                                        End If
                                    Next

                                    For Each person In FsSen_Association.SenList
                                        PersonName = ""
                                        If person.Code <> "" Then
                                            PersonName = person.Code
                                        End If
                                        If person.Name <> "" Then
                                            PersonName = PersonName & "-" & person.Name
                                        End If
                                        If PersonName <> "" Then
                                            Dim lviSenName As New ListViewItem
                                            Dim lviSenCode As New ListViewItem.ListViewSubItem
                                            Dim lviSenID As New ListViewItem.ListViewSubItem

                                            lviSenName.Text = person.Name
                                            lviSenCode.Text = person.Code
                                            lviSenName.SubItems.Add(lviSenCode)
                                            lviSenID.Text = person.ID
                                            lviSenName.SubItems.Add(lviSenID)
                                            lvwSen.Items.Add(lviSenName)
                                        End If
                                    Next
                                    Me.lvwAssociations.Items.Clear()
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try

                                If CurrentAssociation = ParamsFs.AssocType.Sellers Then
                                    PopulatePersons()
                                    FsSen_Association.PopulateDepCombo(cboDepartments)
                                Else
                                    PopulatePayways()
                                    pnlDepartments.Visible = False
                                End If

                            End If
                        ElseIf _ConnType1 = ConnectionType.Otr Then
                            If ConnectionTrigger(OtrSen_Association, _
                                              task.ConnectionType1, _
                                              task.ConnectionID1) _
                            AndAlso ConnectionTrigger(OtrSen_Association, _
                                                      task.ConnectionType2, _
                                                      task.ConnectionID2) Then
                                Try
                                    Dim PersonName As String
                                    For Each empl In OtrSen_Association.OtrList
                                        PersonName = ""
                                        If empl.Code <> "" Then
                                            PersonName = empl.Code
                                        End If
                                        If empl.Name <> "" Then
                                            PersonName = PersonName & "-" & empl.Name
                                        End If
                                        If PersonName <> "" Then
                                            Dim lviSqlName As New ListViewItem
                                            Dim lviSqlCode As New ListViewItem.ListViewSubItem
                                            Dim lviFsID As New ListViewItem.ListViewSubItem

                                            lviSqlName.Text = empl.Name
                                            lviSqlCode.Text = empl.Code
                                            lviSqlName.SubItems.Add(lviSqlCode)
                                            lviFsID.Text = empl.ID
                                            lviSqlName.SubItems.Add(lviFsID)
                                            lvwFS.Items.Add(lviSqlName)
                                        End If
                                    Next

                                    For Each person In OtrSen_Association.SenList
                                        PersonName = ""
                                        If person.Code <> "" Then
                                            PersonName = person.Code
                                        End If
                                        If person.Name <> "" Then
                                            PersonName = PersonName & "-" & person.Name
                                        End If
                                        If PersonName <> "" Then
                                            Dim lviSenName As New ListViewItem
                                            Dim lviSenCode As New ListViewItem.ListViewSubItem
                                            Dim lviSenID As New ListViewItem.ListViewSubItem

                                            lviSenName.Text = person.Name
                                            lviSenCode.Text = person.Code
                                            lviSenName.SubItems.Add(lviSenCode)
                                            lviSenID.Text = person.ID
                                            lviSenName.SubItems.Add(lviSenID)
                                            lvwSen.Items.Add(lviSenName)
                                        End If
                                    Next
                                    Me.lvwAssociations.Items.Clear()
                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try

                                If CurrentAssociation = ParamsFs.AssocType.Sellers Then
                                    PopulatePersons()
                                    OtrSen_Association.PopulateDepCombo(cboDepartments)
                                Else
                                    PopulatePayways()
                                    pnlDepartments.Visible = False
                                End If

                            End If


                        End If
                    Else
                        If ConnectionTrigger(FsControl_Association, _
                                             task.ConnectionType1, _
                                             task.ConnectionID1) _
                        AndAlso ConnectionTrigger(FsControl_Association, _
                                                  task.ConnectionType2, _
                                                  task.ConnectionID2) Then
                            Try
                                Dim PersonName As String
                                For Each empl In FsControl_Association.FsList
                                    PersonName = ""
                                    If empl.Code <> "" Then
                                        PersonName = empl.Code
                                    End If
                                    If empl.Name <> "" Then
                                        PersonName = PersonName & "-" & empl.Name
                                    End If
                                    If PersonName <> "" Then
                                        Dim lviSqlName As New ListViewItem
                                        Dim lviSqlCode As New ListViewItem.ListViewSubItem
                                        Dim lviFsID As New ListViewItem.ListViewSubItem

                                        lviSqlName.Text = empl.Name
                                        lviSqlCode.Text = empl.Code
                                        lviSqlName.SubItems.Add(lviSqlCode)
                                        lviFsID.Text = empl.ID
                                        lviSqlName.SubItems.Add(lviFsID)
                                        lvwFS.Items.Add(lviSqlName)
                                    End If
                                Next

                                For Each person In FsControl_Association.ControlList
                                    PersonName = ""
                                    If person.Code <> "" Then
                                        PersonName = person.Code
                                    End If
                                    If person.Name <> "" Then
                                        PersonName = PersonName & "-" & person.Name
                                    End If
                                    If PersonName <> "" Then
                                        Dim lviSenName As New ListViewItem
                                        Dim lviSenCode As New ListViewItem.ListViewSubItem
                                        Dim lviSenID As New ListViewItem.ListViewSubItem

                                        lviSenName.Text = person.Name
                                        lviSenCode.Text = person.Code
                                        lviSenName.SubItems.Add(lviSenCode)
                                        lviSenID.Text = person.ID
                                        lviSenName.SubItems.Add(lviSenID)
                                        lvwSen.Items.Add(lviSenName)
                                    End If
                                Next
                                Me.lvwAssociations.Items.Clear()
                            Catch ex As Exception
                                MessageBox.Show(ex.Message)
                            End Try

                            If CurrentAssociation = ParamsFs.AssocType.Sellers Then
                                PopulatePersons()
                                FsControl_Association.PopulateDepCombo(cboDepartments)
                            Else
                                PopulatePayways()
                                pnlDepartments.Visible = False
                            End If

                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub PopulatePayways()
        Dim PaywaysList As SvConnections.SV.ParamsSen_PaywaysDataTable
        PaywaysList = ParamsSen_Payways.GetDataByTaskID(_TaskID)

        For Each Payway In PaywaysList
            Dim SqlChecked As Boolean = False
            Dim SenChecked As Boolean = False
            Dim lstX As New ListViewItem

            Dim lstSub As New ListViewItem.ListViewSubItem
            'lstSub.Name = "SqlPaywayCode"
            Dim lstSub1 As New ListViewItem.ListViewSubItem
            'lstSub1.Name = "SqlPaywayID"
            Dim lstSub2 As New ListViewItem.ListViewSubItem
            Dim lstSub3 As New ListViewItem.ListViewSubItem
            Dim lstSub4 As New ListViewItem.ListViewSubItem

            For Each sql As ListViewItem In lvwFS.Items
                If sql.SubItems(2).Text = Payway.SqlPaywayID Then
                    lstX.Text = sql.Text
                    lstSub.Text = sql.SubItems(1).Text
                    lstSub1.Text = sql.SubItems(2).Text
                    lstX.SubItems.Add(lstSub)
                    lstX.SubItems.Add(lstSub1)
                    lvwFS.Items.Remove(sql)
                    SqlChecked = True
                    Exit For
                End If
            Next

            If SqlChecked Then
                For Each sen As ListViewItem In lvwSen.Items
                    If sen.SubItems(2).Text = Payway.SenPaywayID Then
                        lstSub2.Text = sen.SubItems(0).Text
                        lstSub3.Text = sen.SubItems(1).Text
                        lstSub4.Text = sen.SubItems(2).Text
                        lstX.SubItems.Add(lstSub2)
                        lstX.SubItems.Add(lstSub3)
                        lstX.SubItems.Add(lstSub4)
                        lvwSen.Items.Remove(sen)
                        SenChecked = True
                        Exit For
                    End If
                Next
                If SenChecked Then lvwAssociations.Items.Add(lstX)
            End If
        Next
    End Sub

    Private Sub PopulatePersons()
        Dim PersonsList As SvConnections.SV.ParamsSen_SalesPersonsDataTable
        PersonsList = ParamsSen_SalesPersons.GetDataByTaskID(_TaskID)

        For Each person In PersonsList
            Dim SqlChecked As Boolean = False
            Dim SenChecked As Boolean = False
            Dim lstX As New ListViewItem

            Dim lstSub As New ListViewItem.ListViewSubItem
            'lstSub.Name = "SqlPersonCode"
            Dim lstSub1 As New ListViewItem.ListViewSubItem
            'lstSub1.Name = "SqlPersonID"
            Dim lstSub2 As New ListViewItem.ListViewSubItem
            Dim lstSub3 As New ListViewItem.ListViewSubItem
            Dim lstSub4 As New ListViewItem.ListViewSubItem

            For Each sql As ListViewItem In lvwFS.Items
                If sql.SubItems(2).Text = person.SqlPersonID Then
                    lstX.Text = sql.Text
                    lstSub.Text = sql.SubItems(1).Text
                    lstSub1.Text = sql.SubItems(2).Text
                    lstX.SubItems.Add(lstSub)
                    lstX.SubItems.Add(lstSub1)
                    If Not chkMultiple.Checked OrElse chkToErp.Checked Then
                        lvwFS.Items.Remove(sql)
                    End If
                    SqlChecked = True
                    Exit For
                End If
            Next

            If SqlChecked Then
                For Each sen As ListViewItem In lvwSen.Items
                    If sen.SubItems(2).Text = person.SenPersonID Then
                        lstSub2.Text = sen.SubItems(0).Text
                        lstSub3.Text = sen.SubItems(1).Text
                        lstSub4.Text = sen.SubItems(2).Text
                        lstX.SubItems.Add(lstSub2)
                        lstX.SubItems.Add(lstSub3)
                        lstX.SubItems.Add(lstSub4)
                        If Not chkMultiple.Checked OrElse Not chkToErp.Checked Then
                            lvwSen.Items.Remove(sen)
                        End If
                        SenChecked = True
                        Exit For
                    End If
                Next
                If SenChecked Then lvwAssociations.Items.Add(lstX)
            End If
        Next
    End Sub

    Private Sub lvwAssociations_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwAssociations.DoubleClick
        SendAssociationsBack(lvwAssociations.SelectedItems(0))
    End Sub

    Private Sub SendAssociationsBack(ByVal SelectedItem As ListViewItem)
        Dim lviSqlName As New ListViewItem
        Dim lviSqlCode As New ListViewItem.ListViewSubItem
        Dim lviFsID As New ListViewItem.ListViewSubItem

        If Not chkMultiple.Checked Or (chkMultiple.Checked And chkToErp.Checked) Then
            lviSqlName.Text = SelectedItem.SubItems(0).Text
            lviSqlCode.Text = SelectedItem.SubItems(1).Text
            lviSqlName.SubItems.Add(lviSqlCode)
            lviFsID.Text = SelectedItem.SubItems(2).Text
            lviSqlName.SubItems.Add(lviFsID)
            lvwFS.Items.Add(lviSqlName)
        End If

        If SelectedItem.SubItems.Count = 6 Then

            If Not chkMultiple.Checked Or (chkMultiple.Checked And Not chkToErp.Checked) Then

                Dim lviSenName As New ListViewItem
                Dim lviSenCode As New ListViewItem.ListViewSubItem
                Dim lviSenID As New ListViewItem.ListViewSubItem

                lviSenName.Text = SelectedItem.SubItems(3).Text
                lviSenCode.Text = SelectedItem.SubItems(4).Text
                lviSenName.SubItems.Add(lviSenCode)
                lviSenID.Text = SelectedItem.SubItems(5).Text
                lviSenName.SubItems.Add(lviSenID)
                lvwSen.Items.Add(lviSenName)
            End If
        End If

        lvwAssociations.Items.Remove(SelectedItem)
    End Sub

    Private Function SvConnTrigger(ByVal ParamsSen_Association As Object, _
                                  ByVal ConnectionID As Integer) _
                                  As Boolean
        Dim ErrorMessage As String = ""

        Dim SqlList
        SqlList = SqlConn.GetDataByID(ConnectionID).Item(0)
        If SqlList Is Nothing Then
            SvSys.ResultsLog("Δεν υπάρχει σύνδεση με τον SQL Server για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
            Return False
        Else
            ErrorMessage = ParamsSen_Association.ConnectToSql( _
                SqlList.server, _
                SqlList.db, _
                SqlList.user, _
                SqlList.pwd)
            If Not ErrorMessage.Equals(String.Empty) Then
                ParamsSen_Association.DisconnectSql()
                SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                SvSys.ResultsLog("Δεν υπάρχει επικοινωνία με τον SQL Server", My.Application.Info.DirectoryPath)
                Return False
            End If
        End If
        Return True
    End Function

    Private Function ConnectionTrigger(ByVal ParamsSen_Association As Object, _
                                  ByVal ConnectionType As String, _
                                  ByVal ConnectionID As Integer) _
                                  As Boolean
        Dim ErrorMessage As String = ""
        Select Case ConnectionType
            Case "SEN"
                Dim SenList
                SenList = SenConn.GetDataByID(ConnectionID).Item(0)
                If SenList Is Nothing Then
                    MessageBox.Show(ErrorMessage)
                    Return False
                Else
                    ErrorMessage = ParamsSen_Association.ConnectToSen1( _
                        SenList.host, _
                        SenList.sasalias, _
                        SenList.senuser, _
                        SenList.senpwd, _
                        SenList.cmpcode, _
                        SenList.wrhid, _
                        SenList.port)
                    If Not ErrorMessage.Equals(String.Empty) Then
                        ParamsSen_Association.DisconnectSen()
                        MessageBox.Show(ErrorMessage)
                        Return False
                    End If
                End If
            Case "SRS"
                Dim SrsList
                SrsList = SrsConn.GetDataByID(ConnectionID).Item(0)
                If SrsList Is Nothing Then
                    MessageBox.Show(ErrorMessage)
                    Return False
                Else
                    ErrorMessage = ParamsSen_Association.ConnectToSrs( _
                        SrsList.host, _
                        SrsList.port, _
                        SrsList.sasalias, _
                        SrsList.srsuser, _
                        SrsList.srspwd)
                    If Not ErrorMessage.Equals(String.Empty) Then
                        ParamsSen_Association.DisconnectSrs()
                        MessageBox.Show(ErrorMessage)
                        Return False
                    End If
                End If
            Case Else
                If ConnectionType = "Footsteps" Or ConnectionType = "OTR" Then
                    Dim SqlList
                    SqlList = SqlConn.GetDataByID(ConnectionID).Item(0)
                    If SqlList Is Nothing Then
                        MessageBox.Show(ErrorMessage)
                        Return False
                    Else
                        ErrorMessage = ParamsSen_Association.ConnectToSql( _
                            SqlList.server, _
                            SqlList.db, _
                            SqlList.user, _
                            SqlList.pwd)
                        If Not ErrorMessage.Equals(String.Empty) Then
                            ParamsSen_Association.DisconnectSql()
                            MessageBox.Show(ErrorMessage)
                            Return False
                        End If
                    End If
                Else
                    MessageBox.Show("Άγνωστος τύπος σύνδεσης")
                    Return False
                End If
        End Select
        Return True

    End Function

    Private Sub lvwFS_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwFS.DoubleClick
        If lvwAssociations.Items.Count = 0 OrElse _
        lvwAssociations.Items(lvwAssociations.Items.Count - 1).SubItems.Count = 6 Then
            Dim SelectedItem As ListViewItem = lvwFS.SelectedItems(0)
            Dim lstX As New ListViewItem
            Dim lstSub As New ListViewItem.ListViewSubItem
            Dim lstSub1 As New ListViewItem.ListViewSubItem

            lstX.Text = SelectedItem.SubItems(0).Text
            lstSub.Text = SelectedItem.SubItems(1).Text
            lstSub1.Text = SelectedItem.SubItems(2).Text
            lstX.SubItems.Add(lstSub)
            lstX.SubItems.Add(lstSub1)
            lvwAssociations.Items.Add(lstX)

            If Not chkMultiple.Checked Or (chkMultiple.Checked And chkToErp.Checked) Then
                lvwFS.Items.Remove(SelectedItem)
            End If
        End If

    End Sub

    Private Sub lvwSen_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwSen.DoubleClick
        If lvwAssociations.Items.Count > 0 AndAlso _
        lvwAssociations.Items(lvwAssociations.Items.Count - 1).SubItems.Count = 3 Then
            Dim SelectedItem As ListViewItem = lvwSen.SelectedItems(0)

            Dim lstX As ListViewItem = lvwAssociations.Items(lvwAssociations.Items.Count - 1)
            lvwAssociations.Items.Remove(lstX)
            Dim lstSub As New ListViewItem.ListViewSubItem
            Dim lstSub1 As New ListViewItem.ListViewSubItem
            Dim lstSub2 As New ListViewItem.ListViewSubItem

            lstSub.Text = SelectedItem.SubItems(0).Text
            lstSub1.Text = SelectedItem.SubItems(1).Text
            lstSub2.Text = SelectedItem.SubItems(2).Text
            lstX.SubItems.Add(lstSub)
            lstX.SubItems.Add(lstSub1)
            lstX.SubItems.Add(lstSub2)
            lvwAssociations.Items.Add(lstX)
            If Not chkMultiple.Checked Or (chkMultiple.Checked And Not chkToErp.Checked) Then
                lvwSen.Items.Remove(SelectedItem)
            End If
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        Save()
        Close()
    End Sub

    Private Sub Save()
        If CurrentAssociation = ParamsFs.AssocType.Sellers Then
            ParamsSen_SalesPersons.DeleteQuery(_TaskID)
            For Each item As ListViewItem In lvwAssociations.Items
                If item.SubItems.Count = 6 Then
                    ParamsSen_SalesPersons.Insert(_TaskID, _
                                             item.SubItems(2).Text, _
                                             item.SubItems(0).Text, _
                                             item.SubItems(5).Text, _
                                             item.SubItems(3).Text)
                    If chkUpdateSellersCodes.Checked Then
                        If _ConnType2 = ConnectionType.Sen Then
                            If _ConnType1 = ConnectionType.Footsteps Then
                                FsSen_Association.UpdateCode(item.SubItems(2).Text, item.SubItems(4).Text)
                            ElseIf _ConnType1 = ConnectionType.Otr Then
                                OtrSen_Association.UpdateCode(item.SubItems(2).Text, item.SubItems(4).Text)
                            End If
                        Else
                            FsControl_Association.UpdateCode(item.SubItems(2).Text, item.SubItems(4).Text)
                        End If
                    End If
                End If
            Next
            TaskParams.UpdateSellers(chkMultiple.Checked, chkToErp.Checked, chkUpdateSellersCodes.Checked, _TaskID)
        Else
            ParamsSen_Payways.DeleteQuery(_TaskID)
            For Each item As ListViewItem In lvwAssociations.Items
                If item.SubItems.Count = 6 Then
                    ParamsSen_Payways.Insert(_TaskID, _
                                             item.SubItems(2).Text, _
                                             item.SubItems(0).Text, _
                                             item.SubItems(5).Text, _
                                             item.SubItems(3).Text)
                End If
            Next
        End If
    End Sub

    Private Sub btnAutoAssociate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAutoAssociate.Click
        If MessageBox.Show("Θέλετε σίγουρα να γίνει αυτόματη αντιστοίχιση;", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            If CurrentAssociation = ParamsFs.AssocType.Payways OrElse SvClass.ValidComboItem(cboDepartments) OrElse _
            (CurrentAssociation = ParamsFs.AssocType.Sellers AndAlso MessageBox.Show("Δεν έχετε ορίσει τμήμα για τους πωλητές, θέλετε να συνεχίσετε;", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes) Then
                For Each assoc In lvwAssociations.Items
                    SendAssociationsBack(assoc)
                Next

                For Each item In lvwSen.Items
                    Dim lstX As New ListViewItem
                    Dim lstSub As New ListViewItem.ListViewSubItem
                    Dim lstSub1 As New ListViewItem.ListViewSubItem
                    Dim lstSub2 As New ListViewItem.ListViewSubItem
                    Dim lstSub3 As New ListViewItem.ListViewSubItem
                    Dim lstSub4 As New ListViewItem.ListViewSubItem

                    Dim OtherID As Integer
                    If _ConnType2 = ConnectionType.Sen Then
                        If _ConnType1 = ConnectionType.Footsteps Then
                            If SvClass.ValidComboItem(cboDepartments) Then
                                OtherID = FsSen_Association.AutoAssociate(item.SubItems(0).Text, _
                                                                            item.SubItems(1).Text, _
                                                                            cboDepartments.SelectedItem.ID, _
                                                                            cboDepartments.SelectedItem.Name)
                            Else
                                OtherID = FsSen_Association.AutoAssociate(item.SubItems(0).Text, _
                                                                            item.SubItems(1).Text)
                            End If
                        ElseIf _ConnType1 = ConnectionType.Otr Then
                            If SvClass.ValidComboItem(cboDepartments) Then
                                OtherID = OtrSen_Association.AutoAssociate(item.SubItems(0).Text, _
                                                                            item.SubItems(1).Text, _
                                                                            cboDepartments.SelectedItem.ID, _
                                                                            cboDepartments.SelectedItem.Name)
                            Else
                                OtherID = OtrSen_Association.AutoAssociate(item.SubItems(0).Text, _
                                                                            item.SubItems(1).Text)
                            End If

                        End If
                    Else
                        If SvClass.ValidComboItem(cboDepartments) Then
                            OtherID = FsControl_Association.AutoAssociate(item.SubItems(0).Text, _
                                                                        item.SubItems(1).Text, _
                                                                        cboDepartments.SelectedItem.ID, _
                                                                        cboDepartments.SelectedItem.Name)
                        Else
                            OtherID = FsControl_Association.AutoAssociate(item.SubItems(0).Text, _
                                                                        item.SubItems(1).Text)
                        End If
                    End If

                    For Each sql As ListViewItem In lvwFS.Items
                        If sql.SubItems(2).Text = OtherID Then
                            lstX.Text = sql.Text
                            lstSub.Text = sql.SubItems(1).Text
                            lstSub1.Text = sql.SubItems(2).Text
                            lvwFS.Items.Remove(sql)
                            OtherID = 0
                        End If
                    Next

                    If OtherID <> 0 Then
                        lstX.Text = item.SubItems(0).Text
                        lstSub.Text = item.SubItems(1).Text
                        lstSub1.Text = OtherID
                    End If

                    lstSub2.Text = item.SubItems(0).Text
                    lstSub3.Text = item.SubItems(1).Text
                    lstSub4.Text = item.SubItems(2).Text
                    lstX.SubItems.Add(lstSub)
                    lstX.SubItems.Add(lstSub1)
                    lstX.SubItems.Add(lstSub2)
                    lstX.SubItems.Add(lstSub3)
                    lstX.SubItems.Add(lstSub4)
                    lvwAssociations.Items.Add(lstX)

                    lvwSen.Items.Remove(item)
                Next
            End If
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub cboDepartments_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboDepartments.TextChanged
        cboDepartments.BackColor = If(cboDepartments.Text = "", _
                                    Drawing.Color.Salmon, _
                                    Drawing.Color.White)
    End Sub

    Private Sub chkMultiple_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkMultiple.CheckedChanged
        If chkMultiple.Checked Then
            btnAutoAssociate.Enabled = False
            If CurrentAssociation = ParamsFs.AssocType.Sellers Then
                chkUpdateSellersCodes.Enabled = False
                chkUpdateSellersCodes.Checked = False
            End If
            'chkFsToSen.Enabled = True
        Else
            btnAutoAssociate.Enabled = True
            If CurrentAssociation = ParamsFs.AssocType.Sellers Then
                chkUpdateSellersCodes.Enabled = True
            End If
            chkToErp.Enabled = False
            chkToErp.Checked = False
        End If
    End Sub

    Private Sub btnAssociationParams_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAssociationParams.Click
        If MessageBox.Show("Θέλετε να αποθηκεύσετε τις αλλαγές;", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            Save()
        End If
        Dim frm As New AssociationsItemsParamsF
        frm.Params = CurrentTaskParams
        frm.ShowDialog()
        Close()
    End Sub

End Class


﻿Public Class EditTaskF

    Private ConnType1 As New ConnectionType
    Private ConnType2 As New ConnectionType

    Private _ID As Integer = 0
    Private SelectedTask As SvConnections.SV.TasksRow
    Private SelectedScheduleID As Integer = 0

    Private Type1 As String
    Private Type2 As String

    Private LoadedSvConnID As Integer = 0

    Private HasChanges As Boolean = False
    Private FirstLoadDone As Boolean = False

    Public WriteOnly Property ID() As Integer
        Set(ByVal value As Integer)
            _ID = value
            Dim SelectedTasks As SvConnections.SV.TasksDataTable = Tasks.GetDataByID(_ID)
            If SelectedTasks.Count > 0 Then

                SelectedTask = SelectedTasks.Item(0)

                txtName.Text = SelectedTask.TaskName
                chkActive.Checked = SelectedTask.Active
                SelectedScheduleID = SelectedTask.ScheduleID

                cboType1.SelectedItem=SelectedTask.ConnectionType1

                cboType2.SelectedItem = SelectedTask.ConnectionType2

                LoadedSvConnID = SelectedTask.SvConnID

                FirstLoadDone = True
                SelectedTask = Nothing
            End If
        End Set
    End Property

    Private Sub GetDataSource()
        Dim SchedulesData As SvConnections.SV.SchedulesDataTable = Schedules.GetData
        For Each item In SchedulesData
            SvClass.InsertToCombo(item.ID, item.ScheduleName, cboSchedules, SelectedScheduleID, True)
        Next

        Dim ConnectionData As Object
        ConnectionData = SqlConn.GetDataByType(ConnectionType.SvConn)

        If ConnectionData IsNot Nothing Then
            For Each item In ConnectionData
                SvClass.InsertToCombo(item.ID, item.ConnectionName, _
                                      cboSvConn, _
                                      LoadedSvConnID, _
                                      True)
            Next
        End If
    End Sub

    Private Sub GetDataSource1()
        btnParamsSen1.Visible = False
        cboConnection1.Items.Clear()
        cboConnection1.SelectedItem = Nothing
        cboConnection1.Text = ""
        Dim ConnectionData As Object
        Select Case ConnType1
            Case ConnectionType.Footsteps
                ConnectionData = SqlConn.GetDataByType(ConnType1)
            Case ConnectionType.Otr
                ConnectionData = SqlConn.GetDataByType(ConnType1)
            Case ConnectionType.Sen
                ConnectionData = SenConn.GetData
            Case ConnectionType.Srs
                ConnectionData = SrsConn.GetData
            Case ConnectionType.Control
                ConnectionData = SqlConn.GetDataByType(ConnType1)
            Case ConnectionType.Galaxy
                ConnectionData = SqlConn.GetDataByType(ConnType1)
            Case ConnectionType.Pylon
                ConnectionData = PylonConn.GetData
            Case ConnectionType.Inventory
                ConnectionData = SqlConn.GetDataByType(ConnType1)
            Case ConnectionType.Prime
                ConnectionData = PrimeConn.GetData
            Case ConnectionType.OtherSQL
                ConnectionData = SqlConn.GetDataByType(ConnType1)
            Case ConnectionType.File
                ConnectionData = FileConn.GetData
            Case ConnectionType.VirtueMart
                ConnectionData = VirtuemartConn.GetData
        End Select

        For Each item In ConnectionData
            SvClass.InsertToCombo(item.ID, item.ConnectionName, _
                                  cboConnection1, _
                                  If(SelectedTask IsNot Nothing, SelectedTask.ConnectionID1, 0), _
                                  True)
        Next
    End Sub

    Private Sub GetDataSource2()
        btnParamsSen2.Visible = False
        cboConnection2.Items.Clear()
        cboConnection2.SelectedItem = Nothing
        cboConnection2.Text = ""
        Dim ConnectionData As Object
        Select Case ConnType2
            Case ConnectionType.Footsteps
                ConnectionData = SqlConn.GetDataByType(ConnType2)
            Case ConnectionType.Otr
                ConnectionData = SqlConn.GetDataByType(ConnType2)
            Case ConnectionType.Sen
                ConnectionData = SenConn.GetData
            Case ConnectionType.Srs
                ConnectionData = SrsConn.GetData
            Case ConnectionType.Control
                ConnectionData = SqlConn.GetDataByType(ConnType2)
            Case ConnectionType.Galaxy
                ConnectionData = SqlConn.GetDataByType(ConnType2)
            Case ConnectionType.Pylon
                ConnectionData = PylonConn.GetData
            Case ConnectionType.Inventory
                ConnectionData = SqlConn.GetDataByType(ConnType2)
            Case ConnectionType.Prime
                ConnectionData = PrimeConn.GetData
            Case ConnectionType.OtherSQL
                ConnectionData = SqlConn.GetDataByType(ConnType2)
            Case ConnectionType.File
                ConnectionData = FileConn.GetData
            Case ConnectionType.VirtueMart
                ConnectionData = VirtuemartConn.GetData
        End Select

        For Each item In ConnectionData
            SvClass.InsertToCombo(item.ID, item.ConnectionName, _
                                  cboConnection2, _
                                  If(SelectedTask IsNot Nothing, SelectedTask.ConnectionID2, 0), _
                                  True)
        Next
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If SaveTask() Then Close()
    End Sub

    Private Function SaveTask() As Boolean
        If Not txtName.Text.Equals(String.Empty) AndAlso _
                SvClass.ValidComboItem(cboSvConn) AndAlso _
                SvClass.ValidComboItem(cboConnection1) AndAlso _
                SvClass.ValidComboItem(cboConnection2) Then
            Dim ScheduleID As Integer
            If SvClass.ValidComboItem(cboSchedules) Then
                ScheduleID = (Schedules.GetDataByID(cboSchedules.SelectedItem.ID))(0).ID
            Else
                ScheduleID = DefaultScheduleID()
            End If
            If _ID.Equals(0) Then
                Tasks.InsertQuery(txtName.Text, _
                                  chkActive.Checked, _
                                  cboSvConn.SelectedItem.ID, _
                                  Type1, _
                                  cboConnection1.SelectedItem.ID, _
                                  Type2, _
                                  cboConnection2.SelectedItem.ID, _
                                  ScheduleID, Now, Now)

                _ID = Tasks.GetLastID
                Tasks.UpdateLastRun(Now, Now, _ID)

            Else
                Tasks.UpdateQuery(txtName.Text, _
                                  chkActive.Checked, _
                                  cboSvConn.SelectedItem.ID, _
                                  Type1, _
                                  cboConnection1.SelectedItem.ID, _
                                  Type2, _
                                  cboConnection2.SelectedItem.ID, _
                                  ScheduleID, _
                                  _ID)
            End If

            Dim NextOccurrence As Date
            NextOccurrence = SvConnections.modConnector.GetNextRunByID(_ID)

            Tasks.UpdateNextRun(NextOccurrence, _ID)

            HasChanges = False

            Return True
        Else
            MessageBox.Show("Συμπληρώστε σωστά όλα τα απαραίτητα πεδία", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Return False
        End If
    End Function

    Private Function DefaultScheduleID() As Integer
        Schedules.Fill(MDB.Schedules)
        Dim ScheduleID = Schedules.GetDefaultID
        Return If(ScheduleID Is Nothing, 0, ScheduleID)
    End Function

    Private Sub EditTaskForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If _ID = 0 Then
            cboType1.SelectedItem = "Footsteps"
            cboType2.SelectedItem = "SEN"
        End If
        GetDataSource()
        AddHandler btnParamsSen1.Click, AddressOf btnParamsSen
        AddHandler btnParamsSen2.Click, AddressOf btnParamsSen
        HasChanges = False
    End Sub

    Private Sub btnParamsSen()
        If Not HasChanges OrElse _
        (MessageBox.Show("Για να προχωρήσετε πρέπει να αποθηκευθούν οι αλλαγές" & vbCrLf & _
                        "Θέλετε να αποθηκευθούν οι αλλαγές;", "", MessageBoxButtons.YesNo, _
                        MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes AndAlso _
                        SaveTask()) Then
            Dim frm As New AssociationsOptionsF
            frm.TaskID = _ID
            frm.ConnType1 = ConnType1
            frm.ConnType2 = ConnType2
            frm.ShowDialog()
        End If
    End Sub

    Private Sub btnParamsControl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnParamsControl.Click
        If Not HasChanges OrElse _
        (MessageBox.Show("Για να προχωρήσετε πρέπει να αποθηκευθούν οι αλλαγές" & vbCrLf & _
                        "Θέλετε να αποθηκευθούν οι αλλαγές;", "", MessageBoxButtons.YesNo, _
                        MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes AndAlso _
                        SaveTask()) Then
            Dim frm As New AssociationsOptionsF
            frm.TaskID = _ID
            frm.ConnType1 = ConnType1
            frm.ConnType2 = ConnType2
            frm.ShowDialog()
        End If
    End Sub

    Private Sub cboType1_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboType1.SelectedValueChanged
        Select Case cboType1.SelectedItem
            Case "Footsteps"
                ConnType1 = ConnectionType.Footsteps
            Case "SEN"
                ConnType1 = ConnectionType.Sen
            Case "SRS"
                ConnType1 = ConnectionType.Srs
            Case "OTR"
                ConnType1 = ConnectionType.Otr
            Case "Control"
                ConnType1 = ConnectionType.Control
            Case "Galaxy"
                ConnType1 = ConnectionType.Galaxy
            Case "Pylon"
                ConnType1 = ConnectionType.Pylon
            Case "Inventory"
                ConnType1 = ConnectionType.Inventory
            Case "Prime"
                ConnType1 = ConnectionType.Prime
            Case "Virtuemart"
                ConnType1 = ConnectionType.VirtueMart
            Case "Αρχείο"
                ConnType1 = ConnectionType.File
            Case "SQL Server Άλλο"
                ConnType1 = ConnectionType.OtherSQL
            Case "SvConn"
                ConnType1 = ConnectionType.SvConn
        End Select
        Type1 = cboType1.SelectedItem
        GetDataSource1()

        If ConnType1 = ConnectionType.Sen Then
            btnParamsSen1.Visible = (_ID > 0)
        End If

        HasChanges = FirstLoadDone
    End Sub

    Private Sub cboType2_SelectedValueChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboType2.SelectedValueChanged
        Select Case cboType2.SelectedItem
            Case "Footsteps"
                ConnType2 = ConnectionType.Footsteps
            Case "SEN"
                ConnType2 = ConnectionType.Sen
            Case "SRS"
                ConnType2 = ConnectionType.Srs
            Case "OTR"
                ConnType2 = ConnectionType.Otr
            Case "Control"
                ConnType2 = ConnectionType.Control
            Case "Galaxy"
                ConnType2 = ConnectionType.Galaxy
            Case "Pylon"
                ConnType2 = ConnectionType.Pylon
            Case "Inventory"
                ConnType2 = ConnectionType.Inventory
            Case "Prime"
                ConnType2 = ConnectionType.Prime
            Case "Virtuemart"
                ConnType2 = ConnectionType.VirtueMart
            Case "SQL Server Άλλο"
                ConnType2 = ConnectionType.OtherSQL
            Case "Αρχείο"
                ConnType2 = ConnectionType.File
            Case "SvConn"
                ConnType2 = ConnectionType.SvConn
        End Select
        Type2 = cboType2.SelectedItem
        GetDataSource2()

        If ConnType2 = ConnectionType.Sen Then
            btnParamsSen2.Visible = (_ID > 0)
        ElseIf ConnType2 = ConnectionType.Control Then
            btnParamsControl.Visible = (_ID > 0)
        End If

        HasChanges = FirstLoadDone
    End Sub
End Class
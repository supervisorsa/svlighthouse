﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FileF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnCreate = New System.Windows.Forms.Button
        Me.rdbPath = New System.Windows.Forms.RadioButton
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog
        Me.txtPath = New System.Windows.Forms.TextBox
        Me.btnPath = New System.Windows.Forms.Button
        Me.rdbFile = New System.Windows.Forms.RadioButton
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(399, 104)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(366, 48)
        Me.btnExit.TabIndex = 15
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnCreate
        '
        Me.btnCreate.Location = New System.Drawing.Point(27, 104)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(366, 48)
        Me.btnCreate.TabIndex = 14
        Me.btnCreate.Text = "Αποθήκευση αλλαγών"
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'rdbPath
        '
        Me.rdbPath.AutoSize = True
        Me.rdbPath.Location = New System.Drawing.Point(113, 31)
        Me.rdbPath.Name = "rdbPath"
        Me.rdbPath.Size = New System.Drawing.Size(71, 18)
        Me.rdbPath.TabIndex = 16
        Me.rdbPath.TabStop = True
        Me.rdbPath.Text = "Φάκελος"
        Me.rdbPath.UseVisualStyleBackColor = True
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'txtPath
        '
        Me.txtPath.Location = New System.Drawing.Point(27, 65)
        Me.txtPath.Name = "txtPath"
        Me.txtPath.Size = New System.Drawing.Size(699, 22)
        Me.txtPath.TabIndex = 17
        '
        'btnPath
        '
        Me.btnPath.Location = New System.Drawing.Point(732, 65)
        Me.btnPath.Name = "btnPath"
        Me.btnPath.Size = New System.Drawing.Size(33, 23)
        Me.btnPath.TabIndex = 18
        Me.btnPath.Text = "..."
        Me.btnPath.UseVisualStyleBackColor = True
        '
        'rdbFile
        '
        Me.rdbFile.AutoSize = True
        Me.rdbFile.Location = New System.Drawing.Point(27, 31)
        Me.rdbFile.Name = "rdbFile"
        Me.rdbFile.Size = New System.Drawing.Size(60, 18)
        Me.rdbFile.TabIndex = 19
        Me.rdbFile.TabStop = True
        Me.rdbFile.Text = "Αρχείο"
        Me.rdbFile.UseVisualStyleBackColor = True
        '
        'FileF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(783, 178)
        Me.Controls.Add(Me.rdbFile)
        Me.Controls.Add(Me.btnPath)
        Me.Controls.Add(Me.txtPath)
        Me.Controls.Add(Me.rdbPath)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnCreate)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Name = "FileF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Ορισμός διαδρομής"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnCreate As System.Windows.Forms.Button
    Friend WithEvents rdbPath As System.Windows.Forms.RadioButton
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents txtPath As System.Windows.Forms.TextBox
    Friend WithEvents btnPath As System.Windows.Forms.Button
    Friend WithEvents rdbFile As System.Windows.Forms.RadioButton
End Class

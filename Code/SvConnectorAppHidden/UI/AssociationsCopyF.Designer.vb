﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AssociationsCopyF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AssociationsCopyF))
        Me.cboTasks = New System.Windows.Forms.ComboBox
        Me.chkPayways = New System.Windows.Forms.CheckBox
        Me.lblCopy = New System.Windows.Forms.Label
        Me.chkSalespersons = New System.Windows.Forms.CheckBox
        Me.chkCategories = New System.Windows.Forms.CheckBox
        Me.chkContactCategories = New System.Windows.Forms.CheckBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'cboTasks
        '
        Me.cboTasks.FormattingEnabled = True
        Me.cboTasks.Location = New System.Drawing.Point(21, 44)
        Me.cboTasks.Name = "cboTasks"
        Me.cboTasks.Size = New System.Drawing.Size(202, 22)
        Me.cboTasks.TabIndex = 0
        '
        'chkPayways
        '
        Me.chkPayways.AutoSize = True
        Me.chkPayways.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkPayways.Location = New System.Drawing.Point(21, 89)
        Me.chkPayways.Name = "chkPayways"
        Me.chkPayways.Size = New System.Drawing.Size(136, 18)
        Me.chkPayways.TabIndex = 1
        Me.chkPayways.Text = "Τρόποι πληρωμής"
        Me.chkPayways.UseVisualStyleBackColor = True
        '
        'lblCopy
        '
        Me.lblCopy.AutoSize = True
        Me.lblCopy.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblCopy.Location = New System.Drawing.Point(18, 16)
        Me.lblCopy.Name = "lblCopy"
        Me.lblCopy.Size = New System.Drawing.Size(93, 14)
        Me.lblCopy.TabIndex = 2
        Me.lblCopy.Text = "Αντιγραφή σε"
        '
        'chkSalespersons
        '
        Me.chkSalespersons.AutoSize = True
        Me.chkSalespersons.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkSalespersons.Location = New System.Drawing.Point(21, 113)
        Me.chkSalespersons.Name = "chkSalespersons"
        Me.chkSalespersons.Size = New System.Drawing.Size(80, 18)
        Me.chkSalespersons.TabIndex = 3
        Me.chkSalespersons.Text = "Πωλητές"
        Me.chkSalespersons.UseVisualStyleBackColor = True
        '
        'chkCategories
        '
        Me.chkCategories.AutoSize = True
        Me.chkCategories.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkCategories.Location = New System.Drawing.Point(21, 137)
        Me.chkCategories.Name = "chkCategories"
        Me.chkCategories.Size = New System.Drawing.Size(133, 18)
        Me.chkCategories.TabIndex = 4
        Me.chkCategories.Text = "Κατηγορίες ειδών"
        Me.chkCategories.UseVisualStyleBackColor = True
        '
        'chkContactCategories
        '
        Me.chkContactCategories.AutoSize = True
        Me.chkContactCategories.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkContactCategories.Location = New System.Drawing.Point(21, 161)
        Me.chkContactCategories.Name = "chkContactCategories"
        Me.chkContactCategories.Size = New System.Drawing.Size(153, 18)
        Me.chkContactCategories.TabIndex = 5
        Me.chkContactCategories.Text = "Κατηγορίες πελατών"
        Me.chkContactCategories.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.btnExit.Location = New System.Drawing.Point(21, 238)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(202, 40)
        Me.btnExit.TabIndex = 8
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.btnSave.Location = New System.Drawing.Point(20, 192)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(203, 40)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Αποθήκευση"
        '
        'AssociationsCopyF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(250, 293)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.chkContactCategories)
        Me.Controls.Add(Me.chkCategories)
        Me.Controls.Add(Me.chkSalespersons)
        Me.Controls.Add(Me.lblCopy)
        Me.Controls.Add(Me.chkPayways)
        Me.Controls.Add(Me.cboTasks)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AssociationsCopyF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Αντιγραφή αντιστοιχίσεων"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboTasks As System.Windows.Forms.ComboBox
    Friend WithEvents chkPayways As System.Windows.Forms.CheckBox
    Friend WithEvents lblCopy As System.Windows.Forms.Label
    Friend WithEvents chkSalespersons As System.Windows.Forms.CheckBox
    Friend WithEvents chkCategories As System.Windows.Forms.CheckBox
    Friend WithEvents chkContactCategories As System.Windows.Forms.CheckBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
End Class

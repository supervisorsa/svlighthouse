﻿Imports System.IO

Public Class MainF

    Private Sub btnRun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRun.Click
        Dim frm As New TaskExecF
        frm.ShowDialog()
    End Sub

    Private Sub btnConnections_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnections.Click
        Dim frm As New ConnectionsF
        frm.ShowDialog()
    End Sub

    Private Sub btnTasks_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTasks.Click
        Dim frm As New TasksF
        frm.ShowDialog()
    End Sub

    Private Sub btnSchedules_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSchedules.Click
        Dim frm As New SchedulesF
        frm.ShowDialog()
    End Sub

    Private Sub btnInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInfo.Click
        If Not File.Exists(My.Application.Info.DirectoryPath & "\Info.log") Then
            Dim fs As FileStream = File.Create(My.Application.Info.DirectoryPath & "\Info.log")
            fs.Close()
        End If
        SvSys.RunProcess(My.Application.Info.DirectoryPath & "\Info.log")
    End Sub

    Private Sub btnError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnError.Click
        If Not File.Exists(My.Application.Info.DirectoryPath & "\Errors.log") Then
            Dim fs As FileStream = File.Create(My.Application.Info.DirectoryPath & "\Errors.log")
            fs.Close()
        End If
        SvSys.RunProcess(My.Application.Info.DirectoryPath & "\Errors.log")
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnTools_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTools.Click
        Dim frm As New ToolsF
        frm.ShowDialog()
    End Sub
End Class



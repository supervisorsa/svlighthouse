﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditTaskF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cboConnection1 = New System.Windows.Forms.ComboBox
        Me.cboConnection2 = New System.Windows.Forms.ComboBox
        Me.chkActive = New System.Windows.Forms.CheckBox
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.txtName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.cboSchedules = New System.Windows.Forms.ComboBox
        Me.btnParamsSen1 = New System.Windows.Forms.Button
        Me.btnParamsSen2 = New System.Windows.Forms.Button
        Me.btnParamsControl = New System.Windows.Forms.Button
        Me.cboType1 = New System.Windows.Forms.ComboBox
        Me.cboType2 = New System.Windows.Forms.ComboBox
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.cboSvConn = New System.Windows.Forms.ComboBox
        Me.SuspendLayout()
        '
        'cboConnection1
        '
        Me.cboConnection1.FormattingEnabled = True
        Me.cboConnection1.Location = New System.Drawing.Point(12, 112)
        Me.cboConnection1.Name = "cboConnection1"
        Me.cboConnection1.Size = New System.Drawing.Size(208, 24)
        Me.cboConnection1.TabIndex = 17
        '
        'cboConnection2
        '
        Me.cboConnection2.FormattingEnabled = True
        Me.cboConnection2.Location = New System.Drawing.Point(12, 331)
        Me.cboConnection2.Name = "cboConnection2"
        Me.cboConnection2.Size = New System.Drawing.Size(208, 24)
        Me.cboConnection2.TabIndex = 18
        '
        'chkActive
        '
        Me.chkActive.AutoSize = True
        Me.chkActive.Checked = True
        Me.chkActive.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkActive.Location = New System.Drawing.Point(373, 135)
        Me.chkActive.Name = "chkActive"
        Me.chkActive.Size = New System.Drawing.Size(66, 20)
        Me.chkActive.TabIndex = 19
        Me.chkActive.Text = "Ενεργό"
        Me.chkActive.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(373, 305)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(300, 50)
        Me.btnExit.TabIndex = 21
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(373, 232)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(300, 50)
        Me.btnSave.TabIndex = 20
        Me.btnSave.Text = "Αποθήκευση"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'txtName
        '
        Me.txtName.Location = New System.Drawing.Point(373, 98)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(300, 23)
        Me.txtName.TabIndex = 22
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label1.Location = New System.Drawing.Point(9, 93)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 16)
        Me.Label1.TabIndex = 23
        Me.Label1.Text = "Σύνδεση 1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label2.Location = New System.Drawing.Point(9, 312)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(76, 16)
        Me.Label2.TabIndex = 24
        Me.Label2.Text = "Συνδέση 2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label3.Location = New System.Drawing.Point(370, 79)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(115, 16)
        Me.Label3.TabIndex = 25
        Me.Label3.Text = "Όνομα εργασίας"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label4.Location = New System.Drawing.Point(370, 171)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(164, 16)
        Me.Label4.TabIndex = 27
        Me.Label4.Text = "Χρονοπρογραμματισμός"
        '
        'cboSchedules
        '
        Me.cboSchedules.FormattingEnabled = True
        Me.cboSchedules.Location = New System.Drawing.Point(373, 190)
        Me.cboSchedules.Name = "cboSchedules"
        Me.cboSchedules.Size = New System.Drawing.Size(300, 24)
        Me.cboSchedules.TabIndex = 26
        '
        'btnParamsSen1
        '
        Me.btnParamsSen1.Location = New System.Drawing.Point(226, 79)
        Me.btnParamsSen1.Name = "btnParamsSen1"
        Me.btnParamsSen1.Size = New System.Drawing.Size(122, 57)
        Me.btnParamsSen1.TabIndex = 28
        Me.btnParamsSen1.Text = "Παραμετροποίηση SEN"
        Me.btnParamsSen1.UseVisualStyleBackColor = True
        Me.btnParamsSen1.Visible = False
        '
        'btnParamsSen2
        '
        Me.btnParamsSen2.Location = New System.Drawing.Point(226, 298)
        Me.btnParamsSen2.Name = "btnParamsSen2"
        Me.btnParamsSen2.Size = New System.Drawing.Size(122, 57)
        Me.btnParamsSen2.TabIndex = 29
        Me.btnParamsSen2.Text = "Παραμετροποίηση SEN"
        Me.btnParamsSen2.UseVisualStyleBackColor = True
        Me.btnParamsSen2.Visible = False
        '
        'btnParamsControl
        '
        Me.btnParamsControl.Location = New System.Drawing.Point(226, 298)
        Me.btnParamsControl.Name = "btnParamsControl"
        Me.btnParamsControl.Size = New System.Drawing.Size(122, 57)
        Me.btnParamsControl.TabIndex = 30
        Me.btnParamsControl.Text = "Παραμετροποίηση Control"
        Me.btnParamsControl.UseVisualStyleBackColor = True
        Me.btnParamsControl.Visible = False
        '
        'cboType1
        '
        Me.cboType1.FormattingEnabled = True
        Me.cboType1.Items.AddRange(New Object() {"Footsteps", "SEN", "Galaxy", "OTR", "Control", "Pylon", "Prime", "SRS", "Inventory", "SQL Server Άλλο", "Virtuemart", "Αρχείο"})
        Me.cboType1.Location = New System.Drawing.Point(12, 39)
        Me.cboType1.Name = "cboType1"
        Me.cboType1.Size = New System.Drawing.Size(208, 24)
        Me.cboType1.TabIndex = 31
        '
        'cboType2
        '
        Me.cboType2.FormattingEnabled = True
        Me.cboType2.Items.AddRange(New Object() {"Footsteps", "SEN", "Galaxy", "OTR", "Control", "Pylon", "Prime", "SRS", "Inventory", "SQL Server Άλλο", "Virtuemart", "Αρχείο"})
        Me.cboType2.Location = New System.Drawing.Point(12, 247)
        Me.cboType2.Name = "cboType2"
        Me.cboType2.Size = New System.Drawing.Size(208, 24)
        Me.cboType2.TabIndex = 32
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label5.Location = New System.Drawing.Point(9, 228)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(125, 16)
        Me.Label5.TabIndex = 33
        Me.Label5.Text = "Τύπος Σύνδεσης 2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label6.Location = New System.Drawing.Point(9, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(125, 16)
        Me.Label6.TabIndex = 34
        Me.Label6.Text = "Τύπος Σύνδεσης 1"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label7.Location = New System.Drawing.Point(370, 28)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(56, 16)
        Me.Label7.TabIndex = 36
        Me.Label7.Text = "SvConn"
        '
        'cboSvConn
        '
        Me.cboSvConn.FormattingEnabled = True
        Me.cboSvConn.Location = New System.Drawing.Point(432, 25)
        Me.cboSvConn.Name = "cboSvConn"
        Me.cboSvConn.Size = New System.Drawing.Size(241, 24)
        Me.cboSvConn.TabIndex = 35
        '
        'EditTaskF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(699, 383)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.cboSvConn)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.cboType2)
        Me.Controls.Add(Me.cboType1)
        Me.Controls.Add(Me.btnParamsControl)
        Me.Controls.Add(Me.btnParamsSen2)
        Me.Controls.Add(Me.btnParamsSen1)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.cboSchedules)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Controls.Add(Me.chkActive)
        Me.Controls.Add(Me.cboConnection2)
        Me.Controls.Add(Me.cboConnection1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "EditTaskF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Εργασία"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cboConnection1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboConnection2 As System.Windows.Forms.ComboBox
    Friend WithEvents chkActive As System.Windows.Forms.CheckBox
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents cboSchedules As System.Windows.Forms.ComboBox
    Friend WithEvents btnParamsSen1 As System.Windows.Forms.Button
    Friend WithEvents btnParamsSen2 As System.Windows.Forms.Button
    Friend WithEvents btnParamsControl As System.Windows.Forms.Button
    Friend WithEvents cboType1 As System.Windows.Forms.ComboBox
    Friend WithEvents cboType2 As System.Windows.Forms.ComboBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents cboSvConn As System.Windows.Forms.ComboBox
End Class

﻿Public Class ConnectionsF
    Private EnDecrypt As New SvCrypto(SvCrypto.SymmProvEnum.Rijndael)
    Private GlobalKey As String = "Connr"

    Private ConnType As New ConnectionType

    Private Sub ConnectionsForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        rdbSvConn.Checked = True
        GetDataSource()
    End Sub

    Private Sub GetDataSource()
        If ConnType = ConnectionType.Sen Then
            dgvConnections.DataSource = SenConn.GetData
        ElseIf ConnType = ConnectionType.Srs Then
            dgvConnections.DataSource = SrsConn.GetData
        ElseIf ConnType = ConnectionType.Prime Then
            dgvConnections.DataSource = PrimeConn.GetData
        ElseIf ConnType = ConnectionType.Pylon Then
            dgvConnections.DataSource = PylonConn.GetData
        ElseIf ConnType = ConnectionType.VirtueMart Then
            dgvConnections.DataSource = VirtuemartConn.GetData
        ElseIf ConnType = ConnectionType.File Then
            dgvConnections.DataSource = FileConn.GetData
        ElseIf ConnType = -1 Then
            dgvConnections.DataSource = Nothing
        Else
            dgvConnections.DataSource = SqlConn.GetDataByType(ConnType)
        End If

        For Each col As Windows.Forms.DataGridViewColumn In dgvConnections.Columns
            If col.Name.ToLower.Contains("user") Or _
            col.Name.ToLower.Contains("pwd") Or _
            col.Name.ToLower.Contains("password") Or _
            col.Name.ToLower.Contains("date") Or _
            col.Name.ToLower.Equals("sqlconn") Or _
            col.Name.ToLower.Equals("id") Then
                col.Visible = False
            ElseIf col.Name.ToLower.Contains("connectionname") Then
                col.HeaderText = "Όνομασία"
                'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            ElseIf col.Name.ToLower.Contains("server") Then
                col.HeaderText = "Server"
                'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            ElseIf col.Name.ToLower.Contains("db") Then
                col.HeaderText = "Βάση"
                'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            ElseIf col.Name.ToLower.Contains("host") Then
                col.HeaderText = "Host"
                'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            ElseIf col.Name.ToLower.Contains("port") Then
                col.HeaderText = "Θύρα"
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            ElseIf col.Name.ToLower.Contains("sasalias") Then
                col.HeaderText = "Sas Alias"
                'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            ElseIf col.Name.ToLower.Contains("cmpcode") Then
                col.HeaderText = "Εταιρία"
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            ElseIf col.Name.ToLower.Contains("wrhid") Then
                col.HeaderText = "Αποθ.χώρος"
                col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
            End If
        Next
        dgvConnections.Refresh()
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        If ConnType = ConnectionType.Sen Then

            Dim frmSen As New SenForm
            frmSen.ShowDialog()
            If frmSen.Host IsNot Nothing AndAlso _
            frmSen.Port IsNot Nothing AndAlso _
            frmSen.SasAlias IsNot Nothing AndAlso _
            frmSen.SenUser IsNot Nothing AndAlso _
            frmSen.SenPwd IsNot Nothing AndAlso _
            frmSen.CmpCode IsNot Nothing AndAlso _
            frmSen.WrhID IsNot Nothing Then
                Dim ConnName As String = InputBox("Δώστε όνομα για τη σύνδεση")
                If Not ConnName.Equals(String.Empty) Then
                    SenConn.InsertQuery(ConnName, _
                                        frmSen.Host, _
                                        frmSen.Port, _
                                        frmSen.SasAlias, _
                                        frmSen.SenUser, _
                                        frmSen.SenPwd, _
                                        frmSen.CmpCode, _
                                        frmSen.WrhID)
                End If
            End If

        ElseIf ConnType = ConnectionType.Srs Then

            Dim frmSrs As New SrsForm
            frmSrs.ShowDialog()
            If frmSrs.Host IsNot Nothing AndAlso _
            frmSrs.Port IsNot Nothing AndAlso _
            frmSrs.SasAlias IsNot Nothing AndAlso _
            frmSrs.SrsUser IsNot Nothing AndAlso _
            frmSrs.SrsPwd IsNot Nothing Then
                Dim ConnName As String = InputBox("Δώστε όνομα για τη σύνδεση")
                If Not ConnName.Equals(String.Empty) Then
                    SrsConn.InsertQuery(ConnName, _
                                        frmSrs.Host, _
                                        frmSrs.Port, _
                                        frmSrs.SasAlias, _
                                        frmSrs.SrsUser, _
                                        frmSrs.SrsPwd)
                End If
            End If

        ElseIf ConnType = ConnectionType.Prime Then

            Dim frmPrime As New PrimeF
            frmPrime.ShowDialog()
            If frmPrime.DB IsNot Nothing AndAlso _
            frmPrime.Server IsNot Nothing AndAlso _
            frmPrime.DB IsNot Nothing AndAlso _
            frmPrime.DbUser IsNot Nothing AndAlso _
            frmPrime.DbPwd IsNot Nothing AndAlso _
            frmPrime.AppUsername IsNot Nothing AndAlso _
            frmPrime.AppPassword IsNot Nothing Then
                Dim ConnName As String = InputBox("Δώστε όνομα για τη σύνδεση")
                If Not ConnName.Equals(String.Empty) Then
                    PrimeConn.Insert(ConnName, _
                                        frmPrime.Server, _
                                        frmPrime.DB, _
                                        frmPrime.DbUser, _
                                        EnDecrypt.Encrypting(frmPrime.DbPwd, GlobalKey, "-"), _
                                        frmPrime.AppDate, _
                                        frmPrime.AppDate, _
                                        frmPrime.AppUsername, _
                                        EnDecrypt.Encrypting(frmPrime.AppPassword, GlobalKey, "-"))
                End If
            End If

        ElseIf ConnType = ConnectionType.File Then

            Dim frmFile As New FileF
            frmFile.ShowDialog()
            If frmFile.Path <> "" OrElse _
            frmFile.Filename <> "" Then
                Dim ConnName As String = InputBox("Δώστε όνομα για τη σύνδεση")
                If Not ConnName.Equals(String.Empty) Then
                    FileConn.Insert(ConnName, _
                                        frmFile.Path, _
                                        frmFile.Filename)
                End If
            End If

        ElseIf ConnType = ConnectionType.Control Then
            Dim frm As New ControlF
            frm.ShowDialog()

        ElseIf ConnType = -1 Then

            MessageBox.Show("Επιλέξτε τύπο", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        ElseIf ConnType = ConnectionType.Pylon Then
            Dim frmPylon As New PylonF
            frmPylon.ShowDialog()

            If frmPylon.DbAlias IsNot Nothing AndAlso _
            frmPylon.ApplicationName IsNot Nothing AndAlso _
            frmPylon.Extras IsNot Nothing AndAlso _
            frmPylon.Username IsNot Nothing AndAlso _
            frmPylon.Password IsNot Nothing Then
                Dim ConnName As String = InputBox("Δώστε όνομα για τη σύνδεση")
                If Not ConnName.Equals(String.Empty) Then
                    PylonConn.Insert(ConnName, _
                                        frmPylon.ApplicationName, _
                                        frmPylon.DbAlias, _
                                        frmPylon.Username, _
                                        EnDecrypt.Encrypting(frmPylon.Password, GlobalKey, "-"), _
                                        frmPylon.Extras)
                End If
            End If



        ElseIf ConnType = ConnectionType.VirtueMart Then
            Dim frmVirtuemart As New VirtueMartF
            frmVirtuemart.ShowDialog()

            If frmVirtuemart.Username IsNot Nothing AndAlso _
            frmVirtuemart.Password IsNot Nothing Then
                Dim ConnName As String = InputBox("Δώστε όνομα για τη σύνδεση")
                If Not ConnName.Equals(String.Empty) Then
                    VirtuemartConn.Insert(ConnName, _
                                        frmVirtuemart.Username, _
                                        EnDecrypt.Encrypting(frmVirtuemart.Password, GlobalKey, "-"))
                End If
            End If
        Else

            Dim frmSql As New DbConnForm
            frmSql.WriteToConfig = False
            frmSql.ShowDialog()
            If frmSql.Server IsNot Nothing AndAlso _
            frmSql.DB IsNot Nothing AndAlso _
            frmSql.User IsNot Nothing AndAlso _
            frmSql.Pwd IsNot Nothing Then
                Dim ConnName As String = InputBox("Δώστε όνομα για τη σύνδεση")
                If Not ConnName.Equals(String.Empty) Then
                    SqlConn.InsertQuery(ConnName, _
                                        frmSql.Server, _
                                        frmSql.DB, _
                                        frmSql.User, _
                                        EnDecrypt.Encrypting(frmSql.Pwd, GlobalKey, "-"), _
                                        ConnType)
                End If
            End If

        End If


        GetDataSource()
    End Sub

    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click
        If dgvConnections.SelectedCells.Count = 0 Then Exit Sub

        Dim RowIndex As Short
        RowIndex = dgvConnections.SelectedCells(0).RowIndex




        If ConnType = ConnectionType.Sen Then

            Dim frmSen As New SenForm
            frmSen.Host = dgvConnections.Rows(RowIndex).Cells("host").Value
            frmSen.Port = dgvConnections.Rows(RowIndex).Cells("port").Value
            frmSen.SasAlias = dgvConnections.Rows(RowIndex).Cells("sasalias").Value
            frmSen.SenUser = dgvConnections.Rows(RowIndex).Cells("senuser").Value
            frmSen.SenPwd = If(IsDBNull(dgvConnections.Rows(RowIndex).Cells("senpwd").Value), _
                                "", dgvConnections.Rows(RowIndex).Cells("senpwd").Value)
            frmSen.CmpCode = dgvConnections.Rows(RowIndex).Cells("cmpcode").Value
            frmSen.WrhID = dgvConnections.Rows(RowIndex).Cells("wrhid").Value
            frmSen.ShowDialog()
            If frmSen.Host IsNot Nothing AndAlso _
            frmSen.Port IsNot Nothing AndAlso _
            frmSen.SasAlias IsNot Nothing AndAlso _
            frmSen.SenUser IsNot Nothing AndAlso _
            frmSen.SenPwd IsNot Nothing AndAlso _
            frmSen.CmpCode IsNot Nothing AndAlso _
            frmSen.WrhID IsNot Nothing Then
                SenConn.UpdateQuery(dgvConnections.Rows(RowIndex).Cells("connectionname").Value, _
                                    frmSen.Host, _
                                    frmSen.Port, _
                                    frmSen.SasAlias, _
                                    frmSen.SenUser, _
                                    frmSen.SenPwd, _
                                    frmSen.CmpCode, _
                                    frmSen.WrhID, _
                                    dgvConnections.Rows(RowIndex).Cells("id").Value)
            End If

        ElseIf ConnType = ConnectionType.Srs Then

            Dim frmSrs As New SrsForm
            frmSrs.Host = dgvConnections.Rows(RowIndex).Cells("host").Value
            frmSrs.Port = dgvConnections.Rows(RowIndex).Cells("port").Value
            frmSrs.SasAlias = dgvConnections.Rows(RowIndex).Cells("sasalias").Value
            frmSrs.SrsUser = dgvConnections.Rows(RowIndex).Cells("srsuser").Value
            frmSrs.SrsPwd = dgvConnections.Rows(RowIndex).Cells("srspwd").Value
            frmSrs.ShowDialog()
            If frmSrs.Host IsNot Nothing AndAlso _
            frmSrs.Port IsNot Nothing AndAlso _
            frmSrs.SasAlias IsNot Nothing AndAlso _
            frmSrs.SrsUser IsNot Nothing AndAlso _
            frmSrs.SrsPwd IsNot Nothing Then
                SrsConn.UpdateQuery(dgvConnections.Rows(RowIndex).Cells("connectionname").Value, _
                                    frmSrs.Host, _
                                    frmSrs.Port, _
                                    frmSrs.SasAlias, _
                                    frmSrs.SrsUser, _
                                    frmSrs.SrsPwd, _
                                    dgvConnections.Rows(RowIndex).Cells("id").Value)
            End If

        ElseIf ConnType = ConnectionType.Prime Then

            Dim frmPrime As New PrimeF
            frmPrime.Server = dgvConnections.Rows(RowIndex).Cells("server").Value
            frmPrime.DB = dgvConnections.Rows(RowIndex).Cells("db").Value
            frmPrime.DbUser = dgvConnections.Rows(RowIndex).Cells("user").Value
            frmPrime.DbPwd = EnDecrypt.Decrypting(dgvConnections.Rows(RowIndex).Cells("pwd").Value, GlobalKey, "-")
            frmPrime.AppDB = dgvConnections.Rows(RowIndex).Cells("appdb").Value
            frmPrime.AppDate = dgvConnections.Rows(RowIndex).Cells("appdate").Value
            frmPrime.AppUsername = dgvConnections.Rows(RowIndex).Cells("appusername").Value
            frmPrime.AppPassword = EnDecrypt.Decrypting(dgvConnections.Rows(RowIndex).Cells("apppassword").Value, GlobalKey, "-")
            frmPrime.ShowDialog()
            If frmPrime.DB IsNot Nothing AndAlso _
            frmPrime.Server IsNot Nothing AndAlso _
            frmPrime.DB IsNot Nothing AndAlso _
            frmPrime.DbUser IsNot Nothing AndAlso _
            frmPrime.DbPwd IsNot Nothing AndAlso _
            frmPrime.AppUsername IsNot Nothing AndAlso _
            frmPrime.AppPassword IsNot Nothing Then
                PrimeConn.UpdateQuery(dgvConnections.Rows(RowIndex).Cells("connectionname").Value, _
                                      frmPrime.Server, _
                                      frmPrime.DB, _
                                      frmPrime.DbUser, _
                                      EnDecrypt.Encrypting(frmPrime.DbPwd, GlobalKey, "-"), _
                                      frmPrime.AppDB, _
                                      frmPrime.AppDate, _
                                      frmPrime.AppUsername, _
                                      EnDecrypt.Encrypting(frmPrime.AppPassword, GlobalKey, "-"), _
                                      dgvConnections.Rows(RowIndex).Cells("id").Value)
            End If

        ElseIf ConnType = ConnectionType.File Then

            Dim frmFile As New FileF
            frmFile.Path = dgvConnections.Rows(RowIndex).Cells("path").Value
            frmFile.Filename = dgvConnections.Rows(RowIndex).Cells("filename").Value
            frmFile.ShowDialog()
            If frmFile.Path <> "" OrElse _
            frmFile.Filename <> "" Then
                FileConn.UpdateQuery(dgvConnections.Rows(RowIndex).Cells("connectionname").Value, _
                                    frmFile.Path, _
                                    frmFile.Filename, _
                                    dgvConnections.Rows(RowIndex).Cells("id").Value)
            End If

        ElseIf ConnType = ConnectionType.Control Then
            Dim frm As New ControlF
            frm.ShowDialog()

        ElseIf ConnType = 1 Then

            MessageBox.Show("Επιλέξτε τύπο", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

        ElseIf ConnType = ConnectionType.Pylon Then
            Dim frmPylon As New PylonF
            frmPylon.ApplicationName = dgvConnections.Rows(RowIndex).Cells("applicationname").Value
            frmPylon.DbAlias = dgvConnections.Rows(RowIndex).Cells("dbalias").Value
            frmPylon.Extras = dgvConnections.Rows(RowIndex).Cells("extras").Value
            frmPylon.Username = dgvConnections.Rows(RowIndex).Cells("username").Value
            frmPylon.Password = EnDecrypt.Decrypting(dgvConnections.Rows(RowIndex).Cells("password").Value, GlobalKey, "-")
            frmPylon.ShowDialog()
            If frmPylon.DbAlias IsNot Nothing AndAlso _
            frmPylon.ApplicationName IsNot Nothing AndAlso _
            frmPylon.Extras IsNot Nothing AndAlso _
            frmPylon.Username IsNot Nothing AndAlso _
            frmPylon.Password IsNot Nothing Then
                PylonConn.UpdateQuery(dgvConnections.Rows(RowIndex).Cells("connectionname").Value, _
                                      frmPylon.ApplicationName, _
                                      frmPylon.DbAlias, _
                                      frmPylon.Username, _
                                      EnDecrypt.Encrypting(frmPylon.Password, GlobalKey, "-"), _
                                      frmPylon.Extras, _
                                      dgvConnections.Rows(RowIndex).Cells("id").Value)
            End If



        ElseIf ConnType = ConnectionType.VirtueMart Then
            Dim frmVirtuemart As New VirtueMartF
            frmVirtuemart.Username = dgvConnections.Rows(RowIndex).Cells("username").Value
            frmVirtuemart.Password = EnDecrypt.Decrypting(dgvConnections.Rows(RowIndex).Cells("password").Value, GlobalKey, "-")
            frmVirtuemart.ShowDialog()
            If frmVirtuemart.Username IsNot Nothing AndAlso _
            frmVirtuemart.Password IsNot Nothing Then
                VirtuemartConn.UpdateQuery(dgvConnections.Rows(RowIndex).Cells("connectionname").Value, _
                                      frmVirtuemart.Username, _
                                      EnDecrypt.Encrypting(frmVirtuemart.Password, GlobalKey, "-"), _
                                      dgvConnections.Rows(RowIndex).Cells("id").Value)
            End If

        Else

            Dim frmSql As New DbConnForm
            frmSql.Server = dgvConnections.Rows(RowIndex).Cells("server").Value
            frmSql.DB = dgvConnections.Rows(RowIndex).Cells("db").Value
            frmSql.User = dgvConnections.Rows(RowIndex).Cells("user").Value
            frmSql.Pwd = EnDecrypt.Decrypting(dgvConnections.Rows(RowIndex).Cells("pwd").Value, GlobalKey, "-")
            frmSql.WriteToConfig = False
            frmSql.ShowDialog()
            If frmSql.Server IsNot Nothing AndAlso _
            frmSql.DB IsNot Nothing AndAlso _
            frmSql.User IsNot Nothing AndAlso _
            frmSql.Pwd IsNot Nothing Then
                SqlConn.UpdateQuery(dgvConnections.Rows(RowIndex).Cells("connectionname").Value, _
                                    frmSql.Server, _
                                    frmSql.DB, _
                                    frmSql.User, _
                                    EnDecrypt.Encrypting(frmSql.Pwd, GlobalKey, "-"), _
                                    ConnType, _
                                    dgvConnections.Rows(RowIndex).Cells("id").Value)
            End If

        End If

        GetDataSource()

    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If dgvConnections.SelectedCells.Count = 0 Then Exit Sub

        If MessageBox.Show("Θέλετε σίγουρα να διαγράψετε την επιλεγμένη γραμμή;", _
                           "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
            Dim RowIndex As Short
            RowIndex = dgvConnections.SelectedCells(0).RowIndex

            If ConnType = ConnectionType.Sen Then
                SenConn.DeleteQuery(dgvConnections.Rows(RowIndex).Cells("id").Value)
            ElseIf ConnType = ConnectionType.Srs Then
                SrsConn.DeleteQuery(dgvConnections.Rows(RowIndex).Cells("id").Value)
            ElseIf ConnType = ConnectionType.Prime Then
                PrimeConn.DeleteQuery(dgvConnections.Rows(RowIndex).Cells("id").Value)
            ElseIf ConnType = ConnectionType.File Then
                FileConn.DeleteQuery(dgvConnections.Rows(RowIndex).Cells("id").Value)
            Else
                SqlConn.DeleteQuery(dgvConnections.Rows(RowIndex).Cells("id").Value)
            End If

            GetDataSource()
        End If
    End Sub

    Private Sub rdbFs_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbFS.CheckedChanged
        If rdbFS.Checked Then
            ConnType = ConnectionType.Footsteps
            GetDataSource()
        End If
    End Sub

    Private Sub rdbControl_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbControl.CheckedChanged
        If rdbControl.Checked Then
            ConnType = ConnectionType.Control
            GetDataSource()
        End If
    End Sub

    Private Sub rdbSen_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbSen.CheckedChanged
        If rdbSen.Checked Then
            ConnType = ConnectionType.Sen
            GetDataSource()
        End If
    End Sub

    Private Sub rdbSRS_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbSRS.CheckedChanged
        If rdbSRS.Checked Then
            ConnType = ConnectionType.Srs
            GetDataSource()
        End If
    End Sub

    Private Sub rdbOtr_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbOtr.CheckedChanged
        If rdbOtr.Checked Then
            ConnType = ConnectionType.Otr
            GetDataSource()
        End If
    End Sub

    Private Sub rdbPylon_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbPylon.CheckedChanged
        If rdbPylon.Checked Then
            ConnType = ConnectionType.Pylon
            GetDataSource()
        End If
    End Sub

    Private Sub rdbOtherSQL_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbOtherSql.CheckedChanged
        If rdbOtherSql.Checked Then
            ConnType = ConnectionType.OtherSQL
            GetDataSource()
        End If
    End Sub

    Private Sub rdbInventory_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbInventory.CheckedChanged
        If rdbInventory.Checked Then
            ConnType = ConnectionType.Inventory
            GetDataSource()
        End If
    End Sub

    Private Sub rdbPrime_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbPrime.CheckedChanged
        If rdbPrime.Checked Then
            ConnType = ConnectionType.Prime
            GetDataSource()
        End If
    End Sub

    Private Sub rdbVirtuemart_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbVM.CheckedChanged
        If rdbVM.Checked Then
            ConnType = ConnectionType.VirtueMart
            GetDataSource()
        End If
    End Sub

    Private Sub rdbGalaxy_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbGalaxy.CheckedChanged
        If rdbGalaxy.Checked Then
            ConnType = ConnectionType.Galaxy
            GetDataSource()
        End If
    End Sub

    Private Sub rdbFile_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbFile.CheckedChanged
        If rdbFile.Checked Then
            ConnType = ConnectionType.File
            GetDataSource()
        End If
    End Sub

    Private Sub rdbSvConn_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rdbSvConn.CheckedChanged
        If rdbSvConn.Checked Then
            ConnType = ConnectionType.SvConn
            GetDataSource()
        End If
    End Sub
End Class

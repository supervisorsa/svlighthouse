﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ToolsF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtOrderNo = New System.Windows.Forms.TextBox
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rdbDestination = New System.Windows.Forms.RadioButton
        Me.rdbSource = New System.Windows.Forms.RadioButton
        Me.btnOrderRelease = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'txtOrderNo
        '
        Me.txtOrderNo.Location = New System.Drawing.Point(266, 43)
        Me.txtOrderNo.Name = "txtOrderNo"
        Me.txtOrderNo.Size = New System.Drawing.Size(134, 22)
        Me.txtOrderNo.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdbDestination)
        Me.GroupBox1.Controls.Add(Me.rdbSource)
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(12, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(233, 56)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Αριθμός παραγγελίας από :"
        '
        'rdbDestination
        '
        Me.rdbDestination.AutoSize = True
        Me.rdbDestination.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.rdbDestination.Location = New System.Drawing.Point(126, 31)
        Me.rdbDestination.Name = "rdbDestination"
        Me.rdbDestination.Size = New System.Drawing.Size(89, 18)
        Me.rdbDestination.TabIndex = 3
        Me.rdbDestination.Text = "Προορισμός"
        Me.rdbDestination.UseVisualStyleBackColor = True
        '
        'rdbSource
        '
        Me.rdbSource.AutoSize = True
        Me.rdbSource.Checked = True
        Me.rdbSource.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.rdbSource.Location = New System.Drawing.Point(21, 31)
        Me.rdbSource.Name = "rdbSource"
        Me.rdbSource.Size = New System.Drawing.Size(53, 18)
        Me.rdbSource.TabIndex = 2
        Me.rdbSource.TabStop = True
        Me.rdbSource.Text = "Πηγή"
        Me.rdbSource.UseVisualStyleBackColor = True
        '
        'btnOrderRelease
        '
        Me.btnOrderRelease.Location = New System.Drawing.Point(426, 16)
        Me.btnOrderRelease.Name = "btnOrderRelease"
        Me.btnOrderRelease.Size = New System.Drawing.Size(155, 52)
        Me.btnOrderRelease.TabIndex = 3
        Me.btnOrderRelease.Text = "Επαναφορά παραγγελίας για εισαγωγή"
        Me.btnOrderRelease.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label1.Location = New System.Drawing.Point(263, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 14)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Αριθμός παραγγελίας"
        '
        'ToolsF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(595, 128)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnOrderRelease)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtOrderNo)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Name = "ToolsF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Εργαλεία δεδομένων"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtOrderNo As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbDestination As System.Windows.Forms.RadioButton
    Friend WithEvents rdbSource As System.Windows.Forms.RadioButton
    Friend WithEvents btnOrderRelease As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class

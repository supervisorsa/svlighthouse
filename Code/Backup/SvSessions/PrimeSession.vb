﻿Imports Commercial

Public Class PrimeSession

    Public Function ConnectToPrime(ByRef ErrorMessage As String, _
                                  ByVal DB As String, _
                                  ByVal AppDate As Date, _
                                  ByVal AppUsername As String, _
                                  ByVal AppPassword As String) As ICLASession
        Try
            Dim pApp As CLApplication
            Dim pSess As ICLASession

            pApp = New CLApplication
            pSess = pApp.CLSession

            Dim clRes As Long

            clRes = pSess.Login(DB, CStr(AppDate), AppUsername, AppPassword)
            If clRes <> 0 Or Err.Number <> 0 Then
                Throw New Exception("Σφάλμα: " & Err.Description)
            End If
            Return pSess
        Catch ex As Exception
            ErrorMessage = ex.Message
            Return Nothing
        End Try

    End Function

    Public Function AvailableDBs(ByRef ErrorMessage As String) As List(Of String)
        Dim AllDBs As New List(Of String)
        ErrorMessage = ""

        Try
            Dim pApp As CLApplication
            Dim pSess As ICLASession

            pApp = New CLApplication
            pSess = pApp.CLSession

            For Each vComp In pSess.AvailableDBs
                AllDBs.Add(vComp)
            Next vComp
        Catch ex As Exception
            ErrorMessage = ex.Message
            Return Nothing
        End Try

        Return AllDBs
    End Function
End Class

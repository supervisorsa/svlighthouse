﻿Public Class PylonF
    Private _DbAlias As String
    Public Property DbAlias() As String
        Get
            Return _DbAlias
        End Get
        Set(ByVal value As String)
            _DbAlias = value
        End Set
    End Property

    Private _ApplicationName As String
    Public Property ApplicationName() As String
        Get
            Return _ApplicationName
        End Get
        Set(ByVal value As String)
            _ApplicationName = value
        End Set
    End Property

    Private _Extras As String
    Public Property Extras() As String
        Get
            Return _Extras
        End Get
        Set(ByVal value As String)
            _Extras = value
        End Set
    End Property

    Private _Username As String
    Public Property Username() As String
        Get
            Return _Username
        End Get
        Set(ByVal value As String)
            _Username = value
        End Set
    End Property

    Private _Password As String
    Public Property Password() As String
        Get
            Return _Password
        End Get
        Set(ByVal value As String)
            _Password = value
        End Set
    End Property

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        _DbAlias = txtDbAlias.Text
        _ApplicationName = txtApplicationName.Text
        _Extras = txtExtras.Text
        _Username = txtUsername.Text
        _Password = txtPassword.Text
        Close()
    End Sub

    Private Sub PylonForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        txtDbAlias.Text = _DbAlias
        txtApplicationName.Text = _ApplicationName
        txtExtras.Text = _Extras
        txtUsername.Text = _Username
        txtPassword.Text = _Password
    End Sub

End Class
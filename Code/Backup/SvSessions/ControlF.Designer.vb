﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ControlF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnConnectServ = New System.Windows.Forms.Button
        Me.btnConnectWorkArea = New System.Windows.Forms.Button
        Me.Label1 = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.Label4 = New System.Windows.Forms.Label
        Me.Label5 = New System.Windows.Forms.Label
        Me.Label6 = New System.Windows.Forms.Label
        Me.Label7 = New System.Windows.Forms.Label
        Me.edCompanies = New System.Windows.Forms.ListBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.chkControl2 = New System.Windows.Forms.CheckBox
        Me.edWorkAreas = New System.Windows.Forms.ComboBox
        Me.edAppServ = New System.Windows.Forms.TextBox
        Me.TextBox2 = New System.Windows.Forms.TextBox
        Me.edUser = New System.Windows.Forms.TextBox
        Me.edPwd = New System.Windows.Forms.TextBox
        Me.TextBox1 = New System.Windows.Forms.TextBox
        Me.SuspendLayout()
        '
        'btnConnectServ
        '
        Me.btnConnectServ.Location = New System.Drawing.Point(440, 48)
        Me.btnConnectServ.Name = "btnConnectServ"
        Me.btnConnectServ.Size = New System.Drawing.Size(75, 23)
        Me.btnConnectServ.TabIndex = 0
        Me.btnConnectServ.Text = "Button1"
        Me.btnConnectServ.UseVisualStyleBackColor = True
        '
        'btnConnectWorkArea
        '
        Me.btnConnectWorkArea.Location = New System.Drawing.Point(440, 97)
        Me.btnConnectWorkArea.Name = "btnConnectWorkArea"
        Me.btnConnectWorkArea.Size = New System.Drawing.Size(75, 23)
        Me.btnConnectWorkArea.TabIndex = 1
        Me.btnConnectWorkArea.Text = "Button2"
        Me.btnConnectWorkArea.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label1.Location = New System.Drawing.Point(42, 24)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(47, 14)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Label1"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label2.Location = New System.Drawing.Point(42, 57)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(47, 14)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Label2"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label3.Location = New System.Drawing.Point(42, 97)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(47, 14)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Label3"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label4.Location = New System.Drawing.Point(42, 133)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(47, 14)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Label4"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label5.Location = New System.Drawing.Point(42, 169)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(47, 14)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Label5"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label6.Location = New System.Drawing.Point(42, 204)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(47, 14)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Label6"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label7.Location = New System.Drawing.Point(42, 243)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(47, 14)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Label7"
        '
        'edCompanies
        '
        Me.edCompanies.FormattingEnabled = True
        Me.edCompanies.ItemHeight = 14
        Me.edCompanies.Location = New System.Drawing.Point(45, 260)
        Me.edCompanies.Name = "edCompanies"
        Me.edCompanies.Size = New System.Drawing.Size(337, 102)
        Me.edCompanies.TabIndex = 9
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(184, 368)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(75, 23)
        Me.Button3.TabIndex = 10
        Me.Button3.Text = "Button3"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'chkControl2
        '
        Me.chkControl2.AutoSize = True
        Me.chkControl2.Location = New System.Drawing.Point(142, 237)
        Me.chkControl2.Name = "chkControl2"
        Me.chkControl2.Size = New System.Drawing.Size(86, 18)
        Me.chkControl2.TabIndex = 11
        Me.chkControl2.Text = "CheckBox1"
        Me.chkControl2.UseVisualStyleBackColor = True
        '
        'edWorkAreas
        '
        Me.edWorkAreas.FormattingEnabled = True
        Me.edWorkAreas.Location = New System.Drawing.Point(163, 97)
        Me.edWorkAreas.Name = "edWorkAreas"
        Me.edWorkAreas.Size = New System.Drawing.Size(121, 22)
        Me.edWorkAreas.TabIndex = 12
        '
        'edAppServ
        '
        Me.edAppServ.Location = New System.Drawing.Point(159, 21)
        Me.edAppServ.Name = "edAppServ"
        Me.edAppServ.Size = New System.Drawing.Size(100, 22)
        Me.edAppServ.TabIndex = 13
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(159, 54)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(100, 22)
        Me.TextBox2.TabIndex = 14
        '
        'edUser
        '
        Me.edUser.Location = New System.Drawing.Point(163, 130)
        Me.edUser.Name = "edUser"
        Me.edUser.Size = New System.Drawing.Size(100, 22)
        Me.edUser.TabIndex = 15
        '
        'edPwd
        '
        Me.edPwd.Location = New System.Drawing.Point(163, 166)
        Me.edPwd.Name = "edPwd"
        Me.edPwd.Size = New System.Drawing.Size(100, 22)
        Me.edPwd.TabIndex = 16
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(163, 201)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 22)
        Me.TextBox1.TabIndex = 17
        '
        'ControlF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(530, 403)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.edPwd)
        Me.Controls.Add(Me.edUser)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.edAppServ)
        Me.Controls.Add(Me.edWorkAreas)
        Me.Controls.Add(Me.chkControl2)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.edCompanies)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnConnectWorkArea)
        Me.Controls.Add(Me.btnConnectServ)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Name = "ControlF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ControlF"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnConnectServ As System.Windows.Forms.Button
    Friend WithEvents btnConnectWorkArea As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents edCompanies As System.Windows.Forms.ListBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents chkControl2 As System.Windows.Forms.CheckBox
    Friend WithEvents edWorkAreas As System.Windows.Forms.ComboBox
    Friend WithEvents edAppServ As System.Windows.Forms.TextBox
    Friend WithEvents TextBox2 As System.Windows.Forms.TextBox
    Friend WithEvents edUser As System.Windows.Forms.TextBox
    Friend WithEvents edPwd As System.Windows.Forms.TextBox
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
End Class

﻿Imports Microsoft.VisualBasic.Compatibility

Public Class ControlF
    Dim lngJobID As Long


    Private _Host As String
    Public Property Host() As String
        Get
            Return _Host
        End Get
        Set(ByVal value As String)
            _Host = value
        End Set
    End Property


    Private _Port As String
    Public Property Port() As String
        Get
            Return _Port
        End Get
        Set(ByVal value As String)
            _Port = value
        End Set
    End Property


    Private _User As String
    Public Property User() As String
        Get
            Return _User
        End Get
        Set(ByVal value As String)
            _User = value
        End Set
    End Property


    Private _Pwd As String
    Public Property Pwd() As String
        Get
            Return _Pwd
        End Get
        Set(ByVal value As String)
            _Pwd = value
        End Set
    End Property

    Private _WorkArea As String
    Public Property WorkArea() As String
        Get
            Return _WorkArea
        End Get
        Set(ByVal value As String)
            _WorkArea = value
        End Set
    End Property

    Private _Company As String
    Public Property Company() As String
        Get
            Return _Company
        End Get
        Set(ByVal value As String)
            _Company = value
        End Set
    End Property

    Private _Cmp As Integer
    Public Property Cmp() As Integer
        Get
            Return _Cmp
        End Get
        Set(ByVal value As Integer)
            _Cmp = value
        End Set
    End Property

    Private _CmpName As String
    Public Property CmpName() As String
        Get
            Return _CmpName
        End Get
        Set(ByVal value As String)
            _CmpName = value
        End Set
    End Property

    Private _Cmp1 As Integer
    Public Property Cmp1() As Integer
        Get
            Return _Cmp1
        End Get
        Set(ByVal value As Integer)
            _Cmp1 = value
        End Set
    End Property

    Private _IsControl2 As Boolean
    Public Property IsControl2() As Boolean
        Get
            Return _IsControl2
        End Get
        Set(ByVal value As Boolean)
            _IsControl2 = value
        End Set
    End Property


    'Dim adoSel As New ADODB.Connection
    'Dim rdsPrms As New ADODB.Recordset
    Public Property GetJobId() As Long
        Get
            GetJobId = lngJobID
        End Get
        Set(ByVal Value As Long)
            lngJobID = Value
        End Set
    End Property

    Private Function GetWorkAreaNames(ByVal asrvname As String) As String()
        Dim locCon As Object
        locCon = Nothing

        Try
            locCon = CreateObject("MSFTAXM.SngConnection")
            locCon.ServerGUID = "{8D43CD03-218A-11D2-B8B7-008048E56968}"
            locCon.ServerName = asrvname
            locCon.ComputerName = Me.TextBox2.Text
            locCon.Connected = True
            Return locCon.AppServer.WorkAreaNames
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        Finally

            locCon.Connected = False
            locCon = Nothing
            System.GC.Collect()
        End Try
    End Function
    Private Function GetCompanyNames(ByVal asrvname As String, ByVal aworkarea As String) As String(,)
        Dim locCon As Object

        Dim sql As String
        Dim i As Integer
        Dim rS As Object

        Try
            locCon = CreateObject("MSFTAXM.SngConnection")
            locCon.ServerGUID = "{8D43CD03-218A-11D2-B8B7-008048E56968}"
            locCon.ServerName = asrvname
            locCon.ComputerName = Me.TextBox2.Text
            locCon.Connected = True
            locCon.AppServer.WorkAreaConnection(aworkarea, "XLSAPP", "")
            rS = locCon.GetRecordSet("", "")
            sql = "SELECT ID, ABBREVIATION, NAME FROM COM"

            rS.Data = locCon.AppServer.Call("OpenSQL", New Object() {sql, DBNull.Value})
            Dim comnames(1, 1) As String
            If rS.Active Then
                ReDim comnames(rS.RecordCount, 2)

                i = 0
                rS.First()
                While Not rS.EOF
                    comnames(i, 0) = CType(rS.FieldValue("ID"), String)
                    comnames(i, 1) = CStr(rS.FieldValue("ABBREVIATION")) & "-" & CStr(rS.FieldValue("NAME"))
                    rS.Next()
                    i = i + 1
                End While
            End If
            Return comnames
        Catch ex As Exception
            MsgBox("Αδύνατη η σύνδεση με τον Singular Application Server" & vbCrLf & ex.Message)
        Finally
            rS = Nothing
            locCon = Nothing
            System.GC.Collect()
        End Try
    End Function

    Private Sub SetEnabled(ByVal doenable As Boolean)
        Me.btnConnectWorkArea.Enabled = doenable
        Me.edWorkAreas.Enabled = doenable
        Me.edCompanies.Enabled = doenable
        Me.edPwd.Enabled = doenable
        Me.edUser.Enabled = doenable
    End Sub

    Private Sub btnConnectServ_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnectServ.Click
        Dim Lst() As String
        Dim i As Integer
        Me.btnConnectWorkArea.Enabled = True
        Me.edWorkAreas.Enabled = True
        Lst = GetWorkAreaNames(Me.edAppServ.Text)
        For i = 0 To UBound(Lst)
            Me.edWorkAreas.Items.Add(Lst(i))
        Next i
    End Sub

    Private Sub btnConnectWorkArea_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnectWorkArea.Click
        Dim Lst(,) As String
        Dim i As Integer
        Me.edCompanies.Items.Clear()
        Lst = GetCompanyNames(Me.edAppServ.Text, Me.edWorkAreas.Text)
        Try
            For i = 0 To UBound(Lst) - 1
                Me.edCompanies.Items.Add(Lst(i, 1))
                VB6.SetItemData(Me.edCompanies, i, CType(Lst(i, 0), Integer))
            Next
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try
        SetEnabled(True)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Dim indexes As ListBox.SelectedIndexCollection = Me.edCompanies.SelectedIndices
        Dim index As Integer
        If MsgBox("Να αποθηκευθούν οι αλλαγές ;", vbQuestion + vbYesNo) = vbYes Then
            Try
                If _Cmp1 = 0 Then
                    For Each index In indexes
                        _Cmp1 = CType(Me.edCompanies.Items(index), VB6.ListBoxItem).ItemData
                    Next
                End If
                _Host = Me.edAppServ.Text
                _WorkArea = Me.edWorkAreas.Text
                _User = Me.edUser.Text
                _Pwd = Me.edPwd.Text
                _Port = Me.TextBox2.Text
                _Company = Me.TextBox1.Text
                For Each index In indexes
                    _CmpName = Me.edCompanies.Items(index)
                    _Cmp = CType(Me.edCompanies.Items(index), VB6.ListBoxItem).ItemData
                Next
                _IsControl2 = chkControl2.Checked
            Catch ex As Exception
                MsgBox(Err.Number & " " & ex.Message, MsgBoxStyle.Exclamation)
            End Try
        End If
        Me.DialogResult = DialogResult.OK
    End Sub
    Private Sub ReadControlParams()
        Me.edAppServ.Text = _Host
        Me.edUser.Text = _User
        Me.edPwd.Text = _Pwd
        Me.TextBox2.Text = _Port
        Me.chkControl2.Checked = _IsControl2
    End Sub
    Private Sub ControlParams_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            ReadControlParams()
        Catch ex As Exception
            MsgBox(Err.Number & " " & ex.Message, MsgBoxStyle.Exclamation)
        End Try
    End Sub

End Class

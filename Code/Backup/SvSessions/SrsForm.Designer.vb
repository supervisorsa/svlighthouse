﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SrsForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SrsForm))
        Me.txtPWD = New System.Windows.Forms.TextBox
        Me.txtUser = New System.Windows.Forms.TextBox
        Me.lblPwd = New System.Windows.Forms.Label
        Me.lblUser = New System.Windows.Forms.Label
        Me.txtPort = New System.Windows.Forms.TextBox
        Me.txtHost = New System.Windows.Forms.TextBox
        Me.lblPort = New System.Windows.Forms.Label
        Me.lblHost = New System.Windows.Forms.Label
        Me.lblAlias = New System.Windows.Forms.Label
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnCreate = New System.Windows.Forms.Button
        Me.cmbAlias = New System.Windows.Forms.ComboBox
        Me.chkManual = New System.Windows.Forms.CheckBox
        Me.btnSrsLogin = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtPWD
        '
        Me.txtPWD.Location = New System.Drawing.Point(116, 190)
        Me.txtPWD.Name = "txtPWD"
        Me.txtPWD.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPWD.Size = New System.Drawing.Size(243, 22)
        Me.txtPWD.TabIndex = 5
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(116, 154)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(243, 22)
        Me.txtUser.TabIndex = 4
        '
        'lblPwd
        '
        Me.lblPwd.AutoSize = True
        Me.lblPwd.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblPwd.Location = New System.Drawing.Point(14, 193)
        Me.lblPwd.Name = "lblPwd"
        Me.lblPwd.Size = New System.Drawing.Size(65, 14)
        Me.lblPwd.TabIndex = 79
        Me.lblPwd.Text = "Κωδικός :"
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblUser.Location = New System.Drawing.Point(14, 157)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(68, 14)
        Me.lblUser.TabIndex = 78
        Me.lblUser.Text = "Χρήστης :"
        '
        'txtPort
        '
        Me.txtPort.Location = New System.Drawing.Point(116, 83)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(243, 22)
        Me.txtPort.TabIndex = 2
        '
        'txtHost
        '
        Me.txtHost.Location = New System.Drawing.Point(116, 50)
        Me.txtHost.Name = "txtHost"
        Me.txtHost.Size = New System.Drawing.Size(243, 22)
        Me.txtHost.TabIndex = 1
        '
        'lblPort
        '
        Me.lblPort.AutoSize = True
        Me.lblPort.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblPort.Location = New System.Drawing.Point(14, 86)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(42, 14)
        Me.lblPort.TabIndex = 83
        Me.lblPort.Text = "Port :"
        '
        'lblHost
        '
        Me.lblHost.AutoSize = True
        Me.lblHost.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblHost.Location = New System.Drawing.Point(14, 50)
        Me.lblHost.Name = "lblHost"
        Me.lblHost.Size = New System.Drawing.Size(44, 14)
        Me.lblHost.TabIndex = 82
        Me.lblHost.Text = "Host :"
        '
        'lblAlias
        '
        Me.lblAlias.AutoSize = True
        Me.lblAlias.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblAlias.Location = New System.Drawing.Point(14, 122)
        Me.lblAlias.Name = "lblAlias"
        Me.lblAlias.Size = New System.Drawing.Size(68, 14)
        Me.lblAlias.TabIndex = 90
        Me.lblAlias.Text = "Sas Alias :"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(17, 359)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(342, 45)
        Me.btnExit.TabIndex = 13
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnCreate
        '
        Me.btnCreate.Location = New System.Drawing.Point(17, 295)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(342, 45)
        Me.btnCreate.TabIndex = 12
        Me.btnCreate.Text = "Αποθήκευση αλλαγών"
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'cmbAlias
        '
        Me.cmbAlias.FormattingEnabled = True
        Me.cmbAlias.Location = New System.Drawing.Point(116, 119)
        Me.cmbAlias.Name = "cmbAlias"
        Me.cmbAlias.Size = New System.Drawing.Size(243, 22)
        Me.cmbAlias.TabIndex = 3
        '
        'chkManual
        '
        Me.chkManual.AutoSize = True
        Me.chkManual.Location = New System.Drawing.Point(17, 12)
        Me.chkManual.Name = "chkManual"
        Me.chkManual.Size = New System.Drawing.Size(178, 18)
        Me.chkManual.TabIndex = 0
        Me.chkManual.Text = "Χωρίς επιβεβαίωση σύνδεσης"
        Me.chkManual.UseVisualStyleBackColor = True
        '
        'btnSrsLogin
        '
        Me.btnSrsLogin.Location = New System.Drawing.Point(17, 234)
        Me.btnSrsLogin.Name = "btnSrsLogin"
        Me.btnSrsLogin.Size = New System.Drawing.Size(342, 45)
        Me.btnSrsLogin.TabIndex = 6
        Me.btnSrsLogin.Text = "Είσοδος στο SRS"
        Me.btnSrsLogin.UseVisualStyleBackColor = True
        '
        'SrsForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(380, 410)
        Me.Controls.Add(Me.btnSrsLogin)
        Me.Controls.Add(Me.chkManual)
        Me.Controls.Add(Me.cmbAlias)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.lblAlias)
        Me.Controls.Add(Me.txtPort)
        Me.Controls.Add(Me.txtHost)
        Me.Controls.Add(Me.lblPort)
        Me.Controls.Add(Me.lblHost)
        Me.Controls.Add(Me.txtPWD)
        Me.Controls.Add(Me.txtUser)
        Me.Controls.Add(Me.lblPwd)
        Me.Controls.Add(Me.lblUser)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "SrsForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Σύνδεση SRS"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPWD As System.Windows.Forms.TextBox
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents lblPwd As System.Windows.Forms.Label
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents txtPort As System.Windows.Forms.TextBox
    Friend WithEvents txtHost As System.Windows.Forms.TextBox
    Friend WithEvents lblPort As System.Windows.Forms.Label
    Friend WithEvents lblHost As System.Windows.Forms.Label
    Friend WithEvents lblAlias As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnCreate As System.Windows.Forms.Button
    Friend WithEvents cmbAlias As System.Windows.Forms.ComboBox
    Friend WithEvents chkManual As System.Windows.Forms.CheckBox
    Friend WithEvents btnSrsLogin As System.Windows.Forms.Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SenForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SenForm))
        Me.txtPWD = New System.Windows.Forms.TextBox
        Me.txtUser = New System.Windows.Forms.TextBox
        Me.lblPwd = New System.Windows.Forms.Label
        Me.lblUser = New System.Windows.Forms.Label
        Me.txtPort = New System.Windows.Forms.TextBox
        Me.txtHost = New System.Windows.Forms.TextBox
        Me.lblPort = New System.Windows.Forms.Label
        Me.lblHost = New System.Windows.Forms.Label
        Me.lblWarehouse = New System.Windows.Forms.Label
        Me.lblCompany = New System.Windows.Forms.Label
        Me.lblAlias = New System.Windows.Forms.Label
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnCreate = New System.Windows.Forms.Button
        Me.cmbAlias = New System.Windows.Forms.ComboBox
        Me.chkManual = New System.Windows.Forms.CheckBox
        Me.cmbCompanies = New System.Windows.Forms.ComboBox
        Me.cmbFiscal = New System.Windows.Forms.ComboBox
        Me.cmbWarehouses = New System.Windows.Forms.ComboBox
        Me.btnSenConnection = New System.Windows.Forms.Button
        Me.lblDate = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.dteDate = New System.Windows.Forms.DateTimePicker
        Me.btnSenLogin = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'txtPWD
        '
        Me.txtPWD.Location = New System.Drawing.Point(116, 190)
        Me.txtPWD.Name = "txtPWD"
        Me.txtPWD.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPWD.Size = New System.Drawing.Size(243, 22)
        Me.txtPWD.TabIndex = 5
        '
        'txtUser
        '
        Me.txtUser.Location = New System.Drawing.Point(116, 154)
        Me.txtUser.Name = "txtUser"
        Me.txtUser.Size = New System.Drawing.Size(243, 22)
        Me.txtUser.TabIndex = 4
        '
        'lblPwd
        '
        Me.lblPwd.AutoSize = True
        Me.lblPwd.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblPwd.Location = New System.Drawing.Point(14, 193)
        Me.lblPwd.Name = "lblPwd"
        Me.lblPwd.Size = New System.Drawing.Size(65, 14)
        Me.lblPwd.TabIndex = 79
        Me.lblPwd.Text = "Κωδικός :"
        '
        'lblUser
        '
        Me.lblUser.AutoSize = True
        Me.lblUser.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblUser.Location = New System.Drawing.Point(14, 157)
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(68, 14)
        Me.lblUser.TabIndex = 78
        Me.lblUser.Text = "Χρήστης :"
        '
        'txtPort
        '
        Me.txtPort.Location = New System.Drawing.Point(116, 83)
        Me.txtPort.Name = "txtPort"
        Me.txtPort.Size = New System.Drawing.Size(243, 22)
        Me.txtPort.TabIndex = 2
        '
        'txtHost
        '
        Me.txtHost.Location = New System.Drawing.Point(116, 50)
        Me.txtHost.Name = "txtHost"
        Me.txtHost.Size = New System.Drawing.Size(243, 22)
        Me.txtHost.TabIndex = 1
        '
        'lblPort
        '
        Me.lblPort.AutoSize = True
        Me.lblPort.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblPort.Location = New System.Drawing.Point(14, 86)
        Me.lblPort.Name = "lblPort"
        Me.lblPort.Size = New System.Drawing.Size(42, 14)
        Me.lblPort.TabIndex = 83
        Me.lblPort.Text = "Port :"
        '
        'lblHost
        '
        Me.lblHost.AutoSize = True
        Me.lblHost.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblHost.Location = New System.Drawing.Point(14, 50)
        Me.lblHost.Name = "lblHost"
        Me.lblHost.Size = New System.Drawing.Size(44, 14)
        Me.lblHost.TabIndex = 82
        Me.lblHost.Text = "Host :"
        '
        'lblWarehouse
        '
        Me.lblWarehouse.AutoSize = True
        Me.lblWarehouse.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblWarehouse.Location = New System.Drawing.Point(381, 122)
        Me.lblWarehouse.Name = "lblWarehouse"
        Me.lblWarehouse.Size = New System.Drawing.Size(40, 14)
        Me.lblWarehouse.TabIndex = 87
        Me.lblWarehouse.Text = "Α.Χ. :"
        '
        'lblCompany
        '
        Me.lblCompany.AutoSize = True
        Me.lblCompany.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblCompany.Location = New System.Drawing.Point(381, 53)
        Me.lblCompany.Name = "lblCompany"
        Me.lblCompany.Size = New System.Drawing.Size(59, 14)
        Me.lblCompany.TabIndex = 86
        Me.lblCompany.Text = "Εταιρία :"
        '
        'lblAlias
        '
        Me.lblAlias.AutoSize = True
        Me.lblAlias.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblAlias.Location = New System.Drawing.Point(14, 122)
        Me.lblAlias.Name = "lblAlias"
        Me.lblAlias.Size = New System.Drawing.Size(68, 14)
        Me.lblAlias.TabIndex = 90
        Me.lblAlias.Text = "Sas Alias :"
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(17, 297)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(342, 45)
        Me.btnExit.TabIndex = 13
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnCreate
        '
        Me.btnCreate.Location = New System.Drawing.Point(384, 297)
        Me.btnCreate.Name = "btnCreate"
        Me.btnCreate.Size = New System.Drawing.Size(342, 45)
        Me.btnCreate.TabIndex = 12
        Me.btnCreate.Text = "Αποθήκευση αλλαγών"
        Me.btnCreate.UseVisualStyleBackColor = True
        '
        'cmbAlias
        '
        Me.cmbAlias.FormattingEnabled = True
        Me.cmbAlias.Location = New System.Drawing.Point(116, 119)
        Me.cmbAlias.Name = "cmbAlias"
        Me.cmbAlias.Size = New System.Drawing.Size(243, 22)
        Me.cmbAlias.TabIndex = 3
        '
        'chkManual
        '
        Me.chkManual.AutoSize = True
        Me.chkManual.Location = New System.Drawing.Point(17, 12)
        Me.chkManual.Name = "chkManual"
        Me.chkManual.Size = New System.Drawing.Size(178, 18)
        Me.chkManual.TabIndex = 0
        Me.chkManual.Text = "Χωρίς επιβεβαίωση σύνδεσης"
        Me.chkManual.UseVisualStyleBackColor = True
        '
        'cmbCompanies
        '
        Me.cmbCompanies.FormattingEnabled = True
        Me.cmbCompanies.Location = New System.Drawing.Point(483, 50)
        Me.cmbCompanies.Name = "cmbCompanies"
        Me.cmbCompanies.Size = New System.Drawing.Size(243, 22)
        Me.cmbCompanies.TabIndex = 7
        '
        'cmbFiscal
        '
        Me.cmbFiscal.FormattingEnabled = True
        Me.cmbFiscal.Location = New System.Drawing.Point(483, 83)
        Me.cmbFiscal.Name = "cmbFiscal"
        Me.cmbFiscal.Size = New System.Drawing.Size(243, 22)
        Me.cmbFiscal.TabIndex = 8
        '
        'cmbWarehouses
        '
        Me.cmbWarehouses.FormattingEnabled = True
        Me.cmbWarehouses.Location = New System.Drawing.Point(483, 119)
        Me.cmbWarehouses.Name = "cmbWarehouses"
        Me.cmbWarehouses.Size = New System.Drawing.Size(243, 22)
        Me.cmbWarehouses.TabIndex = 9
        '
        'btnSenConnection
        '
        Me.btnSenConnection.Location = New System.Drawing.Point(384, 234)
        Me.btnSenConnection.Name = "btnSenConnection"
        Me.btnSenConnection.Size = New System.Drawing.Size(342, 45)
        Me.btnSenConnection.TabIndex = 11
        Me.btnSenConnection.Text = "Έλεγχος σύνδεσης SEN"
        Me.btnSenConnection.UseVisualStyleBackColor = True
        '
        'lblDate
        '
        Me.lblDate.AutoSize = True
        Me.lblDate.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblDate.Location = New System.Drawing.Point(381, 157)
        Me.lblDate.Name = "lblDate"
        Me.lblDate.Size = New System.Drawing.Size(88, 14)
        Me.lblDate.TabIndex = 111
        Me.lblDate.Text = "Ημερομηνία :"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label1.Location = New System.Drawing.Point(381, 86)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(55, 14)
        Me.Label1.TabIndex = 112
        Me.Label1.Text = "Χρήση :"
        '
        'dteDate
        '
        Me.dteDate.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dteDate.Location = New System.Drawing.Point(483, 151)
        Me.dteDate.Name = "dteDate"
        Me.dteDate.Size = New System.Drawing.Size(243, 22)
        Me.dteDate.TabIndex = 10
        '
        'btnSenLogin
        '
        Me.btnSenLogin.Location = New System.Drawing.Point(17, 234)
        Me.btnSenLogin.Name = "btnSenLogin"
        Me.btnSenLogin.Size = New System.Drawing.Size(342, 45)
        Me.btnSenLogin.TabIndex = 6
        Me.btnSenLogin.Text = "Είσοδος στο SEN"
        Me.btnSenLogin.UseVisualStyleBackColor = True
        '
        'SenForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(741, 352)
        Me.Controls.Add(Me.btnSenLogin)
        Me.Controls.Add(Me.dteDate)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.lblDate)
        Me.Controls.Add(Me.cmbWarehouses)
        Me.Controls.Add(Me.cmbFiscal)
        Me.Controls.Add(Me.cmbCompanies)
        Me.Controls.Add(Me.chkManual)
        Me.Controls.Add(Me.cmbAlias)
        Me.Controls.Add(Me.btnSenConnection)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnCreate)
        Me.Controls.Add(Me.lblAlias)
        Me.Controls.Add(Me.lblWarehouse)
        Me.Controls.Add(Me.lblCompany)
        Me.Controls.Add(Me.txtPort)
        Me.Controls.Add(Me.txtHost)
        Me.Controls.Add(Me.lblPort)
        Me.Controls.Add(Me.lblHost)
        Me.Controls.Add(Me.txtPWD)
        Me.Controls.Add(Me.txtUser)
        Me.Controls.Add(Me.lblPwd)
        Me.Controls.Add(Me.lblUser)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "SenForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Σύνδεση SEN"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtPWD As System.Windows.Forms.TextBox
    Friend WithEvents txtUser As System.Windows.Forms.TextBox
    Friend WithEvents lblPwd As System.Windows.Forms.Label
    Friend WithEvents lblUser As System.Windows.Forms.Label
    Friend WithEvents txtPort As System.Windows.Forms.TextBox
    Friend WithEvents txtHost As System.Windows.Forms.TextBox
    Friend WithEvents lblPort As System.Windows.Forms.Label
    Friend WithEvents lblHost As System.Windows.Forms.Label
    Friend WithEvents lblWarehouse As System.Windows.Forms.Label
    Friend WithEvents lblCompany As System.Windows.Forms.Label
    Friend WithEvents lblAlias As System.Windows.Forms.Label
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnCreate As System.Windows.Forms.Button
    Friend WithEvents cmbAlias As System.Windows.Forms.ComboBox
    Friend WithEvents chkManual As System.Windows.Forms.CheckBox
    Friend WithEvents cmbCompanies As System.Windows.Forms.ComboBox
    Friend WithEvents cmbFiscal As System.Windows.Forms.ComboBox
    Friend WithEvents cmbWarehouses As System.Windows.Forms.ComboBox
    Friend WithEvents btnSenConnection As System.Windows.Forms.Button
    Friend WithEvents lblDate As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents dteDate As System.Windows.Forms.DateTimePicker
    Friend WithEvents btnSenLogin As System.Windows.Forms.Button
End Class

﻿Public Class StockKeepingUnit

    Private _SvProductCode As svProductCode
    Public Property SvProductCode() As svProductCode
        Get
            Return _SvProductCode
        End Get
        Set(ByVal value As svProductCode)
            _SvProductCode = value
        End Set
    End Property

    Private _WarehouseID As String
    Public WriteOnly Property WarehouseID() As String
        Set(ByVal value As String)
            _WarehouseID = value
        End Set
    End Property

    Public Sub ImportStock(ByVal IsNew As Boolean)

        ProductId = _SvProductCode.ProductSourceID

        UpdatedOn = Now
        UpdatedStockOn = UpdatedOn
        UpdatedPriceOn = UpdatedOn

        AvailabilityType = "1"

        If _SvProductCode.CodeBalance(_WarehouseID) Is Nothing Then
            Stock = 0
        Else
            Stock = _SvProductCode.CodeBalance(_WarehouseID).Quantity
        End If
        Currency = 978
        TaxClass = _SvProductCode.CodeProduct.ProductVat.DestinationID
        RetailCurrentPrice = _SvProductCode.CodeProduct.RetailPrice

        ColorId = GetColorID(_SvProductCode.Color)
        SizeId = GetSizeID(_SvProductCode.Size)

        If IsNew Then
            SKUId = _SvProductCode.ID.ToString
            CreatedOn = UpdatedOn
            LhContext.StockKeepingUnits.InsertOnSubmit(Me)
        End If

        LhContext.SubmitChanges()

        _SvProductCode.LastUpdate(SKUId.ToString)
    End Sub

    Public Function GetColorID(ByVal ColorName As String) As String
        Dim ColorExists = (From t In LhContext.Colors _
                           Where t.NameL1.Equals(ColorName)).FirstOrDefault

        If ColorExists Is Nothing Then
            Dim NewColor As New Color

            NewColor.NameL1 = ColorName
            NewColor.ColorId = Left(ColorName, 50)
            NewColor.UpdatedOn = Now
            NewColor.CreatedOn = NewColor.UpdatedOn

            LhContext.Colors.InsertOnSubmit(NewColor)
            LhContext.SubmitChanges()

            Return NewColor.ColorId
        Else
            Return ColorExists.ColorID
        End If

    End Function

    Public Function GetSizeID(ByVal SizeName As String) As String
        Dim SizeExists = (From t In LhContext.Sizes _
                           Where t.NameL1.Equals(SizeName)).FirstOrDefault

        If SizeExists Is Nothing Then
            Dim NewSize As New Size

            NewSize.NameL1 = SizeName
            NewSize.SizeId = Left(SizeName, 50)
            NewSize.UpdatedOn = Now
            NewSize.CreatedOn = NewSize.UpdatedOn

            LhContext.Sizes.InsertOnSubmit(NewSize)
            LhContext.SubmitChanges()

            Return NewSize.SizeId
        Else
            Return SizeExists.SizeID
        End If

    End Function

End Class


﻿Public Class Product

    Private _SvProduct As SvProduct
    Public Property SvProduct() As SvProduct
        Get
            Return _SvProduct
        End Get
        Set(ByVal value As SvProduct)
            _SvProduct = value
        End Set
    End Property

    'Public _ItemsCategories As Dictionary(Of KeyValuePair(Of String, String), String)
    'Public WriteOnly Property ItemsCategories() As Dictionary(Of KeyValuePair(Of String, String), String)
    '    Set(ByVal value As Dictionary(Of KeyValuePair(Of String, String), String))
    '        _ItemsCategories = value
    '    End Set
    'End Property

    Public Sub ImportProduct(ByVal IsNew As Boolean, ByVal PreferredCategories As List(Of Integer))
        For Each item In _SvProduct.Categories
            If item.DestinationID IsNot Nothing Then
                If PreferredCategories.Count = 0 OrElse _
                CategoryIdLevel1 Is Nothing OrElse _
                (PreferredCategories.Contains(CategoryIdLevel1) And PreferredCategories.Contains(item.DestinationID)) OrElse _
                Not PreferredCategories.Contains(CategoryIdLevel1) Then

                    CategoryIdLevel1 = item.DestinationID
                End If
            End If
        Next


        NameL1 = _SvProduct.Name
        ProductCode = _SvProduct.ProductCode
        DescriptionL1 = _SvProduct.Description1

        UpdatedOn = Now

        If IsNew Then
            ProductId = _SvProduct.SourceID
            CreatedOn = UpdatedOn
            LhContext.Products.InsertOnSubmit(Me)
        End If

        LhContext.SubmitChanges()

        _SvProduct.LastUpdate(ProductId.ToString)
    End Sub

End Class

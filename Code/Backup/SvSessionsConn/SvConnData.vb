﻿Public Class SvConnData

    'Private _TraderList As List(Of SvTrader)
    'Public Property TraderList() As List(Of SvTrader)
    '    Get
    '        Return _TraderList
    '    End Get
    '    Set(ByVal value As List(Of SvTrader))
    '        _TraderList = value
    '    End Set
    'End Property

    Public Enum OrderTypes
        Sales = 0
        Purchases = 1
        All = 2
    End Enum


    Public _SalesPersons As List(Of SvStructure)
    Public WriteOnly Property SalesPersons() As List(Of SvStructure)
        Set(ByVal value As List(Of SvStructure))
            _SalesPersons = value
        End Set
    End Property

    Public _Payways As Dictionary(Of String, String)
    Public WriteOnly Property Payways() As Dictionary(Of String, String)
        Set(ByVal value As Dictionary(Of String, String))
            _Payways = value
        End Set
    End Property

    Public _ItemsCategories As Dictionary(Of KeyValuePair(Of String, String), String)
    Public WriteOnly Property ItemsCategories() As Dictionary(Of KeyValuePair(Of String, String), String)
        Set(ByVal value As Dictionary(Of KeyValuePair(Of String, String), String))
            _ItemsCategories = value
        End Set
    End Property

    Public _ContactsCategories As Dictionary(Of KeyValuePair(Of String, String), KeyValuePair(Of String, String))
    Public WriteOnly Property ContactsCategories() As Dictionary(Of KeyValuePair(Of String, String), KeyValuePair(Of String, String))
        Set(ByVal value As Dictionary(Of KeyValuePair(Of String, String), KeyValuePair(Of String, String)))
            _ContactsCategories = value
        End Set
    End Property

    Public Function Connect(ByVal svServer As String, ByVal svUser As String, ByVal svPwd As String, ByVal svDB As String, Optional ByVal Timeout As Integer = 0) As String
        Dim ErrorMsg As String = ""
        Try
            ConnContext = New dataSvConnDataContext(DbConn.ConstructConnString( _
                                                      svServer, _
                                                      svUser, _
                                                      svPwd, _
                                                      svDB, ""))

            If Timeout > 0 Then
                ConnContext.CommandTimeout = Timeout
            End If

        Catch ex As Exception
            ErrorMsg = ex.Message
        End Try
        Return ErrorMsg
    End Function

    Public Function Connect(ByVal ConnString As String, Optional ByVal Timeout As Integer = 0) As String
        Dim ErrorMsg As String = ""
        Try
            ConnContext = New dataSvConnDataContext(ConnString)
            If Timeout > 0 Then
                ConnContext.CommandTimeout = Timeout
            End If
        Catch ex As Exception
            ErrorMsg = ex.Message
        End Try
        Return ErrorMsg
    End Function

    Public Sub Disconnect()
        ConnContext.SvConnShrink()
        ConnContext.Dispose()
    End Sub

    Public Function LoadOrders(ByVal DirtyOnly As Boolean, ByVal OrderType As Short) As List(Of SvOrder)
        Dim OrderList As New List(Of SvOrder)

        Dim AllOrders = From t In ConnContext.svOrders _
                          Select t

        If DirtyOnly Then
            AllOrders = AllOrders.Where(Function(f) f.IsDirty = True)
        End If

        If OrderType = OrderTypes.Sales Then
            AllOrders = AllOrders.Where(Function(f) f.IsPurchase = False)
        End If

        If OrderType = OrderTypes.Purchases Then
            AllOrders = AllOrders.Where(Function(f) f.IsPurchase = True)
        End If

        For Each prd In AllOrders
            OrderList.Add(prd)
        Next

        Return OrderList

    End Function

    Public Function LoadOrdersDetails(ByVal DirtyOnly As Boolean, _
                                      ByVal NoPercOnly As Boolean) As List(Of SvOrderDetail)
        Dim DetailList As New List(Of SvOrderDetail)

        Dim AllDetails = From t In ConnContext.svOrderDetails _
                          Select t

        If DirtyOnly Then
            AllDetails = AllDetails.Where(Function(f) f.IsDirty = True)
        End If

        If NoPercOnly Then
            AllDetails = AllDetails.Where(Function(f) f.FinalDiscountPerc Is Nothing)
        End If

        For Each prd In AllDetails
            DetailList.Add(prd)
        Next

        Return DetailList
    End Function

    Public Sub UpdateTraderForOrder(ByVal SameSource As Boolean) ', Optional ByVal UseCustomerValues As Boolean = False
        Dim TraderList = LoadTraders(False, True, False, False)

        For Each prd In TraderList
            prd.UpdateTraderForOrder(SameSource)
        Next
    End Sub

    Public Sub UpdateCountryForTrader(ByVal SameSource As Boolean) ', Optional ByVal UseCustomerValues As Boolean = False
        Dim CountryList = LoadCountries(False)

        For Each prd In CountryList
            prd.UpdateCountryForTrader(SameSource)
        Next
    End Sub

    Public Sub UpdateTraderForTask(ByVal SameSource As Boolean)
        Dim TraderList = LoadTraders(False, True, False, False)

        For Each prd In TraderList
            prd.UpdateTraderForTask(SameSource)
        Next
    End Sub

    Public Sub UpdateTraderForFinancial(ByVal SameSource As Boolean)
        Dim TraderList = LoadTraders(False, True, False, False)

        For Each prd In TraderList
            prd.UpdateTraderForFinancial(SameSource)
        Next
    End Sub

    Public Sub UpdateProductForOrderDetail(ByVal SameSource As Boolean)
        Dim ProductList = LoadProducts(False, False, False, False, False)

        For Each prd In ProductList
            prd.UpdateProductForOrderDetail(SameSource)
        Next
    End Sub

    Public Sub UpdateProductVatSource(ByVal DefaultVatSourceID As String)
        Dim ProductList = LoadProducts(False, False, False, False, False)

        For Each prd In ProductList
            If prd.VatSourceID Is Nothing Then
                prd.UpdateVat(DefaultVatSourceID)
            End If
        Next
    End Sub

    Public Sub UpdateTraderCategoriesDestinationBelongsTo()
        Dim CategoryList = LoadTradersCategories(False, False)

        For Each cat In CategoryList
            If cat.SourceBelongsTo <> "0" Then
                Dim Belongs = CategoryList.Where(Function(f) f.SourceID.Equals(cat.SourceBelongsTo))
                If Belongs.Count = 1 AndAlso Belongs.FirstOrDefault.DestinationID IsNot Nothing Then
                    cat.DestinationBelongsTo = Belongs.FirstOrDefault.DestinationID
                Else
                    cat.DestinationBelongsTo = 0
                End If
            Else
                cat.DestinationBelongsTo = 0
            End If
        Next
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateParentDestinationCategory1(ByVal SentOnly As Boolean, Optional ByVal ParentDestination As String = "")
        Dim CategoryList = LoadProductsCategories(False, SentOnly)

        For Each cat In CategoryList
            If ParentDestination <> "" Then
                cat.DestinationBelongsTo = ParentDestination
            Else
                If cat.SourceBelongsTo IsNot Nothing Then
                    Dim Belongs = CategoryList.Where(Function(f) f.SourceID.Equals(cat.SourceBelongsTo))
                    If Belongs.Count = 1 AndAlso Belongs.FirstOrDefault.DestinationID IsNot Nothing Then
                        cat.DestinationBelongsTo = Belongs.FirstOrDefault.DestinationID
                    Else
                        cat.DestinationBelongsTo = Nothing
                    End If
                Else
                    cat.DestinationBelongsTo = Nothing
                End If
            End If
        Next
        ConnContext.SubmitChanges()
    End Sub

    Public Function LoadProductsBalance(ByVal DirtyOnly As Boolean, ByVal WarehouseSourceID As String) As List(Of SvProductBalance)
        Dim ProductBalanceList As New List(Of SvProductBalance)

        Dim AllProductBalances = From t In ConnContext.svProductBalances _
                                 Where t.WarehouseSourceID.Equals(WarehouseSourceID) _
                                 Select t

        If DirtyOnly Then
            AllProductBalances = AllProductBalances.Where(Function(f) f.IsDirty = True)
        End If
        For Each prd In AllProductBalances
            ProductBalanceList.Add(prd)
        Next

        Return ProductBalanceList
    End Function

    Public Function LoadProductsCodesBalances(ByVal DirtyOnly As Boolean, ByVal WarehouseSourceID As String) As List(Of svProductCodeBalance)
        Dim ProductCodeBalanceList As New List(Of svProductCodeBalance)

        Dim AllProductCodeBalances = From t In ConnContext.svProductCodeBalances _
                                     Select t

        If WarehouseSourceID <> "" Then
            AllProductCodeBalances = AllProductCodeBalances.Where(Function(f) f.WarehouseSourceID.Equals(WarehouseSourceID))
        End If

        If DirtyOnly Then
            AllProductCodeBalances = AllProductCodeBalances.Where(Function(f) f.IsDirty = True)
        End If
        For Each prd In AllProductCodeBalances
            ProductCodeBalanceList.Add(prd)
        Next

        Return ProductCodeBalanceList
    End Function

    Public Function LoadProducts(ByVal DirtyOnly As Boolean, _
                                 ByVal ActiveOnly As Boolean, _
                                 ByVal NoDestinationOnly As Boolean, _
                                 ByVal StockOnly As Boolean, _
                                 ByVal ServiceOnly As Boolean) As List(Of SvProduct)
        Dim ProductList As New List(Of SvProduct)

        Dim AllProducts = From t In ConnContext.svProducts _
                          Select t

        If DirtyOnly Then
            AllProducts = AllProducts.Where(Function(f) f.IsDirty = True)
        End If

        If ActiveOnly Then
            AllProducts = AllProducts.Where(Function(f) f.IsActive = True)
        End If

        If NoDestinationOnly Then
            AllProducts = AllProducts.Where(Function(f) f.DestinationID Is Nothing)
        End If

        'If MissingOrderDetail Then
        '    AllProducts = AllProducts.Where(Function(f) f.OrderDetailProductSourceID Is Nothing Or f.OrderDetailProductDestinationID Is Nothing)
        'End If

        If StockOnly Then
            AllProducts = AllProducts.Where(Function(f) f.IsService = False)
        End If

        If ServiceOnly Then
            AllProducts = AllProducts.Where(Function(f) f.IsService = True)
        End If


        For Each prd In AllProducts
            ProductList.Add(prd)
        Next

        Return ProductList

    End Function

    Public Function LoadProductsImages(ByVal DirtyOnly As Boolean, ByVal ActiveProductOnly As Boolean) As List(Of svProductImage)
        Dim ImageList As New List(Of svProductImage)

        Dim AllImages = From t In ConnContext.svProductImages Select t

        If DirtyOnly Then
            AllImages = AllImages.Where(Function(f) f.IsDirty = True)
        End If

        If ActiveProductOnly Then
            Dim ProductIDs = (LoadProducts(False, True, False, False, False)).Select(Function(f) f.SourceID).ToList
            AllImages = AllImages.Where(Function(f) ProductIDs.Contains(f.ProductSourceID))
        End If

        For Each prd In AllImages
            ImageList.Add(prd)
        Next

        Return ImageList
    End Function

    Public Function LoadProductsCodes(ByVal DirtyOnly As Boolean, ByVal ActiveProductOnly As Boolean, _
                                      Optional ByVal CodeType As String = "") As List(Of svProductCode)
        Dim CodeList As New List(Of svProductCode)

        Dim AllCodes As IQueryable(Of svProductCode)

        If CodeType = "" Then
            AllCodes = From t In ConnContext.svProductCodes Order By t.ProductSourceID, t.CodeType Select t
        Else
            AllCodes = From t In ConnContext.svProductCodes Where t.CodeType.Equals(CodeType) Order By t.ProductSourceID, t.CodeType Select t
        End If

        If DirtyOnly Then
            AllCodes = AllCodes.Where(Function(f) f.IsDirty = True)
        End If

        If ActiveProductOnly Then
            Dim ProductIDs = (LoadProducts(False, True, False, False, False)).Select(Function(f) f.SourceID).ToList
            AllCodes = AllCodes.Where(Function(f) ProductIDs.Contains(f.ProductSourceID))
        End If

        For Each prd In AllCodes
            CodeList.Add(prd)
        Next

        Return CodeList
    End Function

    Public Function LoadProductsCategories(ByVal DirtyOnly As Boolean, ByVal SentParentOnly As Boolean) As List(Of SvProductCategory)
        Dim CategoryList As New List(Of SvProductCategory)

        Dim AllCategories = From t In ConnContext.svProductCategories _
                          Select t

        If DirtyOnly Then
            AllCategories = AllCategories.Where(Function(f) f.IsDirty = True)
        End If

        If SentParentOnly Then
            Dim CleanParent = (From t In ConnContext.svProductCategories _
                              Where t.DestinationID IsNot Nothing _
                              Select t.SourceID)
            If CleanParent.Count = 0 Then
                AllCategories = AllCategories.Where(Function(f) f.SourceBelongsTo = "0" Or f.SourceBelongsTo = "" Or f.SourceBelongsTo Is Nothing)
            Else
                AllCategories = AllCategories.Where(Function(f) CleanParent.Contains(f.SourceBelongsTo) Or CleanParent.Contains(f.SourceID))
            End If
        End If

        For Each prd In AllCategories
            CategoryList.Add(prd)
        Next

        Return (CategoryList)

    End Function

    Public Function LoadTradersAddresses(ByVal DirtyOnly As Boolean, _
                                        ByVal NoDestinationOnly As Boolean) As List(Of SvTraderAddress)
        Dim AddressList As New List(Of SvTraderAddress)

        Dim AllAddresses = From t In ConnContext.svTraderAddresses _
                          Select t

        If DirtyOnly Then
            AllAddresses = AllAddresses.Where(Function(f) f.IsDirty = True)
        End If

        If NoDestinationOnly Then
            AllAddresses = AllAddresses.Where(Function(f) f.DestinationID Is Nothing)
        End If

        For Each prd In AllAddresses
            AddressList.Add(prd)
        Next

        Return AddressList

    End Function

    Public Function LoadTradersCategories(ByVal DirtyOnly As Boolean, ByVal ParentOnly As Boolean) As List(Of svTraderCategory)
        Dim CategoryList As New List(Of svTraderCategory)

        Dim AllCategories = From t In ConnContext.svTraderCategories _
                          Select t

        If DirtyOnly Then
            AllCategories = AllCategories.Where(Function(f) f.IsDirty = True)
        End If

        If ParentOnly Then
            AllCategories = AllCategories.Where(Function(f) f.SourceBelongsTo = "0")
        End If

        For Each prd In AllCategories
            CategoryList.Add(prd)
        Next

        Return CategoryList

    End Function

    Public Function LoadVat(ByVal DirtyOnly As Boolean) As List(Of svVat)
        Dim VatList As New List(Of svVat)

        Dim AllVats = From t In ConnContext.svVats _
                          Select t

        If DirtyOnly Then
            AllVats = AllVats.Where(Function(f) f.IsDirty = True)
        End If

        For Each vat In AllVats
            VatList.Add(vat)
        Next

        Return VatList
    End Function

    Public Function LoadTasks(ByVal DirtyOnly As Boolean) As List(Of SvTask)
        Dim TaskList As New List(Of SvTask)

        Dim AllTasks = From t In ConnContext.svTasks _
                          Select t

        If DirtyOnly Then
            AllTasks = AllTasks.Where(Function(f) f.IsDirty = True)
        End If

        For Each tsk In AllTasks
            TaskList.Add(tsk)
        Next

        Return TaskList
    End Function

    Public Function LoadWarehouses(ByVal DirtyOnly As Boolean) As List(Of SvWarehouse)
        Dim WarehouseList As New List(Of SvWarehouse)

        Dim AllWarehouses = From t In ConnContext.svWarehouses _
                          Select t

        If DirtyOnly Then
            AllWarehouses = AllWarehouses.Where(Function(f) f.IsDirty = True)
        End If

        For Each tsk In AllWarehouses
            WarehouseList.Add(tsk)
        Next

        Return WarehouseList
    End Function

    Public Function LoadTraders(ByVal DirtyOnly As Boolean, ByVal TypicalUpdate As Boolean, _
                                ByVal NoDestinationOnly As Boolean, ByVal NoDestinationCode As Boolean, _
                                Optional ByVal SourceTraderID As String = "") As List(Of SvTrader)
        Dim TraderList As New List(Of SvTrader)

        Dim AllTraders = From t In ConnContext.svTraders _
                       Select t

        If DirtyOnly Then
            AllTraders = AllTraders.Where(Function(f) f.IsDirty = True)
        End If

        If NoDestinationOnly Then
            If TypicalUpdate Then
                AllTraders = AllTraders.Where(Function(f) f.DestinationCustomerID Is Nothing)
            Else
                AllTraders = AllTraders.Where(Function(f) f.SourceCustomerID.Contains("REVERTED"))
            End If
        End If

        If NoDestinationCode Then
            If TypicalUpdate Then
                AllTraders = AllTraders.Where(Function(f) f.DestinationCustomerCode Is Nothing)
            Else
                AllTraders = AllTraders.Where(Function(f) f.SourceCustomerCode Is Nothing)
            End If
        End If

        If SourceTraderID <> "" Then
            AllTraders = AllTraders.Where(Function(f) f.SourceCustomerID.Equals(SourceTraderID))
        End If

        For Each trd In AllTraders
            TraderList.Add(trd)
        Next

        Return TraderList
    End Function

    Public Function LoadCountries(ByVal DirtyOnly As Boolean) As List(Of svCountry)
        Dim CountryList As New List(Of svCountry)

        Dim AllCountries = From t In ConnContext.svCountries _
                       Select t

        If DirtyOnly Then
            AllCountries = AllCountries.Where(Function(f) f.IsDirty = True)
        End If

        For Each trd In AllCountries
            CountryList.Add(trd)
        Next

        Return CountryList
    End Function

    Public Function LoadTradersFinancial(ByVal DirtyOnly As Boolean) As List(Of SvTraderFinancial)
        Dim TraderList As New List(Of SvTraderFinancial)

        Dim AllTraders = From t In ConnContext.svTraderFinancials _
                       Select t

        If DirtyOnly Then
            AllTraders = AllTraders.Where(Function(f) f.IsDirty = True)
        End If

        For Each trd In AllTraders
            TraderList.Add(trd)
        Next

        Return TraderList
    End Function

    Public Sub UpdateProductDestinationFromOrderDetails()
        Dim ProductsList = LoadProducts(False, False, True, False, False)

        For Each prd In ProductsList
            Dim CurrentItem = (From t In ConnContext.svOrderDetails _
                              Where t.DetailProductSourceID.Equals(prd.ProductCode) _
                              Select t.DetailProductSourceID).Distinct
            If CurrentItem.Count = 1 Then
                prd.DestinationID = CurrentItem.FirstOrDefault.ToString
                ConnContext.SubmitChanges()
            End If
        Next
    End Sub

    Public Sub CatalogueWithVatToFinalWithVat()
        Dim AllDetails = LoadOrdersDetails(True, False)

        For Each dtl In AllDetails
            If dtl.DetailProduct IsNot Nothing Then
                dtl.CatalogueWithVatToFinalWithVat()
            End If
        Next
    End Sub

    Public Sub FinalNoVatToFinalWithVat()
        Dim AllDetails = LoadOrdersDetails(True, False)

        For Each dtl In AllDetails
            If dtl.DetailProduct IsNot Nothing Then
                dtl.FinalNoVatToFinalWithVat()
            End If
        Next
    End Sub

    Public Sub CatalogueWithVatToNoVat()
        Dim AllDetails = LoadOrdersDetails(True, False)

        For Each dtl In AllDetails
            If dtl.DetailProduct IsNot Nothing Then
                dtl.CatalogueWithVatToCatalogueNoVat()
            End If
        Next
    End Sub

    Public Sub CatalogueNoVatToCatalogueWithVat()
        Dim AllDetails = LoadOrdersDetails(True, False)

        For Each dtl In AllDetails
            If dtl.DetailProduct IsNot Nothing Then
                dtl.CatalogueNoVatToCatalogueWithVat()
            End If
        Next
    End Sub

    Public Sub FinalWithVatToFinalNoVat()
        Dim AllDetails = LoadOrdersDetails(True, False)

        For Each dtl In AllDetails
            If dtl.DetailProduct IsNot Nothing Then
                dtl.FinalWithVatToFinalNoVat()
                'dtl.CatalogueWithVatToCatalogueNoVat()
            End If
        Next
    End Sub

    Public Sub ProductRetailPriceToCatalogueNoVat()
        Dim AllDetails = LoadOrdersDetails(True, False)

        For Each dtl In AllDetails
            If dtl.DetailProduct IsNot Nothing Then
                dtl.ProductRetailPriceToCatalogueNoVat()
            End If
        Next
    End Sub

    'Public Sub UpdateDetailDiscount()
    '    Dim AllDetails = LoadOrdersDetails(False, True)

    '    For Each dtl In AllDetails
    '        If dtl.DetailProduct IsNot Nothing Then
    '            dtl.UpdateDiscount(False)
    '        End If
    '    Next

    'End Sub

    'Public Sub UpdateTraderSourceCodes(ByVal TraderCodeMask As String, ByVal CustomerCodeMask As String, ByVal CodeFormat As String)
    '    Dim AllTraders = LoadTraders(False, False, False, True)
    '    For Each trd In AllTraders
    '        trd.SourceTraderCode = TraderCodeMask & If(IsNumeric(trd.SourceTraderCode), String.Format(CodeFormat, Convert.ToInt32(trd.SourceTraderID)), trd.SourceTraderCode)
    '        trd.SourceCustomerCode = CustomerCodeMask & If(IsNumeric(trd.SourceCustomerCode), String.Format(CodeFormat, Convert.ToInt32(trd.SourceCustomerID)), trd.SourceCustomerCode)
    '    Next
    '    ConnContext.SubmitChanges()
    'End Sub

    Public Sub UpdateTraderDestinationCodes(ByVal TraderCodeMask As String, ByVal CustomerCodeMask As String, ByVal CodeFormat As String)
        Dim AllTraders = LoadTraders(False, True, False, True)
        For Each trd In AllTraders
            If CodeFormat = "" Then
                trd.DestinationTraderCode = trd.SourceTraderCode
                trd.DestinationCustomerCode = trd.SourceCustomerCode
            Else
                trd.DestinationTraderCode = TraderCodeMask & If(IsNumeric(trd.SourceTraderCode), String.Format(CodeFormat, Convert.ToInt32(trd.SourceTraderCode)), trd.SourceTraderCode)
                trd.DestinationCustomerCode = CustomerCodeMask & If(IsNumeric(trd.SourceCustomerCode), String.Format(CodeFormat, Convert.ToInt32(trd.SourceCustomerCode)), trd.SourceCustomerCode)
            End If
        Next
        ConnContext.SubmitChanges()
    End Sub

    Public Function LoadPayments(ByVal DirtyOnly As Boolean) As List(Of svPaymentMethod)
        Dim PaymentList As New List(Of svPaymentMethod)

        Dim AllPayments = From t In ConnContext.svPaymentMethods _
                          Select t

        If DirtyOnly Then
            AllPayments = AllPayments.Where(Function(f) f.IsDirty = True)
        End If

        For Each pmnt In AllPayments
            PaymentList.Add(pmnt)
        Next

        Return PaymentList
    End Function

    Public Function LoadSalepersons(ByVal DirtyOnly As Boolean) As List(Of svSaleperson)
        Dim SalepersonList As New List(Of svSaleperson)

        Dim AllSalepersons = From t In ConnContext.svSalepersons _
                          Select t

        If DirtyOnly Then
            AllSalepersons = AllSalepersons.Where(Function(f) f.IsDirty = True)
        End If

        For Each slp In AllSalepersons
            SalepersonList.Add(slp)
        Next

        Return SalepersonList
    End Function

    Public Function LoadConsignments(ByVal DirtyOnly As Boolean) As List(Of svConsignmentMethod)
        Dim ConsignmentList As New List(Of svConsignmentMethod)

        Dim AllConsignments = From t In ConnContext.svConsignmentMethods _
                          Select t

        If DirtyOnly Then
            AllConsignments = AllConsignments.Where(Function(f) f.IsDirty = True)
        End If

        For Each vat In AllConsignments
            ConsignmentList.Add(vat)
        Next

        Return ConsignmentList
    End Function

    Public Sub UpdatePaymentForOrder(ByVal SameSource As Boolean)
        Dim PaymentList = LoadPayments(False)

        For Each prd In PaymentList
            prd.UpdatePaymentForOrder(SameSource)
        Next
    End Sub

    Public Sub UpdatePaymentForTrader(ByVal SameSource As Boolean)
        Dim PaymentList = LoadPayments(False)

        For Each prd In PaymentList
            prd.UpdatePaymentForTrader(SameSource)
        Next
    End Sub

    Public Sub UpdateConsignmentForOrder(ByVal SameSource As Boolean)
        Dim ConsignmentList = LoadConsignments(False)

        For Each prd In ConsignmentList
            prd.UpdateConsignmentForOrder(SameSource)
        Next
    End Sub

    Public Function SetOrderDirty(ByVal UpdateBySource As Boolean, ByVal OrderID As String) As String
        Dim ResultMessage As String = ""

        Dim OrdersList = LoadOrders(False, OrderTypes.All)

        Dim CurrentOrder As SvOrder

        If UpdateBySource Then
            CurrentOrder = OrdersList.Where(Function(f) f.SourceID.Equals(OrderID)).FirstOrDefault
        Else
            CurrentOrder = OrdersList.Where(Function(f) f.DestinationID.Equals(OrderID)).FirstOrDefault
        End If

        If CurrentOrder IsNot Nothing Then
            CurrentOrder.UpdateDirty(True)
            ResultMessage = "Επιτυχής ενημέρωση"
        Else
            ResultMessage = "Δε βρέθηκε η παραγγελία"
        End If

        Return ResultMessage
    End Function

    Public Function HideNoProductsOrders() As String
        Dim OrdersList As String = ""

        Dim OrdersIDs As New List(Of String)

        Dim DetailsList = LoadOrdersDetails(True, False)

        For Each dtl In DetailsList
            If dtl.DetailProduct Is Nothing Then
                dtl.svOrder.UpdateDirty(False)
                If OrdersIDs.IndexOf(dtl.svOrder.SourceID) Then
                    OrdersIDs.Add(dtl.svOrder.SourceID)
                End If
            End If
        Next

        For Each ord In OrdersIDs
            If OrdersList <> "" Then
                OrdersList &= ", "
            End If
            OrdersList &= ord 'dtl.svOrder.SourceID.ToString
        Next
        Return OrdersList
    End Function

    Public Sub ApplyZeroDiscount(ByVal WithFinalPrice As Boolean)
        Dim AllOrders = LoadOrders(True, OrderTypes.All)

        For Each ord In AllOrders
            ord.ApplyZeroDiscount(WithFinalPrice)
        Next
    End Sub

    Public Sub UpdateDefaultSurcharges(ByVal PaymentIDs As List(Of String), ByVal SurchageValue As Double, ByVal SurchargeVatPerc As Double)
        Dim AllOrders = LoadOrders(True, OrderTypes.All)

        For Each ord In AllOrders
            If PaymentIDs.Contains(ord.OrderPaymentID) _
            OrElse (ord.OrderPayment IsNot Nothing AndAlso PaymentIDs.Contains(ord.OrderPayment.SourceID)) Then
                ord.UpdateDefaultSurcharge(SurchageValue, SurchargeVatPerc)
            End If
        Next
    End Sub

    Public Sub UpdateSurchargesNoVat(ByVal SurchargeVatPerc As Double)
        Dim AllOrders = LoadOrders(True, OrderTypes.All)

        For Each ord In AllOrders
            ord.UpdateSurchargeNoVat(SurchargeVatPerc)
        Next
    End Sub

    Public Sub UpdateShippingsNoVat(ByVal ShippingVatPerc As Double)
        Dim AllOrders = LoadOrders(True, OrderTypes.All)

        For Each ord In AllOrders
            ord.UpdateShippingNoVat(ShippingVatPerc)
        Next
    End Sub

    Public Sub ApplyOrderDiscount(ByVal DefaultVatPerc As Double)
        Dim AllOrders = LoadOrders(True, OrderTypes.All)

        For Each ord In AllOrders
            ord.CalcDetailDiscount(DefaultVatPerc)
        Next
    End Sub

    Public Sub UpdateProductCategoriesAssociations()
        Dim AllCategories = LoadProductsCategories(False, False)

        For Each cat In AllCategories
            Dim DestinationID As String = GetSqlCategoryID(cat.SourceID)
            If DestinationID <> "0" Then
                cat.UpdateDestination(DestinationID)
            End If
        Next
    End Sub

    Public Sub UpdateTraderAddressMainFs(ByVal MainAddressType As String)
        Dim AllAddresses = LoadTradersAddresses(True, True)

        For Each adr In AllAddresses
            If adr.AddressType = MainAddressType Then
                If adr.TraderSourceID IsNot Nothing Then
                    Dim CurrentDestination = (From t In ConnContext.svTraders _
                                        Where t.SourceCustomerID.Equals(adr.CustomerSourceID) _
                                        Select t).FirstOrDefault
                    If CurrentDestination IsNot Nothing AndAlso _
                    CurrentDestination.DestinationCustomerID IsNot Nothing Then
                        adr.UpdateFsMainAddress(CurrentDestination.DestinationCustomerID)
                    End If
                End If
            End If
        Next
    End Sub

    Public Sub UpdateTraderCategoriesAssociations()
        Dim AllCategories = LoadTradersCategories(False, False)

        For Each cat In AllCategories
            Dim DestinationID As String = GetSenContactCategoryID(cat.SourceID)
            If DestinationID <> "0" Then
                cat.UpdateDestination(DestinationID)
            End If
        Next
    End Sub

    'Public Sub UpdateTraderSalesmenAssociations()
    '    Dim AllTraders = LoadTraders(True, False, False)

    '    For Each trd In AllTraders
    '        Dim DestinationID As String = GetSqlEmplID(trd.SalesmanSourceID)
    '        If DestinationID <> "0" Then
    '            trd.UpdateDestinationSalesman(DestinationID)
    '        End If
    '    Next
    'End Sub

    'Public Sub UpdateTraderPaymentsAssociations()
    '    Dim AllTraders = LoadTraders(True, False, False)

    '    For Each trd In AllTraders
    '        Dim DestinationID As String = GetSenPaywayID(trd.PaymentMethodSourceID)
    '        If DestinationID <> "0" Then
    '            trd.UpdateDestinationPayment(DestinationID)
    '        End If
    '    Next
    'End Sub

    Public Function GetDestinationSalePersonID(ByVal SourceID As String) As String
        Dim DestinationSalePersonID As String = "0"
        For Each item In _SalesPersons
            If item.KeyID.Equals(SourceID) Then DestinationSalePersonID = item.ValueID
        Next
        Return DestinationSalePersonID
    End Function

    Public Function GetDestinationPaywayID(ByVal SourcePaywayID As String) As String
        Dim DestinationPaywayID As String = "0"
        For Each item In _Payways
            If item.Key.Equals(SourcePaywayID) Then DestinationPaywayID = item.Value
        Next
        Return DestinationPaywayID
    End Function

    Public Function GetSourcePaywayID(ByVal DestinationPaywayID As String) As String
        Dim SourcePaywayID As Integer = 0
        For Each item In _Payways
            If item.Value.Equals(DestinationPaywayID) Then SourcePaywayID = item.Key
        Next
        Return SourcePaywayID
    End Function

    Public Function GetSqlCategoryID(ByVal SenCategoryID As String) As String
        Dim SqlCategoryID As String = "0"
        For Each item In _ItemsCategories
            Dim SqlPair As KeyValuePair(Of String, String)
            SqlPair = item.Key
            If item.Value.Equals(SenCategoryID) Then SqlCategoryID = SqlPair.Key
        Next
        Return SqlCategoryID
    End Function

    Public Function GetSqlEmplID(ByVal SenEmplID As String) As String
        Dim SqlEmplID As Integer = 0
        For Each item In _SalesPersons
            If item.ValueID.Equals(SenEmplID) Then SqlEmplID = item.KeyID
        Next
        Return SqlEmplID
    End Function

    Public Function GetSqlDuration(ByVal SenCategoryID As String) As String
        Dim SqlDuration As String = 0
        If _ItemsCategories IsNot Nothing Then
            For Each item In _ItemsCategories
                Dim SqlPair As KeyValuePair(Of String, String)
                SqlPair = item.Key
                If item.Value.Equals(SenCategoryID) Then SqlDuration = SqlPair.Value
            Next
        End If
        Return SqlDuration
    End Function

    Public Function GetSenContactCategoryID(ByVal SqlCategoryID As String) As String
        Dim SenCategoryID As String = 0
        For Each item In _ContactsCategories
            Dim SqlPair As KeyValuePair(Of String, String)
            Dim SenPair As KeyValuePair(Of String, String)
            SqlPair = item.Key
            SenPair = item.Value

            If SqlPair.Key.Equals(SqlCategoryID) Then SenCategoryID = SenPair.Key
        Next
        Return SenCategoryID
    End Function

    Public Function GetSenContactCategoryParentID(ByVal SqlCategoryID As String) As String
        Dim SenCategoryID As Integer = 0

        If _ContactsCategories IsNot Nothing Then
            For Each item In _ContactsCategories
                Dim SqlPair As KeyValuePair(Of String, String)
                Dim SenPair As KeyValuePair(Of String, String)
                SqlPair = item.Key
                SenPair = item.Value

                If SqlPair.Key.Equals(SqlCategoryID) Then SenCategoryID = SenPair.Value
            Next
        End If
        Return SenCategoryID
    End Function

    Public Sub ImportFileOrders(ByVal AllOrders As List(Of SvOrder), ByVal OrderType As Short)
        Dim OrderList = LoadOrders(False, OrderType)

        For Each ord In AllOrders
            ConnContext = New dataSvConnDataContext(ConnContext.Connection.ConnectionString)

            Dim OrderExists As IQueryable(Of SvOrder)
            If ord.SourceID IsNot Nothing Then
                OrderExists = From t In ConnContext.svOrders _
                                  Where t.SourceID.ToString.Equals(ord.SourceID) _
                                  Select t
            End If

            If OrderExists Is Nothing OrElse OrderExists.Count = 0 Then
                ConnContext.svOrders.InsertOnSubmit(ord)

                For Each det In ord.Details
                    ConnContext.svOrderDetails.InsertOnSubmit(det)
                Next

                ConnContext.SubmitChanges()
            End If
        Next

    End Sub

    Public Sub UpdateSalesFromPurchases1(Optional ByVal PurchaseType As String = "", Optional ByVal SalesType As String = "")
        Dim SalesList = LoadOrders(True, OrderTypes.Sales)

        For Each sal In SalesList
            Dim PurchaseList = LoadOrders(False, OrderTypes.Purchases).Where(Function(f) f.DestinationID IsNot Nothing)

            If PurchaseList.Count > 0 Then
                Dim Purchase = PurchaseList.Where(Function(f) f.DestinationID.Equals(sal.OrderCode)).FirstOrDefault

                If Purchase IsNot Nothing Then
                    If Purchase.TransformationCounter IsNot Nothing AndAlso Purchase.TransformationCounter > 0 Then
                        sal.TransformedFromID = Purchase.SourceID.Replace(":" & Purchase.TransformationCounter.ToString, "")
                    Else
                        sal.TransformedFromID = Purchase.SourceID
                    End If
                    sal.OrderTraderID = Purchase.OrderTraderID
                    sal.OrderTraderAddressID = Purchase.OrderTraderAddressID
                    sal.OrderCode = Purchase.OrderCode
                    sal.OrderPaymentID = Purchase.OrderPaymentID
                    sal.OrderDate = Purchase.OrderDate

                    If Purchase.SalesmanSourceID IsNot Nothing AndAlso Purchase.SalesmanSourceID <> "0" Then
                        sal.SalesmanSourceID = Purchase.SalesmanSourceID
                    End If

                    If Purchase.OrderConsignmentID IsNot Nothing AndAlso Purchase.OrderConsignmentID <> "0" Then
                        sal.OrderConsignmentID = Purchase.OrderConsignmentID
                    End If


                    For Each det In sal.Details
                        Dim CurrentProduct = Purchase.OrderDetails.ToList(0).DetailProduct
                        Dim PurchaseLine = Purchase.OrderDetails.ToList.Where(Function(f) f.DetailProduct.DestinationID = det.DetailProductSourceID).FirstOrDefault
                        If PurchaseLine IsNot Nothing Then
                            det.TransformedFromID = PurchaseLine.SourceID.Replace(":" & Purchase.TransformationCounter.ToString, "")
                            det.UnitCataloguePriceNoVat = PurchaseLine.UnitCataloguePriceNoVat
                            det.UnitCataloguePriceWithVat = PurchaseLine.UnitCataloguePriceWithVat
                            det.UnitFinalPriceNoVat = PurchaseLine.UnitFinalPriceNoVat
                            det.UnitFinalPriceWithVat = PurchaseLine.UnitFinalPriceWithVat
                            det.ItemDiscountPerc = PurchaseLine.ItemDiscountPerc
                            det.FinalDiscountPerc = PurchaseLine.FinalDiscountPerc
                            det.DiscountNoVatDetailPart = PurchaseLine.DiscountNoVatDetailPart
                            det.DiscountWithVatDetailPart = PurchaseLine.DiscountWithVatDetailPart
                            det.DiscountNoVatOrderPart = PurchaseLine.DiscountNoVatOrderPart
                            det.DiscountWithVatOrderPart = PurchaseLine.DiscountWithVatOrderPart
                            det.SortingValue = PurchaseLine.SortingValue
                        End If
                    Next

                    If PurchaseType <> "" And SalesType <> "" Then
                        sal.OrderCode = Purchase.OrderCode.Replace(PurchaseType, SalesType)
                    End If

                End If
            End If
        Next
        ConnContext.SubmitChanges()
    End Sub

    Public Sub EqualizeInventoryDestinations()
        EqualizeTraderDestinations()

        EqualizePaymentDestinations()
    End Sub

    Public Sub EqualizeTraderDestinations()
        Dim TradersList = LoadTraders(True, True, True, False)
        For Each trd In TradersList
            'trd.DestinationCustomerID = trd.SourceCustomerID
            'trd.DestinationTraderID = trd.SourceTraderID
            trd.LastUpdate(True, trd.SourceTraderID, trd.SourceCustomerID)
        Next

        Dim TradersAddressList = LoadTradersAddresses(True, True)
        For Each trd In TradersAddressList
            'trd.DestinationCustomerID = trd.SourceCustomerID
            'trd.DestinationTraderID = trd.SourceTraderID
            trd.TraderDestinationID = trd.TraderSourceID
            trd.CustomerDestinationID = trd.CustomerSourceID
            trd.LastUpdate(True, trd.SourceID)
        Next
    End Sub

    Public Sub EqualizePaymentDestinations()
        Dim PaymentsList = LoadPayments(True)
        For Each pmnt In PaymentsList
            pmnt.LastUpdate(pmnt.SourceID)
        Next
    End Sub

    Public Sub EqualizeProductDestinations(ByVal UseCode As Boolean)
        Dim ProductsList = LoadProducts(True, False, True, False, False)
        For Each prd In ProductsList
            If UseCode Then
                prd.LastUpdate(prd.ProductCode)
            Else
                prd.LastUpdate(prd.SourceID)
            End If
        Next
    End Sub

    Public Sub PopulatePaywaysList()
        Dim PaymentList = LoadPayments(False)
        Dim AllPayments As New Dictionary(Of String, String)

        For Each item In PaymentList
            If item.DestinationID IsNot Nothing Then
                AllPayments.Add(item.SourceID, item.DestinationID)
            End If
        Next
        _Payways = AllPayments


        'Dim SalepersonList = ConnSession.LoadSalepersons(False)
        'Dim AllSalepersons As New Dictionary(Of String, String)

        'For Each item In SalepersonList
        '    If item.DestinationID IsNot Nothing Then
        '        AllSalepersons.Add(item.DestinationID, item.SourceID)
        '    End If
        'Next
        'ConnSession.SalesPersons = AllSalepersons
    End Sub

    Public Sub PopulateSalepersonsList()
        Dim SalepersonList = LoadSalepersons(False)
        Dim AllSalepersons As New List(Of SvSessionsConn.SvConnData.SvStructure)

        For Each item In SalepersonList
            If item.DestinationID IsNot Nothing Then
                AllSalepersons.Add(New SvSessionsConn.SvConnData.SvStructure(item.DestinationID, item.SourceID))
            End If
        Next
        _SalesPersons = AllSalepersons


    End Sub

    Public Sub PopulateContactCategoriesList()
        Dim ContactCategoryList = LoadTradersCategories(False, False)
        Dim AllContactCategories As New Dictionary(Of KeyValuePair(Of String, String), KeyValuePair(Of String, String))

        For Each item In ContactCategoryList
            If item.DestinationID IsNot Nothing Then
                AllContactCategories.Add(New KeyValuePair(Of String, String)(item.DestinationID, item.DestinationBelongsTo), New KeyValuePair(Of String, String)(item.SourceID, item.SourceBelongsTo))
            End If
        Next
        _ContactsCategories = AllContactCategories

    End Sub

    Public Sub UpdateOrderDocument(Optional OrderStatus As String = "", Optional DocTypeID As String = "", Optional DocTypeName As String = "")
        Dim OrdersList = LoadOrders(True, OrderTypes.All)

        For Each ord In OrdersList
            ord.UpdateOrderDocument(OrderStatus, DocTypeID, DocTypeName)
        Next
    End Sub


    '#Region "BillCost"
    '    Public Sub UpdateDetailsSource()
    '        Dim AllCodes = LoadProductsCodes(False, False)

    '        Dim AllDetails = From t In ConnContext.svOrderDetails Where t.IsDirty = True Select t

    '        For Each det In AllDetails
    '            If det.DetailProduct Is Nothing Then
    '                Dim Color As String = ""

    '                Dim CurrentCodes = AllCodes.Where(Function(f) f.ProductSourceID.Equals(det.DetailProduct.OrderDetailProductSourceID) _
    '                                      And f.SourceID.Equals(det.ExtraField1))

    '                For Each cod In CurrentCodes
    '                    If cod.CodeType = "COLOR" Then
    '                        Color = cod.Code
    '                    End If
    '                Next

    '                If Color <> "" AndAlso det.DetailProductID Then

    '                End If
    '            End If
    '        Next
    '    End Sub
    '#End Region


    Public Structure SvStructure

        Private _KeyID As String
        Public Property KeyID() As String
            Get
                Return _KeyID
            End Get
            Set(ByVal value As String)
                _KeyID = value
            End Set
        End Property

        Private _ValueID As String
        Public Property ValueID() As String
            Get
                Return _ValueID
            End Get
            Set(ByVal value As String)
                _ValueID = value
            End Set
        End Property

        Public Sub New(ByVal Key As String, ByVal Value As String)
            _KeyID = Key
            _ValueID = Value
        End Sub
    End Structure
End Class





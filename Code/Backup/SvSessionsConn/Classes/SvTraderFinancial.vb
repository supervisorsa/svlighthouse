﻿Public Class SvTraderFinancial
    Public Sub ImportFinancial(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svTraderFinancials.InsertOnSubmit(Me)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate()
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property FinancialTrader() As SvTrader
        Get
            Dim TraderExists = (From t In ConnContext.svTraders _
                             Where t.FinancialTraderSourceID.Equals(_TraderSourceID) _
                             Select t).FirstOrDefault
            Return TraderExists
        End Get
    End Property
End Class

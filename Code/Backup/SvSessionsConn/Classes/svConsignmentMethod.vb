﻿Public Class svConsignmentMethod
    Public Sub ImportConsignment(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svConsignmentMethods.InsertOnSubmit(Me)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateConsignmentForOrder(ByVal SameSource As Boolean)
        If SameSource Then
            OrderConsignmentSourceID = _SourceID
            OrderConsignmentDestinationID = _DestinationID
        Else
            OrderConsignmentSourceID = _DestinationID
            OrderConsignmentDestinationID = _SourceID
        End If
        ConnContext.SubmitChanges()
    End Sub
End Class

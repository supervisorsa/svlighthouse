﻿Public Class SvTask
    Public Sub ImportTask(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svTasks.InsertOnSubmit(Me)
        End If

        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property TaskTrader() As SvTrader
        Get
            Dim CurrentTrader = (From t In ConnContext.svTraders _
                                 Where t.TaskTraderSourceID.Equals(_TaskTraderID) _
                                 Select t).FirstOrDefault
            If CurrentTrader Is Nothing Then
                Return Nothing
            Else
                Return CurrentTrader
            End If
        End Get
    End Property
End Class

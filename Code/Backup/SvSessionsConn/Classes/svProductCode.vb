﻿Public Class svProductCode
    Public Sub ImportCode(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svProductCodes.InsertOnSubmit(Me)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property CodeProduct() As SvProduct
        Get
            Return (From t In ConnContext.svProducts _
                    Where t.SourceID.Equals(ProductSourceID) _
                    Select t).FirstOrDefault
        End Get
    End Property

    Public Function CodeBalance(ByVal WarehouseSourceID As String) As svProductCodeBalance
        Return (From t In ConnContext.svProductCodeBalances _
                Where t.ProductCodeSourceID.Equals(ID.ToString) _
                And t.WarehouseSourceID.Equals(WarehouseSourceID) _
                Select t).FirstOrDefault
    End Function

End Class

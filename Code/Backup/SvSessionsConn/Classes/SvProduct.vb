﻿Public Class SvProduct
    Public Sub ImportProduct(ByVal IsNew As Boolean, Optional ByVal SingleCategoryID As String = "")
        _LastModificationSource = Now
        _IsDirty = True
        If IsNew Then
            ConnContext.svProducts.InsertOnSubmit(Me)
        End If
        ConnContext.SubmitChanges()

        If SingleCategoryID <> "" Then
            Dim CategoriesIDs As New List(Of String)
            CategoriesIDs.Add(SingleCategoryID)
            ImportProductCategory(CategoriesIDs)
        End If
    End Sub

    Public Sub ImportProductCategory(ByVal CategoriesIDs As List(Of String))
        Dim OldCategories = From t In ConnContext.svProductInCategories _
                    Where t.ProductSourceID.Equals(_SourceID) _
                    And Not CategoriesIDs.Contains(t.ProductCategorySourceID) _
                    Select t

        ConnContext.svProductInCategories.DeleteAllOnSubmit(OldCategories)

        For Each CategoryID In CategoriesIDs
            Dim CategoryExists = From t In ConnContext.svProductCategories _
                                 Where t.SourceID.Equals(CategoryID) _
                                 Select t

            If CategoryExists.Count > 0 Then
                Dim AssocExists = (From t In ConnContext.svProductInCategories _
                                    Where t.ProductSourceID.Equals(_SourceID) _
                                    And t.ProductCategorySourceID.Equals(CategoryID) _
                                    Select t).FirstOrDefault

                If AssocExists Is Nothing Then
                    Dim NewCategory As New svProductInCategory With { _
                    .ID = Guid.NewGuid, _
                    .ProductSourceID = _SourceID, _
                    .ProductCategorySourceID = CategoryID}
                    ConnContext.svProductInCategories.InsertOnSubmit(NewCategory)
                End If
            End If
        Next
        ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property Categories() As List(Of SvProductCategory)
        Get
            Dim ProductCategories = From t In ConnContext.svProductInCategories _
                       Where t.ProductSourceID.Equals(SourceID) _
                       Select t.ProductCategorySourceID

            Dim CategoriesList = From t In ConnContext.svProductCategories _
                                Where ProductCategories.Contains(t.SourceID) _
                                Select t
            Return CategoriesList.ToList
        End Get
    End Property

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateProductForOrderDetail(ByVal SameSource As Boolean)
        If SameSource Then
            OrderDetailProductSourceID = _SourceID
            OrderDetailProductDestinationID = _DestinationID
        Else
            OrderDetailProductSourceID = _DestinationID
            OrderDetailProductDestinationID = _SourceID
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub SetActive(ByVal ActiveStatus As Boolean)
        IsActive = ActiveStatus
        ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property CategoryDestinationID() As String
        Get
            Dim ReturnValue As String = ""

            If Categories.Count = 1 AndAlso Categories.FirstOrDefault.DestinationID IsNot Nothing Then
                ReturnValue = Categories.FirstOrDefault.DestinationID
            End If
            Return ReturnValue
        End Get
    End Property

    Public Sub UpdateVat(ByVal svVatSourceID As String)
        VatSourceID = svVatSourceID
        ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property ProductVat() As svVat
        Get
            Dim CurrentVat = From t In ConnContext.svVats _
                           Where t.SourceID.Equals(_VatSourceID) _
                           Select t
            If CurrentVat.Count = 1 Then
                Return CurrentVat.FirstOrDefault
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public Sub UpdateOrderProductByCode(svDestinationID As String)
        OrderDetailProductDestinationID = svDestinationID
        OrderDetailProductSourceID = ProductCode.ToLower
        ConnContext.SubmitChanges()
    End Sub

    'Public ReadOnly Property ProductBalance() As Decimal
    '    Get
    '        If svProductBalances.Count = 0 Then
    '            Return 99999
    '        Else
    '            Return If(svProductBalances.FirstOrDefault.Quantity = -1, 0, svProductBalances.FirstOrDefault.Quantity)
    '        End If
    '    End Get
    'End Property

End Class

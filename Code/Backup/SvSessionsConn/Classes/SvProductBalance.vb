﻿Public Class SvProductBalance
    Public Sub ImportBalance(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svProductBalances.InsertOnSubmit(Me)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub
End Class

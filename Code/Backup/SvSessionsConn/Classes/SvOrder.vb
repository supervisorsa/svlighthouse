﻿Public Class SvOrder

    'Private 2 As Integer = 2
    'Public Property DecimalDigits() As Integer
    '    Get
    '        Return 2
    '    End Get
    '    Set(ByVal value As Integer)
    '        2 = value
    '    End Set
    'End Property
    Public ReadOnly Property OrderDiscountPercInt() As Decimal
        Get
            If _OrderDiscountWithVat = 0 Then
                Return 0
            Else
                Return 100 * Math.Round(Convert.ToDecimal(_OrderDiscountWithVat / _OrderTotalWithVat), 2)
            End If
        End Get
    End Property

    Public ReadOnly Property ItemsCatalogueValueWithVat() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(OrderDetails.ToList.Sum(Function(f) f.LineCatalogueValueWithVat)), 2)
        End Get
    End Property

    Public ReadOnly Property ItemsCatalogueValueNoVat() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(OrderDetails.ToList.Sum(Function(f) f.LineCatalogueValueNoVat)), 2)
        End Get
    End Property

    Public ReadOnly Property ItemsFinalValueWithVat() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(OrderDetails.ToList.Sum(Function(f) f.LineFinalValueWithVat)), 2)
        End Get
    End Property

    Public ReadOnly Property ItemsFinalValueNoVat() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(OrderDetails.ToList.Sum(Function(f) f.LineFinalValueNoVat)), 2)
        End Get
    End Property

    'Public ReadOnly Property OrderFinalVat() As Decimal
    '    Get
    '        Return OrderFinalValueWithVat - OrderFinalValueNoVat
    '    End Get
    'End Property

    Public ReadOnly Property OrderDiscountVat() As Double
        Get
            Return OrderDiscountWithVat - OrderDiscountNoVat 'OrderDiscountNoVat
            'Return 0
            'Return AllItems.Sum(Function(f) f.DetailRowVatDiscount)
        End Get
    End Property

    Public ReadOnly Property ItemsDiscountValueNoVat() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(OrderDetails.ToList.Sum(Function(f) f.Quantity * f.DiscountNoVatDetailPart)), 2)
        End Get
    End Property

    Public ReadOnly Property ItemsDiscountValueWithVat() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(OrderDetails.ToList.Sum(Function(f) f.Quantity * f.DiscountWithVatDetailPart)), 2)
        End Get
    End Property

    Public ReadOnly Property ItemsDiscountVat() As Decimal
        Get
            Return ItemsDiscountValueWithVat - ItemsDiscountValueNoVat
        End Get
    End Property
    'Public ReadOnly Property OrderDiscountNoVat() As Double
    '    Get
    '        'Return 0
    '        Return Math.Round(Convert.ToDecimal(svOrderDetails.Sum(Function(f) f.LineDiscountNoVat)), 2)
    '    End Get
    'End Property

    'Public ReadOnly Property OrderDiscountWithVat() As Decimal
    '    Get
    '        'Return 0
    '        Return Math.Round(Convert.ToDecimal(svOrderDetails.Sum(Function(f) f.LineDiscountWithVat)), 2)
    '    End Get
    'End Property

    Public ReadOnly Property ItemsCatalogueVat() As Decimal
        Get
            Return ItemsCatalogueValueWithVat - ItemsCatalogueValueNoVat
        End Get
    End Property

    Public ReadOnly Property ItemsFinalVat() As Decimal
        Get
            Return ItemsFinalValueWithVat - ItemsFinalValueNoVat
        End Get
    End Property

    Public ReadOnly Property ShippingVat() As Decimal
        Get
            If _ShippingCostWithVat IsNot Nothing AndAlso ShippingCostNoVat IsNot Nothing Then
                Return _ShippingCostWithVat - ShippingCostNoVat
            Else
                Return 0
            End If
        End Get
    End Property

    Public ReadOnly Property SurchargeVat() As Decimal
        Get
            If _SurchargeWithVat IsNot Nothing AndAlso SurchargeNoVat IsNot Nothing Then
                Return _SurchargeWithVat - SurchargeNoVat
            Else
                Return 0
            End If
        End Get
    End Property

    Public ReadOnly Property OrderTaxesWithVat() As Decimal
        Get
            Return If(_SurchargeWithVat Is Nothing, 0, _SurchargeWithVat) + If(_ShippingCostWithVat Is Nothing, 0, _ShippingCostWithVat)
        End Get
    End Property

    Public ReadOnly Property OrderTaxesNoVat() As Decimal
        Get
            Return If(_SurchargeNoVat Is Nothing, 0, _SurchargeNoVat) + If(_ShippingCostNoVat Is Nothing, 0, _ShippingCostNoVat)
        End Get
    End Property

    Public ReadOnly Property OrderTaxesVat() As Decimal
        Get
            Return OrderTaxesWithVat - OrderTaxesNoVat
        End Get
    End Property

    Public ReadOnly Property OrderValueWithVat() As Decimal
        Get
            Return ItemsFinalValueWithVat + OrderTaxesWithVat
        End Get
    End Property

    Public ReadOnly Property OrderValueNoVat() As Decimal
        Get
            Return ItemsFinalValueNoVat + OrderTaxesNoVat
        End Get
    End Property

    Public ReadOnly Property OrderVat() As Decimal
        Get
            Return OrderValueWithVat - OrderValueNoVat
        End Get
    End Property

    'Public ReadOnly Property ItemsVat() As Decimal
    '    Get
    '        Return OrderVat - SurchargeVat
    '    End Get
    'End Property

    Public Sub ImportOrder(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svOrders.InsertOnSubmit(Me)

            For Each detail In Details
                detail.ImportOrderDetail(IsNew)
            Next
        End If

        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateDirty(ByVal SetDirty As Boolean)
        IsDirty = SetDirty
        DestinationID = Nothing

        Dim OrderDetais = From t In ConnContext.svOrderDetails _
                        Where t.OrderSourceID.Equals(_SourceID) _
                        Select t
        For Each detail In OrderDetais
            detail.UpdateDirty(SetDirty)
        Next
        ConnContext.SubmitChanges()
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        ConnContext.SubmitChanges()
    End Sub

    '    Public Function TotalWithVat(ByVal VatInDetails As Boolean) As Double
    Public ReadOnly Property TotalWithVat() As Double
        Get
            'If VatInDetails Then
            '    Return (svOrderDetails.Select(Function(f) Math.Round(Convert.ToDouble(f.ProductPriceVat), 2) * f.Quantity).Sum) + TotalNoVat
            'Else
            Return Math.Round((OrderDetails.Select(Function(f) Convert.ToDouble(f.UnitFinalPriceWithVat) * f.Quantity).Sum), 2)
            'End If
        End Get
    End Property

    Public ReadOnly Property TotalNoVat() As Double
        Get
            Return Math.Round((OrderDetails.Select(Function(f) Convert.ToDouble(f.UnitFinalPriceNoVat) * f.Quantity).Sum), 2)
        End Get
    End Property

    Public ReadOnly Property TotalVat() As Double
        Get
            Return TotalWithVat - TotalNoVat
        End Get
    End Property

    Public ReadOnly Property OrderTrader() As SvTrader
        Get
            Dim CurrentTrader = (From t In ConnContext.svTraders _
                                 Where t.OrderTraderSourceID.Equals(_OrderTraderID) _
                                 Select t).FirstOrDefault
            If CurrentTrader Is Nothing Then
                Return Nothing
            Else
                Return CurrentTrader
            End If
        End Get
    End Property

    Public ReadOnly Property OrderPayment() As svPaymentMethod
        Get
            Dim CurrentPayment = (From t In ConnContext.svPaymentMethods _
                                 Where t.OrderPaymentSourceID.Equals(_OrderPaymentID) _
                                 Select t).FirstOrDefault
            If CurrentPayment Is Nothing Then
                Return Nothing
            Else
                Return CurrentPayment
            End If
        End Get
    End Property

    Public ReadOnly Property OrderConsignment() As svConsignmentMethod
        Get
            Dim CurrentConsignment = (From t In ConnContext.svConsignmentMethods _
                                 Where t.OrderConsignmentSourceID.Equals(_OrderConsignmentID) _
                                 Select t).FirstOrDefault
            If CurrentConsignment Is Nothing Then
                Return Nothing
            Else
                Return CurrentConsignment
            End If
        End Get
    End Property

    Public ReadOnly Property DeliveryTraderAddress(ByVal SameSource As Boolean) As SvTraderAddress
        Get
            If _OrderTraderAddressID IsNot Nothing Then
                If SameSource Then
                    Dim Address = (From t In ConnContext.svTraderAddresses _
                                   Where t.SourceID.Equals(_OrderTraderAddressID) _
                                   Select t).FirstOrDefault
                    Return Address
                Else
                    Dim Address = (From t In ConnContext.svTraderAddresses _
                                   Where t.DestinationID.Equals(_OrderTraderAddressID) _
                                   Select t).FirstOrDefault
                    Return Address
                End If
            Else
                Return Nothing
            End If
        End Get
    End Property

    Public ReadOnly Property DeliveryAddress(ByVal SameSource As Boolean) As String
        Get
            Dim Address As String = ""
            Dim FullAddress = DeliveryTraderAddress(SameSource)
            If FullAddress IsNot Nothing Then
                Address = FullAddress.FullAddress1(False, True)
            Else
                Address = OrderTrader.FullAddress(False, True)
            End If
            Return Address
        End Get
    End Property

    Public Sub UpdateDefaultSurcharge(ByVal SurchageValue As Double, ByVal SurchargeVatPerc As Double)
        SurchargeWithVat = SurchageValue
        SurchargeNoVat = Math.Round(Convert.ToDecimal(SurchageValue / (1 + (SurchargeVatPerc / 100))), 2)
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateSurchargeNoVat(ByVal SurchargeVatPerc As Double)
        SurchargeNoVat = Math.Round(Convert.ToDecimal(SurchargeWithVat / (1 + (SurchargeVatPerc / 100))), 2)
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateDefaultShipping(ByVal ShippingCostValue As Double, ByVal ShippingVatPerc As Double)
        ShippingCostWithVat = ShippingCostValue
        ShippingCostNoVat = Math.Round(Convert.ToDecimal(ShippingCostValue / (1 + (ShippingVatPerc / 100))), 2)
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateShippingNoVat(ByVal ShippingVatPerc As Double)
        ShippingCostNoVat = Math.Round(Convert.ToDecimal(ShippingCostWithVat / (1 + (ShippingVatPerc / 100))), 2)
        ConnContext.SubmitChanges()
    End Sub

    Public Sub ApplyZeroDiscount(ByVal WithFinalPrice As Boolean)
        OrderDiscountNoVat = 0
        OrderDiscountWithVat = 0
        For Each dtl In Details
            dtl.ItemDiscountPerc = 0
            dtl.FinalDiscountPerc = 0
            If WithFinalPrice Then
                dtl.UnitCataloguePriceWithVat = dtl.UnitFinalPriceWithVat
                dtl.UnitCataloguePriceNoVat = dtl.UnitFinalPriceNoVat
            Else
                dtl.UnitFinalPriceWithVat = dtl.UnitCataloguePriceWithVat
                dtl.UnitFinalPriceNoVat = dtl.UnitCataloguePriceNoVat
            End If
        Next
        ConnContext.SubmitChanges()
    End Sub

    Public Sub CalcDetailDiscount(ByVal DefaultVatPerc As Double)
        Dim DiscountPerc As Double = Convert.ToDecimal(OrderDiscountWithVat / OrderTotalWithVat)

        For Each dtl In Details
            If DiscountPerc = 0 Then
                dtl.FinalDiscountPerc = dtl.ItemDiscountPerc
                dtl.DiscountNoVatDetailPart = (dtl.ItemDiscountPerc / 100) * dtl.UnitCataloguePriceNoVat
                dtl.DiscountWithVatDetailPart = (dtl.ItemDiscountPerc / 100) * dtl.UnitCataloguePriceWithVat
                dtl.DiscountNoVatOrderPart = 0
                dtl.DiscountWithVatOrderPart = 0
            ElseIf dtl.DetailDiscountPerc > 0 Then
                dtl.FinalDiscountPerc = 100 * (1 - (DiscountPerc * (dtl.ItemDiscountPerc / 100)))
                dtl.DiscountNoVatDetailPart = (dtl.ItemDiscountPerc / 100) * dtl.UnitCataloguePriceNoVat
                dtl.DiscountWithVatDetailPart = (dtl.ItemDiscountPerc / 100) * dtl.UnitCataloguePriceWithVat
                dtl.DiscountNoVatOrderPart = DiscountPerc * dtl.UnitCataloguePriceNoVat
                dtl.DiscountWithVatOrderPart = DiscountPerc * dtl.UnitCataloguePriceWithVat
            Else
                'dtl.FinalDiscountPerc = 100 * DiscountPerc
                dtl.DiscountNoVatDetailPart = 0
                dtl.DiscountWithVatDetailPart = 0
                dtl.DiscountNoVatOrderPart = Math.Round(Convert.ToDecimal(DiscountPerc * dtl.UnitCataloguePriceNoVat), 3)
                dtl.DiscountWithVatOrderPart = Math.Round(Convert.ToDecimal(DiscountPerc * dtl.UnitCataloguePriceWithVat), 3)

                dtl.UnitCataloguePriceNoVat += dtl.DiscountNoVatOrderPart
                dtl.UnitCataloguePriceWithVat += dtl.DiscountWithVatOrderPart
            End If
        Next

        OrderDiscountNoVat = Math.Round(Convert.ToDecimal(OrderDiscountWithVat / (1 + (DefaultVatPerc / 100))), 2)
        ConnContext.SubmitChanges()

        'For Each dtl In svOrderDetails
        '    dtl.CatalogueWithVatToFinalWithVat()
        'Next
    End Sub

    Public Sub UpdateDestination(ByVal svDestinationID As String)
        DestinationID = svDestinationID
        ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property Details() As List(Of SvOrderDetail)
        Get
            Return OrderDetails.ToList
        End Get
    End Property

    ''  to be used in server 2003 fk issue
    Public ReadOnly Property OrderDetails() As IQueryable(Of SvOrderDetail)
        Get
            Dim AllDetails = From t In ConnContext.svOrderDetails _
                             Where t.OrderSourceID.Equals(_SourceID) _
                             Select t
            Return AllDetails
        End Get
    End Property

    Public ReadOnly Property OrderTraderName() As String
        Get
            If OrderTrader IsNot Nothing Then
                Return OrderTrader.FullName
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property OrderPaymentName() As String
        Get
            If OrderPayment IsNot Nothing Then
                Return OrderPayment.Name
            Else
                Return ""
            End If
        End Get
    End Property

    Public ReadOnly Property OrderWarehouse() As SvWarehouse
        Get
            Dim CurrentWarehouse As SvWarehouse
            If _WarehouseSourceID IsNot Nothing Then
                CurrentWarehouse = (From t In ConnContext.svWarehouses _
                                     Where t.SourceID.Equals(_WarehouseSourceID) _
                                     Select t).FirstOrDefault
            ElseIf _WarehouseDestinationID IsNot Nothing Then
                CurrentWarehouse = (From t In ConnContext.svWarehouses _
                                     Where t.DestinationID.Equals(_WarehouseDestinationID) _
                                     Select t).FirstOrDefault
            End If
            If CurrentWarehouse Is Nothing Then
                Return Nothing
            Else
                Return CurrentWarehouse
            End If
        End Get
    End Property

    Public Sub UpdateIerarch(ByVal ID1 As String, ByVal ID2 As String, ByVal ID3 As String)
        Currency = ID1
        SalesmanSourceID = ID2
        OrderConsignmentID = ID3
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateOrderDocument(svOrderStatus As String, svDocTypeID As String, svDocTypeName As String)
        If svOrderStatus <> "" AndAlso OrderStatus Is Nothing Then
            OrderStatus = svOrderStatus
        End If
        If svDocTypeID <> "" AndAlso DocTypeID Is Nothing Then
            DocTypeID = svDocTypeID
        End If
        If svDocTypeName <> "" AndAlso DocTypeName Is Nothing Then
            DocTypeName = svDocTypeName
        End If
        ConnContext.SubmitChanges()
    End Sub
End Class


﻿Public Class SvOrderDetail

    'Private 2 As Integer = 2
    'Public WriteOnly Property DecimalDigits() As Integer
    '    Set(ByVal value As Integer)
    '        2 = value
    '    End Set
    'End Property

    Public ReadOnly Property DetailFinalDiscountPercInt() As Double
        Get
            If UnitCataloguePriceWithVat = 0 Then
                Return 0
            ElseIf FinalDiscountPerc Is Nothing Then
                Return 0 'Math.Round((DetailItemPriceCatalogue - DetailItemPriceFinal) / DetailItemPriceCatalogue, 4)
            Else
                Return FinalDiscountPerc
            End If
        End Get
    End Property

    Public ReadOnly Property DetailDiscountPercInt() As Double
        Get
            If UnitCataloguePriceWithVat = 0 Then
                Return 0
            ElseIf ItemDiscountPerc Is Nothing Then
                Return 0 'Math.Round((DetailItemPriceCatalogue - DetailItemPriceFinal) / DetailItemPriceCatalogue, 4)
            Else
                Return ItemDiscountPerc
            End If
        End Get
    End Property

    Public ReadOnly Property DetailDiscountPerc() As Double
        Get
            Return DetailDiscountPercInt / 100
        End Get
    End Property

    Public ReadOnly Property UnitCatalogueVat() As Decimal
        Get
            Return UnitCataloguePriceWithVat - UnitCataloguePriceNoVat
        End Get
    End Property

    Public ReadOnly Property UnitFinalVat() As Decimal
        Get
            Return UnitFinalPriceWithVat - UnitFinalPriceNoVat
        End Get
    End Property

    Public ReadOnly Property UnitDiscountWithVat() As Decimal
        Get
            Return UnitCataloguePriceWithVat - UnitFinalPriceWithVat
        End Get
    End Property

    Public ReadOnly Property UnitDiscountNoVat() As Decimal
        Get
            Return UnitCataloguePriceNoVat - UnitFinalPriceNoVat
        End Get
    End Property

    Public ReadOnly Property UnitDiscountVat() As Decimal
        Get
            Return UnitDiscountWithVat - UnitDiscountNoVat
        End Get
    End Property


    'Public ReadOnly Property UnitCatalogueDiscountWithVat() As Decimal
    '    Get
    '        Return Math.Round(Convert.ToDecimal(UnitCataloguePriceWithVat * DetailDiscountPerc), 2)
    '    End Get
    'End Property

    'Public ReadOnly Property UnitCatalogueDiscountNoVat() As Decimal
    '    Get
    '        Return Math.Round(Convert.ToDecimal(UnitCataloguePriceNoVat * DetailDiscountPerc), 2)
    '    End Get
    'End Property

    'Public ReadOnly Property UnitCatalogueDiscountVat() As Decimal
    '    Get
    '        Return UnitCatalogueDiscountWithVat - UnitCatalogueDiscountNoVat
    '    End Get
    'End Property

    'Public ReadOnly Property UnitFinalDiscountWithVat() As Decimal
    '    Get
    '        Return Math.Round(Convert.ToDecimal(UnitFinalPriceWithVat * DetailDiscountPerc), 2)
    '    End Get
    'End Property

    'Public ReadOnly Property UnitFinalDiscountNoVat() As Decimal
    '    Get
    '        Return Math.Round(Convert.ToDecimal(UnitFinalPriceNoVat * DetailDiscountPerc), 2)
    '    End Get
    'End Property

    'Public ReadOnly Property UnitFinalDiscountVat() As Decimal
    '    Get
    '        Return UnitFinalDiscountWithVat - UnitFinalDiscountNoVat
    '    End Get
    'End Property

    Public ReadOnly Property LineCatalogueValueWithVat() As Decimal
        Get
            'Return DetailCataloguePr - _SvOrderDetail.DetailCatalogueDiscountFinal ' DetailCatalogueDiscount
            Return Math.Round(Convert.ToDecimal(Quantity * UnitCataloguePriceWithVat), 2)
        End Get
    End Property

    Public ReadOnly Property LineCatalogueValueNoVat() As Decimal
        Get
            'Return Math.Round(_SvOrderDetail.Quantity * DetailItemRowPriceNoVat, 2)
            Return Math.Round(Convert.ToDecimal(Quantity * UnitCataloguePriceNoVat), 2)
        End Get
    End Property

    Public ReadOnly Property LineCatalogueVat() As Decimal
        Get
            Return LineCatalogueValueWithVat - LineCatalogueValueNoVat ' ' DetailCataloguePriceNoVat
        End Get
    End Property

    Public ReadOnly Property LineFinalValueWithVat() As Decimal
        Get
            'Return DetailFinalPr - _SvOrderDetail.DetailFinalDiscountFinal ' DetailFinalDiscount
            Return Math.Round(Convert.ToDecimal(Quantity * UnitFinalPriceWithVat), 2)
        End Get
    End Property

    Public ReadOnly Property LineFinalValueNoVat() As Decimal
        Get
            'Return Math.Round(_SvOrderDetail.Quantity * DetailItemRowPriceNoVat, 2)
            Return Math.Round(Convert.ToDecimal(Quantity * UnitFinalPriceNoVat), 2)
        End Get
    End Property

    Public ReadOnly Property LineFinalVat() As Decimal
        Get
            Return LineFinalValueWithVat - LineFinalValueNoVat ' ' DetailFinalPriceNoVat
        End Get
    End Property

    Public ReadOnly Property LineDiscountWithVat() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(Quantity * UnitDiscountWithVat), 2)
        End Get
    End Property

    Public ReadOnly Property LineDiscountNoVat() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(Quantity * UnitDiscountNoVat), 2)
        End Get
    End Property

    Public ReadOnly Property LineDiscountWithVatDetailPart() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(Quantity * DiscountWithVatDetailPart), 2)
        End Get
    End Property

    Public ReadOnly Property LineDiscountNoVatDetailPart() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(Quantity * DiscountNoVatDetailPart), 2)
        End Get
    End Property

    Public ReadOnly Property LineDiscountWithVatOrderPart() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(Quantity * DiscountWithVatOrderPart), 2)
        End Get
    End Property

    Public ReadOnly Property LineDiscountNoVatOrderPart() As Decimal
        Get
            Return Math.Round(Convert.ToDecimal(Quantity * DiscountNoVatOrderPart), 2)
        End Get
    End Property

    Public ReadOnly Property LineDiscountVat() As Decimal
        Get
            Return LineDiscountWithVat - LineDiscountNoVat ' DetailRowPriceNoVatDiscount
        End Get
    End Property

    'Public ReadOnly Property LineCatalogueDiscountNoVat() As Decimal
    '    Get
    '        Return Math.Round(Convert.ToDecimal(Quantity * UnitCatalogueDiscountNoVat), 2)
    '    End Get
    'End Property

    Public Sub ImportOrderDetail(ByVal IsNew As Boolean)
        _LastModificationSource = Now
        _IsDirty = True

        If IsNew Then
            ConnContext.svOrderDetails.InsertOnSubmit(Me)
        End If
        'ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateDirty(ByVal SetDirty As Boolean)
        IsDirty = SetDirty
        DestinationID = Nothing
    End Sub

    Public Sub LastUpdate(Optional ByVal svDestinationID As String = "")
        If svDestinationID <> "" Then
            DestinationID = svDestinationID
        End If
        IsDirty = False
        LastModificationDestination = Now
        'ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property DetailProduct() As SvProduct
        Get
            Dim CurrentProduct = (From t In ConnContext.svProducts _
                                 Where t.OrderDetailProductSourceID.Equals(_DetailProductSourceID) _
                                 Select t).FirstOrDefault
            If CurrentProduct Is Nothing Then
                Return Nothing
            Else
                Return CurrentProduct
            End If
        End Get
    End Property

    Public Sub FinalNoVatToFinalWithVat()
        If DetailProduct IsNot Nothing Then
            UnitFinalPriceWithVat = Math.Round(Convert.ToDecimal(UnitFinalPriceNoVat * (1 + (DetailProduct.ProductVat.VatPercentage / 100))), 6)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub FinalWithVatToFinalNoVat()
        If DetailProduct IsNot Nothing AndAlso DetailProduct.ProductVat IsNot Nothing Then
            UnitFinalPriceNoVat = Math.Round(Convert.ToDecimal(UnitFinalPriceWithVat / (1 + (DetailProduct.ProductVat.VatPercentage / 100))), 6)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub CatalogueWithVatToCatalogueNoVat()
        If DetailProduct IsNot Nothing AndAlso DetailProduct.ProductVat IsNot Nothing Then
            UnitCataloguePriceNoVat = Math.Round(Convert.ToDecimal(UnitCataloguePriceWithVat / (1 + (DetailProduct.ProductVat.VatPercentage / 100))), 6)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub CatalogueNoVatToCatalogueWithVat()
        If DetailProduct IsNot Nothing AndAlso DetailProduct.ProductVat IsNot Nothing Then
            UnitCataloguePriceWithVat = Math.Round(Convert.ToDecimal(UnitCataloguePriceNoVat * (1 + (DetailProduct.ProductVat.VatPercentage / 100))), 6)
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub ProductRetailPriceToCatalogueNoVat()
        If DetailProduct IsNot Nothing Then
            UnitCataloguePriceNoVat = DetailProduct.RetailPrice
        End If
        ConnContext.SubmitChanges()
        CatalogueWithVatToCatalogueNoVat()
    End Sub

    'Public Sub UpdateDiscount(ByVal NoDiscount As Boolean)
    '    If NoDiscount OrElse UnitCataloguePriceWithVat Is Nothing OrElse UnitCataloguePriceWithVat = 0 Then
    '        FinalDiscountPerc = 0
    '    Else
    '        FinalDiscountPerc = 100 * Math.Round(Convert.ToDecimal((UnitCataloguePriceWithVat - UnitFinalPriceWithVat) / UnitCataloguePriceWithVat), 4)
    '        UnitFinalPriceWithVat = UnitCataloguePriceWithVat * ((100 - FinalDiscountPerc) / 100)
    '        'UnitFinalPriceWithVat = CataloguePrice
    '    End If
    '    ConnContext.SubmitChanges()
    'End Sub

    Public Sub CatalogueWithVatToFinalWithVat()
        UnitFinalPriceWithVat = UnitCataloguePriceWithVat * ((100 - FinalDiscountPerc) / 100)
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateUnitCataloguePriceWithVat(ByVal Price As Decimal)
        UnitCataloguePriceWithVat = Price
        ConnContext.SubmitChanges()
    End Sub
End Class

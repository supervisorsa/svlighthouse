﻿Public Class SvTrader

    Public Sub ImportTrader(ByVal IsNew As Boolean, ByVal TypicalUpdate As Boolean, _
                             Optional ByVal LastModDate As Date = Nothing, Optional ByVal SingleCategoryID As String = "")
        If LastModDate = Date.MinValue Then
            If TypicalUpdate Then
                _LastModificationSource = Now
            Else
                _LastModificationDestination = Now
            End If
        Else
            If TypicalUpdate Then
                _LastModificationSource = LastModDate
            Else
                _LastModificationDestination = LastModDate
            End If
        End If
        _IsDirty = True

        If IsNew Then
            ConnContext.svTraders.InsertOnSubmit(Me)
        End If

        'For Each adr In svTraderAddresses
        '    adr.ImportTraderAddress(IsNew)
        'Next

        ConnContext.SubmitChanges()

        If SingleCategoryID <> "" Then
            Dim CategoriesIDs As New List(Of String)
            CategoriesIDs.Add(SingleCategoryID)
            ImportTraderCategory1(CategoriesIDs)
        End If
    End Sub

    Public Sub ImportTraderCategory1(ByVal CategoriesIDs As List(Of String))
        Dim OldCategories = From t In ConnContext.svTraderInCategories _
                    Where t.TraderSourceID.Equals(_SourceTraderID) _
                    And Not CategoriesIDs.Contains(t.TraderCategorySourceID) _
                    Select t
        ConnContext.svTraderInCategories.DeleteAllOnSubmit(OldCategories)

        For Each CategoryID In CategoriesIDs
            Dim CategoryExists = (From t In ConnContext.svTraderInCategories _
                                Where t.TraderSourceID.Equals(_SourceTraderID) _
                                And t.TraderCategorySourceID.Equals(CategoryID) _
                                Select t).FirstOrDefault

            If CategoryExists Is Nothing Then
                Dim NewCategory As New svTraderInCategory With { _
                .ID = Guid.NewGuid, _
                .TraderSourceID = _SourceTraderID, _
                .TraderCategorySourceID = CategoryID}
                ConnContext.svTraderInCategories.InsertOnSubmit(NewCategory)
            End If
        Next
        ConnContext.SubmitChanges()
    End Sub

    Public ReadOnly Property Categories() As List(Of svTraderCategory)
        Get
            Dim TraderCategories = From t In ConnContext.svTraderInCategories _
                                   Where t.TraderSourceID.Equals(SourceTraderID) _
                                   Select t.TraderCategorySourceID

            Dim CategoriesList = From t In ConnContext.svTraderCategories _
                                Where TraderCategories.Contains(t.SourceID) _
                                Select t
            Return CategoriesList.ToList
        End Get
    End Property

    Public Sub LastUpdate(ByVal TypicalUpdate As Boolean, _
                          Optional ByVal svDestinationTraderID As String = "", _
                          Optional ByVal svDestinationCustomerID As String = "", _
                          Optional ByVal svDestinationTraderCode As String = "", _
                          Optional ByVal svDestinationCustomerCode As String = "")
        'Dim CurrentTrader = (From t In ConnContext.svTraders _
        '                     Where t.ID.Equals(_ID) _
        '                     Select t).FirstOrDefault
        If svDestinationTraderID <> "" Then
            If TypicalUpdate Then
                DestinationTraderID = svDestinationTraderID
            Else
                SourceTraderID = svDestinationTraderID
            End If
        End If
        If svDestinationCustomerID <> "" Then
            If TypicalUpdate Then
                DestinationCustomerID = svDestinationCustomerID
            Else
                SourceCustomerID = svDestinationCustomerID
            End If
        End If
        If svDestinationTraderCode <> "" Then
            If TypicalUpdate Then
                DestinationTraderCode = svDestinationTraderCode
            Else
                SourceTraderCode = svDestinationTraderCode
            End If
        End If
        If svDestinationCustomerCode <> "" Then
            If TypicalUpdate Then
                DestinationCustomerCode = svDestinationCustomerCode
            Else
                SourceCustomerCode = svDestinationCustomerCode
            End If
        End If


        IsDirty = False
        If TypicalUpdate Then
            LastModificationDestination = Now
        Else
            LastModificationSource = Now
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateTraderForOrder(ByVal SameSource As Boolean) ', Optional ByVal UseCustomerValues As Boolean = False
        If SameSource Then
            'If UseCustomerValues Then
            OrderTraderSourceID = _SourceCustomerID
            OrderTraderDestinationID = _DestinationCustomerID
            'Else
            'OrderTraderSourceID = _SourceTraderID
            'OrderTraderDestinationID = _DestinationTraderID
            'End If
        Else
            'If UseCustomerValues Then
            OrderTraderSourceID = _DestinationCustomerID
            OrderTraderDestinationID = _SourceCustomerID
            'Else
            'OrderTraderSourceID = _DestinationTraderID
            'OrderTraderDestinationID = _SourceTraderID
            'End If
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateTraderForTask(ByVal SameSource As Boolean)
        If SameSource Then
            TaskTraderSourceID = _SourceTraderID
            TaskTraderDestinationID = _DestinationTraderID
        Else
            TaskTraderSourceID = _DestinationTraderID
            TaskTraderDestinationID = _SourceTraderID
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Sub UpdateTraderForFinancial(ByVal SameSource As Boolean)
        If SameSource Then
            FinancialTraderSourceID = _SourceTraderID
            FinancialTraderDestinationID = _DestinationTraderID
        Else
            FinancialTraderSourceID = _DestinationTraderID
            FinancialTraderDestinationID = _SourceTraderID
        End If
        ConnContext.SubmitChanges()
    End Sub

    Public Function FullAddress(ByVal IncludeHiLevel As Boolean, ByVal IncludePhones As Boolean) As String
        Dim AddressText As String = ""
        If _Address1 IsNot Nothing AndAlso _Address1 <> "" Then AddressText &= _Address1 & ", "
        If _Address2 IsNot Nothing AndAlso _Address2 <> "" Then AddressText &= _Address2 & ", "
        If _City IsNot Nothing AndAlso _City <> "" Then AddressText &= _City & ", "

        If IncludeHiLevel Then
            If _PostalCode IsNot Nothing AndAlso _PostalCode <> "" Then AddressText &= _PostalCode & ", "
            If _State IsNot Nothing AndAlso _State <> "" Then AddressText &= _State & ", "
            If _Country IsNot Nothing AndAlso _Country <> "" Then AddressText &= _Country & ", "
        End If

        If IncludePhones Then
            If _Phone1 IsNot Nothing AndAlso _Phone1 <> "" Then AddressText &= _Phone1 & ", "
            If _Phone2 IsNot Nothing AndAlso _Phone2 <> "" Then AddressText &= _Phone2 & ", "
            If _MobilePhone IsNot Nothing AndAlso _MobilePhone <> "" Then AddressText &= _MobilePhone & ", "
        End If

        If AddressText <> "" Then AddressText.Substring(0, AddressText.Length - 2)

        Return AddressText
    End Function

    Public Sub UpdateDestination(ByVal svDestinationTraderID As String, ByVal svDestinationCustomerID As String, _
                                 ByVal svTraderCode As String, ByVal svCustomerCode As String)
        DestinationTraderID = svDestinationTraderID
        DestinationCustomerID = svDestinationCustomerID
        DestinationTraderCode = svTraderCode
        DestinationCustomerCode = svCustomerCode
        ConnContext.SubmitChanges()
    End Sub

    Public Function TraderAddresses(ByVal TypicalUpdate As Boolean) As List(Of SvTraderAddress)
        Dim AllAddresses As List(Of SvTraderAddress)
        If TypicalUpdate Then
            AllAddresses = (From t In ConnContext.svTraderAddresses _
                             Where t.CustomerSourceID.ToLower.Equals(_SourceCustomerID.ToLower) _
                             Select t).ToList
        Else
            AllAddresses = (From t In ConnContext.svTraderAddresses _
                             Where t.CustomerDestinationID.ToLower.Equals(_DestinationCustomerID.ToLower) _
                             Select t).ToList
        End If
        Return AllAddresses
    End Function

    'Public Sub UpdateDestinationSalesman(ByVal svDestinationSalesmanID As String)
    '    SalesmanDestinationID = svDestinationSalesmanID
    '    ConnContext.SubmitChanges()
    'End Sub

    'Public Sub UpdateDestinationPayment(ByVal svDestinationPaymentID As String)
    '    PaymentMethodDestinationID = svDestinationPaymentID
    '    ConnContext.SubmitChanges()
    'End Sub

    Public Function TraderCountry() As svCountry
        Dim AllCountries = From t In ConnContext.svCountries _
                             Where t.TraderCountrySourceID.Equals(_CountrySourceID) _
                             Select t

        If AllCountries.Count = 1 Then
            Return AllCountries.FirstOrDefault
        Else
            Return Nothing
        End If

    End Function

    Public Function TraderPaymentMethod() As svPaymentMethod
        Dim AllPayments = From t In ConnContext.svPaymentMethods _
                             Where t.TraderPaymentSourceID.Equals(_PaymentMethodSourceID) _
                             Select t

        If AllPayments.Count = 1 Then
            Return AllPayments.FirstOrDefault
        Else
            Return Nothing
        End If

    End Function

    Public Function ParentDestinationID() As Integer
        Dim DestinationID As Integer = 0
        If _ParentSourceID IsNot Nothing AndAlso _ParentSourceID > 0 Then
            Dim ParentTrader = From t In ConnContext.svTraders _
                             Where t.SourceTraderID.Equals(_ParentSourceID) _
                             Select t
            If ParentTrader.Count > 0 AndAlso ParentTrader.FirstOrDefault.DestinationTraderID IsNot Nothing Then
                DestinationID = ParentTrader.FirstOrDefault.DestinationTraderID
            End If
        End If
        Return DestinationID
    End Function
End Class

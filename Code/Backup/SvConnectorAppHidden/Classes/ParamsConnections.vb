﻿Public Class ParamsConnections
    Inherits SvConnections.ConnectorData

    Public Enum AssocType
        Sellers
        Payways
        Categories
        ContactCategories
    End Enum

    Public CurrentAssociation As AssocType

    Public WriteOnly Property AssociantionType() As Short
        Set(ByVal value As Short)
            CurrentAssociation = value
        End Set
    End Property

    Public Structure CodeStruct

        Private _ID As String
        Private _Code As String
        Private _Name As String

        Public Property Name() As String
            Get
                Return _Name
            End Get
            Set(ByVal value As String)
                _Name = value
            End Set
        End Property

        Public Property Code() As String
            Get
                Return _Code
            End Get
            Set(ByVal value As String)
                _Code = value
            End Set
        End Property

        Public Property ID() As String
            Get
                Return _ID
            End Get
            Set(ByVal value As String)
                _ID = value
            End Set
        End Property

        Public Sub New(ByVal svID As String, _
                       ByVal svCode As String, _
                       ByVal svName As String)
            _ID = svID
            _Code = svCode
            _Name = svName
        End Sub

        Public Overrides Function ToString() As String
            Return _Name
        End Function
    End Structure
End Class

﻿Public Class AssociationsCategoriesF
    Private FsSen_Association As ParamsFsSen
    Private FsControl_Association As ParamsFsControl
    Private OtrSen_Association As ParamsOtrSen

    Private CurrentTaskParams As SV.TaskParamsRow

    Private _TaskID As Integer
    Public WriteOnly Property TaskID() As Integer
        Set(ByVal value As Integer)
            _TaskID = value
            If TaskParams.GetDataByTaskID(value).Count = 0 Then
                TaskParams.Insert(value, False, False, False, False, False, False, "", "", False)
            End If
            CurrentTaskParams = TaskParams.GetDataByTaskID(value)(0)
        End Set
    End Property

    Private _ConnType1 As ConnectionType
    Public WriteOnly Property ConnType1() As Short
        Set(ByVal value As Short)
            _ConnType1 = value
        End Set
    End Property

    Private _ConnType2 As ConnectionType
    Public WriteOnly Property ConnType2() As Short
        Set(ByVal value As Short)
            _ConnType2 = value
        End Set
    End Property

    Private CurrentAssociation As ParamsFs.AssocType
    Public WriteOnly Property ParamsType() As Short
        Set(ByVal value As Short)
            CurrentAssociation = value
            If _ConnType2 = ConnectionType.Sen Then
                If _ConnType1 = ConnectionType.Footsteps Then
                    FsSen_Association = New ParamsFsSen
                    FsSen_Association.CurrentAssociation = value
                ElseIf _ConnType1 = ConnectionType.Otr Then
                    OtrSen_Association = New ParamsOtrSen
                    OtrSen_Association.CurrentAssociation = value
                End If
                'Else
                '    FsControl_Association = New ParamsFsControl
                '    FsControl_Association.CurrentAssociation = value
            End If
            Select Case CurrentAssociation
                Case ParamsFs.AssocType.Categories
                    chkAutoAssociation.Checked = CurrentTaskParams.Stock_AutoCategories
                    pnlParams.Visible = False
                Case ParamsFs.AssocType.ContactCategories
                    Me.Text = "Κατηγορίες πελατών"

                    chkAutoAssociation.Enabled = False

                    chkAutoAssociation.Checked = CurrentTaskParams.Contacts_AutoCategories
                    txtCurrency.Text = If(IsDBNull(CurrentTaskParams.Contacts_Currency), "", CurrentTaskParams.Contacts_Currency)
                    txtAcaCode.Text = CurrentTaskParams.Contacts_AcaCode
            End Select
        End Set
    End Property
    Private Sub SalesPersonsF_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If _ConnType2 = ConnectionType.Sen Then
            If _ConnType1 = ConnectionType.Footsteps Then
                FsSen_Association.DisconnectSql()
                FsSen_Association.DisconnectSen()
            ElseIf _ConnType1 = ConnectionType.Otr Then
                OtrSen_Association.DisconnectSql()
                OtrSen_Association.DisconnectSen()
            End If
            'Else
            '    FsControl_Association = New ParamsFsControl
            '    FsControl_Association.CurrentAssociation = value
        End If
    End Sub

    Private Sub SalesPersonF_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim TasksList As SvConnections.SV.TasksDataTable

        'FsSen_Association = New ParamsFsSen
        'FsSen_Association.CurrentAssociation = CurrentAssociation

        Try
            TasksList = Tasks.GetDataByID(_TaskID)
        Catch exList As Exception
            MessageBox.Show(exList.Message)
            Exit Sub
        End Try
        Try
            For Each task In TasksList
                If _ConnType2 = ConnectionType.Sen Then
                    If SvConnTrigger(FsSen_Association, task.SvConnID) Then
                        If _ConnType1 = ConnectionType.Footsteps Then

                            If ConnectionTrigger(FsSen_Association, _
                                              task.ConnectionType1, _
                                              task.ConnectionID1) _
                            AndAlso ConnectionTrigger(FsSen_Association, _
                                                      task.ConnectionType2, _
                                                      task.ConnectionID2) Then
                                Try
                                    AddNodes(FsSen_Association.FsList, trvFS)
                                    AddNodes(FsSen_Association.SenList, trvSEN)

                                    Me.lvwAssociations.Items.Clear()

                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try

                                If CurrentAssociation = ParamsFs.AssocType.Categories Then
                                    PopulateCategories()
                                Else
                                    PopulateContactCategories()
                                End If

                            End If
                        ElseIf _ConnType1 = ConnectionType.Otr Then
                            If ConnectionTrigger(OtrSen_Association, _
                                            task.ConnectionType1, _
                                            task.ConnectionID1) _
                          AndAlso ConnectionTrigger(OtrSen_Association, _
                                                    task.ConnectionType2, _
                                                    task.ConnectionID2) Then
                                Try
                                    AddNodes(OtrSen_Association.OtrList, trvFS)
                                    AddNodes(OtrSen_Association.SenList, trvSEN)

                                    Me.lvwAssociations.Items.Clear()

                                Catch ex As Exception
                                    MessageBox.Show(ex.Message)
                                End Try

                                If CurrentAssociation = ParamsOtr.AssocType.Categories Then
                                    PopulateCategories()
                                Else
                                    PopulateContactCategories()
                                End If

                            End If
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub AddNodes(ByVal CatList As Collections.Generic.List( _
                         Of ParamsFs.CodeStruct), _
                         ByVal trv As TreeView)
        Dim NewNode As TreeNode
        Dim myNode As TreeNode

        For Each item In CatList
            NewNode = trv.Nodes.Add(item.Name)
            NewNode.Tag = item.ID
            NewNode.ToolTipText = item.Code
            NewNode.EnsureVisible()
        Next

        Dim lChange As Boolean
        Do
            lChange = False
            For Each node As TreeNode In trv.Nodes
                If node.ToolTipText <> "0" Then
                    For Each subnode As TreeNode In trv.Nodes
                        If subnode.Tag = node.ToolTipText Then
                            myNode = node.Clone
                            subnode.Nodes.Add(myNode)
                            node.Remove()
                            lChange = True
                            Exit For
                        End If
                    Next
                End If
                If lChange Then Exit For
            Next
            If Not lChange Then Exit Do
        Loop
    End Sub

    Private Sub PopulateCategories()
        Dim CategoriesList As SvConnections.SV.ParamsSen_ItemsDataTable
        CategoriesList = ParamsSen_Items.GetDataByTaskID(_TaskID)

        For Each cat In CategoriesList
            Dim SqlChecked As Boolean = False
            Dim SenChecked As Boolean = False
            Dim lstX As New ListViewItem

            For Each sql As TreeNode In trvFS.Nodes
                lstX = CheckForAssociatedNodes(sql, cat.SqlCategoryID)
                If lstX.SubItems.Count = 3 Then
                    SqlChecked = True
                    Exit For
                End If
            Next

            If SqlChecked Then
                For Each sen As TreeNode In trvSEN.Nodes
                    Dim lstX1 = CheckForAssociatedNodes(sen, cat.SenCategoryID, lstX)

                    If lstX1.SubItems.Count = 6 Then
                        lstX = lstX1
                        SenChecked = True
                        Exit For
                    End If
                Next
                If SenChecked Then lvwAssociations.Items.Add(lstX)
            End If
        Next
    End Sub

    Private Sub PopulateContactCategories()
        Dim CategoriesList As SvConnections.SV.ParamsSen_ContactsDataTable
        CategoriesList = ParamsSen_Contacts.GetDataByTaskID(_TaskID)

        For Each cat In CategoriesList
            Dim SqlChecked As Boolean = False
            Dim SenChecked As Boolean = False
            Dim lstX As New ListViewItem

            For Each sql As TreeNode In trvFS.Nodes
                lstX = CheckForAssociatedNodes(sql, cat.SqlCategoryID)
                If lstX.SubItems.Count = 3 Then
                    SqlChecked = True
                    Exit For
                End If
            Next

            If SqlChecked Then
                For Each sen As TreeNode In trvSEN.Nodes
                    Dim lstX1 = CheckForAssociatedNodes(sen, cat.SenCategoryID, lstX)

                    If lstX1.SubItems.Count = 6 Then
                        lstX = lstX1
                        SenChecked = True
                        Exit For
                    End If
                Next
                If SenChecked Then lvwAssociations.Items.Add(lstX)
            End If
        Next
    End Sub

    Private Sub lvwAssociations_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvwAssociations.DoubleClick
        SendAssociationsBack(lvwAssociations.SelectedItems(0))
    End Sub

    Private Function CheckForAssociatedNodes(ByVal node As TreeNode, _
                                             ByVal CatID As String, _
                                             Optional ByVal ExistingItem As ListViewItem = Nothing) As ListViewItem
        Dim lstX As New ListViewItem
        Dim lstSub1 As New ListViewItem.ListViewSubItem
        Dim lstSub2 As New ListViewItem.ListViewSubItem
        Dim lstSub3 As New ListViewItem.ListViewSubItem

        If node.Tag = CatID Then
            If ExistingItem IsNot Nothing Then
                lstX = ExistingItem
                lstSub1.Text = node.Text
                lstSub2.Text = node.ToolTipText
                lstSub3.Text = node.Tag
                lstX.SubItems.Add(lstSub1)
                lstX.SubItems.Add(lstSub2)
                lstX.SubItems.Add(lstSub3)
                node.ForeColor = Drawing.Color.Red
            Else
                lstX.Text = node.Text
                lstSub1.Text = node.ToolTipText
                lstSub2.Text = node.Tag
                lstX.SubItems.Add(lstSub1)
                lstX.SubItems.Add(lstSub2)
                node.ForeColor = Drawing.Color.Red
            End If
            Return lstX
        Else
            For Each subnode In node.Nodes
                lstX = CheckForAssociatedNodes(subnode, CatID, ExistingItem)
                If lstX.SubItems.Count = 3 Or lstX.SubItems.Count = 6 Then Exit For
            Next
            Return lstX
        End If
    End Function

    Private Sub SendAssociationsBack(ByVal SelectedItem As ListViewItem)
        For Each sql As TreeNode In trvFS.Nodes
            SendNodeBack(sql, SelectedItem.SubItems(2).Text)
        Next
        If SelectedItem.SubItems.Count = 6 Then
            For Each sen As TreeNode In trvSEN.Nodes
                SendNodeBack(sen, SelectedItem.SubItems(5).Text)
            Next
        End If

        lvwAssociations.Items.Remove(SelectedItem)
    End Sub

    Private Sub SendNodeBack(ByVal node As TreeNode, ByVal CatID As String)
        If node.Tag = CatID Then
            node.ForeColor = Drawing.Color.Black
        Else
            For Each subnode In node.Nodes
                SendNodeBack(subnode, CatID)
            Next
        End If
    End Sub

    Private Function ConnectionTrigger(ByVal ParamsSen_Association As Object, _
                                  ByVal ConnectionType As String, _
                                  ByVal ConnectionID As Integer) _
                                  As Boolean
        Dim ErrorMessage As String = ""
        Select Case ConnectionType
            Case "SEN"
                Dim SenList
                SenList = SenConn.GetDataByID(ConnectionID).Item(0)
                If SenList Is Nothing Then
                    MessageBox.Show(ErrorMessage)
                    Return False
                Else
                    ErrorMessage = ParamsSen_Association.ConnectToSen1( _
                        SenList.host, _
                        SenList.sasalias, _
                        SenList.senuser, _
                        SenList.senpwd, _
                        SenList.cmpcode, _
                        SenList.wrhid, _
                        SenList.port)
                    If Not ErrorMessage.Equals(String.Empty) Then
                        ParamsSen_Association.DisconnectSen()
                        MessageBox.Show(ErrorMessage)
                        Return False
                    End If
                End If
            Case "SRS"
                Dim SrsList
                SrsList = SrsConn.GetDataByID(ConnectionID).Item(0)
                If SrsList Is Nothing Then
                    MessageBox.Show(ErrorMessage)
                    Return False
                Else
                    ErrorMessage = ParamsSen_Association.ConnectToSrs( _
                        SrsList.host, _
                        SrsList.port, _
                        SrsList.sasalias, _
                        SrsList.srsuser, _
                        SrsList.srspwd)
                    If Not ErrorMessage.Equals(String.Empty) Then
                        ParamsSen_Association.DisconnectSrs()
                        MessageBox.Show(ErrorMessage)
                        Return False
                    End If
                End If
            Case Else
                If ConnectionType = "Footsteps" Or ConnectionType = "OTR" Then
                    Dim SqlList
                    SqlList = SqlConn.GetDataByID(ConnectionID).Item(0)
                    If SqlList Is Nothing Then
                        MessageBox.Show(ErrorMessage)
                        Return False
                    Else
                        ErrorMessage = ParamsSen_Association.ConnectToSql( _
                            SqlList.server, _
                            SqlList.db, _
                            SqlList.user, _
                            SqlList.pwd)
                        If Not ErrorMessage.Equals(String.Empty) Then
                            ParamsSen_Association.DisconnectSql()
                            MessageBox.Show(ErrorMessage)
                            Return False
                        End If
                    End If
                Else
                    MessageBox.Show("Άγνωστος τύπος σύνδεσης")
                    Return False
                End If
        End Select
        Return True

    End Function

    Private Sub trvFS_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles trvFS.DoubleClick
        If trvFS.SelectedNode.ForeColor <> Drawing.Color.Red Then
            Dim SelectedItem As TreeNode = trvFS.SelectedNode
            Dim lstX As New ListViewItem
            Dim lstSub As New ListViewItem.ListViewSubItem
            Dim lstSub1 As New ListViewItem.ListViewSubItem

            lstX = lvwAssociations.Items.Add(trvFS.SelectedNode.Text)
            lstSub.Text = trvFS.SelectedNode.ToolTipText 'Me.TreeView1.SelectedNode.Tag
            lstSub1.Text = trvFS.SelectedNode.Tag 'Me.TreeView1.SelectedNode.Tag
            lstX.SubItems.Add(lstSub)
            lstX.SubItems.Add(lstSub1)
            trvFS.SelectedNode.ForeColor = Drawing.Color.Red
        End If
    End Sub

    Private Sub trvSEN_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles trvSEN.DoubleClick
        If trvSEN.SelectedNode.ForeColor <> Drawing.Color.Red Then
            Dim ni As Integer
            Dim lstX As New ListViewItem
            Dim lstSub As New ListViewItem.ListViewSubItem
            Dim lstSub1 As New ListViewItem.ListViewSubItem
            Dim lstSub2 As New ListViewItem.ListViewSubItem

            For ni = 0 To lvwAssociations.Items.Count - 1
                If lvwAssociations.Items(ni).SubItems.Count = 3 Then 'lvwAssociations.Items(ni).SubItems(2).Text = "" Then
                    lstSub.Text = trvSEN.SelectedNode.Text
                    lvwAssociations.Items(ni).SubItems.Add(lstSub)
                    lstSub1.Text = trvSEN.SelectedNode.ToolTipText
                    lvwAssociations.Items(ni).SubItems.Add(lstSub1)
                    lstSub2.Text = trvSEN.SelectedNode.Tag
                    lvwAssociations.Items(ni).SubItems.Add(lstSub2)
                    trvSEN.SelectedNode.ForeColor = Drawing.Color.Red
                    Exit For
                    'Else
                    'lstSub.Text = trvSEN.SelectedNode.Text
                    'lvwAssociations.Items(lvwAssociations.Items.Count - 1).SubItems.Add(lstSub)
                    'lstSub1.Text = trvSEN.SelectedNode.ToolTipText
                    'lvwAssociations.Items(lvwAssociations.Items.Count - 1).SubItems.Add(lstSub1)
                    'lstSub2.Text = trvSEN.SelectedNode.Tag
                    'lvwAssociations.Items(ni).SubItems.Add(lstSub2)
                    'trvSEN.SelectedNode.ForeColor = Drawing.Color.Red
                    'Exit For
                End If
            Next ni
        End If
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If CurrentAssociation = ParamsFs.AssocType.Categories Then
            TaskParams.UpdateStock(chkAutoAssociation.Checked, _TaskID)

            ParamsSen_Items.DeleteQuery(_TaskID)
            For Each item As ListViewItem In lvwAssociations.Items
                If item.SubItems.Count = 6 Then
                    ParamsSen_Items.Insert(_TaskID, _
                                             item.SubItems(2).Text, _
                                             item.SubItems(0).Text, _
                                             item.SubItems(5).Text, _
                                             item.SubItems(3).Text, _
                                             0)
                End If
            Next
        Else
            TaskParams.UpdateContacts(txtAcaCode.Text, txtCurrency.Text, chkAutoAssociation.Checked, _TaskID)

            ParamsSen_Contacts.DeleteQuery(_TaskID)
            For Each item As ListViewItem In lvwAssociations.Items
                If item.SubItems.Count = 6 Then
                    ParamsSen_Contacts.Insert(_TaskID, _
                                             item.SubItems(2).Text, _
                                             item.SubItems(0).Text, _
                                             item.SubItems(1).Text, _
                                             item.SubItems(5).Text, _
                                             item.SubItems(3).Text, _
                                             item.SubItems(4).Text)
                End If
            Next
        End If
        Close()
    End Sub

    Private Function SvConnTrigger(ByVal ParamsSen_Association As Object, _
                                  ByVal ConnectionID As Integer) _
                                  As Boolean
        Dim ErrorMessage As String = ""

        Dim SqlList
        SqlList = SqlConn.GetDataByID(ConnectionID).Item(0)
        If SqlList Is Nothing Then
            SvSys.ResultsLog("Δεν υπάρχει σύνδεση με τον SQL Server για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
            Return False
        Else
            ErrorMessage = ParamsSen_Association.ConnectToSql( _
                SqlList.server, _
                SqlList.db, _
                SqlList.user, _
                SqlList.pwd)
            If Not ErrorMessage.Equals(String.Empty) Then
                ParamsSen_Association.DisconnectSql()
                SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                SvSys.ResultsLog("Δεν υπάρχει επικοινωνία με τον SQL Server", My.Application.Info.DirectoryPath)
                Return False
            End If
        End If
        Return True
    End Function

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

End Class


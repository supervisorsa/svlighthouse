﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.btnSchedules = New System.Windows.Forms.Button
        Me.btnTasks = New System.Windows.Forms.Button
        Me.btnConnections = New System.Windows.Forms.Button
        Me.btnRun = New System.Windows.Forms.Button
        Me.btnError = New System.Windows.Forms.Button
        Me.btnInfo = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnTools = New System.Windows.Forms.Button
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.btnSchedules)
        Me.GroupBox1.Controls.Add(Me.btnTasks)
        Me.GroupBox1.Controls.Add(Me.btnConnections)
        Me.GroupBox1.Location = New System.Drawing.Point(17, 13)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(915, 72)
        Me.GroupBox1.TabIndex = 17
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Εργασίες"
        '
        'btnSchedules
        '
        Me.btnSchedules.Location = New System.Drawing.Point(612, 20)
        Me.btnSchedules.Name = "btnSchedules"
        Me.btnSchedules.Size = New System.Drawing.Size(292, 40)
        Me.btnSchedules.TabIndex = 25
        Me.btnSchedules.Text = "Διαχείριση χρονοπρογραμματισμών"
        Me.btnSchedules.UseVisualStyleBackColor = True
        '
        'btnTasks
        '
        Me.btnTasks.Location = New System.Drawing.Point(309, 20)
        Me.btnTasks.Name = "btnTasks"
        Me.btnTasks.Size = New System.Drawing.Size(292, 40)
        Me.btnTasks.TabIndex = 24
        Me.btnTasks.Text = "Διαχείριση εργασιών"
        Me.btnTasks.UseVisualStyleBackColor = True
        '
        'btnConnections
        '
        Me.btnConnections.Location = New System.Drawing.Point(7, 20)
        Me.btnConnections.Name = "btnConnections"
        Me.btnConnections.Size = New System.Drawing.Size(292, 40)
        Me.btnConnections.TabIndex = 23
        Me.btnConnections.Text = "Διαχείριση συνδέσεων"
        Me.btnConnections.UseVisualStyleBackColor = True
        '
        'btnRun
        '
        Me.btnRun.BackColor = System.Drawing.Color.PeachPuff
        Me.btnRun.Location = New System.Drawing.Point(326, 157)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(292, 40)
        Me.btnRun.TabIndex = 14
        Me.btnRun.Text = "Έναρξη τώρα"
        Me.btnRun.UseVisualStyleBackColor = False
        '
        'btnError
        '
        Me.btnError.Location = New System.Drawing.Point(326, 100)
        Me.btnError.Name = "btnError"
        Me.btnError.Size = New System.Drawing.Size(292, 40)
        Me.btnError.TabIndex = 24
        Me.btnError.Text = "Προβολή αρχείου λαθών"
        Me.btnError.UseVisualStyleBackColor = True
        '
        'btnInfo
        '
        Me.btnInfo.Location = New System.Drawing.Point(24, 100)
        Me.btnInfo.Name = "btnInfo"
        Me.btnInfo.Size = New System.Drawing.Size(292, 40)
        Me.btnInfo.TabIndex = 25
        Me.btnInfo.Text = "Προβολή αρχείου καταγραφής"
        Me.btnInfo.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(629, 100)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(292, 40)
        Me.btnExit.TabIndex = 26
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnTools
        '
        Me.btnTools.Location = New System.Drawing.Point(24, 157)
        Me.btnTools.Name = "btnTools"
        Me.btnTools.Size = New System.Drawing.Size(292, 40)
        Me.btnTools.TabIndex = 27
        Me.btnTools.Text = "Εργαλεία δεδομένων"
        Me.btnTools.UseVisualStyleBackColor = True
        '
        'MainF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(947, 213)
        Me.Controls.Add(Me.btnTools)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnError)
        Me.Controls.Add(Me.btnInfo)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.MaximizeBox = False
        Me.Name = "MainF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Παραμετροποίηση"
        Me.GroupBox1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btnRun As System.Windows.Forms.Button
    Friend WithEvents btnTasks As System.Windows.Forms.Button
    Friend WithEvents btnConnections As System.Windows.Forms.Button
    Friend WithEvents btnSchedules As System.Windows.Forms.Button
    Friend WithEvents btnError As System.Windows.Forms.Button
    Friend WithEvents btnInfo As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnTools As System.Windows.Forms.Button

End Class

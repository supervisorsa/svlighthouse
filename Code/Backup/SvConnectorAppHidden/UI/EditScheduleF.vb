﻿Public Class EditScheduleF
    Private _ID As Integer = 0
    Public WriteOnly Property ID() As Integer
        Set(ByVal value As Integer)
            _ID = value
            GetDataSource()
        End Set
    End Property

    Private Sub GetDataSource()
        Dim SelectedSchedule As SvConnections.SV.SchedulesRow
        Dim SelectedSchedules As SvConnections.SV.SchedulesDataTable = Schedules.GetDataByID(_ID)
        If SelectedSchedules.Count > 0 Then

            SelectedSchedule = SelectedSchedules.Item(0)

            Dim SelectedOccurrences As SvConnections.SV.ScheduleOccurrencesDataTable = SchedulesOccurrences.GetDataByScheduleID(_ID)

            rdbScheduleIsDaily.Checked = True
            rdbScheduleIsMinutes.Checked = True

            If SelectedSchedule.IsDaily Then
                rdbScheduleIsDaily.Checked = True
                dgvDaily.DataSource = SelectedOccurrences
                For Each col As Windows.Forms.DataGridViewColumn In dgvDaily.Columns
                    If col.Name.ToLower.Contains("daily") Then
                        col.HeaderText = "Ώρα"
                        col.DisplayIndex = 2
                        col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                        col.DefaultCellStyle.Format = "HH:mm"
                    Else
                        col.Visible = False
                    End If
                Next
                dgvDaily.Refresh()
            ElseIf SelectedSchedule.IsMinutes Then
                rdbScheduleIsMinutes.Checked = True
                dgvDaily.DataSource = Nothing
                nupMinutes.Value = SelectedOccurrences(0).MinutesInterval
            End If

            txtScheduleName.Text = SelectedSchedule.ScheduleName

            chkDefault.Checked = SelectedSchedule.IsDefault

            SelectedSchedule = Nothing
        End If
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        ''  NEEDS TO BE DONE
        ''  FOR EACH TASK IN SCHEDULE
        ''  UPDATENEXTRUN

        If Not txtScheduleName.Text.Equals(String.Empty) Then
            Dim DefaultValue As Boolean = chkDefault.Checked
            If Schedules.GetSchedulesCount(_ID) = 0 Then DefaultValue = True
            If _ID = 0 Then
                Schedules.Insert(txtScheduleName.Text, _
                                 DefaultValue, _
                                 rdbScheduleIsDaily.Checked, _
                                 rdbScheduleIsMinutes.Checked)
                _ID = Schedules.GetLastID
            Else
                Schedules.UpdateQuery(txtScheduleName.Text, _
                                 DefaultValue, _
                                 rdbScheduleIsDaily.Checked, _
                                 rdbScheduleIsMinutes.Checked, _
                                 _ID)

            End If

            SchedulesOccurrences.DeleteByScheduleID(_ID)
            If rdbScheduleIsDaily.Checked Then
                If dgvDaily.DataSource Is Nothing Then
                    SchedulesOccurrences.Insert(_ID, 1000, FormatDateTime("2:00", DateFormat.ShortTime)) '" New  Date(2020,1,1,2,0,0)
                Else
                    For Each item In dgvDaily.DataSource
                        SchedulesOccurrences.Insert(_ID, 1000, FormatDateTime(item.DailyOccurrence, DateFormat.ShortTime)) '" New  Date(2020,1,1,2,0,0)
                    Next
                End If
            ElseIf rdbScheduleIsMinutes.Checked Then
                SchedulesOccurrences.Insert(_ID, nupMinutes.Value, FormatDateTime("2:00", DateFormat.ShortTime)) '" New  Date(2020,1,1,2,0,0)
            End If

            If chkDefault.Checked Then Schedules.ResetDefault(_ID)

            GetDataSource()
        End If
    End Sub

    Private Function DefaultScheduleID() As Integer
        Schedules.Fill(MDB.Schedules)
        Dim ScheduleID = Schedules.GetDefaultID
        Return If(ScheduleID Is Nothing, 0, ScheduleID)
    End Function

    Private Sub EditTaskForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If _ID = 0 Then
            rdbScheduleIsDaily.Checked = True
            If DefaultScheduleID() = 0 Then chkDefault.Checked = True
        End If
    End Sub

    Private Sub rdbScheduleIsDaily_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbScheduleIsDaily.CheckedChanged
        If rdbScheduleIsDaily.Checked Then EnableType(True)
    End Sub

    Private Sub rdbScheduleIsMinutes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdbScheduleIsMinutes.CheckedChanged
        If rdbScheduleIsMinutes.Checked Then EnableType(False)
    End Sub

    Private Sub EnableType(ByVal bool As Boolean)
        dgvDaily.Enabled = bool
        btnAdd.Enabled = bool And _ID <> 0
        btnDelete.Enabled = bool And _ID <> 0
        lblMinutes1.Enabled = Not bool
        lblMinutes2.Enabled = Not bool
        nupMinutes.Enabled = Not bool
    End Sub

    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        Dim frm As New TimeF
        frm.ShowDialog()
        SchedulesOccurrences.Insert(_ID, 1000, FormatDateTime(frm.TimeValue, DateFormat.ShortTime))
        GetDataSource()
    End Sub

    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click
        If dgvDaily.SelectedCells.Count = 0 Then Exit Sub

        If dgvDaily.RowCount > 1 Then
            If MessageBox.Show("Θέλετε σίγουρα να διαγράψετε την επιλεγμένη γραμμή;", _
                               "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) Then
                Dim RowIndex As Short
                RowIndex = dgvDaily.SelectedCells(0).RowIndex
                SchedulesOccurrences.DeleteQuery(dgvDaily.Rows(RowIndex).Cells("id").Value)
                GetDataSource()
            End If
        End If
    End Sub
End Class
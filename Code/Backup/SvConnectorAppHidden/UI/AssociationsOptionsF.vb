﻿Public Class AssociationsOptionsF
    Private _TaskID As Integer
    Public WriteOnly Property TaskID() As Integer
        Set(ByVal value As Integer)
            _TaskID = value
        End Set
    End Property

    Private _ConnType1 As ConnectionType
    Public WriteOnly Property ConnType1() As Short
        Set(ByVal value As Short)
            _ConnType1 = value
        End Set
    End Property

    Private _ConnType2 As ConnectionType
    Public WriteOnly Property ConnType2() As Short
        Set(ByVal value As Short)
            _ConnType2 = value
        End Set
    End Property

    Private Sub btnClose_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnClose.Click
        Close()
    End Sub

    Private Sub btnPayways_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPayways.Click
        Dim frm As New AssociationsItemsF
        frm.TaskID = _TaskID
        frm.ConnType1 = _ConnType1
        frm.ConnType2 = _ConnType2
        frm.ParamsType = 1
        frm.ShowDialog()
    End Sub

    Private Sub btnSalesPersons_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalesPersons.Click
        Dim frm As New AssociationsItemsF
        frm.TaskID = _TaskID
        frm.ConnType1 = _ConnType1
        frm.ConnType2 = _ConnType2
        frm.ParamsType = 0
        frm.ShowDialog()
    End Sub

    Private Sub btnCategories_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCategories.Click
        Dim frm As New AssociationsCategoriesF
        frm.TaskID = _TaskID
        frm.ConnType1 = _ConnType1
        frm.ConnType2 = _ConnType2
        frm.ParamsType = 2
        frm.ShowDialog()
    End Sub

    Private Sub btnContactCategories_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContactCategories.Click
        Dim frm As New AssociationsCategoriesF
        frm.TaskID = _TaskID
        frm.ConnType1 = _ConnType1
        frm.ConnType2 = _ConnType2
        frm.ParamsType = 3
        frm.ShowDialog()
    End Sub

    Private Sub btnCopyFrom_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopyFrom.Click
        Dim frm As New AssociationsCopyF
        frm.TaskID = _TaskID
        frm.CopyFrom = True
        frm.ShowDialog()
    End Sub

    Private Sub btnCopyTo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCopyTo.Click
        Dim frm As New AssociationsCopyF
        frm.TaskID = _TaskID
        frm.CopyFrom = False
        frm.ShowDialog()
    End Sub
End Class
﻿Public Class TaskExecF

    Private Sub TaskSelectionF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        GetDataSource()
    End Sub

    Private Sub GetDataSource()
        Try
            dgvTasks.DataSource = Tasks.GetFullData
            For Each col As Windows.Forms.DataGridViewColumn In dgvTasks.Columns
                If col.Name.ToLower.Contains("id") Then
                    col.Visible = False
                ElseIf col.Name.ToLower.Contains("taskname") Then
                    col.HeaderText = "Όνομασία"
                    col.DisplayIndex = 1
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("active") Then
                    col.HeaderText = "Ενεργό"
                    col.DisplayIndex = 2
                    col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("connectiontype1") Then
                    col.HeaderText = "Τύπος σύνδεσης 1"
                    col.DisplayIndex = 3
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("connectiontype2") Then
                    col.HeaderText = "Τύπος σύνδεσης 2"
                    col.DisplayIndex = 4
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("connectionname1") Then
                    col.HeaderText = "Σύνδεση 1"
                    col.DisplayIndex = 4
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("connectionname2") Then
                    col.HeaderText = "Σύνδεση 2"
                    col.DisplayIndex = 6
                    'col.AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells
                ElseIf col.Name.ToLower.Contains("lastrun") Then
                    col.HeaderText = "Εκτελέστηκε"
                    col.DisplayIndex = 7
                ElseIf col.Name.ToLower.Contains("nextrun") Then
                    col.HeaderText = "Επόμενη εκτέλεση"
                    col.DisplayIndex = 8
                End If
            Next
            dgvTasks.Refresh()
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        If dgvTasks.SelectedRows.Count = 0 Then
            dgvTasks.SelectAll()
        End If

        Dim AllIDs As New System.Collections.Generic.List(Of Integer)
        For Each item In dgvTasks.SelectedRows
            AllIDs.Add(item.Cells(0).Value)
        Next

        Dim Tasks As New SvConnections.TasksExecution

        Tasks.RunConnector(AllIDs)
    End Sub
End Class
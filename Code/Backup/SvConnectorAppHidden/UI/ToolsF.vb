﻿Public Class ToolsF

    Private EnDecrypt As New SvCrypto(SvCrypto.SymmProvEnum.Rijndael)
    Private GlobalKey As String = "Connr"

    Private Sub btnOrderRelease_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOrderRelease.Click
        If txtOrderNo.Text <> "" Then
            Dim ConnSession As New SvSessionsConn.SvConnData

            ConnSession.Connect(Settings.GetString("Server"), Settings.GetString("User"), _
                                EnDecrypt.Decrypting(Settings.GetString("Pwd"), GlobalKey, "-"), Settings.GetString("DB"))

            Dim ResultMessage As String = ConnSession.SetOrderDirty(rdbSource.Checked, txtOrderNo.Text)

            MessageBox.Show(ResultMessage, "Παραγγελία " & txtOrderNo.Text, MessageBoxButtons.OK, MessageBoxIcon.Information)
        End If
    End Sub

End Class
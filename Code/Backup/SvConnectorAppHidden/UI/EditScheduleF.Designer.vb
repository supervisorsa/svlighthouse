﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditScheduleF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rdbScheduleIsMinutes = New System.Windows.Forms.RadioButton
        Me.rdbScheduleIsDaily = New System.Windows.Forms.RadioButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.btnDelete = New System.Windows.Forms.Button
        Me.btnAdd = New System.Windows.Forms.Button
        Me.dgvDaily = New System.Windows.Forms.DataGridView
        Me.nupMinutes = New System.Windows.Forms.NumericUpDown
        Me.lblMinutes1 = New System.Windows.Forms.Label
        Me.lblMinutes2 = New System.Windows.Forms.Label
        Me.chkDefault = New System.Windows.Forms.CheckBox
        Me.txtScheduleName = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvDaily, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.nupMinutes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(293, 163)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(270, 50)
        Me.btnExit.TabIndex = 23
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(12, 163)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(270, 50)
        Me.btnSave.TabIndex = 22
        Me.btnSave.Text = "Αποθήκευση"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdbScheduleIsMinutes)
        Me.GroupBox1.Controls.Add(Me.rdbScheduleIsDaily)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 48)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(195, 96)
        Me.GroupBox1.TabIndex = 24
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Τύπος χρονοπρογραμματισμού"
        '
        'rdbScheduleIsMinutes
        '
        Me.rdbScheduleIsMinutes.AutoSize = True
        Me.rdbScheduleIsMinutes.Location = New System.Drawing.Point(23, 63)
        Me.rdbScheduleIsMinutes.Name = "rdbScheduleIsMinutes"
        Me.rdbScheduleIsMinutes.Size = New System.Drawing.Size(68, 18)
        Me.rdbScheduleIsMinutes.TabIndex = 16
        Me.rdbScheduleIsMinutes.TabStop = True
        Me.rdbScheduleIsMinutes.Text = "Συνεχής"
        Me.rdbScheduleIsMinutes.UseVisualStyleBackColor = True
        '
        'rdbScheduleIsDaily
        '
        Me.rdbScheduleIsDaily.AutoSize = True
        Me.rdbScheduleIsDaily.Location = New System.Drawing.Point(23, 26)
        Me.rdbScheduleIsDaily.Name = "rdbScheduleIsDaily"
        Me.rdbScheduleIsDaily.Size = New System.Drawing.Size(80, 18)
        Me.rdbScheduleIsDaily.TabIndex = 15
        Me.rdbScheduleIsDaily.TabStop = True
        Me.rdbScheduleIsDaily.Text = "Ημερήσιος"
        Me.rdbScheduleIsDaily.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnDelete)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Controls.Add(Me.dgvDaily)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 234)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(575, 228)
        Me.Panel1.TabIndex = 25
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(293, 165)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(270, 50)
        Me.btnDelete.TabIndex = 20
        Me.btnDelete.Text = "Διαγραφή"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(12, 165)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(270, 50)
        Me.btnAdd.TabIndex = 19
        Me.btnAdd.Text = "Προσθήκη"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'dgvDaily
        '
        Me.dgvDaily.AllowUserToAddRows = False
        Me.dgvDaily.AllowUserToDeleteRows = False
        Me.dgvDaily.AllowUserToResizeColumns = False
        Me.dgvDaily.AllowUserToResizeRows = False
        Me.dgvDaily.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvDaily.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDaily.Dock = System.Windows.Forms.DockStyle.Top
        Me.dgvDaily.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvDaily.Location = New System.Drawing.Point(0, 0)
        Me.dgvDaily.Margin = New System.Windows.Forms.Padding(5)
        Me.dgvDaily.Name = "dgvDaily"
        Me.dgvDaily.Size = New System.Drawing.Size(575, 157)
        Me.dgvDaily.TabIndex = 18
        '
        'nupMinutes
        '
        Me.nupMinutes.Location = New System.Drawing.Point(266, 111)
        Me.nupMinutes.Minimum = New Decimal(New Integer() {1, 0, 0, 0})
        Me.nupMinutes.Name = "nupMinutes"
        Me.nupMinutes.Size = New System.Drawing.Size(48, 22)
        Me.nupMinutes.TabIndex = 26
        Me.nupMinutes.Value = New Decimal(New Integer() {1, 0, 0, 0})
        '
        'lblMinutes1
        '
        Me.lblMinutes1.AutoSize = True
        Me.lblMinutes1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblMinutes1.Location = New System.Drawing.Point(223, 113)
        Me.lblMinutes1.Name = "lblMinutes1"
        Me.lblMinutes1.Size = New System.Drawing.Size(37, 14)
        Me.lblMinutes1.TabIndex = 27
        Me.lblMinutes1.Text = "Κάθε"
        '
        'lblMinutes2
        '
        Me.lblMinutes2.AutoSize = True
        Me.lblMinutes2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lblMinutes2.Location = New System.Drawing.Point(320, 113)
        Me.lblMinutes2.Name = "lblMinutes2"
        Me.lblMinutes2.Size = New System.Drawing.Size(43, 14)
        Me.lblMinutes2.TabIndex = 28
        Me.lblMinutes2.Text = "λεπτά"
        '
        'chkDefault
        '
        Me.chkDefault.AutoSize = True
        Me.chkDefault.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkDefault.Location = New System.Drawing.Point(274, 57)
        Me.chkDefault.Name = "chkDefault"
        Me.chkDefault.Size = New System.Drawing.Size(289, 18)
        Me.chkDefault.TabIndex = 29
        Me.chkDefault.Text = "Προκαθορισμένος χρονοπρογραμματισμός"
        Me.chkDefault.UseVisualStyleBackColor = True
        '
        'txtScheduleName
        '
        Me.txtScheduleName.Location = New System.Drawing.Point(203, 19)
        Me.txtScheduleName.Name = "txtScheduleName"
        Me.txtScheduleName.Size = New System.Drawing.Size(360, 22)
        Me.txtScheduleName.TabIndex = 30
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label1.Location = New System.Drawing.Point(32, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(165, 14)
        Me.Label1.TabIndex = 31
        Me.Label1.Text = "Χρονοπρογραμματισμός :"
        '
        'EditScheduleF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(575, 462)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txtScheduleName)
        Me.Controls.Add(Me.chkDefault)
        Me.Controls.Add(Me.lblMinutes2)
        Me.Controls.Add(Me.lblMinutes1)
        Me.Controls.Add(Me.nupMinutes)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnSave)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Name = "EditScheduleF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Χρονοπρογραμματισμός"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvDaily, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.nupMinutes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbScheduleIsMinutes As System.Windows.Forms.RadioButton
    Friend WithEvents rdbScheduleIsDaily As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents dgvDaily As System.Windows.Forms.DataGridView
    Friend WithEvents nupMinutes As System.Windows.Forms.NumericUpDown
    Friend WithEvents lblMinutes1 As System.Windows.Forms.Label
    Friend WithEvents lblMinutes2 As System.Windows.Forms.Label
    Friend WithEvents chkDefault As System.Windows.Forms.CheckBox
    Friend WithEvents txtScheduleName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class

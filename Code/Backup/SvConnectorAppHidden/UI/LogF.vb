﻿Imports System.ServiceProcess
Imports System.IO
Imports System.Threading
Imports System.Globalization
Imports System.Diagnostics
Imports svClientActivation

Public Class LogF
    Private CurrentProcessID As Integer

    Private Sub btnMain_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnMain.Click
#If DEBUG Then
        OpenParams()
#Else
        If Not modConnector.ValidLicense(My.Application.Info.DirectoryPath & "\sv.lck") Then
            Dim frm As New ApplyActivationForm
            frm.OtherApp = True
            frm.Version = "1.0"
            frm.Path = My.Application.Info.DirectoryPath + "\sv.lck"
            frm.ShowDialog()
        Else
            OpenParams()
        End If
#End If
    End Sub

    Private Sub btnInfo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnInfo.Click
        If Not File.Exists(My.Application.Info.DirectoryPath & "\Info.log") Then
            Dim fs As FileStream = File.Create(My.Application.Info.DirectoryPath & "\Info.log")
            fs.Close()
        End If
        File.Copy(My.Application.Info.DirectoryPath & "\Info.log", _
                  String.Format("{0}\Info_{1}.log", My.Application.Info.DirectoryPath, CurrentProcessID.ToString), _
                  True)
        SvSys.RunProcess(String.Format("{0}\Info_{1}.log", My.Application.Info.DirectoryPath, CurrentProcessID.ToString))
    End Sub

    Private Sub btnError_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnError.Click
        If Not File.Exists(My.Application.Info.DirectoryPath & "\Errors.log") Then
            Dim fs As FileStream = File.Create(My.Application.Info.DirectoryPath & "\Errors.log")
            fs.Close()
        End If
        File.Copy(My.Application.Info.DirectoryPath & "\Errors.log", _
                  String.Format("{0}\Errors_{1}.log", My.Application.Info.DirectoryPath, CurrentProcessID.ToString), _
                  True)
        SvSys.RunProcess(String.Format("{0}\Errors_{1}.log", My.Application.Info.DirectoryPath, CurrentProcessID.ToString))
    End Sub

    Private Sub btnMinimize_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Me.WindowState = FormWindowState.Minimized
        Me.Visible = False
    End Sub

    Private Sub LogF_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        DeleteAllTempLogs()
    End Sub

    Private Sub LogF_Resize(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Resize
        If Me.WindowState = FormWindowState.Minimized Then
            Me.Hide()
            Me.Visible = False
        End If
    End Sub

    Private Sub tsmiOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmiOpen.Click
        MaxForm()
    End Sub

    Private Sub NotifyIcon1_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles NotifyIcon1.DoubleClick
        MaxForm()
    End Sub

    Private Sub NotifyIcon1_MouseDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles NotifyIcon1.MouseDoubleClick
        MaxForm()
    End Sub

    Private Sub MaxForm()
        Me.Show()
        Me.WindowState = FormWindowState.Normal
        Me.Focus()
    End Sub

    Private Sub tmrDelay_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrDelay.Tick
        OpenParams()
    End Sub

    Private Function ServiceRuns() As Boolean
#If DEBUG Then
        Return False
#End If
        Dim controller As New ServiceController("SvConnector", ".")
        Try
            If controller.CanPauseAndContinue Then

            End If
        Catch
            Return False
        End Try
        Return (controller.Status = ServiceControllerStatus.Running)
    End Function

    Private Function ActiveProcessExists() As Boolean
        Dim ProcessExists As Boolean = False
        Try
            Dim ActiveProcessID As String = SvRegistry.GetRegistryValue("SvConnector", "ActiveProcess")

            If IsNumeric(ActiveProcessID) Then
                If ActiveProcessID = 0 Then
                    ProcessExists = False
                Else
                    For i = 1 To 3
                        Try
                            Dim ActiveProcess As Process = Process.GetProcessById(Convert.ToInt32(ActiveProcessID))
                            ProcessExists = True
                            Exit For
                        Catch ex As Exception
                        End Try
                    Next
                End If
            End If
        Catch
            ProcessExists = False
        End Try
        Return ProcessExists
    End Function

    Private Sub OpenParams()
        If SvRegistry.GetRegistryValue("SvConnector", "IsRunning") = "1" Then
            Dim ActiveProcessID As String = SvRegistry.GetRegistryValue("SvConnector", "ActiveProcess")
            If Not ServiceRuns() Then
                If IsNumeric(ActiveProcessID) _
                AndAlso CurrentProcessID <> Convert.ToInt32(ActiveProcessID) _
                AndAlso ActiveProcessExists() Then
                    MessageBox.Show("Το πρόγραμμα είναι ήδη ανοικτό από άλλο χρήστη", "Connector", _
                                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    tmrDelay.Enabled = False
                    Exit Sub
                Else
                    SvRegistry.SetRegistryValue("SvConnector", "IsRunning", 0)
                End If
            End If
        End If

        If SvRegistry.GetRegistryValue("SvConnector", "IsRunning") = "1" Then
            Me.Enabled = False
            tmrDelay.Enabled = True
        Else
            Me.Enabled = True
            SvRegistry.SetRegistryValue("SvConnector", "ActiveProcess", CurrentProcessID)
            tmrDelay.Enabled = False
            DeleteAllTempLogs()
            Dim frm As New MainF
            frm.ShowDialog()
            SvRegistry.SetRegistryValue("SvConnector", "ActiveProcess", 0)
        End If
    End Sub

    Private Sub LogF_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR")
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("el-GR")
        CurrentProcessID = Process.GetCurrentProcess.Id
        DeleteAllTempLogs()

#If Not Debug Then
        If Not modConnector.ValidLicense(My.Application.Info.DirectoryPath & "\sv.lck") Then
            Dim frm As New ApplyActivationForm
            frm.OtherApp = True
            frm.Version = "1.0"
            frm.Path = My.Application.Info.DirectoryPath + "\sv.lck"
            frm.ShowDialog()
        End If
#End If
    End Sub

    Private Sub DeleteAllTempLogs()
        For Each item In Directory.GetFiles(My.Application.Info.DirectoryPath)
            If item.Contains("Info_") Or item.Contains("Errors_") Then
                Try
                    File.Delete(item)
                Catch

                End Try
            End If
        Next
    End Sub

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub
End Class

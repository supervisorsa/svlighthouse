﻿Public Class TimeF
    Public ReadOnly Property TimeValue() As Date
        Get
            Return FormatDateTime(DateTimePicker1.Value, DateFormat.ShortTime)
        End Get
    End Property
End Class
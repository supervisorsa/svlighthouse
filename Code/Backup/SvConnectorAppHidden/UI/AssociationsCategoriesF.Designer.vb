﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AssociationsCategoriesF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lvwAssociations = New System.Windows.Forms.ListView
        Me.SqlPersonName = New System.Windows.Forms.ColumnHeader
        Me.SqlPersonCode = New System.Windows.Forms.ColumnHeader
        Me.SqlPersonID = New System.Windows.Forms.ColumnHeader
        Me.SenPersonName = New System.Windows.Forms.ColumnHeader
        Me.SenPersonCode = New System.Windows.Forms.ColumnHeader
        Me.SenPersonID = New System.Windows.Forms.ColumnHeader
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.chkAutoAssociation = New System.Windows.Forms.CheckBox
        Me.pnlParams = New System.Windows.Forms.Panel
        Me.txtAcaCode = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label1 = New System.Windows.Forms.Label
        Me.txtCurrency = New System.Windows.Forms.TextBox
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.trvFS = New System.Windows.Forms.TreeView
        Me.trvSEN = New System.Windows.Forms.TreeView
        Me.Panel2.SuspendLayout()
        Me.pnlParams.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lvwAssociations
        '
        Me.lvwAssociations.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.SqlPersonName, Me.SqlPersonCode, Me.SqlPersonID, Me.SenPersonName, Me.SenPersonCode, Me.SenPersonID})
        Me.lvwAssociations.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lvwAssociations.FullRowSelect = True
        Me.lvwAssociations.GridLines = True
        Me.lvwAssociations.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvwAssociations.Location = New System.Drawing.Point(0, 319)
        Me.lvwAssociations.MultiSelect = False
        Me.lvwAssociations.Name = "lvwAssociations"
        Me.lvwAssociations.Size = New System.Drawing.Size(524, 200)
        Me.lvwAssociations.TabIndex = 18
        Me.lvwAssociations.UseCompatibleStateImageBehavior = False
        Me.lvwAssociations.View = System.Windows.Forms.View.Details
        '
        'SqlPersonName
        '
        Me.SqlPersonName.Width = 200
        '
        'SqlPersonCode
        '
        Me.SqlPersonCode.DisplayIndex = 4
        Me.SqlPersonCode.Width = 0
        '
        'SqlPersonID
        '
        Me.SqlPersonID.DisplayIndex = 1
        Me.SqlPersonID.Width = 0
        '
        'SenPersonName
        '
        Me.SenPersonName.DisplayIndex = 2
        Me.SenPersonName.Width = 200
        '
        'SenPersonCode
        '
        Me.SenPersonCode.DisplayIndex = 5
        Me.SenPersonCode.Width = 0
        '
        'SenPersonID
        '
        Me.SenPersonID.DisplayIndex = 3
        Me.SenPersonID.Width = 0
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnExit)
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Controls.Add(Me.chkAutoAssociation)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 519)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(524, 74)
        Me.Panel2.TabIndex = 17
        '
        'btnExit
        '
        Me.btnExit.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.btnExit.Location = New System.Drawing.Point(352, 19)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(160, 40)
        Me.btnExit.TabIndex = 9
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.btnSave.Location = New System.Drawing.Point(182, 19)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(160, 40)
        Me.btnSave.TabIndex = 8
        Me.btnSave.Text = "Αποθήκευση"
        '
        'chkAutoAssociation
        '
        Me.chkAutoAssociation.AutoSize = True
        Me.chkAutoAssociation.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkAutoAssociation.Location = New System.Drawing.Point(14, 31)
        Me.chkAutoAssociation.Name = "chkAutoAssociation"
        Me.chkAutoAssociation.Size = New System.Drawing.Size(151, 18)
        Me.chkAutoAssociation.TabIndex = 7
        Me.chkAutoAssociation.Text = "Αυτόματη εισαγωγή"
        Me.chkAutoAssociation.UseVisualStyleBackColor = True
        '
        'pnlParams
        '
        Me.pnlParams.Controls.Add(Me.txtAcaCode)
        Me.pnlParams.Controls.Add(Me.Label2)
        Me.pnlParams.Controls.Add(Me.Label1)
        Me.pnlParams.Controls.Add(Me.txtCurrency)
        Me.pnlParams.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlParams.Location = New System.Drawing.Point(0, 0)
        Me.pnlParams.Name = "pnlParams"
        Me.pnlParams.Size = New System.Drawing.Size(524, 42)
        Me.pnlParams.TabIndex = 21
        '
        'txtAcaCode
        '
        Me.txtAcaCode.Location = New System.Drawing.Point(348, 10)
        Me.txtAcaCode.Name = "txtAcaCode"
        Me.txtAcaCode.Size = New System.Drawing.Size(164, 22)
        Me.txtAcaCode.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label2.Location = New System.Drawing.Point(217, 13)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(125, 14)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Κωδικός λογιστικής"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 13)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(58, 14)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Νόμισμα"
        '
        'txtCurrency
        '
        Me.txtCurrency.Location = New System.Drawing.Point(76, 10)
        Me.txtCurrency.Name = "txtCurrency"
        Me.txtCurrency.Size = New System.Drawing.Size(128, 22)
        Me.txtCurrency.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 42)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.trvFS)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.trvSEN)
        Me.SplitContainer1.Size = New System.Drawing.Size(524, 277)
        Me.SplitContainer1.SplitterDistance = 261
        Me.SplitContainer1.SplitterWidth = 5
        Me.SplitContainer1.TabIndex = 22
        '
        'trvFS
        '
        Me.trvFS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.trvFS.Location = New System.Drawing.Point(0, 0)
        Me.trvFS.Name = "trvFS"
        Me.trvFS.Size = New System.Drawing.Size(261, 277)
        Me.trvFS.TabIndex = 0
        '
        'trvSEN
        '
        Me.trvSEN.Dock = System.Windows.Forms.DockStyle.Fill
        Me.trvSEN.Location = New System.Drawing.Point(0, 0)
        Me.trvSEN.Name = "trvSEN"
        Me.trvSEN.Size = New System.Drawing.Size(258, 277)
        Me.trvSEN.TabIndex = 1
        '
        'AssociationsCategoriesF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 593)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.pnlParams)
        Me.Controls.Add(Me.lvwAssociations)
        Me.Controls.Add(Me.Panel2)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Name = "AssociationsCategoriesF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Κατηγορίες ειδών"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.pnlParams.ResumeLayout(False)
        Me.pnlParams.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents lvwAssociations As System.Windows.Forms.ListView
    Friend WithEvents SqlPersonName As System.Windows.Forms.ColumnHeader
    Friend WithEvents SqlPersonCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents SqlPersonID As System.Windows.Forms.ColumnHeader
    Friend WithEvents SenPersonName As System.Windows.Forms.ColumnHeader
    Friend WithEvents SenPersonCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents SenPersonID As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pnlParams As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents trvFS As System.Windows.Forms.TreeView
    Friend WithEvents trvSEN As System.Windows.Forms.TreeView
    Friend WithEvents chkAutoAssociation As System.Windows.Forms.CheckBox
    Friend WithEvents txtAcaCode As System.Windows.Forms.TextBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtCurrency As System.Windows.Forms.TextBox
End Class

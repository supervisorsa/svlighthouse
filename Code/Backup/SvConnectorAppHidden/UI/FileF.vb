﻿Public Class FileF

    Private _Path As String
    Public Property Path() As String
        Get
            Return _Path
        End Get
        Set(ByVal value As String)
            _Path = value
        End Set
    End Property

    Private _Filename As String
    Public Property Filename() As String
        Get
            Return _Filename
        End Get
        Set(ByVal value As String)
            _Filename = value
        End Set
    End Property

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Close()
    End Sub

    Private Sub btnCreate_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCreate.Click
        If rdbPath.Checked Then
            _Path = txtPath.Text
            _Filename = ""
        Else
            _Path = ""
            _Filename = txtPath.Text
        End If

        Close()
    End Sub

    Private Sub FileForm_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        OpenFileDialog1.FileName = ""
        If _Filename = "" Then
            txtPath.Text = _Path
            rdbPath.Checked = True
            OpenFileDialog1.InitialDirectory = _Path
            FolderBrowserDialog1.SelectedPath = _Path
        Else
            txtPath.Text = _Filename
            rdbFile.Checked = True
            OpenFileDialog1.InitialDirectory = System.IO.Path.GetDirectoryName(_Filename)
            FolderBrowserDialog1.SelectedPath = System.IO.Path.GetDirectoryName(_Filename)
        End If
    End Sub

    Private Sub btnPath_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPath.Click
        If rdbPath.Checked Then
            If FolderBrowserDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                txtPath.Text = FolderBrowserDialog1.SelectedPath
            End If
        Else
            OpenFileDialog1.ShowDialog()
            If OpenFileDialog1.FileName <> "" Then
                txtPath.Text = OpenFileDialog1.FileName
            End If
        End If
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AssociationsItemsF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.

    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(AssociationsItemsF))
        Me.pnlDepartments = New System.Windows.Forms.Panel
        Me.Label1 = New System.Windows.Forms.Label
        Me.cboDepartments = New System.Windows.Forms.ComboBox
        Me.Panel2 = New System.Windows.Forms.Panel
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnAutoAssociate = New System.Windows.Forms.Button
        Me.btnSave = New System.Windows.Forms.Button
        Me.lvwAssociations = New System.Windows.Forms.ListView
        Me.SqlPersonName = New System.Windows.Forms.ColumnHeader
        Me.SqlPersonCode = New System.Windows.Forms.ColumnHeader
        Me.SqlPersonID = New System.Windows.Forms.ColumnHeader
        Me.SenPersonName = New System.Windows.Forms.ColumnHeader
        Me.SenPersonCode = New System.Windows.Forms.ColumnHeader
        Me.SenPersonID = New System.Windows.Forms.ColumnHeader
        Me.pnlParams = New System.Windows.Forms.Panel
        Me.btnAssociationParams = New System.Windows.Forms.Button
        Me.chkUpdateSellersCodes = New System.Windows.Forms.CheckBox
        Me.chkToErp = New System.Windows.Forms.CheckBox
        Me.chkMultiple = New System.Windows.Forms.CheckBox
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer
        Me.lvwFS = New System.Windows.Forms.ListView
        Me.SqlName = New System.Windows.Forms.ColumnHeader
        Me.SqlID = New System.Windows.Forms.ColumnHeader
        Me.SqlCode = New System.Windows.Forms.ColumnHeader
        Me.lvwSen = New System.Windows.Forms.ListView
        Me.SenName = New System.Windows.Forms.ColumnHeader
        Me.SenID = New System.Windows.Forms.ColumnHeader
        Me.SenCode = New System.Windows.Forms.ColumnHeader
        Me.pnlDepartments.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlParams.SuspendLayout()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'pnlDepartments
        '
        Me.pnlDepartments.Controls.Add(Me.Label1)
        Me.pnlDepartments.Controls.Add(Me.cboDepartments)
        Me.pnlDepartments.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlDepartments.Location = New System.Drawing.Point(0, 0)
        Me.pnlDepartments.Name = "pnlDepartments"
        Me.pnlDepartments.Size = New System.Drawing.Size(524, 46)
        Me.pnlDepartments.TabIndex = 11
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 15)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(117, 14)
        Me.Label1.TabIndex = 11
        Me.Label1.Text = "Τμήμα πωλητών :"
        '
        'cboDepartments
        '
        Me.cboDepartments.BackColor = System.Drawing.Color.Salmon
        Me.cboDepartments.FormattingEnabled = True
        Me.cboDepartments.Location = New System.Drawing.Point(135, 12)
        Me.cboDepartments.Name = "cboDepartments"
        Me.cboDepartments.Size = New System.Drawing.Size(377, 22)
        Me.cboDepartments.TabIndex = 10
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.btnExit)
        Me.Panel2.Controls.Add(Me.btnAutoAssociate)
        Me.Panel2.Controls.Add(Me.btnSave)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel2.Location = New System.Drawing.Point(0, 613)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(524, 69)
        Me.Panel2.TabIndex = 12
        '
        'btnExit
        '
        Me.btnExit.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.btnExit.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.btnExit.Location = New System.Drawing.Point(352, 17)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(160, 40)
        Me.btnExit.TabIndex = 6
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnAutoAssociate
        '
        Me.btnAutoAssociate.Cursor = System.Windows.Forms.Cursors.Arrow
        Me.btnAutoAssociate.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.btnAutoAssociate.Location = New System.Drawing.Point(12, 17)
        Me.btnAutoAssociate.Name = "btnAutoAssociate"
        Me.btnAutoAssociate.Size = New System.Drawing.Size(160, 40)
        Me.btnAutoAssociate.TabIndex = 4
        Me.btnAutoAssociate.Text = "Αυτόματη αντιστοίχιση"
        Me.btnAutoAssociate.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.btnSave.Location = New System.Drawing.Point(182, 17)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(160, 40)
        Me.btnSave.TabIndex = 5
        Me.btnSave.Text = "Αποθήκευση"
        '
        'lvwAssociations
        '
        Me.lvwAssociations.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.SqlPersonName, Me.SqlPersonCode, Me.SqlPersonID, Me.SenPersonName, Me.SenPersonCode, Me.SenPersonID})
        Me.lvwAssociations.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.lvwAssociations.FullRowSelect = True
        Me.lvwAssociations.GridLines = True
        Me.lvwAssociations.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvwAssociations.Location = New System.Drawing.Point(0, 398)
        Me.lvwAssociations.MultiSelect = False
        Me.lvwAssociations.Name = "lvwAssociations"
        Me.lvwAssociations.Size = New System.Drawing.Size(524, 215)
        Me.lvwAssociations.TabIndex = 13
        Me.lvwAssociations.UseCompatibleStateImageBehavior = False
        Me.lvwAssociations.View = System.Windows.Forms.View.Details
        '
        'SqlPersonName
        '
        Me.SqlPersonName.Width = 200
        '
        'SqlPersonCode
        '
        Me.SqlPersonCode.DisplayIndex = 4
        Me.SqlPersonCode.Width = 0
        '
        'SqlPersonID
        '
        Me.SqlPersonID.DisplayIndex = 1
        Me.SqlPersonID.Width = 0
        '
        'SenPersonName
        '
        Me.SenPersonName.DisplayIndex = 2
        Me.SenPersonName.Width = 200
        '
        'SenPersonCode
        '
        Me.SenPersonCode.DisplayIndex = 5
        Me.SenPersonCode.Width = 0
        '
        'SenPersonID
        '
        Me.SenPersonID.DisplayIndex = 3
        Me.SenPersonID.Width = 0
        '
        'pnlParams
        '
        Me.pnlParams.Controls.Add(Me.btnAssociationParams)
        Me.pnlParams.Controls.Add(Me.chkUpdateSellersCodes)
        Me.pnlParams.Controls.Add(Me.chkToErp)
        Me.pnlParams.Controls.Add(Me.chkMultiple)
        Me.pnlParams.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlParams.Location = New System.Drawing.Point(0, 46)
        Me.pnlParams.Name = "pnlParams"
        Me.pnlParams.Size = New System.Drawing.Size(524, 74)
        Me.pnlParams.TabIndex = 15
        '
        'btnAssociationParams
        '
        Me.btnAssociationParams.Location = New System.Drawing.Point(325, 11)
        Me.btnAssociationParams.Name = "btnAssociationParams"
        Me.btnAssociationParams.Size = New System.Drawing.Size(187, 23)
        Me.btnAssociationParams.TabIndex = 3
        Me.btnAssociationParams.Text = "Παράμετροι αντιστοίχισης"
        Me.btnAssociationParams.UseVisualStyleBackColor = True
        '
        'chkUpdateSellersCodes
        '
        Me.chkUpdateSellersCodes.AutoSize = True
        Me.chkUpdateSellersCodes.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkUpdateSellersCodes.Location = New System.Drawing.Point(15, 40)
        Me.chkUpdateSellersCodes.Name = "chkUpdateSellersCodes"
        Me.chkUpdateSellersCodes.Size = New System.Drawing.Size(208, 18)
        Me.chkUpdateSellersCodes.TabIndex = 2
        Me.chkUpdateSellersCodes.Text = "Ενημέρωση κωδικού από ERP"
        Me.chkUpdateSellersCodes.UseVisualStyleBackColor = True
        '
        'chkToErp
        '
        Me.chkToErp.AutoSize = True
        Me.chkToErp.Enabled = False
        Me.chkToErp.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkToErp.Location = New System.Drawing.Point(194, 11)
        Me.chkToErp.Name = "chkToErp"
        Me.chkToErp.Size = New System.Drawing.Size(104, 18)
        Me.chkToErp.TabIndex = 1
        Me.chkToErp.Text = "Προς το ERP"
        Me.chkToErp.UseVisualStyleBackColor = True
        '
        'chkMultiple
        '
        Me.chkMultiple.AutoSize = True
        Me.chkMultiple.Enabled = False
        Me.chkMultiple.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.chkMultiple.Location = New System.Drawing.Point(15, 11)
        Me.chkMultiple.Name = "chkMultiple"
        Me.chkMultiple.Size = New System.Drawing.Size(169, 18)
        Me.chkMultiple.TabIndex = 0
        Me.chkMultiple.Text = "Πολλαπλή αντιστοίχιση"
        Me.chkMultiple.UseVisualStyleBackColor = True
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 120)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.lvwFS)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.lvwSen)
        Me.SplitContainer1.Size = New System.Drawing.Size(524, 278)
        Me.SplitContainer1.SplitterDistance = 261
        Me.SplitContainer1.TabIndex = 16
        '
        'lvwFS
        '
        Me.lvwFS.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.SqlName, Me.SqlID, Me.SqlCode})
        Me.lvwFS.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvwFS.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lvwFS.FullRowSelect = True
        Me.lvwFS.GridLines = True
        Me.lvwFS.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvwFS.Location = New System.Drawing.Point(0, 0)
        Me.lvwFS.Name = "lvwFS"
        Me.lvwFS.Size = New System.Drawing.Size(261, 278)
        Me.lvwFS.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvwFS.TabIndex = 8
        Me.lvwFS.UseCompatibleStateImageBehavior = False
        Me.lvwFS.View = System.Windows.Forms.View.Details
        '
        'SqlName
        '
        Me.SqlName.Width = 200
        '
        'SqlID
        '
        Me.SqlID.Width = 0
        '
        'SqlCode
        '
        Me.SqlCode.Width = 0
        '
        'lvwSen
        '
        Me.lvwSen.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.SenName, Me.SenID, Me.SenCode})
        Me.lvwSen.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lvwSen.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.lvwSen.FullRowSelect = True
        Me.lvwSen.GridLines = True
        Me.lvwSen.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None
        Me.lvwSen.Location = New System.Drawing.Point(0, 0)
        Me.lvwSen.Name = "lvwSen"
        Me.lvwSen.Size = New System.Drawing.Size(259, 278)
        Me.lvwSen.Sorting = System.Windows.Forms.SortOrder.Ascending
        Me.lvwSen.TabIndex = 9
        Me.lvwSen.UseCompatibleStateImageBehavior = False
        Me.lvwSen.View = System.Windows.Forms.View.Details
        '
        'SenName
        '
        Me.SenName.Width = 200
        '
        'SenID
        '
        Me.SenID.Width = 0
        '
        'SenCode
        '
        Me.SenCode.Width = 0
        '
        'AssociationsItemsF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(524, 682)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.pnlParams)
        Me.Controls.Add(Me.lvwAssociations)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.pnlDepartments)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "AssociationsItemsF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Πωλητές"
        Me.pnlDepartments.ResumeLayout(False)
        Me.pnlDepartments.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.pnlParams.ResumeLayout(False)
        Me.pnlParams.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents cboDepartments As System.Windows.Forms.ComboBox
    Friend WithEvents pnlDepartments As System.Windows.Forms.Panel
    Friend WithEvents btnAutoAssociate As System.Windows.Forms.Button
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents lvwAssociations As System.Windows.Forms.ListView
    Friend WithEvents SqlPersonName As System.Windows.Forms.ColumnHeader
    Friend WithEvents SqlPersonCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents SqlPersonID As System.Windows.Forms.ColumnHeader
    Friend WithEvents SenPersonName As System.Windows.Forms.ColumnHeader
    Friend WithEvents SenPersonCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents SenPersonID As System.Windows.Forms.ColumnHeader
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents pnlParams As System.Windows.Forms.Panel
    Friend WithEvents SplitContainer1 As System.Windows.Forms.SplitContainer
    Friend WithEvents lvwFS As System.Windows.Forms.ListView
    Friend WithEvents SqlName As System.Windows.Forms.ColumnHeader
    Friend WithEvents SqlID As System.Windows.Forms.ColumnHeader
    Friend WithEvents SqlCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents lvwSen As System.Windows.Forms.ListView
    Friend WithEvents SenName As System.Windows.Forms.ColumnHeader
    Friend WithEvents SenID As System.Windows.Forms.ColumnHeader
    Friend WithEvents SenCode As System.Windows.Forms.ColumnHeader
    Friend WithEvents chkUpdateSellersCodes As System.Windows.Forms.CheckBox
    Friend WithEvents chkToErp As System.Windows.Forms.CheckBox
    Friend WithEvents chkMultiple As System.Windows.Forms.CheckBox
    Friend WithEvents btnAssociationParams As System.Windows.Forms.Button

End Class

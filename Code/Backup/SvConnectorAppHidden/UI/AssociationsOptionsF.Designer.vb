﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AssociationsOptionsF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnPayways = New System.Windows.Forms.Button
        Me.btnSalesPersons = New System.Windows.Forms.Button
        Me.btnClose = New System.Windows.Forms.Button
        Me.btnInvoices = New System.Windows.Forms.Button
        Me.btnCategories = New System.Windows.Forms.Button
        Me.btnContactCategories = New System.Windows.Forms.Button
        Me.btnCopyTo = New System.Windows.Forms.Button
        Me.btnCopyFrom = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'btnPayways
        '
        Me.btnPayways.Location = New System.Drawing.Point(12, 12)
        Me.btnPayways.Name = "btnPayways"
        Me.btnPayways.Size = New System.Drawing.Size(266, 44)
        Me.btnPayways.TabIndex = 0
        Me.btnPayways.Text = "Τρόποι πληρωμής"
        Me.btnPayways.UseVisualStyleBackColor = True
        '
        'btnSalesPersons
        '
        Me.btnSalesPersons.Location = New System.Drawing.Point(12, 77)
        Me.btnSalesPersons.Name = "btnSalesPersons"
        Me.btnSalesPersons.Size = New System.Drawing.Size(266, 44)
        Me.btnSalesPersons.TabIndex = 1
        Me.btnSalesPersons.Text = "Πωλητές"
        Me.btnSalesPersons.UseVisualStyleBackColor = True
        '
        'btnClose
        '
        Me.btnClose.Location = New System.Drawing.Point(12, 283)
        Me.btnClose.Name = "btnClose"
        Me.btnClose.Size = New System.Drawing.Size(568, 44)
        Me.btnClose.TabIndex = 2
        Me.btnClose.Text = "Έξοδος"
        Me.btnClose.UseVisualStyleBackColor = True
        '
        'btnInvoices
        '
        Me.btnInvoices.Location = New System.Drawing.Point(314, 12)
        Me.btnInvoices.Name = "btnInvoices"
        Me.btnInvoices.Size = New System.Drawing.Size(266, 44)
        Me.btnInvoices.TabIndex = 3
        Me.btnInvoices.Text = "Παραστατικά"
        Me.btnInvoices.UseVisualStyleBackColor = True
        '
        'btnCategories
        '
        Me.btnCategories.Location = New System.Drawing.Point(12, 146)
        Me.btnCategories.Name = "btnCategories"
        Me.btnCategories.Size = New System.Drawing.Size(266, 44)
        Me.btnCategories.TabIndex = 4
        Me.btnCategories.Text = "Κατηγορίες ειδών"
        Me.btnCategories.UseVisualStyleBackColor = True
        '
        'btnContactCategories
        '
        Me.btnContactCategories.Location = New System.Drawing.Point(12, 217)
        Me.btnContactCategories.Name = "btnContactCategories"
        Me.btnContactCategories.Size = New System.Drawing.Size(266, 44)
        Me.btnContactCategories.TabIndex = 5
        Me.btnContactCategories.Text = "Κατηγορίες πελατών"
        Me.btnContactCategories.UseVisualStyleBackColor = True
        '
        'btnCopyTo
        '
        Me.btnCopyTo.Location = New System.Drawing.Point(314, 146)
        Me.btnCopyTo.Name = "btnCopyTo"
        Me.btnCopyTo.Size = New System.Drawing.Size(266, 44)
        Me.btnCopyTo.TabIndex = 7
        Me.btnCopyTo.Text = "Αντιγραφή σε"
        Me.btnCopyTo.UseVisualStyleBackColor = True
        '
        'btnCopyFrom
        '
        Me.btnCopyFrom.Location = New System.Drawing.Point(314, 77)
        Me.btnCopyFrom.Name = "btnCopyFrom"
        Me.btnCopyFrom.Size = New System.Drawing.Size(266, 44)
        Me.btnCopyFrom.TabIndex = 6
        Me.btnCopyFrom.Text = "Αντιγραφή από"
        Me.btnCopyFrom.UseVisualStyleBackColor = True
        '
        'AssociationsOptionsF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(592, 346)
        Me.Controls.Add(Me.btnCopyTo)
        Me.Controls.Add(Me.btnCopyFrom)
        Me.Controls.Add(Me.btnContactCategories)
        Me.Controls.Add(Me.btnCategories)
        Me.Controls.Add(Me.btnInvoices)
        Me.Controls.Add(Me.btnClose)
        Me.Controls.Add(Me.btnSalesPersons)
        Me.Controls.Add(Me.btnPayways)
        Me.Font = New System.Drawing.Font("Tahoma", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "AssociationsOptionsF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Αντιστοιχίσεις"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnPayways As System.Windows.Forms.Button
    Friend WithEvents btnSalesPersons As System.Windows.Forms.Button
    Friend WithEvents btnClose As System.Windows.Forms.Button
    Friend WithEvents btnInvoices As System.Windows.Forms.Button
    Friend WithEvents btnCategories As System.Windows.Forms.Button
    Friend WithEvents btnContactCategories As System.Windows.Forms.Button
    Friend WithEvents btnCopyTo As System.Windows.Forms.Button
    Friend WithEvents btnCopyFrom As System.Windows.Forms.Button
End Class

﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ConnectionsF
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnAdd = New System.Windows.Forms.Button
        Me.btnEdit = New System.Windows.Forms.Button
        Me.btnExit = New System.Windows.Forms.Button
        Me.btnDelete = New System.Windows.Forms.Button
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.rdbVM = New System.Windows.Forms.RadioButton
        Me.rdbOtherSql = New System.Windows.Forms.RadioButton
        Me.rdbInventory = New System.Windows.Forms.RadioButton
        Me.rdbFile = New System.Windows.Forms.RadioButton
        Me.rdbPylon = New System.Windows.Forms.RadioButton
        Me.rdbSvConn = New System.Windows.Forms.RadioButton
        Me.rdbGalaxy = New System.Windows.Forms.RadioButton
        Me.rdbPrime = New System.Windows.Forms.RadioButton
        Me.rdbOtr = New System.Windows.Forms.RadioButton
        Me.rdbControl = New System.Windows.Forms.RadioButton
        Me.rdbSRS = New System.Windows.Forms.RadioButton
        Me.rdbSen = New System.Windows.Forms.RadioButton
        Me.rdbFS = New System.Windows.Forms.RadioButton
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.dgvConnections = New System.Windows.Forms.DataGridView
        Me.GroupBox1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.dgvConnections, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(474, 12)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(300, 50)
        Me.btnAdd.TabIndex = 7
        Me.btnAdd.Text = "Προσθήκη"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(474, 83)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(300, 50)
        Me.btnEdit.TabIndex = 8
        Me.btnEdit.Text = "Επεξεργασία"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(474, 222)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(300, 50)
        Me.btnExit.TabIndex = 9
        Me.btnExit.Text = "Έξοδος"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(474, 158)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(300, 50)
        Me.btnDelete.TabIndex = 13
        Me.btnDelete.Text = "Διαγραφή"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.rdbVM)
        Me.GroupBox1.Controls.Add(Me.rdbOtherSql)
        Me.GroupBox1.Controls.Add(Me.rdbInventory)
        Me.GroupBox1.Controls.Add(Me.rdbFile)
        Me.GroupBox1.Controls.Add(Me.rdbPylon)
        Me.GroupBox1.Controls.Add(Me.rdbSvConn)
        Me.GroupBox1.Controls.Add(Me.rdbGalaxy)
        Me.GroupBox1.Controls.Add(Me.rdbPrime)
        Me.GroupBox1.Controls.Add(Me.rdbOtr)
        Me.GroupBox1.Controls.Add(Me.rdbControl)
        Me.GroupBox1.Controls.Add(Me.rdbSRS)
        Me.GroupBox1.Controls.Add(Me.rdbSen)
        Me.GroupBox1.Controls.Add(Me.rdbFS)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 12)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(442, 260)
        Me.GroupBox1.TabIndex = 14
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Τύπος σύνδεσης"
        '
        'rdbVM
        '
        Me.rdbVM.AutoSize = True
        Me.rdbVM.Location = New System.Drawing.Point(318, 153)
        Me.rdbVM.Name = "rdbVM"
        Me.rdbVM.Size = New System.Drawing.Size(86, 20)
        Me.rdbVM.TabIndex = 27
        Me.rdbVM.TabStop = True
        Me.rdbVM.Text = "VirtueMart"
        Me.rdbVM.UseVisualStyleBackColor = True
        '
        'rdbOtherSql
        '
        Me.rdbOtherSql.AutoSize = True
        Me.rdbOtherSql.Location = New System.Drawing.Point(23, 193)
        Me.rdbOtherSql.Name = "rdbOtherSql"
        Me.rdbOtherSql.Size = New System.Drawing.Size(122, 20)
        Me.rdbOtherSql.TabIndex = 26
        Me.rdbOtherSql.TabStop = True
        Me.rdbOtherSql.Text = "SQL Server Άλλο"
        Me.rdbOtherSql.UseVisualStyleBackColor = True
        '
        'rdbInventory
        '
        Me.rdbInventory.AutoSize = True
        Me.rdbInventory.Location = New System.Drawing.Point(23, 153)
        Me.rdbInventory.Name = "rdbInventory"
        Me.rdbInventory.Size = New System.Drawing.Size(79, 20)
        Me.rdbInventory.TabIndex = 25
        Me.rdbInventory.TabStop = True
        Me.rdbInventory.Text = "Inventory"
        Me.rdbInventory.UseVisualStyleBackColor = True
        '
        'rdbFile
        '
        Me.rdbFile.AutoSize = True
        Me.rdbFile.Location = New System.Drawing.Point(173, 153)
        Me.rdbFile.Name = "rdbFile"
        Me.rdbFile.Size = New System.Drawing.Size(64, 20)
        Me.rdbFile.TabIndex = 24
        Me.rdbFile.TabStop = True
        Me.rdbFile.Text = "Αρχείο"
        Me.rdbFile.UseVisualStyleBackColor = True
        '
        'rdbPylon
        '
        Me.rdbPylon.AutoSize = True
        Me.rdbPylon.Location = New System.Drawing.Point(23, 111)
        Me.rdbPylon.Name = "rdbPylon"
        Me.rdbPylon.Size = New System.Drawing.Size(56, 20)
        Me.rdbPylon.TabIndex = 23
        Me.rdbPylon.TabStop = True
        Me.rdbPylon.Text = "Pylon"
        Me.rdbPylon.UseVisualStyleBackColor = True
        '
        'rdbSvConn
        '
        Me.rdbSvConn.AutoSize = True
        Me.rdbSvConn.Location = New System.Drawing.Point(23, 26)
        Me.rdbSvConn.Name = "rdbSvConn"
        Me.rdbSvConn.Size = New System.Drawing.Size(69, 20)
        Me.rdbSvConn.TabIndex = 22
        Me.rdbSvConn.TabStop = True
        Me.rdbSvConn.Text = "SvConn"
        Me.rdbSvConn.UseVisualStyleBackColor = True
        '
        'rdbGalaxy
        '
        Me.rdbGalaxy.AutoSize = True
        Me.rdbGalaxy.Location = New System.Drawing.Point(23, 67)
        Me.rdbGalaxy.Name = "rdbGalaxy"
        Me.rdbGalaxy.Size = New System.Drawing.Size(63, 20)
        Me.rdbGalaxy.TabIndex = 21
        Me.rdbGalaxy.TabStop = True
        Me.rdbGalaxy.Text = "Galaxy"
        Me.rdbGalaxy.UseVisualStyleBackColor = True
        '
        'rdbPrime
        '
        Me.rdbPrime.AutoSize = True
        Me.rdbPrime.Location = New System.Drawing.Point(173, 111)
        Me.rdbPrime.Name = "rdbPrime"
        Me.rdbPrime.Size = New System.Drawing.Size(59, 20)
        Me.rdbPrime.TabIndex = 20
        Me.rdbPrime.TabStop = True
        Me.rdbPrime.Text = "Prime"
        Me.rdbPrime.UseVisualStyleBackColor = True
        '
        'rdbOtr
        '
        Me.rdbOtr.AutoSize = True
        Me.rdbOtr.Location = New System.Drawing.Point(173, 67)
        Me.rdbOtr.Name = "rdbOtr"
        Me.rdbOtr.Size = New System.Drawing.Size(51, 20)
        Me.rdbOtr.TabIndex = 19
        Me.rdbOtr.TabStop = True
        Me.rdbOtr.Text = "OTR"
        Me.rdbOtr.UseVisualStyleBackColor = True
        '
        'rdbControl
        '
        Me.rdbControl.AutoSize = True
        Me.rdbControl.Location = New System.Drawing.Point(318, 67)
        Me.rdbControl.Name = "rdbControl"
        Me.rdbControl.Size = New System.Drawing.Size(67, 20)
        Me.rdbControl.TabIndex = 18
        Me.rdbControl.TabStop = True
        Me.rdbControl.Text = "Control"
        Me.rdbControl.UseVisualStyleBackColor = True
        '
        'rdbSRS
        '
        Me.rdbSRS.AutoSize = True
        Me.rdbSRS.Location = New System.Drawing.Point(318, 111)
        Me.rdbSRS.Name = "rdbSRS"
        Me.rdbSRS.Size = New System.Drawing.Size(50, 20)
        Me.rdbSRS.TabIndex = 17
        Me.rdbSRS.TabStop = True
        Me.rdbSRS.Text = "SRS"
        Me.rdbSRS.UseVisualStyleBackColor = True
        '
        'rdbSen
        '
        Me.rdbSen.AutoSize = True
        Me.rdbSen.Location = New System.Drawing.Point(318, 26)
        Me.rdbSen.Name = "rdbSen"
        Me.rdbSen.Size = New System.Drawing.Size(49, 20)
        Me.rdbSen.TabIndex = 16
        Me.rdbSen.TabStop = True
        Me.rdbSen.Text = "SEN"
        Me.rdbSen.UseVisualStyleBackColor = True
        '
        'rdbFS
        '
        Me.rdbFS.AutoSize = True
        Me.rdbFS.Location = New System.Drawing.Point(173, 26)
        Me.rdbFS.Name = "rdbFS"
        Me.rdbFS.Size = New System.Drawing.Size(81, 20)
        Me.rdbFS.TabIndex = 15
        Me.rdbFS.TabStop = True
        Me.rdbFS.Text = "Footsteps"
        Me.rdbFS.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.GroupBox1)
        Me.Panel1.Controls.Add(Me.btnAdd)
        Me.Panel1.Controls.Add(Me.btnDelete)
        Me.Panel1.Controls.Add(Me.btnEdit)
        Me.Panel1.Controls.Add(Me.btnExit)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 262)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(784, 288)
        Me.Panel1.TabIndex = 15
        '
        'dgvConnections
        '
        Me.dgvConnections.AllowUserToAddRows = False
        Me.dgvConnections.AllowUserToDeleteRows = False
        Me.dgvConnections.AllowUserToResizeColumns = False
        Me.dgvConnections.AllowUserToResizeRows = False
        Me.dgvConnections.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvConnections.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvConnections.Dock = System.Windows.Forms.DockStyle.Fill
        Me.dgvConnections.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvConnections.Location = New System.Drawing.Point(0, 0)
        Me.dgvConnections.Margin = New System.Windows.Forms.Padding(5)
        Me.dgvConnections.Name = "dgvConnections"
        Me.dgvConnections.Size = New System.Drawing.Size(784, 262)
        Me.dgvConnections.TabIndex = 16
        '
        'ConnectionsF
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(784, 550)
        Me.Controls.Add(Me.dgvConnections)
        Me.Controls.Add(Me.Panel1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(161, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "ConnectionsF"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent
        Me.Text = "Συνδέσεις"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        CType(Me.dgvConnections, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Friend WithEvents btnExit As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents rdbSRS As System.Windows.Forms.RadioButton
    Friend WithEvents rdbSen As System.Windows.Forms.RadioButton
    Friend WithEvents rdbFS As System.Windows.Forms.RadioButton
    Friend WithEvents rdbControl As System.Windows.Forms.RadioButton
    Friend WithEvents rdbPrime As System.Windows.Forms.RadioButton
    Friend WithEvents rdbOtr As System.Windows.Forms.RadioButton
    Friend WithEvents rdbGalaxy As System.Windows.Forms.RadioButton
    Friend WithEvents rdbSvConn As System.Windows.Forms.RadioButton
    Friend WithEvents rdbPylon As System.Windows.Forms.RadioButton
    Friend WithEvents rdbFile As System.Windows.Forms.RadioButton
    Friend WithEvents rdbInventory As System.Windows.Forms.RadioButton
    Friend WithEvents rdbOtherSql As System.Windows.Forms.RadioButton
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents dgvConnections As System.Windows.Forms.DataGridView
    Friend WithEvents rdbVM As System.Windows.Forms.RadioButton
End Class

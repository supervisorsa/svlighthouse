﻿Public Class AssociationsCopyF

    Private _TaskID As Integer
    Public WriteOnly Property TaskID() As Integer
        Set(ByVal value As Integer)
            _TaskID = value
            Dim OtherTasks  = Tasks.GetFullData.Select("ID<>" & _TaskID.ToString)
            For Each task In OtherTasks
                SvClass.InsertToCombo(task.Item("ID"), task.Item("TaskName"), _
                      cboTasks, _
                      0, _
                      True)
            Next
        End Set
    End Property

    Private _CopyFrom As Boolean
    Public WriteOnly Property CopyFrom() As Boolean
        Set(ByVal value As Boolean)
            _CopyFrom = value
            If _CopyFrom Then lblCopy.Text = "Αντιγραφή από"
        End Set
    End Property

    Private Sub btnExit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnExit.Click
        Me.Close()
    End Sub

    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click
        If MessageBox.Show("Θέλετε σίγουρα να κάνετε αντιγραφή;" & vbCrLf & _
                           "Η αντιγραφή θα διαγράψει όλες τις υπάρχουσες αντιστοιχίσεις.", _
                           "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

            Dim OtherTaskID As Integer = cboTasks.SelectedItem.ID
            If _CopyFrom Then
                CopyAssociations(OtherTaskID, _TaskID)
            Else
                CopyAssociations(_TaskID, OtherTaskID)
            End If

            Close()
        End If
    End Sub

    Private Sub CopyAssociations(ByVal OriginalTaskID As Integer, ByVal UpdatedTaskID As Integer)
        If chkSalespersons.Checked Then
            ParamsSen_SalesPersons.DeleteQuery(UpdatedTaskID)
            For Each person In ParamsSen_SalesPersons.GetDataByTaskID(OriginalTaskID)
                ParamsSen_SalesPersons.Insert(UpdatedTaskID, _
                                              person.SqlPersonID, _
                                              person.SqlPersonName, _
                                              person.SenPersonID, _
                                              person.SenPersonName)
            Next
        End If

        If chkPayways.Checked Then
            ParamsSen_Payways.DeleteQuery(UpdatedTaskID)
            For Each person In ParamsSen_Payways.GetDataByTaskID(OriginalTaskID)
                ParamsSen_Payways.Insert(UpdatedTaskID, _
                                         person.SqlPaywayID, _
                                         person.SqlPaywayName, _
                                         person.SenPaywayID, _
                                         person.SenPaywayName)
            Next
        End If

        If chkCategories.Checked Then
            ParamsSen_Items.DeleteQuery(UpdatedTaskID)
            For Each person In ParamsSen_Items.GetDataByTaskID(OriginalTaskID)
                ParamsSen_Items.Insert(UpdatedTaskID, _
                                       person.SqlCategoryID, _
                                       person.SqlCategoryName, _
                                       person.SenCategoryID, _
                                       person.SenCategoryName, _
                                       person.Duration)
            Next
        End If

        If chkContactCategories.Checked Then
            ParamsSen_Contacts.DeleteQuery(UpdatedTaskID)
            For Each person In ParamsSen_Contacts.GetDataByTaskID(OriginalTaskID)
                ParamsSen_Contacts.Insert(UpdatedTaskID, _
                                          person.SqlCategoryID, _
                                          person.SqlCategoryName, _
                                          person.SqlParentID, _
                                          person.SenCategoryID, _
                                          person.SenCategoryName, _
                                          person.SenParentID)
            Next
        End If

    End Sub
End Class
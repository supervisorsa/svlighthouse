﻿Imports System
Imports System.Threading
Imports System.Globalization

'Imports AppSettings

Public Class TasksExecution
    Private _IsService As Boolean = False
    Public WriteOnly Property IsService() As Boolean
        Set(ByVal value As Boolean)
            _IsService = value
        End Set
    End Property

    Private IsSen As Boolean = False
    Private IsPrime As Boolean = False
    Private IsSrs As Boolean = False
    Private IsSql As Boolean = False
    Private IsControl As Boolean = False

    Private CloseIt As Boolean = False
    Private ConnectorParams As String

    Private ActiveProcessID As Integer
    Private CurrentProcessID As Integer
    Private UpdatedNextRun As Boolean = True

    Public Function RunConnector(ByVal TaskIDsList As List(Of Integer)) As Boolean
        Dim SuccessfulRun As Boolean = True

        Dim CustomRun As Boolean
        CustomRun = Not TaskIDsList.Contains(0)

        SvRegistry.SetRegistryValue("SvConnector", "IsRunning", 1)
        Dim ConnectorData As Object
        Dim TasksList As SV.TasksDataTable
        Try
            If CustomRun Then
                TasksList = Tasks.GetFullData
            Else
                TasksList = Tasks.GetDataActive
            End If
        Catch exList As Exception
            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog(exList.Message, My.Application.Info.DirectoryPath)
            If Settings.GetBoolean("UnhandledExceptionManager/SendEmail") Then
                Dim MailError As String = SimpleMail.SendConnEmail(exList.Message, "")
                If MailError <> String.Empty Then SvSys.ErrorLog(MailError, My.Application.Info.DirectoryPath)
            End If
            Exit Function
        End Try
        Try
            Dim ci As CultureInfo = Thread.CurrentThread.CurrentCulture

            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΕΝΑΡΞΗ : " & Environment.UserName & vbTab & ci.DisplayName, 60) & vbTab & "***", My.Application.Info.DirectoryPath)

            UpdatedNextRun = False
            For Each task In TasksList
                Try
                    ConnectorData = New Object
                    ConnectorData = CreateObject("ConnectorData.Data")
                    'ConnectorData = New SvConnections.ConnectorData.Data
                    Dim NextOccurrence As Date
                    Dim LastOccurance As Date

                    If TaskIDsList.Contains(task.ID) OrElse Not CustomRun Then
                        If SvConnTrigger(ConnectorData, task.SvConnID) Then
                            If ConnectionTrigger(ConnectorData, _
                                                 task.ConnectionType1, _
                                                 task.ConnectionID1) Then
                                If ConnectionTrigger(ConnectorData, _
                                                     task.ConnectionType2, _
                                                     task.ConnectionID2) Then

                                    NextOccurrence = GetNextRunByID(task.ID)

                                    LastOccurance = Now

                                    If TaskIDsList.Contains(task.ID) OrElse _
                                    (Not CustomRun AndAlso task.NextRun <= LastOccurance) Then
                                        SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength(task.TaskName, 60) & vbTab & "***", My.Application.Info.DirectoryPath)

                                        Tasks.UpdateLastRun(LastOccurance, NextOccurrence, task.ID)

                                        If IsSen And IsSql Then
                                            Dim SalesPersons As New List(Of SvSessionsConn.SvConnData.SvStructure)
                                            For Each person In ParamsSen_SalesPersons.GetDataByTaskID(task.ID)
                                                SalesPersons.Add(New SvSessionsConn.SvConnData.SvStructure(person.SqlPersonID, person.SenPersonID))
                                            Next
                                            ConnectorData.SalesPersons = SalesPersons

                                            Dim Payways As New Dictionary(Of String, String)
                                            For Each person In ParamsSen_Payways.GetDataByTaskID(task.ID)
                                                Payways.Add(person.SqlPaywayID, person.SenPaywayID)
                                            Next
                                            ConnectorData.Payways = Payways

                                            Dim ItemsCategories As New Dictionary(Of KeyValuePair(Of String, String), String)
                                            'Dim ItemsDuration As New Dictionary(Of String, String)
                                            For Each person In ParamsSen_Items.GetDataByTaskID(task.ID)
                                                ItemsCategories.Add(New KeyValuePair(Of String, String)(person.SqlCategoryID, person.Duration), person.SenCategoryID)
                                                'ItemsDuration.Add(person.SenCategoryID, person.Duration)
                                            Next
                                            ConnectorData.ItemsCategories = ItemsCategories
                                            'ConnectorData.ItemsDuration = ItemsDuration

                                            Dim ContactsCategories As New Dictionary(Of KeyValuePair(Of String, String), KeyValuePair(Of String, String))
                                            For Each person In ParamsSen_Contacts.GetDataByTaskID(task.ID)
                                                ContactsCategories.Add(New KeyValuePair(Of String, String)(person.SqlCategoryID, person.SqlParentID), New KeyValuePair(Of String, String)(person.SenCategoryID, person.SenParentID))
                                            Next
                                            ConnectorData.ContactsCategories = ContactsCategories

                                            Dim CurrentTaskParams As SV.TaskParamsRow

                                            If TaskParams.GetDataByTaskID(task.ID).Count = 1 Then
                                                CurrentTaskParams = TaskParams.GetDataByTaskID(task.ID)(0)
                                                ConnectorData.AcaCode = CurrentTaskParams.Contacts_AcaCode
                                                ConnectorData.Currency = CurrentTaskParams.Contacts_Currency
                                                ConnectorData.ContactAutoCat = CurrentTaskParams.Contacts_AutoCategories
                                                ConnectorData.StockAutoCat = CurrentTaskParams.Stock_AutoCategories
                                            End If
                                        End If

                                        ConnectorData.RunData(My.Application.Info.DirectoryPath, task.TaskName, task.ConnectionID1, task.ConnectionID2)

                                        If ConnectorData IsNot Nothing Then
                                            ConnectorData.DisconnectPrime()
                                            ConnectorData.DisconnectSen()
                                            ConnectorData.DisconnectSrs()
                                            ConnectorData.DisconnectSql()
                                            ConnectorData.Dispose()
                                        End If

                                        NextOccurrence = GetNextRunByID(task.ID)

                                        Tasks.UpdateNextRun(NextOccurrence, task.ID)
                                        'Else
                                        '    SvSys.ResultsLog("RUN".PadRight(10) & SvSys.LeftFixedLength(task.TaskName, 60) & vbTab & "***", My.Application.Info.DirectoryPath)
                                        '    SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength(task.NextRun.ToString & "---" & LastOccurance.ToString, 60) & vbTab & "***", My.Application.Info.DirectoryPath)
                                    End If

                                Else
                                    If task.SchedulesRow.IsDaily Then
                                        Throw New Exception("Connection 2 failed")
                                    Else
                                        SuccessfulRun = False
                                    End If
                                End If
                            Else
                                If task.SchedulesRow.IsDaily Then
                                    Throw New Exception("Connection 1 failed")
                                Else
                                    SuccessfulRun = False
                                End If
                            End If
                        Else
                            If task.SchedulesRow.IsDaily Then
                                Throw New Exception("Sv Connection failed")
                            Else
                                SuccessfulRun = False
                            End If
                        End If
                    End If

                    If _IsService Then
                        If IsNumeric(SvRegistry.GetRegistryValue("SvConnector", "ActiveProcess")) _
                        AndAlso SvRegistry.GetRegistryValue("SvConnector", "ActiveProcess") <> "0" Then
                            Exit For
                        End If
                    End If
                Catch ex As Exception
                    SuccessfulRun = False

                    SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
                    SvSys.ErrorLog(task.TaskName & " : " & ex.Message, My.Application.Info.DirectoryPath)
                    If Settings.GetBoolean("UnhandledExceptionManager/SendEmail") Then
                        Dim MailError As String = SimpleMail.SendConnEmail(task.TaskName & " : " & ex.Message, "")
                        If MailError <> String.Empty Then SvSys.ErrorLog(MailError, My.Application.Info.DirectoryPath)
                    End If
                End Try
            Next
            UpdatedNextRun = True

            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΟΛΟΚΛΗΡΩΣΗ ΔΙΑΔΙΚΑΣΙΑΣ", 30) & vbTab & "***", My.Application.Info.DirectoryPath)

        Catch ex As Exception
            SuccessfulRun = False

            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog(ex.Message, My.Application.Info.DirectoryPath)
            If Settings.GetBoolean("UnhandledExceptionManager/SendEmail") Then
                Dim MailError As String = SimpleMail.SendConnEmail(ex.Message, "")
                If MailError <> String.Empty Then SvSys.ErrorLog(MailError, My.Application.Info.DirectoryPath)
            End If
        Finally
            Try
                If ConnectorData IsNot Nothing Then
                    ConnectorData.DisconnectPrime()
                    ConnectorData.DisconnectSen()
                    ConnectorData.DisconnectSrs()
                    ConnectorData.DisconnectSql()
                    ConnectorData.Dispose()
                End If
                CalcInterval()
            Catch ex As Exception
                SvSys.ErrorLog(ex.Message, My.Application.Info.DirectoryPath)
            End Try
        End Try
        SvRegistry.SetRegistryValue("SvConnector", "IsRunning", 0)
        Return SuccessfulRun
    End Function

    Private Function SvConnTrigger(ByVal ConnectorData As Object, _
                                  ByVal ConnectionID As Integer) _
                                  As Boolean
        Dim ErrorMessage As String = ""

        Dim SqlList
        SqlList = SqlConn.GetDataByID(ConnectionID).Item(0)
        If SqlList Is Nothing Then
            SvSys.ResultsLog("Δεν υπάρχει σύνδεση με τον SQL Server για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
            Return False
        Else
            ErrorMessage = ConnectorData.ConnectToSql( _
                SqlList.server, _
                SqlList.db, _
                SqlList.user, _
                SqlList.pwd)
            If Not ErrorMessage.Equals(String.Empty) Then
                ConnectorData.DisconnectSql()
                SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                SvSys.ResultsLog("Δεν υπάρχει επικοινωνία με τον SQL Server", My.Application.Info.DirectoryPath)
                Return False
            End If
            IsSql = True
        End If
        Return True
    End Function

    Private Function ConnectionTrigger(ByVal ConnectorData As Object, _
                                  ByVal ConnectionType As String, _
                                  ByVal ConnectionID As Integer) _
                                  As Boolean
        Dim ErrorMessage As String = ""
        Select Case ConnectionType
            Case "SEN"
                Dim SenList
                SenList = SenConn.GetDataByID(ConnectionID).Item(0)
                If SenList Is Nothing Then
                    SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                    SvSys.ResultsLog("Δεν υπάρχει σύνδεση με το SEN για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
                    Return False
                Else
                    ErrorMessage = ConnectorData.ConnectToSen1( _
                        SenList.host, _
                        SenList.sasalias, _
                        SenList.senuser, _
                        SenList.senpwd, _
                        SenList.cmpcode, _
                        SenList.wrhid, _
                        SenList.port)
                    If Not ErrorMessage.Equals(String.Empty) Then
                        ConnectorData.DisconnectSen()
                        SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                        SvSys.ResultsLog("Δεν υπάρχει επικοινωνία με το SEN", My.Application.Info.DirectoryPath)
                        Return False
                    End If
                    IsSen = True
                End If
            Case "SRS"
                Dim SrsList
                SrsList = SrsConn.GetDataByID(ConnectionID).Item(0)
                If SrsList Is Nothing Then
                    SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                    SvSys.ResultsLog("Δεν υπάρχει σύνδεση με τον SRS για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
                    Return False
                Else
                    ErrorMessage = ConnectorData.ConnectToSrs( _
                        SrsList.host, _
                        SrsList.port, _
                        SrsList.sasalias, _
                        SrsList.srsuser, _
                        SrsList.srspwd)
                    If Not ErrorMessage.Equals(String.Empty) Then
                        ConnectorData.DisconnectSrs()
                        SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                        SvSys.ResultsLog("Δεν υπάρχει επικοινωνία με το SRS", My.Application.Info.DirectoryPath)
                        Return False
                    End If
                    IsSrs = True
                End If
            Case "Prime"
                Dim PrimeList
                PrimeList = PrimeConn.GetDataByID(ConnectionID).Item(0)
                If PrimeList Is Nothing Then
                    SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                    SvSys.ResultsLog("Δεν υπάρχει σύνδεση με το Prime για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
                    Return False
                Else
                    ErrorMessage = ConnectorData.ConnectToPrime( _
                        PrimeList.Server, _
                        PrimeList.DB, _
                        PrimeList.User, _
                        PrimeList.Pwd, _
                        PrimeList.AppDB, _
                        PrimeList.AppDate, _
                        PrimeList.AppUsername, _
                        PrimeList.AppPassword)
                    If Not ErrorMessage.Equals(String.Empty) Then
                        ConnectorData.DisconnectPrime()
                        SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                        SvSys.ResultsLog("Δεν υπάρχει επικοινωνία με το Prime", My.Application.Info.DirectoryPath)
                        Return False
                    End If
                    IsPrime = True
                End If
            Case "Pylon"
                Dim PylonList
                PylonList = PylonConn.GetDataByID(ConnectionID).Item(0)
                If PylonList Is Nothing Then
                    SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                    SvSys.ResultsLog("Δεν υπάρχει σύνδεση με το Pylon για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
                    Return False
                    'Else
                    '    ErrorMessage = ConnectorData.ConnectToPylon( _
                    '        PylonList.Server, _
                    '        PylonList.DB, _
                    '        PylonList.User, _
                    '        PylonList.Pwd, _
                    '        PylonList.AppDB, _
                    '        PylonList.AppDate, _
                    '        PylonList.AppUsername, _
                    '        PylonList.AppPassword)
                    '    If Not ErrorMessage.Equals(String.Empty) Then
                    '        SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                    '        SvSys.ResultsLog("Δεν υπάρχει επικοινωνία με το Pylon", My.Application.Info.DirectoryPath)
                    '        Return False
                    '    End If
                End If

            Case "Virtuemart"
                Dim VirtuemartList
                VirtuemartList = VirtuemartConn.GetDataByID(ConnectionID).Item(0)
                If VirtuemartList Is Nothing Then
                    SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                    SvSys.ResultsLog("Δεν υπάρχει σύνδεση με το Virtuemart για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
                    Return False
                    'Else
                    '    ErrorMessage = ConnectorData.ConnectToVirtuemart( _
                    '        VirtuemartList.Server, _
                    '        VirtuemartList.DB, _
                    '        VirtuemartList.User, _
                    '        VirtuemartList.Pwd, _
                    '        VirtuemartList.AppDB, _
                    '        VirtuemartList.AppDate, _
                    '        VirtuemartList.AppUsername, _
                    '        VirtuemartList.AppPassword)
                    '    If Not ErrorMessage.Equals(String.Empty) Then
                    '        SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                    '        SvSys.ResultsLog("Δεν υπάρχει επικοινωνία με το Virtuemart", My.Application.Info.DirectoryPath)
                    '        Return False
                    '    End If
                End If

            Case "Αρχείο"
                Dim FileList
                FileList = FileConn.GetDataByID(ConnectionID).Item(0)
                If FileList Is Nothing Then
                    SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                    SvSys.ResultsLog("Δεν υπάρχει σύνδεση με αρχείο για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
                    Return False
                Else
                    ErrorMessage = ConnectorData.ConnectToFile( _
                        FileList.Path, _
                        FileList.Filename)
                    If Not ErrorMessage.Equals(String.Empty) Then
                        SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                        SvSys.ResultsLog("Δεν υπάρχει φάκελος/αρχείο", My.Application.Info.DirectoryPath)
                        Return False
                    End If
                End If
            Case Else
                If ConnectionType <> "" Then
                    Dim SqlList
                    SqlList = SqlConn.GetDataByID(ConnectionID).Item(0)
                    If SqlList Is Nothing Then
                        SvSys.ResultsLog("Δεν υπάρχει σύνδεση με τον SQL Server για το ID" & ConnectionID, My.Application.Info.DirectoryPath)
                        Return False
                    Else
                        ErrorMessage = ConnectorData.ConnectToSql( _
                            SqlList.server, _
                            SqlList.db, _
                            SqlList.user, _
                            SqlList.pwd)
                        If Not ErrorMessage.Equals(String.Empty) Then
                            ConnectorData.DisconnectSql()
                            SvSys.ErrorLog(ErrorMessage, My.Application.Info.DirectoryPath)
                            SvSys.ResultsLog("Δεν υπάρχει επικοινωνία με τον SQL Server", My.Application.Info.DirectoryPath)
                            Return False
                        End If
                        IsSql = True
                    End If
                Else
                    SvSys.ErrorLog("Άγνωστος τύπος σύνδεσης", My.Application.Info.DirectoryPath)
                    SvSys.ResultsLog("Άγνωστος τύπος σύνδεσης", My.Application.Info.DirectoryPath)
                    Return False
                End If
        End Select
        Return True

    End Function

    Public Function CalcInterval() As Integer
        Dim intv As Integer = 0
        Try
            Dim tempOccurrence = Tasks.GetNextRun
            If tempOccurrence IsNot Nothing Then
                Dim NextOccurrence As Date = Tasks.GetNextRun
                If NextOccurrence < Now Then
                    intv = If(UpdatedNextRun, 10000, 1000000)
                Else
                    intv = DateDiff(DateInterval.Second, TimeValue(Now), TimeValue(NextOccurrence)) * 1000
                End If
                If intv < 0 Then
                    intv += 86400000
                End If
                If intv = 0 Then
                    intv = 10002
                End If
                'lblNextRun.Text = FormatDateTime(DateAdd(DateInterval.Second, (intv / 1000), Now), DateFormat.GeneralDate).ToString
            End If
        Catch ex As Exception
            intv = 1000004
            SvSys.ErrorLog(ex.Message, My.Application.Info.DirectoryPath)
        End Try
        Return intv
    End Function

End Class



﻿Imports System.Threading
Imports System.Globalization
Public Class SvSen
    Implements IDisposable

    Public Enum GlobalIndeces
        AcaCode = 0
        Currency = 1
        'ContactAutoCat = 2
    End Enum

    Public Enum OrderIndeces
        IsOfficial = 0
        DefaultSalepersonID = 1
        DefaultConsignment = 2
        DefaultConsignmentVat = 3
    End Enum

    Public Enum TraderIndeces
        DefaultSalepersonID = 0
    End Enum

    Private ConnSession As SvConnData

    Public Function Connect(ByVal svSession As SvConnData, _
                       ByVal ParamsList As List(Of String), _
                       Optional ByVal Host As String = "", _
                       Optional ByVal Port As Integer = 400, _
                       Optional ByVal SasAliasName As String = "", _
                       Optional ByVal UserName As String = "", _
                       Optional ByVal PassWord As String = "", _
                       Optional ByVal CmpCode As String = "", _
                       Optional ByVal WrhID As Integer = 0) As String
        'Dim fileIni As String


        ConnSession = svSession
        Dim ErrorMessage As String = ""

        'Dim textLine As String
        'Dim SplitTextLine As Object

        Try
            SacSession = New SacSessionX

            SacSession.Host = Host
            SacSession.Port = Port
            SacSession.Application = "SEN"
            SacSession.Open()

            Dim oSenApp As SacObjectX
            Dim v As Object
            Dim arr(5) As Object
            Dim arr0(1) As Object
            Dim VersionStr As String
            oSenApp = SacSession.CreateObject("TSenApplication", False)

            arr0(0) = ""
            VersionStr = oSenApp.Call("ISenApplication_GetVersionString", arr0)(0)
            arr(0) = UserName
            arr(1) = PassWord
            arr(2) = SasAliasName
            arr(3) = ""
            arr(4) = ""
            v = oSenApp.Call("ISenApplication_Login", arr)

            Dim arr1(2) As Object
            arr1(0) = "cmpcode"
            arr1(1) = GetFullCode(CmpCode)
            v = oSenApp.Call("ISenApplication_SetVariable", arr1)
            Dim fis As SacObjectX
            fis = SacSession.CreateObject("TFiscalYearSrv")
            Dim currentfiyid As String
            currentfiyid = fis.Call("IFiscalYear_GetCurrentFiscalYear")(0)
            arr1(0) = "fiyid"
            arr1(1) = currentfiyid
            v = oSenApp.Call("ISenApplication_SetVariable", arr1)
            arr1(0) = "wrhid"
            arr1(1) = GetFullCode(WrhID)
            v = oSenApp.Call("ISenApplication_SetVariable", arr1)
            arr1(0) = "workingdate"
            arr1(1) = Now
            v = oSenApp.Call("ISenApplication_SetVariable", arr1)

            SacSession.Timeout = 100000

            InitParams(ParamsList)
        Catch ex As Exception
            ErrorMessage = ex.Message
        End Try
        Return ErrorMessage
    End Function

    Public Sub InitParams(ByVal ParamsList As List(Of String))
        CurrencyID = GetCurrencyID(ParamsList(GlobalIndeces.Currency))
        AcaCode = ParamsList(GlobalIndeces.AcaCode)
    End Sub

    Private Function GetFullCode(ByVal code As String) As String
        Dim TempCode As String = code
        Do Until TempCode.Length >= 3
            TempCode = "0" & TempCode
        Loop
        Return TempCode
    End Function

    Public Sub ExportProducts(ByVal ExportCategories As Boolean, ByRef MCIIDs As String, Optional ByVal CodeFilter As String = "") '(ByVal FullImport As Boolean, ByVal OnlyNew As Boolean)


        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentUICulture = New CultureInfo("el-GR", True)
        Try
            Dim kbmStock As New kbm

            Dim kbmStockPrices As New kbm
            Dim kbmStockItemCode As New kbm
            Dim kbmDFT As New kbm
            Dim oSenAppl = SacSession.CreateObject("TSenApplication")
            Dim CompanyID = oSenAppl.Call("ISenApplication_GetVariable", New Object() {"CMPID"})(0)
            Dim sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)
            Dim SenQuery As String

            Dim sacStockItem As SacObjectX
            Dim sacStockPrices As SacObjectX
            Dim sacDFT As SacObjectX
            sacStockItem = SacSession.CreateObject("TStockItemSrv")
            'kbmStock.EmptyAll()

            kbmStock.DataPacket = sacStockItem.Call("ISEnDataBroker_GetData", New Object() {"CODCODE LIKE '" & CodeFilter & "%'", "", 0})(0)
            'kbmStock.DataPacket = sacStockItem.Call("ISEnDataBroker_GetData", New Object() {"INSTR(CODCODE, '" & CodeFilter & "')=1 ", "", 0})
            'SacSession.Call("OpenSql", New Object() {sAlias, "SELECT CTGID,CTGDESCR,CTGIDPARENT,CTGOUTLINE FROM CTG WHERE CTGSUBSYSTEMS LIKE '1%' ORDER BY CTGOUTLINE", 0})


            Dim ProductList = ConnSession.LoadProducts(False, False, False, False, False)

            Do Until kbmStock.Eof

                Dim ProductID As String = kbmStock.FieldValues("MCIID").ToString
                MCIIDs &= ProductID & ","

                Dim ProductExists = (From t In ProductList _
                                         Where t.SourceID.ToString.Equals(ProductID) _
                                         Select t).FirstOrDefault

                Dim RetailPrice As Double = 0
                Dim WholesalePrice As Double = 0
                Dim BuyPrice As Double = 0
                Dim DFAID1 As String = ""
                Dim DFAID2 As String = ""
                Dim VolumeWeight As Decimal = 0
                Dim Weight As Decimal = 0

                sacStockPrices = SacSession.CreateObject("TStockItemPriceSrv")
                kbmStockPrices.DataPacket = sacStockPrices.Call("ISEnDataBroker_GetData", New Object() {"MCIID=" & ProductID, "", 0})(0)

                If Not kbmStockPrices.Eof Then
                    Do Until kbmStockPrices.Eof
                        If kbmStockPrices.FieldValues("PTPID") = -1 Then
                            WholesalePrice = If(IsDBNull(kbmStockPrices.FieldValues("ITPGROSSPRICE")), 0, kbmStockPrices.FieldValues("ITPGROSSPRICE"))
                            RetailPrice = If(IsDBNull(kbmStockPrices.FieldValues("ITPRETAILPRICE")), 0, kbmStockPrices.FieldValues("ITPRETAILPRICE"))
                        End If
                        kbmStockPrices.Next()
                    Loop

                End If

                sacDFT = SacSession.CreateObject("TDefAttributesTemplateSrv")
                Dim _DFTID As String = kbmStock.FieldValues("DFTID").ToString()

                If Not _DFTID = "" Then
                    kbmDFT.DataPacket = sacDFT.Call("ISEnDataBroker_GetData", New Object() {" DFTID = " & kbmStock.FieldValues("DFTID"), "", 0})(0)
                End If

                If Not kbmDFT.Eof Then
                    Do Until kbmDFT.Eof
                        DFAID1 = If(IsDBNull(kbmDFT.FieldValues("DFAID1")), "", kbmDFT.FieldValues("DFAID1"))
                        DFAID2 = If(IsDBNull(kbmDFT.FieldValues("DFAID2")), "", kbmDFT.FieldValues("DFAID2"))
                        kbmDFT.Next()
                    Loop

                End If

                If ProductExists Is Nothing Then
                    Dim NewProduct As New SvProduct
                    NewProduct.ID = Guid.NewGuid
                    NewProduct.SourceID = kbmStock.FieldValues("MCIID").ToString
                    NewProduct.Name = kbmStock.FieldValues("ITMNAME")
                    NewProduct.ProductCode = kbmStock.FieldValues("CODCODE")
                    NewProduct.RetailPrice = RetailPrice
                    NewProduct.WholesalePrice = WholesalePrice
                    NewProduct.CurrencyName = "EUR"
                    'nick 21-12-2015----------------------------
                    'NewProduct.Description1 = CheckNull(kbmStock.FieldValues("ITMREMARK"))
                    NewProduct.Description1 = CheckNull(kbmStock.FieldValues("STIANALDESCR"))
                    NewProduct.Description2 = CheckNull(kbmStock.FieldValues("STIFOREIGNDESCR")).ToString
                    NewProduct.VatSourceID = kbmStock.FieldValues("VATID")
                    NewProduct.IsActive = kbmStock.FieldValues("ITMACTIVE")
                    'Volumetric OR Weight
                    VolumeWeight = If(IsDBNull(kbmStock.FieldValues("STIVOLUME")), 0, Convert.ToDecimal(kbmStock.FieldValues("STIVOLUME")))
                    NewProduct.ProductVolumeWeight = VolumeWeight
                    Weight = If(IsDBNull(kbmStock.FieldValues("STIWEIGHT")), 0, Convert.ToDecimal(kbmStock.FieldValues("STIWEIGHT")))
                    NewProduct.ProductWeight = Weight
                    NewProduct.ProductWidth = 0
                    NewProduct.ProductHeight = 0
                    NewProduct.DefaultBalance = 0
                    NewProduct.CountUnitName = kbmStock.FieldValues("UNINAMEA")
                    NewProduct.WeightUnitName = "γρμ."
                    NewProduct.LengthUnitName = "mm"

                    NewProduct.BuyPrice = BuyPrice
                    NewProduct.DFTID = If(IsDBNull(kbmStock.FieldValues("DFTID")), 0, Convert.ToDecimal(kbmStock.FieldValues("DFTID")))

                    NewProduct.DFAID1 = DFAID1
                    NewProduct.DFAID2 = DFAID2

                    NewProduct.ImportProduct(True)

                    ProductExists = NewProduct

                Else

                    ProductExists.LastModificationSource = Now
                    ProductExists.IsDirty = True

                    ProductExists.Name = kbmStock.FieldValues("ITMNAME").ToString
                    ProductExists.ProductCode = kbmStock.FieldValues("CODCODE").ToString
                    ProductExists.Description2 = CheckNull(kbmStock.FieldValues("STIFOREIGNDESCR")).ToString
                    ProductExists.RetailPrice = RetailPrice
                    ProductExists.WholesalePrice = WholesalePrice
                    'nick 21-12-2015----------------------------
                    'NewProduct.Description1 = CheckNull(kbmStock.FieldValues("ITMREMARK"))
                    ProductExists.Description1 = CheckNull(kbmStock.FieldValues("STIANALDESCR"))
                    ProductExists.VatSourceID = kbmStock.FieldValues("VATID").ToString
                    ProductExists.IsActive = kbmStock.FieldValues("ITMACTIVE").ToString
                    'Volumetric OR Weight
                    VolumeWeight = If(IsDBNull(kbmStock.FieldValues("STIVOLUME")), 0, Convert.ToDecimal(kbmStock.FieldValues("STIVOLUME")))
                    ProductExists.ProductVolumeWeight = VolumeWeight
                    Weight = If(IsDBNull(kbmStock.FieldValues("STIWEIGHT")), 0, Convert.ToDecimal(kbmStock.FieldValues("STIWEIGHT")))
                    ProductExists.ProductWeight = Weight
                    ProductExists.ProductLength = 0
                    ProductExists.ProductWidth = 0
                    ProductExists.ProductHeight = 0
                    ProductExists.DefaultBalance = 0

                    ProductExists.BuyPrice = BuyPrice

                    ProductExists.DFTID = If(IsDBNull(kbmStock.FieldValues("DFTID")), 0, Convert.ToDecimal(kbmStock.FieldValues("DFTID")))
                    ProductExists.DFAID1 = DFAID1
                    ProductExists.DFAID2 = DFAID2
                    ProductExists.ImportProduct(False)

                End If

                If ExportCategories Then
                    Dim kbmStockCategories As New kbm

                    SenQuery = String.Format("SELECT * FROM SIG WHERE MCIID={0} and (ctgidroot = {1} or ctgidroot = {2})", _
                                                                kbmStock.FieldValues("MCIID"), 1653, 6235)
                    kbmStockCategories.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, SenQuery, 0})

                    Dim CategoriesIDs As New List(Of String)

                    Do Until kbmStockCategories.Eof

                        'Dim CategoryID = ConnSession.GetSqlCategoryID(kbmStockCategories.FieldValues("CTGID"))
                        'If CategoryID <> "0" Then
                        CategoriesIDs.Add(kbmStockCategories.FieldValues("CTGID"))

                        Dim Duration = ConnSession.GetSqlDuration(kbmStockCategories.FieldValues("CTGID"))
                        If Duration > 0 Then
                            ProductExists.WithExpiry = True
                            ProductExists.Duration = Duration
                        End If
                        'End If                        
                        kbmStockCategories.Next()
                    Loop

                    ProductExists.ImportProductCategory(CategoriesIDs)

                    If (Not kbmStockCategories Is Nothing) Then
                        kbmStockCategories.EmptyAll()
                        kbmStockCategories = Nothing
                    End If

                End If

                kbmStock.Next()

            Loop
            kbmStock.EmptyAll()

        Catch ex As Exception
            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog("ExportProducts" & " : " & ex.Message, My.Application.Info.DirectoryPath)
        End Try
    End Sub

    Public Sub ExportProductCodes(ByRef MCIIDs As String, Optional ByVal CodeFilter As String = "")

        Try
            Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
            Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)

            Dim sacStockCode As SacObjectX

            Dim kbmStockCode As New kbm

            sacStockCode = SacSession.CreateObject("TStockItemCodeSrv")

            MCIIDs = MCIIDs.Remove(MCIIDs.LastIndexOf(MCIIDs.Last), 1)
            Dim sWhere As String = " MCIID in (" & MCIIDs & " ) and COKID = 21 AND DFVCODE1 IS NOT NULL AND DFVCODE2 IS NOT NULL "
            kbmStockCode.DataPacket = sacStockCode.Call("ISEnDataBroker_GetData", New Object() {sWhere, "", 0})(0)


            Dim ProductCodeList = ConnSession.LoadProductsCodes(False, False)
            Dim ColorValue As String = "", SizeValue = ""

            Do Until kbmStockCode.Eof
                Try
                    Dim ProductID As String = kbmStockCode.FieldValues("MCIID").ToString
                    Dim Code As String = kbmStockCode.FieldValues("CODCODE").ToString
                    Dim DfvCode1 As String = kbmStockCode.FieldValues("DFVCODE1").ToString
                    Dim DfvCode2 As String = kbmStockCode.FieldValues("DFVCODE2").ToString

                    Dim sacDfv As SacObjectX
                    Dim kbmDfv As New kbm

                    Dim Product = ConnSession.GetProductsById(ProductID)
                    If Not Product Is Nothing Then

                        sWhere = String.Format(" (DFAID={0} AND DFVCODE='{1}') Or (DFAID={2} AND DFVCODE='{3}') ", Product.Item(0).DFAID1.ToString(), DfvCode1, Product.Item(0).DFAID2.ToString(), DfvCode2)
                        sacDfv = SacSession.CreateObject("TSimpleDefAttributeValueSrv")
                        kbmDfv.DataPacket = sacDfv.Call("ISEnDataBroker_GetData", New Object() {sWhere, "", 0})(0)

                        If Not kbmDfv.Eof Then
                            Do Until kbmDfv.Eof
                                If (kbmDfv.FieldValues("DFVCODE").ToString.ToUpper = DfvCode1.ToUpper) Then
                                    ColorValue = kbmDfv.FieldValues("DFVVALUE").ToString

                                End If

                                If (kbmDfv.FieldValues("DFVCODE").ToString.ToUpper = DfvCode2.ToUpper) Then
                                    SizeValue = kbmDfv.FieldValues("DFVVALUE").ToString

                                End If
                                kbmDfv.Next()
                            Loop
                        End If
                    End If
                    Dim ProductExists = (From t In ProductCodeList _
                                             Where t.SourceID.ToString.Equals(Code) _
                                             And t.ProductSourceID.Equals(ProductID) _
                                             Select t).FirstOrDefault


                    If ProductExists Is Nothing Then
                        Dim NewProduct As New svProductCode
                        NewProduct.ID = Guid.NewGuid
                        NewProduct.SourceID = Code
                        NewProduct.ProductSourceID = ProductID
                        NewProduct.Code = Code
                        NewProduct.Color = DfvCode1
                        NewProduct.Size = DfvCode2
                        NewProduct.ColorValue = ColorValue
                        NewProduct.SizeValue = SizeValue

                        NewProduct.ImportCode(True)

                        ProductExists = NewProduct
                    Else
                        If Not ProductExists.Code = Code OrElse
                            Not ProductExists.Color = DfvCode1 OrElse
                            Not ProductExists.Size = DfvCode2 OrElse
                            Not ProductExists.ColorValue = ColorValue OrElse
                            Not ProductExists.SizeValue = SizeValue Then

                            ProductExists.LastModificationSource = Now
                            ProductExists.IsDirty = True

                            ProductExists.Code = Code
                            ProductExists.Color = DfvCode1
                            ProductExists.Size = DfvCode2
                            ProductExists.ColorValue = ColorValue
                            ProductExists.SizeValue = SizeValue

                            ProductExists.ImportCode(False)

                        End If
                    End If




                Catch ex As Exception
                    SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
                    SvSys.ErrorLog("ExportProductCodes" & " : " & ex.Message, My.Application.Info.DirectoryPath)
                End Try

                kbmStockCode.Next()
            Loop

            kbmStockCode.EmptyAll()
            If (Not kbmStockCode Is Nothing) Then
                kbmStockCode = Nothing
            End If
        Catch ex As Exception
            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog("ExportProductsCodes" & " : " & ex.Message, My.Application.Info.DirectoryPath)
        End Try
    End Sub

    Public Sub ExportProductsCategories()
        Dim kbmCategories As New kbm
        Dim AddNew As Boolean = False
        Dim counterInsert As Integer = 0
        Dim counterUpdate As Integer = 0

        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)


        Try
            Dim CategoryList = ConnSession.LoadProductsCategories(False, False)

            Dim oSenAppl = SacSession.CreateObject("TSenApplication")
            Dim CompanyID = oSenAppl.Call("ISenApplication_GetVariable", New Object() {"CMPID"})(0)
            Dim sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)

            kbmCategories.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, "SELECT CTGID,CTGDESCR,CTGIDPARENT,CTGOUTLINE FROM CTG WHERE CTGSUBSYSTEMS LIKE '1%' ORDER BY CTGOUTLINE", 0})
            If kbmCategories.Eof Then
            Else
                Do Until kbmCategories.Eof
                    Dim CategoryExists = (From t In CategoryList _
                                      Where t.SourceID.Equals(kbmCategories.FieldValues("CTGID").ToString) _
                                      Select t).FirstOrDefault


                    If CategoryExists Is Nothing Then
                        Dim NewCategory As New SvProductCategory With { _
                        .ID = Guid.NewGuid, _
                        .SourceID = kbmCategories.FieldValues("CTGID").ToString, _
                        .LastModificationSource = Now, _
                        .IsDirty = True, _
                        .IsActive = True, _
                        .SourceBelongsTo = CheckNull(kbmCategories.FieldValues("CTGIDPARENT")), _
                        .Name = kbmCategories.FieldValues("CTGDESCR")}

                        NewCategory.ImportCategory(True)
                    Else
                        If Not CategoryExists.Name = kbmCategories.FieldValues("CTGDESCR") Then

                            CategoryExists.LastModificationSource = Now
                            CategoryExists.IsDirty = True
                            CategoryExists.Name = kbmCategories.FieldValues("CTGDESCR")
                            CategoryExists.IsActive = True
                            CategoryExists.SourceBelongsTo = CheckNull(kbmCategories.FieldValues("CTGIDPARENT"))

                            CategoryExists.ImportCategory(False)
                        End If
                    End If

                    kbmCategories.Next()

                Loop
            End If
            kbmCategories.EmptyAll()

        Catch ex As Exception
            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog("ExportProductsCategories" & " : " & ex.Message, My.Application.Info.DirectoryPath)
        End Try
    End Sub

    Private Function CheckNull(ByVal aObj) As String
        On Error Resume Next
        CheckNull = ""
        If IsDBNull(aObj) Then

        ElseIf aObj.ToString = "" Then

        Else
            CheckNull = CStr(aObj)
        End If
    End Function

    Public Function Disconnect() As String
        Try
            Try
                Dim oSenAppl As Object
                Dim v As Object

                oSenAppl = SacSession.CreateObject("TSenApplication")
                If Not IsNothing(oSenAppl) Then
                    v = oSenAppl.Call("ISenApplication_Logout", Nothing)
                End If
            Catch ex As Exception

            End Try

            If SacSession IsNot Nothing AndAlso SacSession.active = True Then
                SacSession.Disconnect()
                SacSession.Close()
            End If
            Try
                If Not IsNothing(SacSession) Then SacSession.Close()
            Catch ex As Exception

            End Try

            Return ""
        Catch ex As Exception
            Return ex.Message
        End Try
    End Function

    Public Sub ExportVat()
        Dim VatList = ConnSession.LoadVat(False)

        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)


        Dim oSenAppl = SacSession.CreateObject("TSenApplication")
        Dim kbmKind As New kbm

        kbmKind.DataPacket = SacSession.Call("OpenSql", New Object() _
                {oSenAppl.Call("ISenApplication_GetAliasName", _
                New Object() {})(0), _
                "SELECT vatid, vatdescr, vatcode, vatratenormal, vatratereduced FROM vat", 0})

        Do Until kbmKind.Eof
            Dim VatExists = (From t In VatList _
                              Where t.SourceID.Equals(kbmKind.FieldValues("VATID").ToString) _
                              Select t).FirstOrDefault
            If VatExists Is Nothing Then
                Dim NewVat As New svVat With { _
                .ID = Guid.NewGuid, _
                .SourceID = kbmKind.FieldValues("VATID").ToString, _
                .LastModificationSource = Now, _
                .IsDirty = True, _
                .Name = kbmKind.FieldValues("VATDESCR"), _
                .VatCode = kbmKind.FieldValues("VATCODE"), _
                .VatPercentage = Convert.ToDecimal(kbmKind.FieldValues("VATRATENORMAL")), _
                .VatPercentageReduced = Convert.ToDecimal(kbmKind.FieldValues("VATRATEREDUCED"))}

                NewVat.ImportVat(True)
            Else
                VatExists.LastModificationSource = Now
                VatExists.IsDirty = True
                VatExists.Name = kbmKind.FieldValues("VATDESCR")
                VatExists.VatCode = kbmKind.FieldValues("VATCODE")
                VatExists.VatPercentage = Convert.ToDecimal(kbmKind.FieldValues("VATRATENORMAL"))
                VatExists.VatPercentageReduced = Convert.ToDecimal(kbmKind.FieldValues("VATRATEREDUCED"))

                VatExists.ImportVat(False)
            End If
            kbmKind.Next()
        Loop
        kbmKind.EmptyAll()

    End Sub

    Public Sub ExportTraderFinancial()
        Dim oSenAppl = SacSession.CreateObject("TSenApplication")
        Dim kbmKind As New kbm

        Dim FinancialList As List(Of SvTraderFinancial)
        FinancialList = ConnSession.LoadTradersFinancial(False)

        Dim TraderList As List(Of SvTrader)
        TraderList = ConnSession.LoadTraders(False, True, False, False)

        For Each trd In TraderList
            If trd.FinancialTraderSourceID IsNot Nothing Then
                kbmKind = New kbm

                Dim Rest As Decimal
                Dim Cheques As Decimal
                Dim Turnover As Decimal
                Dim Charge As Decimal
                Dim Credit As Decimal

                kbmKind.DataPacket = SacSession.Call("OpenSql", New Object() _
                {oSenAppl.Call("ISenApplication_GetAliasName", _
                New Object() {})(0), _
                My.Resources.Queries.Rest & " AND TRAID=" & trd.FinancialTraderSourceID, 0})

                Rest = If(IsDBNull(kbmKind.FieldValues("LOGISTIKO_YPOLOIPO")), 0, kbmKind.FieldValues("LOGISTIKO_YPOLOIPO"))
                Cheques = If(IsDBNull(kbmKind.FieldValues("EPITAGES")), 0, kbmKind.FieldValues("EPITAGES"))

                kbmKind.EmptyAll()

                kbmKind = New kbm

                kbmKind.DataPacket = SacSession.Call("OpenSql", New Object() _
                {oSenAppl.Call("ISenApplication_GetAliasName", _
                New Object() {})(0), _
                My.Resources.Queries.Tziros.ToLower.Replace("GROUP BY TRAID".ToLower, " AND TRAID=" & trd.FinancialTraderSourceID & " GROUP BY TRAID"), 0})

                Turnover = If(IsDBNull(kbmKind.FieldValues("Tziros")), 0, kbmKind.FieldValues("Tziros"))
                Charge = If(IsDBNull(kbmKind.FieldValues("Xreosi")), 0, kbmKind.FieldValues("Xreosi"))
                Credit = If(IsDBNull(kbmKind.FieldValues("Pistosi")), 0, kbmKind.FieldValues("Pistosi"))

                kbmKind.EmptyAll()

                Dim FinancialExists = (FinancialList.Where(Function(f) f.TraderSourceID.Equals(trd.FinancialTraderSourceID))).FirstOrDefault
                If FinancialExists Is Nothing Then
                    Dim NewFinancial As New SvTraderFinancial With { _
                    .ID = Guid.NewGuid, _
                    .SourceID = trd.FinancialTraderSourceID, _
                    .TraderSourceID = trd.FinancialTraderSourceID, _
                    .Charge = Charge, _
                    .Credit = Credit, _
                    .Rest = Rest, _
                    .Turnover = Turnover, _
                    .Cheques = Cheques}

                    NewFinancial.ImportFinancial(True)
                Else
                    FinancialExists.Charge = Charge
                    FinancialExists.Credit = Credit
                    FinancialExists.Rest = Rest
                    FinancialExists.Turnover = Turnover
                    FinancialExists.Cheques = Cheques
                    FinancialExists.ImportFinancial(False)
                End If
            End If
        Next
    End Sub

    Public Function ImportTraders(ByVal ParamsList As List(Of String), _
                              ByVal TypicalUpdate As Boolean, _
                              ByVal AllowInsert As Boolean, _
                              ByVal AllowUpdate As Boolean, _
                              Optional ByVal OtherTraderID As String = "", _
                              Optional ByRef Trader As SvTrader = Nothing, _
                              Optional ByRef CheckedTraders As List(Of SvTrader) = Nothing, _
                                          Optional ByVal CodeMask As String = "", _
                                          Optional ByVal CounterMask As String = "") As List(Of String)

        Dim ErrorsList As New List(Of String)

        Try
            Dim TraderList As List(Of SvTrader)
            If OtherTraderID <> "" Then
                TraderList = ConnSession.LoadTraders(False, TypicalUpdate, False, False)
                If TypicalUpdate Then
                    TraderList = (TraderList.AsQueryable.Where(Function(f) f.SourceTraderID.Equals(OtherTraderID))).ToList
                Else
                    TraderList = (TraderList.AsQueryable.Where(Function(f) f.DestinationTraderID.Equals(OtherTraderID))).ToList
                End If
            Else
                TraderList = ConnSession.LoadTraders(True, TypicalUpdate, False, False)
            End If

            If OtherTraderID <> "" Then
                If TraderList.Count = 1 Then
                    If AllowUpdate Or AllowInsert Then
                        Trader = ImportSingleTrader(ParamsList, TraderList.FirstOrDefault, AllowInsert, AllowUpdate, TypicalUpdate, CodeMask, CounterMask)
                    Else
                        Trader = TraderList.FirstOrDefault

                        If CheckedTraders IsNot Nothing Then
                            CheckedTraders.Add(Trader)
                        End If
                        'If TypicalUpdate Then
                        '    FsContactID = TraderList.FirstOrDefault.DestinationTraderID
                        'Else
                        '    FsContactID = TraderList.FirstOrDefault.SourceTraderID
                        'End If
                    End If
                Else
                    Throw New Exception("Θα έπρεπε να υπάρχει μοναδική εγγραφή Trader")
                End If
            Else
                For Each trd In TraderList
                    Try
                        If CheckedTraders IsNot Nothing Then
                            CheckedTraders.Add(trd)
                        End If
                        ImportSingleTrader(ParamsList, trd, AllowInsert, AllowUpdate, TypicalUpdate, CodeMask, CounterMask)
                    Catch ex As Exception
                        ErrorsList.Add(ex.Message)
                        SvSys.ErrorLog(("SEN ImportTraders " & trd.SourceCustomerID).PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
                    End Try
                Next
            End If
        Catch ex As Exception
            ErrorsList.Add(ex.Message)
            SvSys.ErrorLog(("SEN ImportTraders").PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return ErrorsList
    End Function

    Private Function ImportSingleTrader(ByVal ParamsList As List(Of String), _
                              ByVal trd As SvTrader, _
                                          ByVal AllowInsert As Boolean, _
                                          ByVal AllowUpdate As Boolean, _
                                          ByVal TypicalUpdate As Boolean, _
                                          ByVal CodeMask As String, _
                                          ByVal CounterMask As String) As SvTrader

        Dim sacCus As SacObjectX
        Dim sacLee As SacObjectX

        Dim kbmCus As New kbm
        Dim kbmLee As New kbm
        Dim kbmLeePacket As New kbm
        Dim kbmCusPacket As New kbm

        Dim kbmCategory As New kbm

        Dim LeePacket As Object
        Dim CusPacket As Object

        Dim kbmAddress As New kbm
        Dim kbmPacketAddress As New kbm
        Dim kbmPerson As New kbm

        Dim NewCusID As Integer = 0
        Dim NewLeeID As Integer = 0

        Dim LeeID As Integer = -1001
        Dim AddressID As Integer = -1001
        Dim MainAddressID As Integer = 0
        Dim PersonID As Integer = -1001

        Dim UpdateAddress As Boolean

        Dim counter As Integer = 0

        Dim aWhereStatement As String

        Try
            sacCus = SacSession.CreateObject("TCustomerSrv")
            sacLee = SacSession.CreateObject("TLegalEntitySrv")

            Dim UpdateDate As Date

            Dim oSenAppl = SacSession.CreateObject("TSenApplication")
            Dim CompanyID = oSenAppl.Call("ISenApplication_GetVariable", New Object() {"CMPID"})(0)

            Dim SenPaywayID As Integer
            Dim SenEmplID As Integer

            UpdateAddress = False

            UpdateDate = trd.LastModificationSource

            Dim TraderAddresses = trd.TraderAddresses(TypicalUpdate) ' ConnSession.LoadTraderAddresses(trd)
            'Dim TraderPersons = trd.TraderPersons.ToList

            If trd.DestinationTraderID Is Nothing Then
                ''  NEW
                NewCusID = 0
                NewLeeID = 0
                kbmLee.DataPacket = sacLee.Call("ISEnDataBroker_GetMetaData", Nothing)(0)
                kbmLee.Insert()
                kbmLee.FieldValues("LEEID") = LeeID
                If trd.JobDestinationID IsNot Nothing Then
                    If trd.JobDestinationID <> 0 Then
                        kbmLee.FieldValues("PRFID") = trd.JobDestinationID
                    End If
                End If
                kbmLee.FieldValues("LEENAME") = trd.FullName

                Dim TraderCode As String = GetValidTraCode(trd.DestinationTraderCode.ToString, counter)
                kbmLee.FieldValues("LEECODE") = TraderCode

                kbmLee.FieldValues("CURID") = CurrencyID

                If trd.TraderCountry IsNot Nothing AndAlso trd.TraderCountry.TraderCountryDestinationID IsNot Nothing Then
                    kbmLee.FieldValues("CNTID") = trd.TraderCountry.TraderCountryDestinationID
                End If

                If trd.TaxOfficeCode IsNot Nothing Then
                    If trd.TaxOfficeCode <> "" Then
                        kbmLee.FieldValues("TXOCODE") = trd.TaxOfficeCode
                    End If
                End If
                If trd.TIN IsNot Nothing Then kbmLee.FieldValues("LEEAFM") = trd.TIN
                kbmLee.FieldValues("ADRIDMAIN") = AddressID
                kbmLee.Post()


                If kbmLee.DetailCount > 0 Then
                    kbmAddress = kbmLee.Detail("AddressSrv")
                    If kbmAddress.Eof Then
                        'kbmAddress.Insert()
                        'UpdateAddress = True
                        'kbmAddress.FieldValues("ADRID") = AddressID
                        'If trd.Address1 IsNot Nothing Then kbmAddress.FieldValues("ADRSTREET") = trd.Address1
                        'If trd.Address2 IsNot Nothing Then kbmAddress.FieldValues("ADRNUMBER") = trd.Address2
                        'If trd.City IsNot Nothing Then kbmAddress.FieldValues("ADRCITY") = trd.City
                        'If trd.PostalCode IsNot Nothing Then kbmAddress.FieldValues("ADRZIPCODE") = trd.PostalCode
                        'If trd.Phone1 IsNot Nothing Then kbmAddress.FieldValues("ADRPHONE1") = trd.Phone1
                        'If trd.Phone2 IsNot Nothing Then kbmAddress.FieldValues("ADRPHONE2") = trd.Phone2
                        'If trd.MobilePhone IsNot Nothing Then kbmAddress.FieldValues("ADRPHONE3") = trd.MobilePhone
                        'If trd.FaxNumber IsNot Nothing Then kbmAddress.FieldValues("ADRFAX") = trd.FaxNumber
                        'If trd.Email IsNot Nothing Then kbmAddress.FieldValues("ADREMAIL") = trd.Email
                        'If trd.CountryDestinationID IsNot Nothing Then
                        '    If trd.CountryDestinationID <> 0 Then
                        '        kbmAddress.FieldValues("CNTID") = trd.CountryDestinationID
                        '    End If
                        'End If
                        'kbmAddress.FieldValues("ADRCODE") = "1000"
                        'kbmAddress.FieldValues("ADRDESCRIPTION") = "1000"

                        'kbmAddress.Post()

                        For Each adr In TraderAddresses
                            AddressID -= 1

                            UpdateAddress = True

                            kbmAddress.Insert()
                            kbmAddress.FieldValues("ADRID") = If(adr.IsMain, -1001, AddressID)
                            kbmAddress.FieldValues("ADRSTREET") = adr.Address1
                            kbmAddress.FieldValues("ADRNUMBER") = adr.Address2
                            kbmAddress.FieldValues("ADRDISTRICT") = adr.State
                            kbmAddress.FieldValues("ADRCITY") = adr.City
                            kbmAddress.FieldValues("ADRZIPCODE") = adr.PostalCode
                            kbmAddress.FieldValues("ADRPHONE1") = adr.Phone1
                            kbmAddress.FieldValues("ADRPHONE2") = adr.Phone2
                            kbmAddress.FieldValues("ADRPHONE3") = adr.MobilePhone
                            kbmAddress.FieldValues("ADRFAX") = adr.FaxNumber
                            kbmAddress.FieldValues("ADREMAIL") = adr.Email
                            'If Not IsDBNull(drAddress("CountryID")) Then
                            '    If drAddress("CountryID") > 0 Then
                            '        kbmAddress.FieldValues("CNTID") = drAddress("CountryID")
                            '    End If
                            'End If
                            If adr.IsMain Then
                                kbmAddress.FieldValues("IsOffice") = 0

                            Else
                                kbmAddress.FieldValues("IsOffice") = 1
                            End If
                            kbmAddress.FieldValues("ADRCODE") = If(adr.IsMain, "1000", adr.SourceID)

                            kbmAddress.FieldValues("ADRDESCRIPTION") = adr.AddressType 'If(adr.IsMain, "ΚΥΡΙΑ", adr.AddressType)

                        Next
                        kbmAddress.Post()
                        AddressID -= 1
                    End If

                    kbmPerson = kbmLee.Detail("AssociatedPersonSrv")
                    'For Each prs In TraderPersons
                    '    kbmPerson.Insert()
                    '    kbmPerson.FieldValues("ASPID") = PersonID
                    '    kbmPerson.FieldValues("ASPNAME") = prs.Name
                    '    kbmPerson.FieldValues("ASPDUTY") = prs.JobTitle
                    '    kbmPerson.Post()
                    '    PersonID -= 1
                    'Next



                    'kbmPerson = kbmLee.Detail("AssociatedPersonSrv")
                    'Dim connFsPerson As SqlConnection = CopyConnection()
                    'Dim drPerson As SqlDataReader
                    'Dim cmdPerson As SqlCommand
                    'cmdPerson = New SqlCommand( _
                    '    "Select * from ContactPersons where contactid=" & dr("ID"), connFsPerson)
                    'cmdPerson.CommandType = CommandType.Text
                    'drPerson = cmdPerson.ExecuteReader()
                    'While drPerson.Read
                    '    If Not IsDBNull(drPerson("NAME")) AndAlso drPerson("NAME") <> "" Then
                    '        kbmPerson.Insert()
                    '        kbmPerson.FieldValues("ASPID") = PersonID
                    '        kbmPerson.FieldValues("ASPNAME") = drPerson("NAME")
                    '        If Not IsDBNull(drPerson("Position")) Then
                    '            kbmPerson.FieldValues("ASPDUTY") = drPerson("Position")
                    '        End If
                    '        kbmPerson.Post()
                    '        PersonID -= 1
                    '    End If
                    'End While
                    'drPerson.Close()
                End If

                LeePacket = sacLee.Call("ISenDataBroker_ApplyUpdates", New Object() {kbmLee.DataPacket, Nothing, True})
                kbmLeePacket.DataPacket = LeePacket(2)

                NewLeeID = kbmLeePacket.FieldValues("LEEID")
                kbmPacketAddress = kbmLeePacket.Detail("AddressSrv")
                kbmCus.DataPacket = sacCus.Call("ISEnDataBroker_GetMetaData", Nothing)(0)
                kbmCus.Insert()
                kbmCus.FieldValues("TRAID") = LeeID
                kbmCus.FieldValues("LEEID") = NewLeeID
                kbmCus.FieldValues("LEENAME") = trd.FullName

                kbmCus.FieldValues("TRASTARTDATE") = Now.Date

                If trd.Notes IsNot Nothing Then kbmCus.FieldValues("TRAMEMO") = trd.Notes

                kbmCus.FieldValues("TRACODE") = TraderCode

                If Not trd.IsWholesale Then
                    kbmCus.FieldValues("CUSPRICEBASE") = 2
                Else
                    kbmCus.FieldValues("CUSPRICEBASE") = 1
                End If

                If trd.DefaultDiscount IsNot Nothing Then
                    kbmCus.FieldValues("TRADISCOUNT") = trd.DefaultDiscount
                End If

                Do Until kbmPacketAddress.Eof
                    MainAddressID = 0
                    If Not IsDBNull(kbmLeePacket.FieldValues("ADRIDMAIN")) Then
                        MainAddressID = kbmLeePacket.FieldValues("ADRIDMAIN")
                    End If
                    If kbmPacketAddress.FieldValues("ADRID") = MainAddressID Then
                        kbmCus.FieldValues("ADRIDMAIN") = kbmPacketAddress.FieldValues("ADRID")
                        kbmCus.FieldValues("ADRIDDEST") = kbmPacketAddress.FieldValues("ADRID")
                    End If

                    kbmPacketAddress.Next()
                Loop

                If trd.PaymentMethodSourceID IsNot Nothing Then
                    SenPaywayID = ConnSession.GetDestinationPaywayID(trd.PaymentMethodSourceID)
                    If SenPaywayID > 0 Then
                        kbmCus.FieldValues("PMTID") = SenPaywayID
                    End If
                End If

                If trd.TraderCountry IsNot Nothing AndAlso trd.TraderCountry.VatDestinationID IsNot Nothing AndAlso trd.TraderCountry.VatDestinationID <> "0" Then
                    kbmCus.FieldValues("CUSVATSTATUS") = trd.TraderCountry.VatDestinationID
                ElseIf trd.VatSourceID IsNot Nothing AndAlso trd.VatSourceID = "M" Then
                    kbmCus.FieldValues("CUSVATSTATUS") = "2"
                Else
                    kbmCus.FieldValues("CUSVATSTATUS") = "1"
                End If

                If trd.TraderCountry IsNot Nothing AndAlso trd.TraderCountry.KepyoDestinationID IsNot Nothing AndAlso trd.TraderCountry.KepyoDestinationID <> "0" Then
                    kbmCus.FieldValues("TRAKEPYOSTATUS") = trd.TraderCountry.KepyoDestinationID
                ElseIf trd.KepyoDestinationID IsNot Nothing AndAlso trd.KepyoDestinationID <> "0" Then
                    kbmCus.FieldValues("TRAKEPYOSTATUS") = CStr(trd.KepyoDestinationID)
                Else
                    kbmCus.FieldValues("TRAKEPYOSTATUS") = "1"
                End If


                kbmCus.FieldValues("ACACODE") = AcaCode

                If ParamsList(TraderIndeces.DefaultSalepersonID) IsNot Nothing AndAlso ParamsList(TraderIndeces.DefaultSalepersonID) <> "0" Then
                    SenEmplID = ParamsList(TraderIndeces.DefaultSalepersonID)
                Else
                    SenEmplID = ConnSession.GetDestinationSalePersonID(trd.SalesmanSourceID)
                End If

                If SenEmplID <> "0" Then
                    kbmCus.FieldValues("SLMID") = SenEmplID
                End If

                kbmCus.FieldValues("CURID") = CurrencyID

                If kbmCus.DetailCount > 0 Then

                    For Each item In trd.Categories
                        If item.DestinationID IsNot Nothing Then
                            'Dim CategoryID = ConnSession.GetSenContactCategoryID(item.DestinationID)
                            If item.DestinationID <> "0" Then
                                kbmCategory = kbmCus.Detail("CustomerCategoryDetailSrv")

                                kbmCategory.Insert()
                                Dim ParentID As String = ConnSession.GetSenContactCategoryParentID(item.SourceID)
                                kbmCategory.FieldValues("CTGIDROOT") = 42 'If(ParentID = "0", item.DestinationID, ParentID)
                                kbmCategory.FieldValues("CTGID") = item.DestinationID
                                kbmCategory.FieldValues("TRAID") = LeeID
                                kbmCategory.Post()
                            End If
                        End If
                    Next

                    'Dim connCategory As SqlConnection = CopyConnection()
                    'Dim drCategory As SqlDataReader
                    'Dim cmdCategory As SqlCommand
                    'cmdCategory = New SqlCommand( _
                    '    "SELECT ContactAndCategories.ContactID, ContactAndCategories.CategoryID  FROM  ContactANDCategories  Where ContactAndCategories.ContactID=" & dr("ID"), connCategory)
                    'cmdCategory.CommandType = CommandType.Text
                    'drCategory = cmdCategory.ExecuteReader()
                    'While drCategory.Read

                    'End While
                    'drCategory.Close()
                End If

                CusPacket = sacCus.Call("ISenDataBroker_ApplyUpdates", New Object() {kbmCus.DataPacket, Nothing, True})
                kbmCusPacket.DataPacket = CusPacket(2)

                NewCusID = kbmCusPacket.FieldValues("TRAID")

                trd.LastUpdate(True, NewLeeID, NewCusID, TraderCode, TraderCode)
            Else
                If AllowUpdate Then
                    ''  UPDATE
                    aWhereStatement = ""
                    aWhereStatement = "LEEID=" & trd.DestinationTraderID
                    kbmLee.DataPacket = sacLee.Call("ISEnDataBroker_GetData", New Object() {aWhereStatement, "", 0})(0)

                    If Not kbmLee.Eof Then
                        kbmLee.Edit()
                        If trd.JobDestinationID IsNot Nothing Then
                            If trd.JobDestinationID <> 0 Then
                                kbmLee.FieldValues("PRFID") = trd.JobDestinationID
                            End If
                        End If
                        kbmLee.FieldValues("LEENAME") = trd.FullName

                        If trd.TraderCountry IsNot Nothing AndAlso trd.TraderCountry.TraderCountryDestinationID IsNot Nothing Then
                            kbmLee.FieldValues("CNTID") = trd.TraderCountry.TraderCountryDestinationID
                        End If

                        If trd.TaxOfficeCode IsNot Nothing Then
                            If trd.TaxOfficeCode <> "" Then
                                kbmLee.FieldValues("TXOCODE") = trd.TaxOfficeCode
                            End If
                        End If
                        If trd.TIN IsNot Nothing Then kbmLee.FieldValues("LEEAFM") = trd.TIN

                        kbmLee.Post()

                        If kbmLee.DetailCount > 0 Then
                            kbmAddress = kbmLee.Detail("AddressSrv")


                            Do Until kbmAddress.Eof
                                If Not IsDBNull(kbmLee.FieldValues("ADRIDMAIN")) AndAlso kbmAddress.FieldValues("ADRID") = kbmLee.FieldValues("ADRIDMAIN") Then

                                    kbmAddress.Edit()

                                    If trd.Address1 IsNot Nothing Then kbmAddress.FieldValues("ADRSTREET") = trd.Address1
                                    If trd.Address2 IsNot Nothing Then kbmAddress.FieldValues("ADRNUMBER") = trd.Address2
                                    If trd.State IsNot Nothing Then kbmAddress.FieldValues("ADRDISTRICT") = trd.State
                                    If trd.City IsNot Nothing Then kbmAddress.FieldValues("ADRCITY") = trd.City
                                    If trd.PostalCode IsNot Nothing Then kbmAddress.FieldValues("ADRZIPCODE") = trd.PostalCode
                                    If trd.Phone1 IsNot Nothing Then kbmAddress.FieldValues("ADRPHONE1") = trd.Phone1
                                    If trd.Phone2 IsNot Nothing Then kbmAddress.FieldValues("ADRPHONE2") = trd.Phone2
                                    If trd.MobilePhone IsNot Nothing Then kbmAddress.FieldValues("ADRPHONE3") = trd.MobilePhone
                                    If trd.FaxNumber IsNot Nothing Then kbmAddress.FieldValues("ADRFAX") = trd.FaxNumber
                                    If trd.Email IsNot Nothing Then kbmAddress.FieldValues("ADREMAIL") = trd.Email

                                    If trd.TraderCountry IsNot Nothing AndAlso trd.TraderCountry.TraderCountryDestinationID IsNot Nothing Then
                                        kbmAddress.FieldValues("CNTID") = trd.TraderCountry.TraderCountryDestinationID
                                    End If

                                    kbmAddress.Post()
                                End If

                                For Each adr In TraderAddresses
                                    If adr.DestinationID IsNot Nothing Then

                                        If kbmAddress.FieldValues("ADRID") = adr.DestinationID Then
                                            kbmAddress.Edit()
                                            kbmAddress.FieldValues("ADRSTREET") = adr.Address1
                                            kbmAddress.FieldValues("ADRNUMBER") = adr.Address2
                                            kbmAddress.FieldValues("ADRDISTRICT") = adr.State
                                            kbmAddress.FieldValues("ADRCITY") = adr.City
                                            kbmAddress.FieldValues("ADRZIPCODE") = adr.PostalCode
                                            kbmAddress.FieldValues("ADRPHONE1") = adr.Phone1
                                            kbmAddress.FieldValues("ADRPHONE2") = trd.Phone2
                                            kbmAddress.FieldValues("ADRPHONE3") = trd.MobilePhone
                                            kbmAddress.FieldValues("ADRFAX") = adr.FaxNumber
                                            kbmAddress.FieldValues("ADREMAIL") = adr.Email
                                            'If Not IsDBNull(drAddress("CountryID")) Then
                                            '    If drAddress("CountryID") > 0 Then
                                            '        kbmAddress.FieldValues("CNTID") = drAddress("CountryID")
                                            '    End If
                                            'End If
                                            'If drAddress("IsBra") = 0 Then
                                            kbmAddress.FieldValues("IsOffice") = 0
                                            'Else
                                            '    kbmAddress.FieldValues("IsOffice") = 1
                                            'End If

                                            kbmAddress.FieldValues("ADRDESCRIPTION") = If(adr.AddressType = "Main", "ΚΥΡΙΑ", adr.AddressType)
                                            kbmAddress.Post()
                                        End If

                                    Else
                                        AddressID -= 1

                                        UpdateAddress = True

                                        kbmAddress.Insert()
                                        kbmAddress.FieldValues("ADRID") = If(adr.AddressType = "Main", -1001, AddressID)
                                        kbmAddress.FieldValues("ADRSTREET") = adr.Address1
                                        kbmAddress.FieldValues("ADRNUMBER") = adr.Address2
                                        kbmAddress.FieldValues("ADRDISTRICT") = adr.State
                                        kbmAddress.FieldValues("ADRCITY") = adr.City
                                        kbmAddress.FieldValues("ADRZIPCODE") = adr.PostalCode
                                        kbmAddress.FieldValues("ADRPHONE1") = adr.Phone1
                                        kbmAddress.FieldValues("ADRPHONE2") = trd.Phone2
                                        kbmAddress.FieldValues("ADRPHONE3") = trd.MobilePhone
                                        kbmAddress.FieldValues("ADRFAX") = adr.FaxNumber
                                        kbmAddress.FieldValues("ADREMAIL") = adr.Email
                                        'If Not IsDBNull(drAddress("CountryID")) Then
                                        '    If drAddress("CountryID") > 0 Then
                                        '        kbmAddress.FieldValues("CNTID") = drAddress("CountryID")
                                        '    End If
                                        'End If
                                        'If drAddress("IsBra") = 0 Then
                                        kbmAddress.FieldValues("IsOffice") = 0
                                        'Else
                                        'kbmAddress.FieldValues("IsOffice") = 1
                                        'End If
                                        kbmAddress.FieldValues("ADRCODE") = If(adr.AddressType = "Main", "1000", adr.SourceID)

                                        kbmAddress.FieldValues("ADRDESCRIPTION") = If(adr.AddressType = "Main", "ΚΥΡΙΑ", adr.AddressType)

                                        kbmAddress.Post()
                                        AddressID -= 1
                                    End If
                                Next

                                kbmAddress.Next()
                            Loop

                        End If

                        LeePacket = sacLee.Call("ISenDataBroker_ApplyUpdates", New Object() {kbmLee.DataPacket, Nothing, True})
                        kbmLeePacket.DataPacket = LeePacket(2)

                        Try
                            kbmPacketAddress = kbmLeePacket.Detail("AddressSrv")
                        Catch ex As Exception
                            kbmPacketAddress = New kbm
                            SvSys.ErrorLog(("SEN ImportSingleTrader " & trd.SourceCustomerID).PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
                        End Try
                    End If


                    aWhereStatement = ""
                    aWhereStatement = "LEEID=" & trd.DestinationTraderID & " AND TRAID=" & trd.DestinationCustomerID
                    kbmCus.DataPacket = sacCus.Call("ISEnDataBroker_GetData", New Object() {aWhereStatement, "", 0})(0)

                    If Not kbmCus.Eof Then
                        kbmCus.Edit()
                        kbmCus.FieldValues("LEENAME") = trd.FullName
                        If trd.Notes IsNot Nothing Then kbmCus.FieldValues("TRAMEMO") = trd.Notes

                        ' pk    Don't use this, remove after 31/12/2014
                        'kbmCus.FieldValues("TRASTARTDATE") = trd.LastModificationSource.Date

                        If Not trd.IsWholesale Then
                            kbmCus.FieldValues("CUSPRICEBASE") = 2
                        Else
                            kbmCus.FieldValues("CUSPRICEBASE") = 1
                        End If

                        If trd.DefaultDiscount IsNot Nothing Then
                            kbmCus.FieldValues("TRADISCOUNT") = trd.DefaultDiscount
                        End If

                        Do Until kbmPacketAddress.Eof
                            MainAddressID = 0
                            If Not IsDBNull(kbmLeePacket.FieldValues("ADRIDMAIN")) Then
                                MainAddressID = kbmLeePacket.FieldValues("ADRIDMAIN")
                            End If
                            If kbmPacketAddress.FieldValues("ADRID") = MainAddressID Then
                                kbmCus.FieldValues("ADRIDMAIN") = kbmPacketAddress.FieldValues("ADRID")
                                kbmCus.FieldValues("ADRIDDEST") = kbmPacketAddress.FieldValues("ADRID")
                            End If

                            kbmPacketAddress.Next()
                        Loop

                        If trd.PaymentMethodSourceID IsNot Nothing Then
                            SenPaywayID = ConnSession.GetDestinationPaywayID(trd.PaymentMethodSourceID)
                            If SenPaywayID > 0 Then
                                kbmCus.FieldValues("PMTID") = SenPaywayID
                            End If
                        End If

                        SenEmplID = ConnSession.GetDestinationSalePersonID(trd.SalesmanSourceID)
                        If SenEmplID > 0 Then
                            kbmCus.FieldValues("SLMID") = SenEmplID
                        End If

                        If kbmCus.DetailCount > 0 Then

                            Dim CatImported As New List(Of Integer)

                            For Each item In trd.Categories
                                If item.DestinationID IsNot Nothing Then

                                    Dim CategoryID = ConnSession.GetSenContactCategoryID(item.SourceID)
                                    If CategoryID <> "0" Then
                                        CatImported.Add(CategoryID)

                                        Dim kbmKind As New kbm
                                        Dim SenQuery As String
                                        Dim sAlias As String = ""

                                        Dim ParentID As String = ConnSession.GetSenContactCategoryParentID(item.SourceID)
                                        ParentID = If(ParentID = "0", CategoryID, ParentID)

                                        If Not IsNothing(oSenAppl) Then
                                            sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)
                                        End If

                                        SenQuery = String.Format("SELECT TRAID FROM CCD WHERE TRAID={0} AND CTGID={1} AND CTGIDROOT={2}", _
                                                                 trd.DestinationCustomerID, CategoryID, ParentID)

                                        kbmKind.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, SenQuery, 0})

                                        If IsDBNull(kbmKind.FieldValues("TRAID")) Then

                                            kbmCategory = kbmCus.Detail("CustomerCategoryDetailSrv")

                                            kbmCategory.Insert()
                                            kbmCategory.FieldValues("CTGIDROOT") = ParentID
                                            kbmCategory.FieldValues("CTGID") = CategoryID
                                            kbmCategory.FieldValues("TRAID") = trd.DestinationCustomerID
                                            kbmCategory.Post()
                                        End If
                                        kbmKind.EmptyAll()
                                    End If
                                End If
                            Next



                            kbmCategory = kbmCus.Detail("CustomerCategoryDetailSrv")
                            kbmCategory.First()
                            Dim CatDeleted As New List(Of String)

                            Do Until kbmCategory.Eof
                                If Not CatImported.Contains(kbmCategory.FieldValues("CTGID")) Then
                                    CatDeleted.Add(kbmCategory.FieldValues("CTGID"))
                                End If
                                kbmCategory.Next()
                            Loop
                            For Each cat In CatDeleted
                                kbmCategory.Locate("CTGID", cat)
                                kbmCategory.Delete()
                                kbmCategory.Post()
                            Next

                        End If

                        kbmCus.Post()

                        kbmCus.Post()
                        CusPacket = sacCus.Call("ISenDataBroker_ApplyUpdates", New Object() {kbmCus.DataPacket, Nothing, True})
                        kbmCusPacket.DataPacket = CusPacket(2)

                        'trd address
                    End If
                    trd.LastUpdate(TypicalUpdate)
                End If
            End If

            If trd.ParentSourceID IsNot Nothing And trd.ParentDestinationID <> 0 Then
                Dim kbmCheck As New kbm
                Dim sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)

                kbmCheck.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, "select udt_cus_zcusid as ID, SYSTASH from UDT_CUS_ZCUS WHERE TRAID=" & trd.DestinationCustomerID, 0})
                If kbmCheck.Eof Then
                    SacSession.Call("ExecSql", New Object() _
                            {oSenAppl.Call("ISenApplication_GetAliasName", _
                            New Object() {})(0), _
                            String.Format("INSERT INTO UDT_CUS_ZCUS (udt_cus_zcusid, SYSTASH, TRAID) VALUES({0},{1},{2})", trd.DestinationCustomerID, trd.ParentDestinationID, trd.DestinationCustomerID) _
                            , 0})
                Else
                    SacSession.Call("ExecSql", New Object() _
                            {oSenAppl.Call("ISenApplication_GetAliasName", _
                            New Object() {})(0), _
                            String.Format("UPDATE UDT_CUS_ZCUS SET SYSTASH={0} WHERE udt_cus_zcusid={1} AND TRAID={2}", trd.ParentDestinationID, kbmCheck("ID").Value, trd.DestinationCustomerID) _
                            , 0})
                End If
                'ElseIf trd.ParentSourceID IsNot Nothing Then
                '    If trd.ParentDestinationID <> 0 Then
                '        SvSys.ResultsLog(trd.DestinationCustomerID, My.Application.Info.DirectoryPath)
                '    Else
                '        SvSys.ResultsLog(trd.SourceTraderID, My.Application.Info.DirectoryPath)
                '    End If
            End If


            If UpdateAddress Then
                If kbmPacketAddress.RecordCount > 0 Then kbmPacketAddress.First()
                Do Until kbmPacketAddress.Eof
                    MainAddressID = 0
                    If Not IsDBNull(kbmLeePacket.FieldValues("ADRIDMAIN")) Then
                        MainAddressID = kbmLeePacket.FieldValues("ADRIDMAIN")
                    End If
                    'If kbmPacketAddress.FieldValues("ADRID") = MainAddressID Then
                    Dim CurrentAddress As SvTraderAddress
                    If kbmPacketAddress.FieldValues("ADRCODE").ToString = "1000" Then
                        CurrentAddress = TraderAddresses.Where(Function(f) f.AddressType = "Main").FirstOrDefault
                    Else
                        CurrentAddress = TraderAddresses.Where(Function(f) f.SourceID.Equals(kbmPacketAddress.FieldValues("ADRCODE").ToString)).FirstOrDefault
                    End If

                    If CurrentAddress IsNot Nothing Then
                        CurrentAddress.UpdateDestination(kbmPacketAddress.FieldValues("ADRID").ToString)
                    End If
                    'Dim connFsAddress As SqlConnection = CopyConnection()

                    'Dim cmdAddress = New SqlCommand(String.Format( _
                    '"UPDATE Contacts SET SenAdrid={0} WHERE ID={1}", kbmPacketAddress.FieldValues("ADRID"), dr("ID")), connFsAddress)
                    'cmdAddress.CommandType = CommandType.Text
                    'cmdAddress.ExecuteNonQuery()
                    'Else
                    'Dim connFsAddress As SqlConnection = CopyConnection()
                    'Dim cmdAddress = New SqlCommand(String.Format( _
                    '        "UPDATE ContactAddresses SET SENID={0} WHERE ID={1}", _
                    '        kbmPacketAddress.FieldValues("ADRID"), kbmPacketAddress.FieldValues("ADRCODE")), connFsLocal)
                    'cmdAddress.CommandType = CommandType.Text
                    'cmdAddress.ExecuteNonQuery()
                    'End If
                    kbmPacketAddress.Next()
                Loop
            End If

            kbmCus.EmptyAll()
            kbmCusPacket.EmptyAll()
            kbmLee.EmptyAll()
            kbmLeePacket.EmptyAll()

        Catch ex As Exception

            If NewLeeID > 0 Or NewCusID > 0 Then
                DeleteRecords(NewLeeID, NewCusID)
            End If
            SvSys.ErrorLog(("ImportSingleTrader " & trd.SourceCustomerID).PadRight(10) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try

        trd.UpdateTraderForOrder(TypicalUpdate)

        'If TypicalUpdate Then
        '    Return trd.DestinationTraderID
        'Else
        '    Return trd.SourceTraderID
        'End If
        Return trd
    End Function

    Private Sub DeleteRecords(ByVal LeeID As Integer, ByVal CusID As Integer)
        Dim kbmCusDel As New kbm
        Dim kbmLeeDel As New kbm

        Dim sacCusDel As SacObjectX
        Dim sacLeeDel As SacObjectX
        Try
            Dim awherestatement As String = ""

            sacCusDel = SacSession.CreateObject("TCustomerSrv")
            sacLeeDel = SacSession.CreateObject("TLegalEntitySrv")

            If CusID > 0 Then
                awherestatement = "TRAID=" & CusID
                kbmCusDel.DataPacket = sacCusDel.Call("ISEnDataBroker_GetData", New Object() {awherestatement, "", 0})(0)
                If Not kbmCusDel.Eof Then
                    kbmCusDel.Delete()
                    'ares2Del = 
                    sacCusDel.Call("ISenDataBroker_ApplyUpdates", New Object() {kbmCusDel.DataPacket, Nothing, True})
                End If
            End If

            If LeeID > 0 Then
                awherestatement = "LEEID=" & LeeID
                kbmLeeDel.DataPacket = sacLeeDel.Call("ISEnDataBroker_GetData", New Object() {awherestatement, "", 0})(0)
                If Not kbmLeeDel.Eof Then
                    kbmLeeDel.Delete()
                    'ares1Del = 
                    sacLeeDel.Call("ISenDataBroker_ApplyUpdates", New Object() {kbmLeeDel.DataPacket, Nothing, True})
                End If

            End If

            kbmCusDel.EmptyAll()
            kbmLeeDel.EmptyAll()
            sacCusDel = Nothing
            sacLeeDel = Nothing
        Catch ex As Exception
            kbmCusDel.EmptyAll()
            kbmLeeDel.EmptyAll()
            sacCusDel = Nothing
            sacLeeDel = Nothing

            'SvSys.ErrorLog("DeleteRecords".PadRight(10) & vbTab & ex.Message.PadRight(15), AppPath)

        End Try

    End Sub

    Private Function GetValidTraCode(ByVal TraCode As String, ByVal Counter As Integer) As String
        Dim oSenAppl As Object
        Dim kbmKind As New kbm
        Dim SenQuery As String
        Dim sAlias As String = ""

        Dim FinalCode As String = TraCode

        oSenAppl = SacSession.CreateObject("TSenApplication")
        If Not IsNothing(oSenAppl) Then
            sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)
        End If

        If TraCode <> "" Then
            SenQuery = " SELECT TRACODE AS ABBR FROM CUS" & vbCrLf & "" & _
                    " WHERE TRACODE='" & TraCode & "'"
            kbmKind.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, SenQuery, 0})
        End If

        If TraCode = "" OrElse Not IsDBNull(kbmKind.FieldValues("Abbr")) Then

            If TraCode <> "" Then kbmKind.EmptyAll()

            SenQuery = " SELECT MAX(TRACODE) AS ABBR FROM CUS" & vbCrLf & "" & _
                    " WHERE TRACODE>='000000' AND TRACODE<='999999'" & vbCrLf & "" & _
                    "AND TRACODE LIKE '______' AND INSTR(TRACODE,'.')=0 AND INSTR(TRACODE,',')=0"
            kbmKind.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, SenQuery, 0})

            If IsDBNull(kbmKind.FieldValues("Abbr")) Then
                FinalCode = "000001"
            Else
                FinalCode = Format(Val(kbmKind.FieldValues("Abbr")) + CStr(Counter + 1), "000000")
            End If
        End If
        Return FinalCode
    End Function

    Public Function ImportOrders(ByVal ParamsList As List(Of String), _
                                 ByVal TraderParamsList As List(Of String), _
                                 Optional ByVal DefaultTraderID As String = "", _
                                 Optional ByVal AllowInsert As Boolean = True, _
                                 Optional ByVal AllowUpdate As Boolean = True, _
                                 Optional ByVal SameDirection As Boolean = True) As List(Of String)
        Dim ErrorsList As New List(Of String)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)

        Try
            Dim Trader As SvTrader

            Dim OrderList = ConnSession.LoadOrders(True, SvConnData.OrderTypes.Sales)

            'OrderList = OrderList.Where(Function(f) f.SourceID = 21256).ToList

            For Each ord In OrderList
                Try
                    If DefaultTraderID = "" Then
                        ImportTraders(TraderParamsList, SameDirection, AllowInsert, AllowUpdate, ord.OrderTraderID, Trader)
                        If SameDirection Then
                            Trader.UpdateTraderForOrder(SameDirection)
                        End If
                    Else
                        Dim TraderList = ConnSession.LoadTraders(True, True, False, False)
                        Trader = TraderList.Where(Function(f) f.OrderTraderDestinationID.Equals(DefaultTraderID)).FirstOrDefault
                    End If


                    Dim sacCus As SacObjectX
                    Dim sacSales As SacObjectX

                    Dim kbmCus As kbm = New kbm
                    Dim kbmSales As kbm = New kbm
                    Dim kbmPCL As kbm = New kbm
                    Dim kbmSales1 As kbm = New kbm
                    Dim kbmPart As kbm = New kbm
                    Dim kbmDetails As kbm = New kbm
                    Dim kbmSurcharges As New kbm
                    Dim kbmSN As kbm = New kbm

                    Dim ares As System.Array

                    Dim apclid As Integer
                    Dim docid As Integer
                    Dim lineno As Integer
                    Dim sAlias As String = ""

                    Dim AllowCommit As Boolean
                    Dim counter As Integer = 0

                    docid = -1001

                    Dim SenEmplID As String = "0"
                    Dim SenPaywayID As Integer

                    Dim oSenAppl
                    oSenAppl = SacSession.CreateObject("TSenApplication")
                    If Not IsNothing(oSenAppl) Then
                        sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)
                    End If
                    kbmPCL.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, "SELECT MAX(PCLID) AS PCLID FROM SPL", 0})
                    If Not IsDBNull(kbmPCL.FieldValues("PCLID")) Then
                        apclid = kbmPCL.FieldValues("PCLID") + 1
                    Else
                        apclid = 1
                    End If

                    sacCus = SacSession.CreateObject("TCustomerSrv")
                    kbmCus.DataPacket = sacCus.Call("ISEnDataBroker_GetData", New Object() {"", "", 0})(0)
                    sacSales = SacSession.CreateObject("TSalesDocumentSrv")
                    kbmSales.DataPacket = sacSales.Call("ISenDataBroker_GetMetaData", Nothing)(0)

                    AllowCommit = False

                    lineno = 1
                    kbmSales.Insert()
                    kbmSales.FieldValues("TRAID") = Trader.OrderTraderDestinationID

                    kbmSales.FieldValues("DOTID") = ord.OrderStatus
                    kbmSales.FieldValues("DOSID") = ord.DocTypeID '"973" 
                    kbmSales.FieldValues("DOSCODE") = ord.DocTypeName '"7102" 

                    kbmSales.FieldValues("DOCID") = docid


                    kbmSales.FieldValues("DOCKATAHORISISDATE") = ord.OrderDate


                    If ord.Fiscal Is Nothing Then
                        kbmSales.FieldValues("FIYID") = ord.OrderDate.Year.ToString & "01001"
                    Else
                        kbmSales.FieldValues("FIYID") = ord.Fiscal
                    End If

                    ''  pk
                    ''  Official invoice
                    'kbmSales.FieldValues("DOCEKDOSISDATE") = Now.Date


                    Select Case ord.OrderPaymentID
                        Case "1"
                            SenPaywayID = 314
                        Case "2"
                            SenPaywayID = 315
                        Case "3"
                            SenPaywayID = 316
                        Case "5"
                            SenPaywayID = 317
                        Case Else
                            Throw New Exception(ord.OrderPaymentID & " Άγνωστος Τροπος Πληρωμης")
                    End Select
                    kbmSales.FieldValues("PMTIDPAYMENT") = SenPaywayID


                    If ParamsList(OrderIndeces.DefaultSalepersonID) IsNot Nothing AndAlso ParamsList(OrderIndeces.DefaultSalepersonID) <> "0" Then
                        SenEmplID = ParamsList(OrderIndeces.DefaultSalepersonID)
                    Else
                        SenEmplID = ConnSession.GetDestinationSalePersonID(ord.SalesmanSourceID)
                    End If

                    If SenEmplID <> "0" Then
                        kbmSales.FieldValues("SLMID") = SenEmplID
                    End If

                    If ord.OrderConsignment IsNot Nothing Then
                        kbmSales.FieldValues("SHVID") = ord.OrderConsignment.OrderConsignmentDestinationID
                    End If

                    kbmSales.FieldValues("DOCCOMMENT") = ord.Notes.ToString.Substring(0, Math.Min(255, ord.Notes.ToString.Length))

                    If ord.BraDestinationID Is Nothing Then
                        kbmSales.FieldValues("BRAID") = -1
                    Else
                        kbmSales.FieldValues("BRAID") = ord.BraDestinationID
                    End If

                    kbmSales.FieldValues("TDOTRADERDISCPCNT") = If(ord.OrderDiscountPerc Is Nothing, 0, ord.OrderDiscountPerc)

                    If Convert.ToInt32(ord.BraDestinationID) > 0 Then
                        kbmSales.FieldValues("WRHID") = ord.WarehouseDestinationID
                    End If

                    Dim CurrentAddress = ord.DeliveryTraderAddress(SameDirection)

                    If CurrentAddress Is Nothing Then Throw New Exception("Δε βρέθηκε η διεύθυνση " & ord.OrderTraderAddressID)

                    If CurrentAddress.DestinationID IsNot Nothing Then

                        If Convert.ToInt32(ord.BraDestinationID) > 0 Then
                            kbmSales.FieldValues("ADRIDPROORISMOU") = CurrentAddress.DestinationID
                            kbmSales.FieldValues("ADRIDBRANCH") = CurrentAddress.DestinationID
                        Else
                            kbmSales.FieldValues("ADRIDPROORISMOU") = CurrentAddress.DestinationID
                        End If
                    End If

                    kbmSales.Post()

                    kbmDetails = kbmSales.Detail("SalesStockItemDocDetailSrv")

                    For Each item In ord.Details
                        If item.DetailProduct Is Nothing Then Throw New Exception(item.DetailProductSourceID & " Άγνωστο προϊόν")
                        Dim CurrentProduct = item.DetailProduct

                        If CurrentProduct.OrderDetailProductDestinationID > 0 Then
                            kbmDetails.Insert()
                            kbmDetails.FieldValues("LINENO") = lineno
                            kbmDetails.FieldValues("MCIID") = CurrentProduct.OrderDetailProductDestinationID
                            kbmDetails.FieldValues("STDQTYA") = item.Quantity
                            kbmDetails.FieldValues("SLMID") = SenEmplID

                            kbmDetails.FieldValues("SSDDISCPOLICYPCNT") = 0 'ζώνης
                            kbmDetails.FieldValues("SSDITEMDISCPCNT") = item.ItemDiscountPerc ' 0 'drDetails("DiscPerc")

                            kbmDetails.FieldValues("SSDUNITPRICEBC") = item.UnitCataloguePriceWithVat 'UnitFinalPriceNoVat

                            If item.Color IsNot Nothing Then
                                kbmDetails.FieldValues("DFVCODE1") = item.Color.ToUpper
                            End If

                            If item.Size IsNot Nothing Then
                                kbmDetails.FieldValues("DFVCODE2") = item.Size.ToUpper
                            End If

                            kbmPart.EmptyAll()

                            kbmDetails.Post()

                            lineno = lineno + 1

                        End If

                    Next

                    If ord.ShippingCostNoVat IsNot Nothing AndAlso ord.ShippingCostNoVat > 0 Then
                        kbmSurcharges = kbmSales.Detail("SalesSurChargeDocDetailSrv")

                        kbmSurcharges.Insert()
                        kbmSurcharges.FieldValues("LINENO") = 1
                        kbmSurcharges.FieldValues("SCHID") = ParamsList(OrderIndeces.DefaultConsignment)
                        kbmSurcharges.FieldValues("VATID") = ParamsList(OrderIndeces.DefaultConsignmentVat)
                        kbmSurcharges.FieldValues("SDONETVALUE") = ord.ShippingCostNoVat
                        kbmSurcharges.FieldValues("SDONETVALUEBC") = ord.ShippingCostNoVat

                        kbmSurcharges.FieldValues("SDOUSERPRICE") = 1
                        kbmSurcharges.FieldValues("SDOAFFECTSPAYMENT") = 1

                        kbmSurcharges.Post()
                    End If

                    AllowCommit = True

                    If AllowCommit Then
                        ares = sacSales.Call("ISenDataBroker_ApplyUpdates", New Object() {kbmSales.DataPacket, Nothing, True})
                        kbmSales1.DataPacket = ares(2)

                        ord.LastUpdate(kbmSales1.FieldValues("DOCID"))

                        docid = docid - 1

                        counter += 1
                    End If

                    kbmSales.EmptyAll()
                    kbmSales1.EmptyAll()
                    kbmPCL.EmptyAll()

                Catch ex As Exception
                    ErrorsList.Add(ex.Message)
                    SvSys.ErrorLog(("SEN ImportOrders").PadRight(20) & vbTab & ord.SourceID & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
                End Try
            Next
        Catch ex As Exception
            ErrorsList.Add(ex.Message)
            SvSys.ErrorLog(("SEN ImportOrders").PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return ErrorsList
    End Function

    Public Function ImportPurchases(ByVal ParamsList As List(Of String), _
                                 ByVal TraderParamsList As List(Of String), _
                          Optional ByVal DefaultTraderID As String = "", _
                          Optional ByVal AllowInsert As Boolean = True, _
                          Optional ByVal AllowUpdate As Boolean = True, _
                          Optional ByVal SameDirection As Boolean = True, _
                                          Optional ByVal CodeMask As String = "", _
                                          Optional ByVal CounterMask As String = "") As List(Of String)
        Dim ErrorsList As New List(Of String)
        Try
            Dim Trader As SvTrader

            Dim OrderList = ConnSession.LoadOrders(True, SvConnData.OrderTypes.Purchases)

            'OrderList = OrderList.Where(Function(f) f.SourceID = 8544).ToList

            For Each ord In OrderList
                Try
                    If DefaultTraderID = "" Then

                        ImportTraders(TraderParamsList, SameDirection, AllowInsert, AllowUpdate, ord.OrderTraderID, Trader)
                        Trader.UpdateTraderForOrder(SameDirection)
                    Else
                        Dim TraderList = ConnSession.LoadTraders(True, True, False, False)
                        Trader = TraderList.Where(Function(f) f.OrderTraderDestinationID.Equals(DefaultTraderID)).FirstOrDefault
                    End If


                    Dim sacCus As SacObjectX
                    Dim sacPurchases As SacObjectX

                    Dim kbmCus As kbm = New kbm
                    Dim kbmPurchases As kbm = New kbm
                    Dim kbmPurchases1 As kbm = New kbm
                    Dim kbmDetails As kbm = New kbm

                    Dim ares As System.Array

                    Dim docid As Integer
                    Dim lineno As Integer
                    Dim sAlias As String = ""

                    'Dim AllowCommit As Boolean
                    Dim counter As Integer = 0

                    docid = -1001

                    Dim oSenAppl
                    oSenAppl = SacSession.CreateObject("TSenApplication")
                    If Not IsNothing(oSenAppl) Then
                        sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)
                    End If

                    sacCus = SacSession.CreateObject("TCustomerSrv")
                    kbmCus.DataPacket = sacCus.Call("ISEnDataBroker_GetData", New Object() {"", "", 0})(0)
                    sacPurchases = SacSession.CreateObject("TPurchaseDocumentSrv")
                    kbmPurchases.DataPacket = sacPurchases.Call("ISEnDataBroker_GetMetaData", Nothing)(0)

                    'AllowCommit = False

                    lineno = 1
                    kbmPurchases.Insert()
                    kbmPurchases.FieldValues("TRAID") = Trader.OrderTraderDestinationID '4814

                    kbmPurchases.FieldValues("DOTID") = ord.OrderStatus '497 
                    kbmPurchases.FieldValues("DOSID") = ord.DocTypeID '1752
                    kbmPurchases.FieldValues("DOSCODE") = ord.DocTypeName '"ΛΣΑ" 

                    kbmPurchases.FieldValues("DOCID") = docid

                    kbmPurchases.FieldValues("DOCKATAHORISISDATE") = ord.OrderDate '"10/01/2015" 
                    kbmPurchases.FieldValues("DOCEKDOSISDATE") = ord.ExecutionDate
                    kbmPurchases.FieldValues("FIYID") = ord.Fiscal
                    kbmPurchases.FieldValues("PMTIDPAYMENT") = ord.OrderPayment.OrderPaymentDestinationID '27

                    kbmPurchases.FieldValues("TDOOTHERNUMBER") = ord.OrderCode
                    kbmPurchases.FieldValues("TDOOTHERDATE") = ord.OtherDate

                    kbmPurchases.FieldValues("BRAID") = ord.BraDestinationID

                    'If Convert.ToInt32(ord.BraDestinationID) > 0 Then
                    kbmPurchases.FieldValues("WRHID") = ord.OrderWarehouse.SourceID '197
                    'End If


                    ''  IERARXIES
                    If ord.OrderConsignmentID IsNot Nothing Then
                        kbmPurchases.FieldValues("DIMIDA") = ord.Currency
                        kbmPurchases.FieldValues("DIMIDC") = ord.SalesmanSourceID
                        kbmPurchases.FieldValues("DIMIDD") = ord.OrderConsignmentID
                    End If

                    'kbmPurchases.FieldValues("STDNETVALUEBC") = ord.OrderTotalNoVat
                    'kbmPurchases.FieldValues("PSDVATVALUEBC") = ord.OrderVat
                    'kbmPurchases.FieldValues("PSDGROSSVALUEBC") = ord.OrderTotalWithVat


                    kbmPurchases.Post()

                    kbmDetails = kbmPurchases.Detail("PurchaseStockItemDocDetailSrv")

                    For Each item In ord.Details
                        If item.DetailProduct Is Nothing Then Throw New Exception(item.DetailProductSourceID & " Άγνωστο προϊόν")
                        Dim CurrentProduct = item.DetailProduct

                        If CurrentProduct.OrderDetailProductDestinationID > 0 Then
                            kbmDetails.Insert()
                            kbmDetails.FieldValues("LINENO") = lineno
                            kbmDetails.FieldValues("MCIID") = CurrentProduct.OrderDetailProductDestinationID
                            kbmDetails.FieldValues("STDQTYA") = item.Quantity

                            kbmDetails.FieldValues("PSDUNITPRICEBC") = item.UnitCataloguePriceWithVat 'UnitFinalPriceNoVat


                            'kbmDetails.FieldValues("STDNETVALUEBC") = ord.OrderTotalNoVat

                            'kbmDetails.FieldValues("PSDVATVALUEBC") = (ord.OrderTotalWithVat - ord.OrderTotalNoVat)
                            'kbmDetails.FieldValues("PSDGROSSVALUEBC") = ord.OrderTotalWithVat

                            kbmDetails.Post()

                            lineno = lineno + 1

                        End If

                    Next

                    'AllowCommit = True

                    'If AllowCommit Then
                    ares = sacPurchases.Call("ISenDataBroker_ApplyUpdates", New Object() {kbmPurchases.DataPacket, Nothing, True})
                    kbmPurchases1.DataPacket = ares(2)

                    ord.LastUpdate(kbmPurchases1.FieldValues("DOCID"))

                    docid = docid - 1

                    counter += 1
                    'End If

                    kbmPurchases.EmptyAll()

                Catch ex As Exception
                    ErrorsList.Add(ex.Message)
                    SvSys.ErrorLog(("SEN ImportPurchases").PadRight(20) & vbTab & ord.SourceID & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
                End Try
            Next
        Catch ex As Exception
            ErrorsList.Add(ex.Message)
            SvSys.ErrorLog(("SEN ImportOrders").PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
        Return ErrorsList
    End Function

    Function GetCurrencyID(ByVal CurrencyName As String) As Integer
        Dim Condition As String
        Dim sacCurrency As SacObjectX
        Dim kbmCurrency As New kbm

        sacCurrency = SacSession.CreateObject("TCurrencySrv", True)
        Condition = "CURNAME='" & CurrencyName & "'"

        kbmCurrency.DataPacket = sacCurrency.Call("ISenServerPDC_LoadRecords", New Object() {Condition, "", "CURID"})(0)
        If Not kbmCurrency.Eof Then
            Return kbmCurrency.Field("CURID").Value
        Else
            Return 0
        End If
    End Function

    Public Sub ExportConsignmentMethods()
        Dim sacConsignment As SacObjectX

        Dim kbmConsignment As New kbm

        Dim oSenAppl = SacSession.CreateObject("TSenApplication")
        Dim CompanyID = oSenAppl.Call("ISenApplication_GetVariable", New Object() {"CMPID"})(0)
        Dim sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)

        'Dim aUniId As Integer
        sacConsignment = SacSession.CreateObject("TShipViaSrv")
        kbmConsignment.DataPacket = sacConsignment.Call("ISEnDataBroker_GetData", New Object() {"", "", 0})(0)

        Dim ConsignmentList = ConnSession.LoadConsignments(False)

        Do Until kbmConsignment.Eof
            Dim ConsignmentID As String = kbmConsignment.FieldValues("SHVID").ToString

            Dim ConsignmentExists = (From t In ConsignmentList _
                                     Where t.SourceID.ToString.Equals(ConsignmentID) _
                                     Select t).FirstOrDefault

            If ConsignmentExists Is Nothing Then
                Dim NewConsignment As New svConsignmentMethod
                NewConsignment.ID = Guid.NewGuid
                NewConsignment.SourceID = kbmConsignment.FieldValues("SHVID").ToString
                NewConsignment.Name = kbmConsignment.FieldValues("SHVDESCR")

                NewConsignment.ImportConsignment(True)

                ConsignmentExists = NewConsignment
            Else
                ConsignmentExists.LastModificationSource = Now
                ConsignmentExists.IsDirty = True

                ConsignmentExists.Name = kbmConsignment.FieldValues("SHVDESCR").ToString

                ConsignmentExists.ImportConsignment(False)
            End If

            kbmConsignment.Next()

        Loop
        kbmConsignment.EmptyAll()
    End Sub

    Public Sub ExportPayments(ByVal IncludePaymentDays As Boolean)

        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)

        Dim sacPayment As SacObjectX

        Dim kbmPayment As New kbm

        Dim oSenAppl = SacSession.CreateObject("TSenApplication")

        'Dim aUniId As Integer
        sacPayment = SacSession.CreateObject("TCustomerPaymentMethodSrv")
        kbmPayment.DataPacket = sacPayment.Call("ISEnDataBroker_GetData", New Object() {"", "", 0})(0)

        Dim PaymentList = ConnSession.LoadPayments(False)

        Do Until kbmPayment.Eof
            Dim PaymentID As String = kbmPayment.FieldValues("PMTID").ToString

            Dim PaymentExists = (From t In PaymentList _
                                     Where t.SourceID.ToString.Equals(PaymentID) _
                                     Select t).FirstOrDefault

            If PaymentExists Is Nothing Then
                Dim NewPayment As New svPaymentMethod
                NewPayment.ID = Guid.NewGuid
                NewPayment.SourceID = kbmPayment.FieldValues("PMTID").ToString
                NewPayment.Name = kbmPayment.FieldValues("PMTTITLE")
                NewPayment.PaymentCode = kbmPayment.FieldValues("PMTCODE")

                NewPayment.ImportPayment(True)

                PaymentExists = NewPayment
            Else
                PaymentExists.LastModificationSource = Now
                PaymentExists.IsDirty = True

                PaymentExists.Name = kbmPayment.FieldValues("PMTTITLE")
                PaymentExists.PaymentCode = kbmPayment.FieldValues("PMTCODE")

                PaymentExists.ImportPayment(False)
            End If

            If IncludePaymentDays AndAlso Not IsDBNull(kbmPayment.FieldValues("TRMID")) Then
                Dim kbmPaymentDays As New kbm
                Dim sacPaymentDays As SacObjectX = SacSession.CreateObject("TCustomerTermSrv")
                kbmPaymentDays.DataPacket = sacPaymentDays.Call("ISEnDataBroker_GetData", New Object() {"TRMID=" & kbmPayment.FieldValues("TRMID"), "", 0})(0)

                Do Until kbmPaymentDays.Eof
                    PaymentExists.PaymentDays = If(IsDBNull(kbmPaymentDays.FieldValues("TRMMEANDUEDAYS")), 0, kbmPaymentDays.FieldValues("TRMMEANDUEDAYS"))

                    PaymentExists.ImportPayment(False)


                    kbmPaymentDays.Next()
                Loop
                kbmPaymentDays.EmptyAll()
            Else
                PaymentExists.IsDirty = False
            End If

            kbmPayment.Next()

        Loop
        kbmPayment.EmptyAll()


    End Sub

    Public Sub ExportProductBalance(Optional ByVal WarehouseID As String = "") 'UNUSED
        Dim sacBalance As SacObjectX

        Dim kbmBalance As New kbm

        Dim oSenAppl = SacSession.CreateObject("TSenApplication")
        Dim sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)

        'Dim aUniId As Integer
        sacBalance = SacSession.CreateObject("TWarehouseItemSrv")
        kbmBalance.DataPacket = sacBalance.Call("ISEnDataBroker_GetData", New Object() { _
                                                If(WarehouseID = "", "", "WRHID=" & WarehouseID), "", 0})(0)

        Dim ProductList = ConnSession.LoadProducts(False, False, False, False, False)
        Dim BalanceList = ConnSession.LoadProductsBalance(False, If(WarehouseID = "", "-1", WarehouseID))

        Do Until kbmBalance.Eof
            Dim ProductID As String = kbmBalance.FieldValues("MCIID").ToString

            Dim ProductExists = (From t In ProductList _
                              Where t.SourceID.Equals(ProductID) _
                              Select t).FirstOrDefault
            If ProductExists IsNot Nothing Then

                Dim BalanceExists = (From t In BalanceList _
                                   Where t.ProductSourceID.Equals(ProductID) _
                                   And t.WarehouseSourceID.Equals(kbmBalance.FieldValues("WRHID").ToString) _
                                   Select t).FirstOrDefault

                If BalanceExists Is Nothing Then
                    Dim NewBalance As New SvProductBalance
                    NewBalance.ID = Guid.NewGuid
                    NewBalance.LastModificationSource = Now
                    NewBalance.IsDirty = True
                    NewBalance.ProductSourceID = ProductID
                    NewBalance.Quantity = kbmBalance.FieldValues("WITBALANCEA")
                    NewBalance.WarehouseSourceID = kbmBalance.FieldValues("WRHID")

                    NewBalance.ImportBalance(True)
                Else
                    BalanceExists.LastModificationSource = Now
                    BalanceExists.IsDirty = True
                    BalanceExists.Quantity = kbmBalance.FieldValues("WITBALANCEA")

                    BalanceExists.ImportBalance(False)

                End If
            End If
            kbmBalance.Next()

        Loop
        kbmBalance.EmptyAll()

    End Sub

    Public Sub ExportProductCodeBalance(Optional ByVal WarehouseID As String = "")



        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
        Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)



        Dim sacBalance As SacObjectX

        Dim kbmBalance As New kbm

        Dim oSenAppl = SacSession.CreateObject("TSenApplication")
        Dim sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)

        'Dim aUniId As Integer
        sacBalance = SacSession.CreateObject("TWarehouseItemSrv")

        kbmBalance.DataPacket = sacBalance.Call("ISEnDataBroker_GetData", New Object() { _
                                                 If(WarehouseID = "", "", " WRHID=" & WarehouseID), "", 0})(0)

        '"MCIID=" & "204082" & If(WarehouseID = "", "", " AND WRHID=" & WarehouseID), "", 0})(0)

        Dim ProductCodeList = ConnSession.LoadProductsCodes(False, False)
        Dim BalanceList = ConnSession.LoadProductsCodesBalances(False, WarehouseID)

        Do Until kbmBalance.Eof
            Dim ProductID As String = kbmBalance.FieldValues("MCIID").ToString
            Dim Color As String = kbmBalance.FieldValues("DFVCODE1").ToString
            Dim Size As String = kbmBalance.FieldValues("DFVCODE2").ToString

            'Product(Code)Exists 
            Dim ProductExists = (From t In ProductCodeList _
                              Where t.ProductSourceID.Equals(ProductID) _
                              And t.Color.Equals(Color) _
                              And t.Size.Equals(Size) _
                              Select t).FirstOrDefault
            If ProductExists IsNot Nothing Then
                Dim ProductCodeID = ProductExists.ID

                Dim BalanceExists = (From t In BalanceList _
                                   Where t.ProductCodeSourceID.Equals(ProductCodeID) _
                                   And t.WarehouseSourceID.Equals(kbmBalance.FieldValues("WRHID").ToString) _
                                   Select t).FirstOrDefault

                If BalanceExists Is Nothing Then
                    Dim NewBalance As New svProductCodeBalance
                    NewBalance.ID = Guid.NewGuid
                    NewBalance.LastModificationSource = Now
                    NewBalance.IsDirty = True
                    NewBalance.ProductCodeSourceID = ProductCodeID
                    NewBalance.Quantity = kbmBalance.FieldValues("WITBALANCEA")
                    NewBalance.WarehouseSourceID = kbmBalance.FieldValues("WRHID")

                    NewBalance.ImportBalance(True)
                Else 'I DO NOT UNDERSTAND
                    If Not BalanceExists.Quantity = kbmBalance.FieldValues("WITBALANCEA") Then


                        BalanceExists.LastModificationSource = Now
                        BalanceExists.IsDirty = True
                        BalanceExists.Quantity = kbmBalance.FieldValues("WITBALANCEA")

                        BalanceExists.ImportBalance(False)
                    End If

                End If
            End If
            kbmBalance.Next()

        Loop
        kbmBalance.EmptyAll()

    End Sub

    Public Sub ExportWarehouses()

        Try




            Dim sacWarehouse As SacObjectX

            Dim kbmWarehouse As New kbm

            Dim oSenAppl = SacSession.CreateObject("TSenApplication")
            Dim sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)

            'Dim aUniId As Integer
            sacWarehouse = SacSession.CreateObject("TWareHouseSrv")
            kbmWarehouse.DataPacket = sacWarehouse.Call("ISEnDataBroker_GetData", New Object() {"", "", 0})(0)

            Dim WarehouseList = ConnSession.LoadWarehouses(False)

            Do Until kbmWarehouse.Eof
                Dim WarehouseExists = (From t In WarehouseList _
                                   Where t.SourceID.Equals(kbmWarehouse.FieldValues("WRHID").ToString) _
                                   Select t).FirstOrDefault

                If WarehouseExists Is Nothing Then
                    Dim NewWarehouse As New SvWarehouse
                    NewWarehouse.ID = Guid.NewGuid
                    NewWarehouse.LastModificationSource = Now
                    NewWarehouse.IsDirty = True
                    NewWarehouse.SourceID = kbmWarehouse.FieldValues("WRHID")
                    NewWarehouse.Code = kbmWarehouse.FieldValues("WRHCODE")
                    NewWarehouse.Name = kbmWarehouse.FieldValues("WRHNAME")

                    NewWarehouse.ImportWarehouse(True)
                Else
                    WarehouseExists.LastModificationSource = Now
                    WarehouseExists.IsDirty = True
                    WarehouseExists.Code = kbmWarehouse.FieldValues("WRHCODE")
                    WarehouseExists.Name = kbmWarehouse.FieldValues("WRHNAME")

                    WarehouseExists.ImportWarehouse(False)

                End If
                kbmWarehouse.Next()

            Loop
            kbmWarehouse.EmptyAll()
        Catch ex As Exception
            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog("ExportWarehouses" & " : " & ex.Message, My.Application.Info.DirectoryPath)
        End Try
    End Sub

    Public Sub UpdateTraderDestinationByCode(Optional TypicalUpdate As Boolean = True)
        Dim AllTraders = ConnSession.LoadTraders(True, TypicalUpdate, True, False)

        Try
            For Each trd In AllTraders
                Dim kbmKind As New kbm
                Dim SenQuery As String

                Dim oSenAppl = SacSession.CreateObject("TSenApplication")
                Dim sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)


                SenQuery = String.Format("SELECT TRAID, LEEID FROM CUS WHERE TRACODE='{0}'", _
                                         trd.DestinationCustomerCode)

                kbmKind.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, SenQuery, 0})

                If Not IsDBNull(kbmKind.FieldValues("TRAID")) Then
                    trd.UpdateDestination(kbmKind.FieldValues("LEEID"), kbmKind.FieldValues("TRAID"), trd.DestinationTraderCode, trd.DestinationCustomerCode)
                End If
                kbmKind.EmptyAll()
            Next

        Catch ex As Exception
            SvSys.ErrorLog(("SEN UpdateTraderDestinationByCode").PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try
    End Sub

    Public Sub UpdateOrderProductByCode()
        Dim AllProducts = ConnSession.LoadProducts(True, True, True, False, False)

        Try
            For Each prd In AllProducts
                Dim kbmKind As New kbm
                Dim SenQuery As String

                Dim oSenAppl = SacSession.CreateObject("TSenApplication")
                Dim sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)


                SenQuery = String.Format("SELECT MCIID FROM STI WHERE CODCODE='{0}'", _
                                         prd.ProductCode)

                kbmKind.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, SenQuery, 0})

                If Not IsDBNull(kbmKind.FieldValues("MCIID")) Then
                    prd.UpdateOrderProductByCode(kbmKind.FieldValues("MCIID"))
                End If
                kbmKind.EmptyAll()
            Next

        Catch ex As Exception
            SvSys.ErrorLog(("SEN UpdateOrderProductByCode").PadRight(20) & vbTab & ex.Message.PadRight(15), My.Application.Info.DirectoryPath)
        End Try

    End Sub

    Public Function GetMCIID(ByRef MCIIDs As String, ByVal CodeFilter As String) As Boolean

        Try


            Dim sacStock As SacObjectX
            Dim kbmStock As New kbm
            Dim kbmStockItemCode As New kbm
            'Dim oSenAppl = SacSession.CreateObject("TSenApplication")            

            sacStock = SacSession.CreateObject("TStockItemSrv")
            kbmStock.DataPacket = sacStock.Call("ISEnDataBroker_GetData", New Object() {"INSTR(CODCODE, '" & CodeFilter & "')=1 ", "", 0})(0)
            Do Until kbmStock.Eof
                kbmStockItemCode = kbmStock.Detail("StockItemCodeSrv")
                If Not kbmStockItemCode.Eof Then
                    With kbmStockItemCode
                        If Not String.IsNullOrEmpty(.FieldValues("COKID").ToString) AndAlso (Convert.ToInt32(.FieldValues("COKID").ToString) = 21) Then
                            MCIIDs &= .FieldValues("MCIID").ToString() & ","
                        End If
                    End With
                End If
                kbmStock.Next()
            Loop
            MCIIDs = MCIIDs.Remove(MCIIDs.LastIndexOf(MCIIDs.Last), 1)
            GetMCIID = MCIIDs.Count > 0
            If GetMCIID Then
                kbmStockItemCode.EmptyAll()
                kbmStock.EmptyAll()
            End If
            If (Not kbmStockItemCode Is Nothing) Then
                kbmStockItemCode = Nothing
            End If
            If (Not kbmStock Is Nothing) Then
                kbmStock = Nothing
            End If
        Catch ex As Exception
            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog("GetMCIID" & " : " & ex.Message, My.Application.Info.DirectoryPath)
        End Try
    End Function
    Private disposedvalue As Boolean = False        ' To detect redundant calls

    ' IDisposable
    Protected Overridable Sub Dispose(ByVal disposing As Boolean)
        If Not Me.disposedvalue Then
            If disposing Then
                ' TODO: free other state (managed objects).
            End If

            ' TODO: free your own state (unmanaged objects).
            ' TODO: set large fields to null.
        End If
        Me.disposedvalue = True
    End Sub

#Region " IDisposable Support "
    ' This code added by Visual Basic to correctly implement the disposable pattern.
    Public Sub Dispose() Implements IDisposable.Dispose
        ' Do not change this code.  Put cleanup code in Dispose(ByVal disposing As Boolean) above.
        Dispose(True)
        GC.SuppressFinalize(Me)
    End Sub
#End Region

#Region "Industrial REVOIL"



#End Region

    Private Function GetMaskedCustomerCode(ByVal CodeMask As String, ByVal CounterMask As String) As String
        Dim oSenAppl As Object
        Dim kbmKind As New kbm
        Dim SenQuery As String
        Dim sAlias As String = ""

        Dim FinalCode As String

        oSenAppl = SacSession.CreateObject("TSenApplication")
        If Not IsNothing(oSenAppl) Then
            sAlias = oSenAppl.Call("ISenApplication_GetAliasName", Nothing)(0)
        End If

        SenQuery = String.Format( _
                "SELECT max(customers.cnt) as max_cnt FROM " & _
                "(select TO_NUMBER(replace(TRACODE, '{0}', '')) AS cnt, TRACODE from cus where INSTR(TRACODE, '{0}')=1 " & _
                "UNION " & _
                "select 0 as cnt, TRACODE from cus where INSTR(TRACODE, '{0}')<>1 AND ROWNUM=1) customers", CodeMask)
        kbmKind.DataPacket = SacSession.Call("OpenSql", New Object() {sAlias, SenQuery, 0})

        If Not IsDBNull(kbmKind.FieldValues("max_cnt")) Then
            Dim max_cnt = CInt(kbmKind.FieldValues("max_cnt"))

            FinalCode = CodeMask & Format(max_cnt + 1, CounterMask)
        Else
            FinalCode = CodeMask & CounterMask.Substring(0, CounterMask.Length - 1) & "1"

        End If
        Return FinalCode
    End Function



End Class

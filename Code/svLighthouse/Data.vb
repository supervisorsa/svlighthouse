﻿Imports System.Web.Script.Serialization
Imports System.Net
Imports System.Runtime.Serialization.Json
Imports svGlobal
Imports System.IO
Imports System.Threading
Imports System.Globalization
Public Class Data
    Inherits SvConnections.ConnectorData

    Public Overrides Sub RunData(ByVal path As String, ByVal description As String, ByVal id1 As Integer, ByVal id2 As Integer)
        Try





            MyBase.RunData(path, description, id1, id2)


            Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
            Thread.CurrentThread.CurrentCulture = New CultureInfo("el-GR", True)
            Dim ci As CultureInfo = Thread.CurrentThread.CurrentCulture


            Dim MCIIDs As String = ""
            Dim ErrorMessage As String = ""
            Dim ParamsList As New List(Of String)
            Dim ErrorsList As New List(Of String)

            Dim ConnSession As New SvSessionsConn.SvConnData
            Dim SenSession As New SvSessionsSen.SvSen

            ConnSession.Connect(SvConnSession.ConnectionString)

            ConnSession.PopulatePaywaysList()
            ConnSession.PopulateSalepersonsList()
            ConnSession.PopulateContactCategoriesList()

            ParamsList = New List(Of String)
            ParamsList.Insert(SenSession.GlobalIndeces.AcaCode, "30.000")
            ParamsList.Insert(SenSession.GlobalIndeces.Currency, "EURO")
            Dim SenList = SvConnections.SenConn.GetDataByID(ConnectionId2).Item(0)
            ErrorMessage &= SenSession.Connect(ConnSession, _
                                   ParamsList, _
                                   SenList.Host, _
                                   SenList.Port, _
                                   SenList.SasAlias, _
                                   SenList.SenUser, _
                                   SenList.SenPwd, _
                                   SenList.CmpCode, _
                                   SenList.WrhID)

            If ErrorMessage <> "" Then Throw New Exception(ErrorMessage)

            

            Dim LhSession As New SvSessionsLH.SvLH
            LhSession.Connect(SqlSession1.ConnectionString, ConnSession)

            If TaskDescription.ToLower.Contains("product") Then

                Dim CodeFilter As String = "05.1529"


                SenSession.ExportVat()

                SenSession.ExportProductsCategories()
                SenSession.ExportProducts(True, MCIIDs, CodeFilter)
                SenSession.ExportProductCodes(MCIIDs, CodeFilter)
                SenSession.ExportProductCodeBalance("-1")

                
            ElseIf TaskDescription.ToLower.Contains("test") Then
                LhSession.ImportProductsCategories(False)
                LhSession.ImportProducts(New List(Of Integer))
                LhSession.ImportStocks("-1", SqlSession1.ConnectionString)
                LhSession.ImportProductsResources(SqlSession1.ConnectionString)

            ElseIf TaskDescription.Contains("orders") Then
                SenSession.ExportPayments(True)
                SenSession.ExportVat()
                LhSession.ExportOrders(True, , , , False)

                ConnSession.UpdateOrderDocument("3448", "10935", "7999")   '"DOTID, DOSID, DOSCODE"
                'ConnSession.UpdateOrderDocument("289", "4", "7050") '"OrderStatus, DocTypeID, DocTypeName"
                ConnSession.UpdateProductForOrderDetail(False)

                ParamsList = New List(Of String)
                ParamsList.Insert(SvSessionsSen.SvSen.OrderIndeces.IsOfficial, "0")
                ParamsList.Insert(SvSessionsSen.SvSen.OrderIndeces.DefaultSalepersonID, "3744")
                ParamsList.Insert(SvSessionsSen.SvSen.OrderIndeces.DefaultConsignment, "42")
                ParamsList.Insert(SvSessionsSen.SvSen.OrderIndeces.DefaultConsignmentVat, "3744")

                Dim TraderParamsList As New List(Of String)
                TraderParamsList.Insert(SvSessionsSen.SvSen.TraderIndeces.DefaultSalepersonID, "3744")
                'TraderParamsList.Insert(SvSessionsSen.SvSen.TraderIndeces.DefaultSalepersonID, "11")

                SenSession.ImportOrders(ParamsList, TraderParamsList)
            End If

            ConnSession.Disconnect()
            DisconnectSen()
            DisconnectSql()
        Catch ex As Exception
            SvSys.ResultsLog("***".PadRight(10) & SvSys.LeftFixedLength("ΑΠΟΤΥΧΙΑ", 20) & vbTab & "***", My.Application.Info.DirectoryPath)
            SvSys.ErrorLog("RunData" & " : " & ex.Message, My.Application.Info.DirectoryPath)
        End Try
    End Sub

End Class
